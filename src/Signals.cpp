/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-22T08:46:33+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef NTOFSIGNALS_HPP__
#define NTOFSIGNALS_HPP__

#include "Signals.hpp"

using namespace ntof::utils;
using std::chrono::steady_clock;

ScopedSlot::~ScopedSlot()
{
    disconnect();
}

void ScopedSlot::disconnect()
{
    m_track.reset();
    std::lock_guard<std::mutex> lock(m_mutex);
}

SignalWaiter::SignalWaiter() : m_count(0) {}

void SignalWaiter::reset()
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_count = 0;
}

bool SignalWaiter::wait(std::size_t count, const std::chrono::milliseconds &ms)
{
    std::unique_lock<std::mutex> lock(m_lock);
    const steady_clock::time_point deadline = steady_clock::now() + ms;

    while (m_count < count)
    {
        if (m_cond.wait_until(lock, deadline) == std::cv_status::timeout)
            break;
    }
    return m_count >= count;
}

bool SignalWaiter::wait(std::function<bool()> check,
                        const std::chrono::milliseconds &ms)
{
    std::unique_lock<std::mutex> lock(m_lock);
    const steady_clock::time_point deadline = steady_clock::now() + ms;

    while (!check())
    {
        if (m_cond.wait_until(lock, deadline) == std::cv_status::timeout)
            return false;
    }
    return true;
}

SignalWaiter::~SignalWaiter()
{
    m_slot.disconnect();
}

void SignalWaiter::operator()()
{
    std::unique_lock<std::mutex> lock(m_lock);
    ++m_count;
    m_cond.notify_all();
}

#endif
