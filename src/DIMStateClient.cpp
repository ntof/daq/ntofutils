/*
 * DIMStateClient.cpp
 *
 *  Created on: Jan 29, 2015
 *      Author: mdonze
 */
#include "DIMStateClient.h"

#include <chrono>
#include <sstream>

#include <errno.h>

#include "DIMXMLInfo.h"

namespace ntof {
namespace dim {

DIMStateException::DIMStateException(const std::string &Msg,
                                     const std::string &file,
                                     int Line,
                                     int error) :
    NTOFException(Msg.c_str(), file, Line, error)
{}

DIMStateException::~DIMStateException() throw() {}

DIMStateClient::DIMStateClient(const std::string &serviceName) :
    m_info(new DIMXMLInfo(serviceName, this)),
    m_hld(NULL),
    m_svcName(serviceName),
    m_state(DIMState::NotReadyState),
    m_stateStr("NOT READY")
{}

DIMStateClient::DIMStateClient(const std::string &serviceName,
                               DIMStateHandler *handler) :
    m_info(new DIMXMLInfo(serviceName, this)),
    m_hld(handler),
    m_svcName(serviceName),
    m_state(DIMState::NotReadyState),
    m_stateStr("NOT READY")
{}

DIMStateClient::~DIMStateClient()
{
    // It will call unsubscribe
    m_info.reset();
}

int32_t DIMStateClient::getActualState()
{
    std::lock_guard<std::mutex> l(m_lock);
    return m_state;
}

std::string DIMStateClient::getActualStateAsString()
{
    std::lock_guard<std::mutex> l(m_lock);
    return m_stateStr;
}

int DIMStateClient::waitForNewState(long timeOutMs)
{
    std::unique_lock<std::mutex> l(m_lock);
    if (timeOutMs < 0)
    {
        m_cond.wait(l);
    }
    else
    {
        if (m_cond.wait_for(l, std::chrono::milliseconds(timeOutMs)) ==
            std::cv_status::timeout)
        {
            throw DIMStateException(
                std::string("Timeout while waiting for state"), __FILE__,
                __LINE__, -1);
        }
    }
    return getActualState();
}

std::string DIMStateClient::waitForNewStateAsString(long timeOutMs)
{
    waitForNewState(timeOutMs);
    return getActualStateAsString();
}

DIMStateClient::ErrWarnVector DIMStateClient::getActiveErrors()
{
    std::lock_guard<std::mutex> l(m_lock);
    DIMStateClient::ErrWarnVector ret = m_errors;
    return ret;
}

DIMStateClient::ErrWarnVector DIMStateClient::getActiveWarnings()
{
    std::lock_guard<std::mutex> l(m_lock);
    DIMStateClient::ErrWarnVector ret = m_warnings;
    return ret;
}

void DIMStateClient::errorReceived(std::string errMsg,
                                   const DIMXMLInfo * /*info*/)
{
    errorSignal(errMsg, *this);
    if (m_hld != NULL)
    {
        m_hld->errorReceived(-1, errMsg, this);
    }
    std::lock_guard<std::mutex> l(m_lock);
    m_cond.notify_all();
}

void DIMStateClient::dataReceived(pugi::xml_document &doc,
                                  const DIMXMLInfo * /*info*/)
{
    std::unique_lock<std::mutex> lock(m_lock);
    // DIM data received
    // Parse the XML data sent by DIMState
    pugi::xml_node root = doc.child("state");
    DIMState::State state = m_state = root.attribute("value").as_int(0);
    std::string stateStr = m_stateStr =
        root.attribute("strValue").as_string("UNKNOWN");

    m_errors.clear();
    m_warnings.clear();
    pugi::xml_node errorsNode = root.child("errors");
    if (errorsNode)
    {
        for (pugi::xml_node errNode = errorsNode.first_child(); errNode;
             errNode = errNode.next_sibling())
        {
            DIMErrorWarning err(errNode.attribute("code").as_int(0),
                                errNode.first_child().value());
            m_errors.push_back(err);
        }
    }
    else
    {
        pugi::xml_node errNode = root.child("error");
        int32_t errCode_ = errNode.attribute("code").as_int(0);
        std::string errStr_ = errNode.first_child().value();
        if (errCode_ != 0)
        {
            DIMErrorWarning err(errCode_, errStr_);
            m_errors.push_back(err);
        }
    }

    pugi::xml_node warningsNode = root.child("warnings");
    for (pugi::xml_node warnNode = warningsNode.first_child(); warnNode;
         warnNode = warnNode.next_sibling())
    {
        DIMErrorWarning warn(warnNode.attribute("code").as_int(0),
                             warnNode.first_child().value());
        m_warnings.push_back(warn);
    }
    m_cond.notify_all();
    lock.unlock();

    if (m_hld != NULL)
        m_hld->stateReceived(state, stateStr, this);
    dataSignal(*this);
}

void DIMStateClient::noLink(const DIMXMLInfo * /*info*/)
{
    std::lock_guard<std::mutex> l(m_lock);
    m_state = DIMState::NotReadyState;
    m_stateStr = "NOT READY";

    m_cond.notify_all();
    if (m_hld != NULL)
    {
        m_hld->noLink(this);
    }
    errorSignal(NoLinkError, *this);
}

void DIMStateClient::getLatestError(int32_t &errCode, std::string &errMessage)
{
    ErrWarnVector::iterator it = m_errors.begin();
    if (it == m_errors.end())
    {
        errCode = 0;
        errMessage = "";
    }
    else
    {
        errCode = it->getCode();
        errMessage = it->getMessage();
    }
}

} /* namespace dim */
} /* namespace ntof */
