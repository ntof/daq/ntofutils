/*
 * Timestamp.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: mdonze
 */

#include "Timestamp.h"

#include <stdlib.h>
#ifndef WIN32
#include <sys/time.h>
#else
#include <stdio.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <time.h>
#include <windows.h>
#define TIMESPEC_TO_FILETIME_OFFSET \
    (((LONGLONG) 27111902 << 32) + (LONGLONG) 3577643008)
#endif // !WIN32
namespace ntof {
namespace utils {

/**
 * Gets time in milliseconds since EPOCH
 * @return
 */
int64_t getTimeUs()
{
#ifndef WIN32
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (tv.tv_sec * 1000000L) + tv.tv_usec;
#else
    FILETIME ft;
    // Gets Windows time (since Jan 1 1601)
    GetSystemTimeAsFileTime(&ft);
    // Convert it to timespce structure
    long tv_sec = (int) ((*(LONGLONG *) &ft - TIMESPEC_TO_FILETIME_OFFSET) /
                         10000000);
    long tv_nsec = (int) ((*(LONGLONG *) &ft - TIMESPEC_TO_FILETIME_OFFSET -
                           ((LONGLONG) tv_sec * (LONGLONG) 10000000)) *
                          100);
    // Calculate timestamp
    return ((int64_t) tv_sec * 1000000000) + (int64_t) tv_nsec;
#endif
}

/**
 * Default constructor
 * Initialize the time with actual computer clock
 */
Timestamp::Timestamp() : timeUsec_(getTimeUs()) {}

/**
 * Constructor
 * @param time Timestamp in usec since epoch
 */
Timestamp::Timestamp(int64_t time) : timeUsec_(time) {}

/**
 * Destructor
 */
Timestamp::~Timestamp() {}

bool Timestamp::operator<(const Timestamp &other) const
{
    return timeUsec_ < other.timeUsec_;
}

bool Timestamp::operator>(const Timestamp &other) const
{
    return timeUsec_ > other.timeUsec_;
}

bool Timestamp::operator==(const Timestamp &other) const
{
    return timeUsec_ == other.timeUsec_;
}
/**
 * Gets the timestamp in usec since epoch UTC
 * @return
 */
int64_t Timestamp::getValue() const
{
    return timeUsec_;
}

Timestamp::operator int64_t() const
{
    return timeUsec_;
}

Timestamp Timestamp::operator-(const Timestamp &other) const
{
    return Timestamp(timeUsec_ - other.timeUsec_);
}

Timestamp Timestamp::operator+(const Timestamp &other) const
{
    return Timestamp(timeUsec_ + other.timeUsec_);
}

Timestamp Timestamp::operator-(int64_t uSec) const
{
    return Timestamp(timeUsec_ - uSec);
}

Timestamp Timestamp::operator+(int64_t uSec) const
{
    return Timestamp(timeUsec_ + uSec);
}

} /* namespace utils */
} /* namespace ntof */
