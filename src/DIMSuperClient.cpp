/**
 * \file DIMSuperClient.cpp
 * \brief Dim client with an acknowledge system
 * \author A. GIRAUD
 * \version 1.0
 * \date Sept 18, 2014
 *
 * This class is a dim client used to deal with a Dim super command
 *
 */

#include "DIMSuperClient.h"

#include <cerrno>
#include <cstring>
#include <iostream>

#include "DIMCmd.h"
#include "DIMException.h"
#include "DIMXMLCommand.h"
#include "NTOFLogging.hpp"

namespace ntof {
namespace dim {
bool randInitialized = false;

/**
 * \fn DIMSuperClient(char* name)
 * \brief Constructor of the class
 *
 * \param char* name name of the DIM command to send
 * \return nothing.
 */
DIMSuperClient::DIMSuperClient(const std::string &name, bool isRpc) :
    ack("cmdQueue", 10),
    m_timeOut(11000),
    serviceName(name),
    listening(false),
    m_isRpc(isRpc),
    m_dimInfo(new DIMXMLInfo(name + (isRpc ? "/RpcOut" : "/Ack"), this))
{
    if (!randInitialized)
    {
#ifndef WIN32
        srand(time(NULL) + getpid());
#else
        srand(time(NULL));
#endif // !WIN32
        randInitialized = true;
    }
}

/**
 * \fn ~DIMSuperClient()
 * \brief Destructor of the class
 *
 * \return nothing.
 */
DIMSuperClient::~DIMSuperClient()
{
    m_dimInfo.reset();
}

/**
 * \fn void dataReceived(pugi::xml_document& doc, const DIMXMLInfo* info)
 * \brief To catch the data from the "ack" service
 *
 * \return nothing.
 */
void DIMSuperClient::dataReceived(pugi::xml_document &doc,
                                  const DIMXMLInfo * /*info*/)
{
    /* services are empty by default, thus likely to send empty messages */
    if (listening && doc.document_element())
    {
        try
        {
            ack.post(new DIMAck(doc));
        }
        catch (DIMException &ex)
        {
            LOG(ERROR) << "DIMSuperClient::dataReceived error (" << serviceName
                       << "): " << ex.what();
        }
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param cmd DIMXMLInfo object who made this callback
 */
void DIMSuperClient::noLink(const DIMXMLInfo * /*info*/)
{
    // Don't care of no-link, we will go in timeout
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param cmd DIMXMLInfo object who made this callback
 */
void DIMSuperClient::errorReceived(std::string errMsg,
                                   const DIMXMLInfo * /*info*/)
{
    // XML parsing error
    if (listening)
    {
        try
        {
            DIMAck *errAck = new DIMAck();
            errAck->setKey(0);
            errAck->setMessage(errMsg);
            errAck->setErrorCode(-1);
            ack.post(errAck);
        }
        catch (DIMException &ex)
        {
            LOG(ERROR) << "DIMSuperClient::errorReceived error (" << serviceName
                       << "): " << ex.what();
        }
    }
}

/**
 * \fn void sendCommand()
 * \brief Send the command to the DIM server
 *
 * \return nothing.
 */
DIMAck DIMSuperClient::sendCommand(pugi::xml_document &doc)
{
    int32_t id = rand() % 10000; // id in the range 0 to 9999
    DIMCmd cmd(id, doc);
    std::string cmdName = serviceName + (m_isRpc ? "/RpcIn" : "/Cmd");
    ack.clear();
    listening = true;
    DIMXMLCommand::sendCommandNB(cmdName, cmd.getRootData());

    DIMAck *theAck = NULL;
    for (int i = 0; i < 2; ++i)
    {
        try
        {
            theAck = ack.pop(m_timeOut);
            if (theAck->getKey() != id)
            {
                delete theAck;
                if (i != 0)
                {
                    listening = false;
                    // Acknowledge key mismatch
                    throw DIMException("Command acknowledge key mismatch!",
                                       __LINE__, -2);
                }
                else
                {
                    continue;
                }
            }
            if (theAck->getErrorCode() != 0)
            {
                int errCode = theAck->getErrorCode();
                std::string errMsg = theAck->getMessage();

                delete theAck;

                listening = false;
                // Acknowledge error
                throw DIMException(errMsg.c_str(), __LINE__, errCode);
            }
            break;
        }
        catch (const NTOFException &e)
        {
            listening = false;
            // Acknowledge timeout
            throw DIMException("Timeout while sending command!", __LINE__,
                               ETIMEDOUT);
        }
    }
    listening = false;
    DIMAck ret(*theAck);
    delete theAck;
    return ret;
}

void DIMSuperClient::setTimeOut(const std::chrono::milliseconds &ms)
{
    m_timeOut = ms;
}

const std::chrono::milliseconds &DIMSuperClient::getTimeOut() const
{
    return m_timeOut;
}
} // namespace dim
} // namespace ntof
