/*
 * DIMSynchro.cpp
 *
 *  Created on: Sep 22, 2014
 *      Author: mdonze
 */

#include "Synchro.h"

#include <iostream>
#include <sstream>
#ifndef WIN32
#include <sys/time.h>
#include <unistd.h>
#endif
#include <cstring>

#include <stdint.h>
#include <sys/types.h>

namespace ntof {
namespace utils {

/**
 * Mutex constructor
 */
Mutex::Mutex()
{
#ifndef WIN32
    mlock = new pthread_mutex_t;
    // Initialize the mutex
    pthread_mutexattr_t mutexAttr;
    pthread_mutexattr_init(&mutexAttr);

    pthread_mutex_init(mlock, NULL);

    pthread_mutexattr_destroy(&mutexAttr);
#else
    InitializeCriticalSection(&mlock);
#endif
}

/**
 * Mutex detructor
 */
Mutex::~Mutex()
{
#ifndef WIN32
    pthread_mutex_destroy(mlock);
    delete mlock;
#endif
}

/**
 * Acquire lock on this
 */
void Mutex::lock()
{
#ifndef WIN32
    pthread_mutex_lock(mlock);
#else
    EnterCriticalSection(&mlock);
#endif
}

/**
 * Unlock this
 */
void Mutex::unlock()
{
#ifndef WIN32
    pthread_mutex_unlock(mlock);
#else
    LeaveCriticalSection(&mlock);
#endif
}

/**
 * Gets pthread mutex
 */
#ifndef WIN32
pthread_mutex_t *Mutex::getMutex()
{
    return mlock;
#else
CRITICAL_SECTION *Mutex::getMutex()
{
    return &mlock;
#endif
}

/**
 * Condition variable constructor
 */
CondVar::CondVar()
{
#ifndef WIN32
    pthread_cond_init(&mCondition, NULL);
#else
    InitializeConditionVariable(&mCondition);
#endif
}

/**
 * Destructor
 */
CondVar::~CondVar()
{
#ifndef WIN32
    pthread_cond_destroy(&mCondition);
#endif
}

/**
 * Signal this
 */
void CondVar::signal()
{
#ifndef WIN32
    pthread_cond_signal(&mCondition);
#else
    WakeConditionVariable(&mCondition);
#endif
}

/**
 * Signal all waiting threads
 */
void CondVar::signalAll()
{
#ifndef WIN32
    pthread_cond_broadcast(&mCondition);
#else
    WakeAllConditionVariable(&mCondition);
#endif
}

/**
 * Wait for this to be signaled
 * The Mutex object must be locked before
 */
void CondVar::wait(Mutex &m)
{
#ifndef WIN32

    int ret = pthread_cond_wait(&mCondition, m.getMutex());
    if (ret != 0)
    {
        std::cerr << "Error in CondVar::wait (pthread_cond_wait)" << std::endl;
    }
#else
    EnterCriticalSection(m.getMutex());
    SleepConditionVariableCS(&mCondition, m.getMutex(), INFINITE);
    LeaveCriticalSection(m.getMutex());
#endif // !WIN32
}

int CondVar::wait(Mutex &m, long delayMs)
{
#ifndef WIN32

    struct timespec delay;
    int sec = delayMs / 1000;
    delayMs = delayMs - sec * 1000;
    clock_gettime(CLOCK_REALTIME, &delay);
    // perform the addition
    delay.tv_nsec += delayMs * 1000000;
    // adjust the time
    delay.tv_sec += delay.tv_nsec / 1000000000 + sec;
    delay.tv_nsec = delay.tv_nsec % 1000000000;

    return pthread_cond_timedwait(&mCondition, m.getMutex(), &delay);
#else
    int ret = 0;
    EnterCriticalSection(m.getMutex());
    ret = SleepConditionVariableCS(&mCondition, m.getMutex(), INFINITE);
    LeaveCriticalSection(m.getMutex());
    if (ret != 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
#endif // !WIN32
}

} // namespace utils
} // namespace ntof
