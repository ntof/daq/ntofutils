/*
 * DIMCmd.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#include "DIMCmd.h"

#include <cstring>
#include <iostream>

#include "DIMException.h"

using std::cout;
using std::endl;

namespace ntof {
namespace dim {
//! Constructs an empty DIM command
/*!
 */
DIMCmd::DIMCmd()
{
    doc_.append_child("command").append_attribute("key") = 0;
}

//! Constructs a DIM command object from DIM buffer
/*!
\param doc command data comming from DIM
*/
DIMCmd::DIMCmd(pugi::xml_document &doc)
{
    doc_.reset(doc);
    // Check command validity
    pugi::xml_node cmdNod = doc_.child("command");
    if (cmdNod.empty())
    {
        throw DIMException("Not a valid command!", __LINE__);
    }
    if (doc_.child("command").attribute("key").empty())
    {
        throw DIMException("Not a valid command (missing key attribute)!",
                           __LINE__);
    }
}

//! Constructs a DIM command object to be sent
/*!
\param key command data key to set
\param doc data buffer to be set
*/
DIMCmd::DIMCmd(int32_t key, pugi::xml_document &doc)
{
    pugi::xml_node cmdNod = doc_.append_child("command");
    cmdNod.append_attribute("key") = key;
    cmdNod.append_copy(doc.first_child());
}

/**
 *  Copy constructor to help object copy
 */
DIMCmd::DIMCmd(const DIMCmd &other)
{
    doc_.reset(other.doc_);
}

DIMCmd &DIMCmd::operator=(DIMCmd other)
{
    doc_.reset(other.doc_);
    return *this;
}

DIMCmd::~DIMCmd() {}

/**
 * Gets the key of the command response
 * \return key
 */
int32_t DIMCmd::getKey()
{
    pugi::xml_node cmdNod = doc_.child("command");
    if (cmdNod.empty())
    {
        throw DIMException("Not a valid command!", __LINE__);
    }
    return doc_.child("command").attribute("key").as_int(0);
}

//! Set the command key
/*!
\param key command data key to set
*/
void DIMCmd::setKey(int32_t key)
{
    if (doc_.child("command").attribute("key"))
    {
        doc_.child("command").attribute("key") = key;
    }
    else
    {
        doc_.child("command").append_attribute("key") = key;
    }
}

//! Get associated data
/*!
\param size Data size
*/
pugi::xml_node DIMCmd::getData()
{
    pugi::xml_node cmdNod = doc_.child("command");
    if (cmdNod.empty())
    {
        throw DIMException("Not a valid command!", __LINE__);
    }
    return cmdNod.first_child();
}

//! Sets associated data
/*!
\param cmdData Data to be set
*/
void DIMCmd::setData(pugi::xml_document &cmdData)
{
    pugi::xml_node cmdNod = doc_.child("command");
    if (cmdNod.empty())
    {
        throw DIMException("Not a valid command!", __LINE__);
    }
    doc_.child("command").append_copy(cmdData);
}

/**
 * Gets the complete XML document representing this
 * @return xml_document containing command data and command header
 */
pugi::xml_document &DIMCmd::getRootData()
{
    return doc_;
}

} // namespace dim
} // namespace ntof
