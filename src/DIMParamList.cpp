/*
 * DIMParamList.cpp
 *  Represents a list of parameter
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#include "DIMParamList.h"

#include <cassert>
#include <cstring>
#include <iostream>
#include <sstream>

#include "DIMException.h"

using std::cout;
using std::endl;
using namespace ntof::dim;

const std::string DIMParamList::s_rootName = "parameters";
const DIMData::Index DIMParamList::NbParamsIndex = 0;

DIMParamList::DIMParamList(const std::string &serviceName) :
    DIMSuperCommand(serviceName),
    handler(0),
    emptySvc(0),
    svcName(serviceName),
    m_dataset(new DIMDataSet(serviceName + "/Aqn", s_rootName))
{
    addParameter(0, "Number of parameters", "", int32_t(1), AddMode::CREATE,
                 true);
}

DIMParamList::~DIMParamList()
{
    m_dataset.reset();
}

void DIMParamList::clearParameters(bool updateNow)
{
    m_dataset->clearData(false);
    addParameter(0, "Number of parameters", "", int32_t(1), AddMode::CREATE,
                 updateNow);
}

void DIMParamList::setHandler(DIMParamListHandler *hdl)
{
    handler = hdl;
}

DIMData DIMParamList::getParameterAt(Index index) const
{
    return m_dataset->getDataAt(index);
}

//! Called by DIM when a new command arrives
void DIMParamList::commandReceived(DIMCmd &cmd)
{
    std::vector<DIMData> newSettings;
    pugi::xml_node params = cmd.getData();
    for (pugi::xml_node param = params.first_child(); param;
         param = param.next_sibling())
    {
        bool found = false;
        newSettings.emplace_back(param);
        DIMData &p = newSettings.back();
        try
        {
            std::lock_guard<std::mutex> lock(m_dataset->m_lock);
            // Check if setting exists
            const DIMData &oldParam = m_dataset->_findData(p.getIndex());
            found = true;
            // Check is user try to change setting 0 (reserved)
            if (p.getIndex() == 0)
            {
                setError(cmd.getKey(), -2,
                         "Number of parameters cannot be changed!");
                return;
            }

            // Check data type
            if (!p.isSameDataType(oldParam))
            {
                std::ostringstream oss;
                oss << "Parameter " << oldParam.getName() << " with index "
                    << p.getIndex() << " : wrong data type.";
                setError(cmd.getKey(), -2, oss.str());
                return;
            }

            // Copy extra data from original parameter
            p.copyParameterInfo(oldParam);
        }
        catch (const DIMException &e)
        {
            if (found)
            {
                setError(cmd.getKey(), -2, e.getMessage());
            }
            else
            {
                // If exception thrown, setting doesn't exists in memory
                std::ostringstream oss;
                oss << "Parameter with index " << p.getIndex()
                    << " doesn't exists.";
                setError(cmd.getKey(), -2, oss.str());
            }
            return;
        }
    }

    // If handler is installed, delegate settings verification to it
    if (handler)
    {
        int errorCode = 0;
        std::string errMsg;
        if (handler->parameterChanged(newSettings, *this, errorCode, errMsg))
        {
            setError(cmd.getKey(), errorCode, errMsg);
            return;
        }
    }
    // No error while executing, set new settings to this collection
    for (DIMData &update : newSettings)
    {
        std::lock_guard<std::mutex> lock(m_dataset->m_lock);
        m_dataset->_findData(update.getIndex())
            .updateValue(std::move(update), false);
    }
    setOk(cmd.getKey(), "Settings accepted");
    updateAcquisition();
}

void DIMParamList::updateAcquisition()
{
    // Update the number of parameter
    m_dataset->setValue<int32_t>(0, m_dataset->getDataCount(), false);
    m_dataset->updateData();
}

const std::string &DIMParamList::getServiceName() const
{
    return svcName;
}

void DIMParamList::updateList()
{
    updateAcquisition();
}
