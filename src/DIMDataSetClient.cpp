/*
 * DIMDataSetClient.cpp
 *
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#include "DIMDataSetClient.h"

#include <pugixml.hpp>

#include "DIMSuperClient.h"
#include "easylogging++.h"

namespace ntof {
namespace dim {

DIMDataSetClientHandler::DIMDataSetClientHandler()
{
    // Should use signals/slots
    LOG(WARNING) << "[deprecated] " << __func__;
}

DIMDataSetClient::DIMDataSetClient(const std::string &srvName,
                                   DIMDataSetClientHandler *hdl) :
    m_info(new DIMXMLInfo(srvName, this)), m_handler(hdl), m_svcName(srvName)
{}

DIMDataSetClient::DIMDataSetClient(DIMDataSetClientHandler *hdl) :
    m_handler(hdl)
{}

DIMDataSetClient::~DIMDataSetClient()
{
    // It will call the unsubscribe function
    m_info.reset();
}

void DIMDataSetClient::subscribe(const std::string &service)
{
    m_info.reset();
    m_svcName = service;
    m_info.reset(new DIMXMLInfo(service, this));
}

void DIMDataSetClient::unsubscribe()
{
    m_info.reset();
    m_svcName.clear();
}

std::vector<DIMData> DIMDataSetClient::getLatestData() const
{
    std::unique_lock<std::mutex> lock(m_lock);
    /* data is duplicated on purpose */
    return m_datas;
}

void DIMDataSetClient::setHandler(DIMDataSetClientHandler *hdl)
{
    m_handler = hdl;
}

void DIMDataSetClient::errorReceived(std::string errMsg,
                                     const DIMXMLInfo * /*info*/)
{
    errorSignal(errMsg, *this);
    if (m_handler != NULL)
    {
        m_handler->errorReceived(errMsg, *this);
    }
    std::unique_lock<std::mutex> lock(m_lock);
    m_datas.clear();
}

void DIMDataSetClient::dataReceived(pugi::xml_document &doc,
                                    const DIMXMLInfo * /*info*/)
{
    std::unique_lock<std::mutex> lock(m_lock);

    m_datas.clear();
    // Test if there is DIM link
    pugi::xml_node params = doc.first_child();
    try
    {
        for (pugi::xml_node param = params.first_child(); param;
             param = param.next_sibling())
        {
            m_datas.push_back(DIMData(param));
        }
    }
    catch (const DIMException &ex)
    {
        m_datas.clear();

        lock.unlock();
        errorSignal(ex.getMessage(), *this);
        if (m_handler)
        {
            m_handler->errorReceived(ex.getMessage(), *this);
        }
        LOG(ERROR) << ex.getMessage();
        return;
    }
    if (m_handler)
    {
        m_handler->dataChanged(m_datas, *this);
    }
    lock.unlock();
    dataSignal(*this);
}

void DIMDataSetClient::noLink(const DIMXMLInfo * /*info*/)
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_datas.clear();

    if (m_handler)
    {
        m_handler->errorReceived(NoLinkError, *this);
    }
    lock.unlock();
    errorSignal(NoLinkError, *this);
}

static void delayed_deleter(void *data)
{
    DIMDataSetClient *info = static_cast<DIMDataSetClient *>(data);
    DISABLE_AST;
    delete info;
    ENABLE_AST;
}

void DIMDataSetClient::deleteLater()
{
    dtq_start_timer(0, delayed_deleter, this);
}

} // namespace dim
} // namespace ntof
