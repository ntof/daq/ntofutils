/*
 * DIMState.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: mdonze
 */

#include "DIMState.h"

#include <sstream>

#include <errno.h>
#include <pugixml.hpp>

#include "DIMException.h"

#define STATE_ERROR_NAME "ERROR"

namespace ntof {
namespace dim {

static const std::string empty = "";
const DIMState::State DIMState::ErrorState = -2;
const DIMState::State DIMState::NotReadyState = -1;

DIMErrorWarning::DIMErrorWarning() : m_code(0) {}

DIMErrorWarning::DIMErrorWarning(int32_t code, const std::string &msg) :
    m_code(code), m_msg(msg)
{}

DIMErrorWarning::~DIMErrorWarning() {}

const std::string &DIMErrorWarning::getMessage() const
{
    return m_msg;
}

void DIMErrorWarning::setMessage(const std::string &msg)
{
    m_msg = msg;
}

int32_t DIMErrorWarning::getCode() const
{
    return m_code;
}

bool DIMErrorWarning::operator==(int32_t code) const
{
    return code == m_code;
}

bool DIMErrorWarning::operator==(const DIMErrorWarning &val) const
{
    return val.m_code == m_code;
}

DIMErrorWarning &DIMErrorWarning::operator=(const DIMErrorWarning &val)
{
    m_msg = val.m_msg;
    m_code = val.m_code;
    return *this;
}

DIMState::DIMState(const std::string &svcName) :
    m_svc(new DIMXMLService(svcName)),
    m_actualValue(NotReadyState),
    m_autoRefresh(true)
{
    addStateValue(NotReadyState, "NOT READY", false);
    addStateValue(ErrorState, STATE_ERROR_NAME, false);
    refresh();
}

DIMState::~DIMState()
{
    m_svc.reset();
}

void DIMState::addStateValue(State stateValue,
                             const std::string &name,
                             bool refreshNow)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        // Check if item exists already
        std::map<State, std::string>::iterator it = m_values.find(stateValue);
        if (it != m_values.end())
        {
            std::ostringstream oss;
            oss << __FILE__ << "State " << name << "[" << stateValue
                << "] already exists!";
            throw DIMException(oss.str(), __LINE__);
        }

        m_values[stateValue] = name;
    }

    if (refreshNow)
    {
        refreshDIM();
    }
}

DIMState::State DIMState::getValue() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_errors.empty() ? m_actualValue : ErrorState;
}

void DIMState::setValue(DIMState::State value)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        // Check if item exists already
        std::map<State, std::string>::iterator it = m_values.find(value);
        if (it == m_values.end())
        {
            std::ostringstream oss;
            oss << __FILE__ << " State with value " << value
                << " doesn't exists!";
            throw DIMException(oss.str(), __LINE__);
        }

        m_actualValue = value;
    }
    clearError();
}

std::string DIMState::getValueAsString() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_values.at(m_actualValue);
}

void DIMState::setAutoRefresh(bool enabled)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_autoRefresh = enabled;
}

void DIMState::refresh()
{
    refreshDIM(true);
}

void DIMState::refreshDIM(bool force)
{
    std::unique_lock<std::mutex> lock(m_lock);
    if (!force && !m_autoRefresh)
    {
        return;
    }

    pugi::xml_document doc;
    pugi::xml_node stateNode = doc.append_child("state");
    if (m_errors.empty())
    {
        const std::string value(m_values.at(m_actualValue));
        stateNode.append_attribute("value").set_value(m_actualValue);
        stateNode.append_attribute("strValue").set_value(value.c_str());
    }
    else
    {
        stateNode.append_attribute("value").set_value(ErrorState);
        stateNode.append_attribute("strValue").set_value(STATE_ERROR_NAME);
    }
    for (const std::map<int32_t, std::string>::value_type &it : m_values)
    {
        pugi::xml_node valNode = stateNode.append_child("value");
        valNode.append_attribute("value").set_value(it.first);
        valNode.append_attribute("strValue").set_value(it.second.c_str());
    }

    pugi::xml_node errNode = stateNode.append_child("error");
    ErrorMap::reverse_iterator it = m_errors.rbegin();
    if (it == m_errors.rend())
    {
        errNode.append_attribute("active").set_value(false);
        errNode.append_attribute("code").set_value(0);
    }
    else
    {
        errNode.append_attribute("active").set_value(true);
        errNode.append_attribute("code").set_value(it->second.getCode());
        errNode.append_child(pugi::node_pcdata)
            .set_value(it->second.getMessage().c_str());
    }

    pugi::xml_node errsNode = stateNode.append_child("errors");
    for (it = m_errors.rbegin(); it != m_errors.rend(); ++it)
    {
        pugi::xml_node err = errsNode.append_child("error");
        err.append_attribute("code").set_value(it->second.getCode());
        err.append_child(pugi::node_pcdata)
            .set_value(it->second.getMessage().c_str());
    }

    pugi::xml_node warnNode = stateNode.append_child("warnings");
    for (it = m_warnings.rbegin(); it != m_warnings.rend(); ++it)
    {
        pugi::xml_node warn = warnNode.append_child("warning");
        warn.append_attribute("code").set_value(it->second.getCode());
        warn.append_child(pugi::node_pcdata)
            .set_value(it->second.getMessage().c_str());
    }

    lock.unlock();

    m_svc->setData(doc);
    m_cond.notify_all();
}

std::string DIMState::getErrorMessage() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    ErrorMap::const_reverse_iterator it = m_errors.rbegin();
    if (it == m_errors.rend())
        return empty;
    else
        return it->second.getMessage();
}

DIMState::ErrorCode DIMState::getErrorCode() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    ErrorMap::const_reverse_iterator it = m_errors.rbegin();
    if (it == m_errors.rend())
        return 0;
    else
        return it->second.getCode();
}

void DIMState::addError(DIMState::ErrorCode errorCode, const std::string &msg)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        ErrorMap::iterator it = m_errors.find(errorCode);
        if (it != m_errors.end())
            m_errors.erase(it);

        DIMErrorWarning newErr(errorCode, msg);
        m_errors[errorCode] = newErr;
    }
    refreshDIM();
}

bool DIMState::removeError(DIMState::ErrorCode errorCode)
{
    std::unique_lock<std::mutex> lock(m_lock);
    ErrorMap::iterator it = m_errors.find(errorCode);
    if (it != m_errors.end())
    {
        m_errors.erase(it);
        lock.unlock();
        refreshDIM();
        return true;
    }
    return false;
}

DIMState::ErrorMap DIMState::getErrors() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_errors;
}

std::size_t DIMState::errorsCount() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_errors.size();
}

void DIMState::clearErrors()
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_errors.clear();
    }
    refreshDIM();
}

void DIMState::addWarning(DIMState::ErrorCode warningCode,
                          const std::string &msg)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        ErrorMap::iterator it = m_warnings.find(warningCode);
        if (it != m_warnings.end())
        {
            m_warnings.erase(it);
        }
        DIMErrorWarning newErr(warningCode, msg);
        m_warnings[warningCode] = newErr;
    }
    refreshDIM();
}

bool DIMState::removeWarning(DIMState::ErrorCode warningCode)
{
    std::unique_lock<std::mutex> lock(m_lock);
    ErrorMap::iterator it = m_warnings.find(warningCode);
    if (it != m_warnings.end())
    {
        m_warnings.erase(it);
        lock.unlock();
        refreshDIM();
        return true;
    }
    return false;
}

DIMState::WarningMap DIMState::getWarnings() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_warnings;
}

std::size_t DIMState::warningsCount() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_warnings.size();
}

void DIMState::clearWarnings()
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_warnings.clear();
    }
    refreshDIM();
}

int DIMState::waitForNewState(long timeOutMs)
{
    std::unique_lock<std::mutex> l(m_lock);
    if (timeOutMs < 0)
    {
        m_cond.wait(l);
    }
    else
    {
        if (m_cond.wait_for(l, std::chrono::milliseconds(timeOutMs)) ==
            std::cv_status::timeout)
        {
            throw DIMException("Timeout while waiting for state", __LINE__);
        }
    }
    // can't call getValue since we're holding the lock
    return (m_errors.empty()) ? m_actualValue : ErrorState;
}

std::string DIMState::waitForNewStateAsString(long timeOutMs)
{
    waitForNewState(timeOutMs);
    return getValueAsString();
}

} /* namespace dim */
} /* namespace ntof */
