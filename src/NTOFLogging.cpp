/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-26T11:42:02+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "NTOFLogging.hpp"

#include <iomanip>

using ntof::dim::DIMData;

namespace ntof {
namespace log {

const int IndentGuard::s_key = std::ios_base::xalloc();
const int DebugFlag::s_key = std::ios_base::xalloc();

void init(int argc, char *argv[])
{
    if (argc != 0 && argv)
    {
        START_EASYLOGGINGPP(argc, argv);
    }
    VLOG(1) << "verbose mode enabled";
}

IndentGuard::IndentGuard(std::ostream &os, int level) :
    m_os(&os), m_level(level)
{
    os.iword(s_key) += level;
    m_value = level;
}

IndentGuard::IndentGuard(int level) : m_os(nullptr), m_level(level)
{
    m_value = level;
}

IndentGuard::~IndentGuard()
{
    if (m_os)
        m_os->iword(s_key) -= m_value;
}

IndentGuard &IndentGuard::operator++()
{
    if (m_os)
        m_os->iword(s_key) += m_level;
    m_value += m_level;
    return *this;
}

IndentGuard &IndentGuard::operator--()
{
    if (m_os)
        m_os->iword(s_key) -= m_level;
    m_value -= m_level;
    return *this;
}

std::ostream &IndentGuard::operator()(std::ostream &os) const
{
    if (!m_os)
    {
        m_os = &os;
        m_os->iword(s_key) += m_value;
    }
    os << std::setw(os.iword(IndentGuard::s_key)) << " ";
    return os;
}

DebugFlag::DebugFlag() : m_os(nullptr) {}

DebugFlag::~DebugFlag()
{
    if (m_os)
    {
        setDebug(*m_os, false);
    }
}

DebugFlag debug()
{
    return DebugFlag();
}

bool DebugFlag::isDebug(const std::ostream &os)
{
    return const_cast<std::ostream &>(os).iword(s_key) != 0;
}

void DebugFlag::setDebug(std::ostream &os, bool value) const
{
    m_os = &os;
    if (value)
        os.iword(s_key)++;
    else if (os.iword(s_key) > 0)
        os.iword(s_key)--;
    else
        os.iword(s_key) = 0;
}

FlagsGuard::FlagsGuard(std::ostream &oss) : m_oss(&oss), m_flags(oss.flags()) {}
FlagsGuard::FlagsGuard(el::base::Writer &writer)
{
    writer << *this;
}

FlagsGuard::~FlagsGuard()
{
    if (m_oss)
    {
        m_oss->flags(m_flags);
    }
}

void FlagsGuard::save(std::ostream &oss) const
{
    m_oss = &oss;
    m_flags = oss.flags();
}

std::ostream &operator<<(std::ostream &oss, const FlagsGuard &saver)
{
    saver.save(oss);
    return oss;
}

std::ostream &operator<<(std::ostream &os, const IndentGuard &indent)
{
    return indent(os);
}

std::ostream &operator<<(std::ostream &os, const DebugFlag &fmt)
{
    fmt.setDebug(os, true);
    return os;
}

} // namespace log
} // namespace ntof

struct NameDump
{
    explicit NameDump(const DIMData &data) : data(data) {}

    const DIMData &data;
};
static el::base::MessageBuilder &operator<<(el::base::MessageBuilder &stream_,
                                            const NameDump &e)
{
    stream_ << "#" << e.data.getIndex() << " name:\"" << e.data.getName()
            << "\"";
    if (!e.data.getUnit().empty())
        stream_ << " unit:\"" << e.data.getUnit() << "\"";
    return stream_;
}

namespace el {
namespace base {
template<>
MessageBuilder &MessageBuilder::operator<< <ntof::dim::DIMData>(
    const ntof::dim::DIMData &data)
{
    MessageBuilder &stream_ = *this;
    switch (data.getDataType())
    {
    case DIMData::TypeInvalid: stream_ << "DIMData(Invalid)"; return stream_;
    case DIMData::TypeNested: {
        stream_ << "DIMData(Nested" << NameDump(data)
                << " value:" << data.getNestedValue() << ")";
        return stream_;
    }
    case DIMData::TypeInt:
        stream_ << "DIMData(Int" << NameDump(data)
                << " value:" << data.getIntValue() << ")";
        return stream_;
    case DIMData::TypeDouble:
        stream_ << "DIMData(Double" << NameDump(data)
                << " value:" << data.getDoubleValue() << ")";
        return stream_;
    case DIMData::TypeString:
        stream_ << "DIMData(String" << NameDump(data) << " value:\""
                << data.getStringValue() << "\")";
        return stream_;
    case DIMData::TypeLong:
        stream_ << "DIMData(Long" << NameDump(data)
                << " value:" << data.getLongValue() << ")";
        return stream_;
    case DIMData::TypeEnum:
        stream_ << "DIMData(Enum" << NameDump(data)
                << " value:" << data.getEnumValue().getValue() << ")";
        return stream_;
    case DIMData::TypeByte:
        stream_ << "DIMData(Byte" << NameDump(data)
                << " value:" << data.getByteValue() << ")";
        return stream_;
    case DIMData::TypeShort:
        stream_ << "DIMData(Short" << NameDump(data)
                << " value:" << data.getShortValue() << ")";
        return stream_;
    case DIMData::TypeFloat:
        stream_ << "DIMData(Float" << NameDump(data)
                << " value:" << data.getFloatValue() << ")";
        return stream_;
    case DIMData::TypeBool:
        stream_ << "DIMData(Bool" << NameDump(data)
                << " value:" << data.getBoolValue() << ")";
        return stream_;
    case DIMData::TypeUInt:
        stream_ << "DIMData(UInt" << NameDump(data)
                << " value:" << data.getUIntValue() << ")";
        return stream_;
    case DIMData::TypeULong:
        stream_ << "DIMData(ULong" << NameDump(data)
                << " value:" << data.getULongValue() << ")";
        return stream_;
    case DIMData::TypeUShort:
        stream_ << "DIMData(UShort" << NameDump(data)
                << " value:" << data.getUShortValue() << ")";
        return stream_;
    case DIMData::TypeUByte:
        stream_ << "DIMData(UByte" << NameDump(data)
                << " value:" << static_cast<uint16_t>(data.getUByteValue())
                << ")";
        return stream_;
    default: stream_ << "DIMData(Unknown)"; return stream_;
    }
}
} // namespace base
} // namespace el
