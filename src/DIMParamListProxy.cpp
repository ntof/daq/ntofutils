/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T19:00:55+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "proxy/DIMParamListProxy.hpp"

#include <algorithm>
#include <set>

#include "NTOFLogging.hpp"

using namespace ntof::dim;

DIMParamListProxy::DIMParamListProxy(const std::string &serviceName,
                                     const std::string &remoteService) :
    DIMParamList(serviceName), m_remoteService(remoteService), m_flags(0)
{}

DIMParamListProxy::~DIMParamListProxy()
{
    disconnect();
}

void DIMParamListProxy::setSyncing(bool syncing)
{
    if (syncing && !(m_flags & Syncing))
    {
        m_flags |= Syncing;
        if (!m_client)
            m_client.reset(new DIMParamListClient(m_remoteService));

        m_client->dataSignal.connect(
            0, [this](DIMParamListClient &client) { (*this)(client); });
        m_client->errorSignal.connect(
            0, [this](const std::string &, DIMParamListClient &client) {
                (*this)(client);
            });

        (*this)(*m_client.get());
    }
    else if (!syncing && (m_flags & Syncing))
    {
        m_flags &= ~Syncing;
        if (m_client)
        {
            m_client->dataSignal.disconnect(0);
            m_client->errorSignal.disconnect(0);
        }
    }
}

void DIMParamListProxy::disconnect()
{
    setSyncing(false);
    DIMParamListClient *client = m_client.release();
    /*
     * delayed client deletion to be able to disconnect from message handling
     * callback without destroying object that is actually handling the message
     */
    if (client)
        client->deleteLater();
}

void DIMParamListProxy::setNoUpdatesOnSync(bool value)
{
    if (value)
        m_flags |= NoUpdatesOnSync;
    else
        m_flags &= ~NoUpdatesOnSync;
}

void DIMParamListProxy::setReadOnly(DIMData::Index index, bool readOnly)
{
    if (readOnly)
        m_readOnly.insert(index);
    else
        m_readOnly.erase(index);
}

DIMAck DIMParamListProxy::sendParameters()
{
    std::vector<DIMData> data;

    {
        std::lock_guard<std::mutex> lock(m_dataset->m_lock);
        for (const DIMDataSet::DataMap::value_type &pair : getDataMap())
        {
            if ((pair.first != DIMParamList::NbParamsIndex) &&
                (m_readOnly.count(pair.first) == 0))
            {
                data.push_back(pair.second);
            }
        }
    }

    if (!m_client)
        m_client.reset(new DIMParamListClient(m_remoteService));
    return m_client->sendParameters(data);
}

void DIMParamListProxy::operator()(DIMParamListClient &client)
{
    std::vector<DIMData> data(client.getParameters());
    std::set<uint32_t> indexes;
    bool updated;

    {
        std::lock_guard<std::mutex> lock(m_dataset->m_lock);
        DIMDataSet::DataMap &dataList = getDataMap();
        // first element is a builtin
        updated = !(data.size() <= 1) || !(dataList.size() <= 1);

        for (DIMData &d : data)
        {
            uint32_t index = d.getIndex();
            indexes.insert(index);
            dataList[index] = std::move(d);
        }
        if (indexes.size() != dataList.size())
        {
            for (DIMDataSet::DataMap::iterator it = dataList.begin();
                 it != dataList.end();)
            {
                if (indexes.count(it->second.getIndex()))
                    ++it;
                else
                    dataList.erase(it++);
            }
        }
    }

    if (updated)
    {
        syncSignal(*this);
        if (!hasParameterAt(0))
            clearParameters(true);
        else
            updateAcquisition();
    }
}

void DIMParamListProxy::commandReceived(DIMCmd &cmd)
{
    if (isNoUpdatesOnSync() && isSyncing())
        setError(cmd.getKey(), -2, "Service unavailable: syncing in progress");
    else
        DIMParamList::commandReceived(cmd);
}
