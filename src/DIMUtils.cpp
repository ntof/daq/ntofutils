/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T14:11:50+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "DIMUtils.hpp"

#include <dim.h>

using namespace ntof::dim;

DIMLockGuard::DIMLockGuard()
{
    DIM_LOCK;
}

DIMLockGuard::~DIMLockGuard()
{
    DIM_UNLOCK;
}

DIMScopedConnection::DIMScopedConnection(const utils::connection &connection) :
    m_conn(connection)
{}

DIMScopedConnection::~DIMScopedConnection()
{
    disconnect();
}

DIMScopedConnection &DIMScopedConnection::operator=(const utils::connection &conn)
{
    if (conn != m_conn)
    {
        disconnect();
        m_conn = conn;
    }
    return *this;
}

DIMScopedConnection::DIMScopedConnection(DIMScopedConnection &&other)
{
    m_conn.swap(other.m_conn);
}

DIMScopedConnection &DIMScopedConnection::operator=(DIMScopedConnection &&other)
{
    if (other.m_conn != m_conn)
    {
        disconnect();
        m_conn.swap(other.m_conn);
    }
    return *this;
}

void DIMScopedConnection::disconnect()
{
    if (m_conn.connected())
    {
        DIMLockGuard lock;
        m_conn.disconnect();
    }
}
