/*
 * DIMDataSet.cpp
 *
 *  Created on: Oct 2, 2015
 *      Author: mdonze
 */

#include "DIMDataSet.h"

#include "DIMData.h"
#include "DIMException.h"
#include "DIMXMLService.h"

using namespace ntof::dim;

const std::string DIMDataSet::s_rootName = "dataset";

DIMDataSet::DIMDataSet(const std::string &serviceName) :
    svcName(serviceName),
    m_svc(new DIMXMLService(svcName)),
    m_rootName(s_rootName)
{
    updateData();
}

DIMDataSet::DIMDataSet(const std::string &serviceName,
                       const std::string &rootName) :
    svcName(serviceName), m_svc(new DIMXMLService(svcName)), m_rootName(rootName)
{
    updateData();
}

DIMDataSet::~DIMDataSet()
{
    m_svc.reset();
}

void DIMDataSet::removeData(Index index, bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    DataMap::iterator it = dataList.find(index);
    if (it != dataList.end())
    {
        dataList.erase(it);
        if (updateNow)
            _updateData(lock);
        return;
    }
    std::ostringstream oss;
    oss << __FILE__ << " : Data " << index << " doesn't exists in list!";
    throw DIMException(oss.str(), __LINE__);
}

void DIMDataSet::removeData(const std::string &name, bool updateNow)
{
    Index index = getIndex(name);
    removeData(index, updateNow);
}

void DIMDataSet::clearData(bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    dataList.clear();
    if (updateNow)
        _updateData(lock);
}

/**
 * Build a DataMap from XML document
 * @param doc
 * @return
 */
DIMDataSet::DataMap DIMDataSet::buildDataMap(const pugi::xml_document &doc)
{
    DataMap ret;
    pugi::xml_node root = doc.first_child();
    for (pugi::xml_node param = root.first_child(); param;
         param = param.next_sibling())
    {
        DIMData d(param);
        ret[d.getIndex()] = std::move(d);
    }
    return ret;
}

/**
 * Refreshes associated DIM service
 */
void DIMDataSet::_updateData(std::unique_lock<std::mutex> &lock)
{
    pugi::xml_document doc;
    pugi::xml_node params = doc.append_child(m_rootName.c_str());
    for (DataMap::iterator it = dataList.begin(); it != dataList.end(); ++it)
    {
        it->second.insertIntoAqn(params);
    }
    lock.unlock();
    m_svc->setData(doc);
    lock.lock();
}

/**
 * Gets the name of the data specified by index
 * @param index
 * @return
 */
std::string DIMDataSet::getDataName(Index index) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return _findData(index).getName();
}

const DIMData &DIMDataSet::_findData(Index index) const
{
    DataMap::const_iterator it = dataList.find(index);
    if (it != dataList.end())
    {
        return it->second;
    }
    std::ostringstream oss;
    oss << __FILE__ << " : Data with index " << index << " not found!";
    throw DIMException(oss.str(), __LINE__);
}

DIMData &DIMDataSet::_findData(Index index)
{
    DataMap::iterator it = dataList.find(index);
    if (it != dataList.end())
    {
        return it->second;
    }
    std::ostringstream oss;
    oss << __FILE__ << " : Data with index " << index << " not found!";
    throw DIMException(oss.str(), __LINE__);
}

int DIMDataSet::getDataCount() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return dataList.size();
}

bool DIMDataSet::hasDataAt(Index index) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return dataList.count(index) != 0;
}

DIMData::Index DIMDataSet::_getIndex(const std::string &name) const
{
    for (DataMap::const_iterator it = dataList.begin(); it != dataList.end();
         ++it)
    {
        if (it->second.getName() == name)
        {
            return it->first;
        }
    }
    std::ostringstream oss;
    oss << __FILE__ << " : Parameter with name " << name << " not found!";
    throw DIMException(oss.str(), __LINE__);
}

DIMData::Index DIMDataSet::_getNextIndex() const
{
    if (dataList.empty())
    {
        return 0;
    }
    else
    {
        DataMap::const_iterator it = dataList.end();
        --it;
        return it->first + 1;
    }
}

void DIMDataSet::setHidden(Index index, bool value, bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    _findData(index).setHidden(value);

    if (updateNow)
        _updateData(lock);
}
