/*
 * DIMAck.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#include "DIMAck.h"

#include <iostream>
#include <string>

#include "DIMException.h"

using std::cout;
using std::endl;

namespace ntof {
namespace dim {
/**
 * Default Constructor of acknowledge
 */
DIMAck::DIMAck() : doc_(new pugi::xml_document)
{
    root_ = doc_->append_child("acknowledge");
    root_.append_attribute("key") = 0;
    root_.append_attribute("status") = 0;
    root_.append_attribute("error") = 0;
}

/**
 * Constructor of acknowledge
 */
DIMAck::DIMAck(pugi::xml_document &doc) : doc_(new pugi::xml_document)
{
    pugi::xml_node root = doc.child("acknowledge");
    if (!root)
        throw DIMException("Bad XML acknowledge document received (root)!",
                           __LINE__);

    if (!root.attribute("key"))
        throw DIMException("Bad XML acknowledge document received (key)!",
                           __LINE__);

    if (!root.attribute("status"))
        throw DIMException("Bad XML acknowledge document received (status)!",
                           __LINE__);

    if (!root.attribute("error"))
        throw DIMException("Bad XML acknowledge document received (error)!",
                           __LINE__);

    clone(doc);
}

DIMAck::DIMAck(int32_t key,
               DIMAck::Status status,
               int32_t errorCode,
               const std::string &message) :
    doc_(new pugi::xml_document)
{
    root_ = doc_->append_child("acknowledge");
    root_.append_attribute("key") = key;
    root_.append_attribute("status") = status;
    root_.append_attribute("error") = errorCode;
    msg_ = root_.append_child(pugi::node_pcdata);
    msg_.set_value(message.c_str());
}

DIMAck::DIMAck(int32_t key, int32_t status, int32_t errorCode) :
    doc_(new pugi::xml_document)
{
    root_ = doc_->append_child("acknowledge");
    root_.append_attribute("key") = key;
    root_.append_attribute("status") = status;
    root_.append_attribute("error") = errorCode;
}

DIMAck::DIMAck(const DIMAck &other) : doc_(new pugi::xml_document)
{
    clone(*other.doc_);
}

DIMAck::DIMAck(DIMAck &&other)
{
    doc_.swap(other.doc_);
    root_ = other.root_;
    msg_ = other.msg_;
}

DIMAck &DIMAck::operator=(const DIMAck &other)
{
    if (&other != this)
    {
        clone(*other.doc_);
    }
    return *this;
}

DIMAck &DIMAck::operator=(DIMAck &&other)
{
    if (&other != this)
    {
        doc_.swap(other.doc_);
        root_ = other.root_;
        msg_ = other.msg_;
    }
    return *this;
}

void DIMAck::clone(pugi::xml_document &doc)
{
    doc_->reset(doc);
    root_ = doc_->child("acknowledge");
    for (pugi::xml_node node : root_.children())
    {
        if (node.type() == pugi::node_pcdata)
        {
            msg_ = node;
            break;
        }
    }
}

DIMAck::~DIMAck() {}

/**
 * Gets the execution message
 * \return error/acknowledge message
 */
std::string DIMAck::getMessage()
{
    return root_.child_value();
}

/**
 * Gets the command status
 * \return status of command
 */
DIMAck::Status DIMAck::getStatus()
{
    return static_cast<DIMAck::Status>(root_.attribute("status").as_int(0));
}

/**
 * Get the code of the error
 * \return error code
 */
int32_t DIMAck::getErrorCode()
{
    return root_.attribute("error").as_int(0);
}

/**
 * Gets the key of the command response
 * \return key
 */
int32_t DIMAck::getKey()
{
    return root_.attribute("key").as_int(0);
}

void DIMAck::setMessage(const std::string &message)
{
    if (!msg_)
        msg_ = root_.append_child(pugi::node_pcdata);
    msg_.set_value(message.c_str());
}

void DIMAck::setStatus(DIMAck::Status status)
{
    pugi::xml_attribute attr = root_.attribute("status");
    if (attr)
        attr.set_value(status);
    else
        root_.append_attribute("status") = status;
}

void DIMAck::setErrorCode(int32_t code)
{
    pugi::xml_attribute attr = root_.attribute("error");
    if (attr)
        attr.set_value(code);
    else
        root_.append_attribute("error") = code;
}

void DIMAck::setKey(int32_t key)
{
    pugi::xml_attribute attr = root_.attribute("key");
    if (attr)
        attr.set_value(key);
    else
        root_.append_attribute("key") = key;
}

} // namespace dim
} // namespace ntof
