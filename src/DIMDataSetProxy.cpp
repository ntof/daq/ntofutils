/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-09T22:20:13+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "proxy/DIMDataSetProxy.hpp"

#include <set>

#include "NTOFLogging.hpp"
#include "easylogging++.h"

using namespace ntof::dim;

DIMDataSetProxy::DIMDataSetProxy(const std::string &serviceName,
                                 const std::string &remoteService) :
    DIMDataSet(serviceName), m_remoteService(remoteService), m_syncing(false)
{}

DIMDataSetProxy::~DIMDataSetProxy()
{
    disconnect();
}

void DIMDataSetProxy::setSyncing(bool syncing)
{
    if (syncing && !m_syncing)
    {
        m_syncing = true;
        if (!m_client)
            m_client.reset(new DIMDataSetClient(m_remoteService));

        m_client->dataSignal.connect(
            0, [this](DIMDataSetClient &client) { (*this)(client); });
        m_client->errorSignal.connect(
            0, [this](const std::string &, DIMDataSetClient &client) {
                (*this)(client);
            });

        (*this)(*m_client.get());
    }
    else if (!syncing && m_syncing)
    {
        m_syncing = false;
        m_client->dataSignal.disconnect(0);
        m_client->errorSignal.disconnect(0);
    }
}

void DIMDataSetProxy::disconnect()
{
    setSyncing(false);
    DIMDataSetClient *client = m_client.release();
    /*
     * delayed client deletion to be able to disconnect from message handling
     * callback without destroying object that is actually handling the message
     */
    if (client)
        client->deleteLater();
}

void DIMDataSetProxy::operator()(DIMDataSetClient &client)
{
    std::vector<DIMData> data(client.getLatestData());
    std::set<uint32_t> indexes;
    bool updated = !data.empty() || !dataList.empty();

    for (DIMData &d : data)
    {
        uint32_t index = d.getIndex();
        indexes.insert(index);
        dataList[index] = std::move(d);
    }
    if (indexes.size() != dataList.size())
    {
        for (DataMap::iterator it = dataList.begin(); it != dataList.end();)
        {
            if (indexes.count(it->second.getIndex()))
                ++it;
            else
                dataList.erase(it++);
        }
    }
    if (updated)
    {
        syncSignal(*this);
        updateData();
    }
}
