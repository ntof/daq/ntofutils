/*
 * EventReader.cpp
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#include "EventReader.h"

#include <algorithm>
#include <ctime>
#include <iostream>

#include "DaqTypes.h"
#include "NTOFException.h"
#include "NTOFLogging.hpp"

namespace ntof {
namespace dim {

int EventReader::maxEvents = 50;
const std::string EventReader::DefaultService = "Timing/event";

EventReader::Data::Data() :
    cycleNb(-1), timeStamp(-1), cycleStamp(-1), evtNumber(-1)
{}

bool EventReader::Data::fromXML(const pugi::xml_node &event)
{
    timeStamp = event.attribute("timestamp").as_llong(-1);
    cycleStamp = event.attribute("cyclestamp").as_llong(-1);
    cycleNb = event.attribute("periodNB").as_int(-1);
    evtType = event.attribute("name").as_string("");
    dest = event.attribute("dest").as_string("");
    dest2 = event.attribute("dest2").as_string("");
    evtNumber = event.attribute("eventNumber").as_llong(-1);
    lsaCycle = event.attribute("lsaCycle").as_string("");
    user = event.attribute("user").as_string("");
    return isValid();
}

bool EventReader::Data::toXML(pugi::xml_node &node) const
{
    if (!isValid())
        return false;
    node.append_attribute("timestamp") = (long long) timeStamp;
    node.append_attribute("cyclestamp") = (long long) cycleStamp;
    node.append_attribute("periodNB") = cycleNb;
    node.append_attribute("name") = evtType.c_str();
    node.append_attribute("dest") = dest.c_str();
    node.append_attribute("dest2") = dest2.c_str();
    node.append_attribute("eventNumber") = (long long) evtNumber;
    node.append_attribute("lsaCycle") = lsaCycle.c_str();
    node.append_attribute("user") = user.c_str();
    return true;
}

bool EventReader::Data::operator==(const EventReader::Data &other) const
{
    return (timeStamp == other.timeStamp) && (cycleStamp == other.cycleStamp) &&
        (cycleNb == other.cycleNb) && (evtNumber == other.evtNumber) &&
        (evtType == other.evtType) && (dest == other.dest) &&
        (dest2 == other.dest2) && (lsaCycle == other.lsaCycle) &&
        (user == other.user);
}

/**
 * Sets the maximum event numbers to keep
 * @param size
 */
void EventReader::setMaxEventsToKeep(int size)
{
    maxEvents = size;
}

/**
 * Default constructor
 * @param eventService
 */
EventReader::EventReader(std::string eventService) : m_margin(100000)
{
    /* must be done after other members initialization,
     * to prevent uninitialized data access
     */
    m_info.reset(new DIMXMLInfo(eventService, this));
}

EventReader::~EventReader()
{
    /*
    must unsubscribe here, handler may be called whilst this class is half
    destroyed otherwise
     */
    m_info->unsubscribe();
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param info DIMXMLInfo object who made this callback
 */
void EventReader::errorReceived(std::string errMsg,
                                const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[EventReader]: event error recevied: " << errMsg;
}

/**
 * Callback when a new DIMXMLInfo is received
 * @param doc XML document containing new data
 * @param info DIMXMLInfo object who made this callback
 */
void EventReader::dataReceived(pugi::xml_document &doc,
                               const ntof::dim::DIMXMLInfo * /*info*/)
{
    Data event;
    event.fromXML(doc.first_child().child("event"));
    // Gets timestamp in usec
    ntof::utils::Timestamp now(event.timeStamp / 1000);

    if (!event.isValid())
    {
        LOG(ERROR) << "[EventReader]: invalid event received!";
        return;
    }

    {
        std::lock_guard<std::mutex> lock(m_mutex);
        // Do cleanup
        for (uint32_t i = maxEvents; i < m_events.size(); ++i)
        {
            m_events.erase(m_events.begin());
        }
        m_events[now] = event;
        VLOG(1) << "[EventReader]: event received timeStamp:" << now;
    }
    m_cond.notify_all();

    {
        std::lock_guard<std::mutex> lock(m_hdlrsMutex);
        for (std::vector<EventHandler *>::iterator it = m_handlers.begin();
             it != m_handlers.end(); ++it)
        {
            (*it)->eventReceived(event);
        }
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param info DIMXMLInfo object who made this callback
 */
void EventReader::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[EventReader]: no link on event!";
    std::lock_guard<std::mutex> lock(m_hdlrsMutex);
    for (std::vector<EventHandler *>::iterator it = m_handlers.begin();
         it != m_handlers.end(); ++it)
    {
        (*it)->noLink();
    }
}

/**
 * Waits for a new event
 */
void EventReader::waitForNewEvent(EventReader::Data &event)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_cond.wait(lock);
    if (m_events.empty())
        event = Data();
    else
        event = m_events.rbegin()->second; /* last event */
}

bool EventReader::getEventByTime(EventReader::Data &event,
                                 const ntof::utils::Timestamp &time,
                                 int32_t timeout)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    // Check if the oldest timestamp in our collection is not recent than the
    // one we want to test
    if (!m_events.empty() && (m_events.begin()->first > (time + m_margin)))
    {
        event = Data();
        LOG(ERROR)
            << "[EventReader]: no possible match for " << time.getValue()
            << " oldest event: " << m_events.begin()->second << " ("
            << (m_events.begin()->first - time.getValue()) << "us older)";
        return false;
    }

    EventsMap::iterator it;
    m_cond.wait_for(lock, std::chrono::milliseconds(timeout),
                    [this, &time, &it]() {
                        it = std::find_if(m_events.begin(), m_events.end(),
                                          checkTS(time, m_margin));
                        return (it != m_events.end());
                    });

    if (it != m_events.end())
    {
        m_currentEvent = event = it->second;
        // Discard all past events
        m_events.erase(m_events.begin(), ++it);
        LOG(INFO) << "[EventReader]: event for " << time.getValue()
                  << " event:" << m_currentEvent;
        return true;
    }
    LOG(ERROR) << "[EventReader]: no event found for " << time.getValue()
               << " buffered:" << m_events.size();
    return false;
}

void EventReader::getCurrentEvent(EventReader::Data &event) const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    event = m_currentEvent;
}

/**
 * Registers a new event handler
 * @param handler
 */
void EventReader::registerHandler(EventHandler *handler)
{
    std::lock_guard<std::mutex> lock(m_hdlrsMutex);
    m_handlers.push_back(handler);
}

/**
 * Remove specified handler
 * @param handler
 */
void EventReader::removeHandler(EventHandler *handler)
{
    std::lock_guard<std::mutex> lock(m_hdlrsMutex);
    m_handlers.erase(std::remove(m_handlers.begin(), m_handlers.end(), handler),
                     m_handlers.end());
}

/**
 * Set the time margin in us for comparing timestamps
 * @param margin Timestamp tolerance in us
 */
void EventReader::setTimeMargin(int64_t margin)
{
    m_margin = llabs(margin);
}

/**
 * Get the time margin in us for comparing timestamps
 */
int64_t EventReader::getTimeMargin()
{
    return m_margin;
}

EventReader::checkTS::checkTS(const ntof::utils::Timestamp &ts, int64_t margin) :
    chkVal(ts), m_margin(margin)
{}

bool EventReader::checkTS::operator()(
    const std::pair<ntof::utils::Timestamp, Data> &v) const
{
    int64_t diff = chkVal.getValue() - v.first.getValue();
    if (llabs(diff) > m_margin)
    {
        // Not within margin
        return false;
    }
    // Equal
    return true;
}

} // namespace dim
} /* namespace ntof */

EventHeader &operator<<(EventHeader &header,
                        const ntof::dim::EventReader::Data &data)
{
    header.eventNumber = data.evtNumber;

    if (data.evtType == "CALIBRATION")
    {
        header.beamType = EVENT_HEADER::CALIBRATION;
    }
    else if (data.evtType == "PRIMARY")
    {
        header.beamType = EVENT_HEADER::PRIMARY;
    }
    else if (data.evtType == "PARASITIC")
    {
        header.beamType = EVENT_HEADER::PARASITIC;
    }
    else if (data.evtType == "DOUBLE")
    {
        header.beamType = EVENT_HEADER::DOUBLE;
    }
    else
    {
        header.beamType = -1;
    }

    header.updateDateTime(static_cast<std::time_t>(data.cycleStamp / 1E9));
    header.bctTS = data.cycleStamp;

    return header;
}

namespace el {
namespace base {
template<>
MessageBuilder &MessageBuilder::operator<< <ntof::dim::EventReader::Data>(
    const ntof::dim::EventReader::Data &data)
{
    MessageBuilder &stream = *this;
    stream << "EventReader::Data{"
           << "cycleNb:" << data.cycleNb << " cycleStamp:" << data.cycleStamp
           << " timeStamp:" << data.timeStamp << " evtType:" << data.evtType
           << " dest:" << data.dest << " dest2:" << data.dest2
           << " evtNumber:" << data.evtNumber << " lsaCycle:" << data.lsaCycle
           << " user:" << data.user << "}";

    return stream;
}

} // namespace base
} // namespace el
