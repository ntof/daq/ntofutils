/*
 * DIMProxyClient.cpp
 *
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#include "DIMProxyClient.h"

#include <pugixml.hpp>

#include "DIMSuperClient.h"

namespace ntof {
namespace dim {
//! Construct this
/*!
\param srvName Name of the DIMProxy to subscribe
*/
DIMProxyClient::DIMProxyClient(const std::string &srvName) :
    m_info(new DIMXMLInfo(srvName, this)), handler(0), svcName(srvName)
{}

//! Destruct this
/*!
 */
DIMProxyClient::~DIMProxyClient()
{
    /* it will call unsubscribe before other members gets deleted */
    m_info.reset();
}

const std::string &DIMProxyClient::getServiceName() const
{
    return svcName;
}

/**
 * Send a data to proxy
 * @param service Name of the service to be set
 * @param newParam vector containing data to be sent to server
 */
void DIMProxyClient::sendData(std::string &service,
                              std::vector<DIMData> &newParam)
{
    if (!newParam.empty())
    {
        DIMSuperClient client(service);
        pugi::xml_document doc;
        pugi::xml_node params = doc.append_child("dataset");
        for (std::vector<DIMData>::iterator it = newParam.begin();
             it != newParam.end(); ++it)
        {
            it->insertIntoCmd(params);
        }
        client.sendCommand(doc);
    }
}

/**
 * Gets the list of actual data
 * @return A vector (copy) containing data (can be empty if no link or error)
 */
std::vector<DIMData> DIMProxyClient::getLatestData()
{
    std::lock_guard<std::mutex> l(mutex);
    return datas;
}

//! Set replace callback handler for this list
/*!
\param hdl Callback handler, NULL to remove it
*/
void DIMProxyClient::setHandler(DIMProxyClientHandler *hdl)
{
    handler = hdl;
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param cmd DIMXMLInfo object who made this callback
 */
void DIMProxyClient::errorReceived(std::string errMsg,
                                   const DIMXMLInfo * /*info*/)
{
    if (handler != NULL)
    {
        handler->errorReceived(errMsg, *this);
    }
    std::lock_guard<std::mutex> l(mutex);
    datas.clear();
}

/**
 * Callback when a new DIMXMLInfo is received
 * @param doc XML document containing new data
 * @param cmd DIMXMLInfo object who made this callback
 */
void DIMProxyClient::dataReceived(pugi::xml_document &doc,
                                  const DIMXMLInfo * /*info*/)
{
    std::lock_guard<std::mutex> l(mutex);
    datas.clear();
    // Test if there is DIM link
    pugi::xml_node params = doc.first_child();
    if (std::string(params.name()) == "error")
    {
        datas.clear();
        if (handler)
        {
            handler->errorReceived(params.child_value(), *this);
        }
    }
    else
    {
        for (pugi::xml_node param = params.first_child(); param;
             param = param.next_sibling())
        {
            datas.push_back(DIMData(param));
        }
        if (handler)
        {
            handler->dataChanged(datas, *this);
        }
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param cmd DIMXMLInfo object who made this callback
 */
void DIMProxyClient::noLink(const DIMXMLInfo * /*info*/)
{
    std::lock_guard<std::mutex> l(mutex);
    datas.clear();
    if (handler)
    {
        handler->errorReceived(NoLinkError, *this);
    }
}

} // namespace dim
} // namespace ntof
