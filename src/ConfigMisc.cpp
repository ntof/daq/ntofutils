/*
 * ConfigMisc.cpp
 *
 *  Created on: Jan 29, 2015
 *      Author: agiraud
 */

#include "ConfigMisc.h"

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
#include <utility>

#include <dim.h>
#include <pugixml.hpp>
#include <sys/types.h>

#include "Flock.hpp"
#include "NTOFException.h"
#ifndef WIN32
#include <pwd.h>
#include <unistd.h>
#else
#include <Windows.h>
#endif
#include "base64.h"

#define PRIVKEY \
    "9f558bf153b6540cecb725661b0898afdcd33ea20e9d3bfad67f8d724c370b86"

namespace ntof {
namespace utils {
/* ************************************************************************** */
/* CONSTRUCTOR AND DESTRUCTOR                                                 */
/* ************************************************************************** */

ConfigMisc::ConfigMisc(const std::string &configFile)
{
    loadConfig(configFile);
}

ConfigMisc::ConfigMisc()
{
    std::string confFile = "/etc/ntof/misc.xml";

    loadConfig(confFile);
}

ConfigMisc::~ConfigMisc() {}

void ConfigMisc::loadConfig(const std::string &configFile)
{
    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(configFile.c_str());
    if (!result)
        throw ntof::NTOFException("A problem occurred during the opening of "
                                  "the configuration file : " +
                                      configFile,
                                  __FILE__, __LINE__);
    else
    {
        pugi::xml_node miscNode = doc.child("misc");
        dimDns_ = miscNode.child("dimDns").attribute("value").as_string("");
        if (dimDns_.find(':') != std::string::npos)
        {
            std::string::size_type idx = dimDns_.find(':');
            dimDnsPort_ = std::atoi(dimDns_.substr(idx + 1).c_str());
            dimDns_ = dimDns_.substr(0, idx);
        }
        else
        {
            dimDnsPort_ = DNS_PORT;
        }

        experimentalArea_ =
            miscNode.child("experimentalArea").attribute("value").as_string("");

        oracleUser_ = miscNode.child("oracle").attribute("user").as_string("");
        pugi::xml_attribute passAttr = miscNode.child("oracle").attribute(
            "pass");
        if (passAttr)
        {
            oraclePass_ = passAttr.as_string("");
        }
        else
        {
            oraclePass_ = decodePassword(
                miscNode.child("oracle").attribute("password").as_string(""));
        }

        oracleServer_ = miscNode.child("oracle").attribute("server").as_string(
            "");
        eventsPerFile_ =
            miscNode.child("eventsPerFile").attribute("value").as_int(1);

        pugi::xml_node daqsNode = miscNode.child("daqs");
        for (pugi::xml_node nodes = daqsNode.first_child(); nodes;
             nodes = nodes.next_sibling())
        {
            if (strcmp(nodes.name(), "daq") == 0)
            {
                int32_t crateId =
                    nodes.child("crateId").attribute("value").as_int(-1);
                std::string hostname = nodes.child("hostname")
                                           .attribute("value")
                                           .as_string("UNKNOWN");
                daq_.insert(std::pair<int32_t, std::string>(crateId, hostname));
            }
        }

        m_lockFile = miscNode.child("lockFile")
                         .attribute("value")
                         .as_string(Flock::DefaultLock.c_str());
    }
}

/* ************************************************************************** */
/* GETTERS AND SETTERS                                                        */
/* ************************************************************************** */

int32_t ConfigMisc::getDaqID(std::string daqName)
{
    for (DaqMap::iterator it = daq_.begin(); it != daq_.end(); ++it)
    {
        if ((*it).second == daqName)
        {
            return (*it).first;
        }
    }
    return -1;
}

const ConfigMisc::DaqMap &ConfigMisc::getDaq() const
{
    return daq_;
}

const std::string &ConfigMisc::getDimDns() const
{
    return dimDns_;
}

const std::string &ConfigMisc::getExperimentalArea() const
{
    return experimentalArea_;
}

const std::string &ConfigMisc::getOraclePass() const
{
    return oraclePass_;
}

const std::string &ConfigMisc::getOracleServer() const
{
    return oracleServer_;
}

const std::string &ConfigMisc::getOracleUser() const
{
    return oracleUser_;
}

int32_t ConfigMisc::getEventsPerFile() const
{
    return eventsPerFile_;
}

/**
 * Encode a password
 * @param pass
 * @return
 */
std::string ConfigMisc::encodePassword(const std::string &pass)
{
    std::string ret;
    ret.reserve(pass.length());
    std::string key = PRIVKEY;
    unsigned int keyPos = 0;
    for (unsigned int i = 0; i < pass.length(); ++i)
    {
        ret += key[keyPos] ^ pass[i];
        keyPos++;
        if (keyPos > key.length())
        {
            keyPos = 0;
        }
    }
    return base64_encode((const unsigned char *) ret.c_str(), ret.length());
}

/**
 * Encode a password
 * @param pass
 * @return
 */
std::string ConfigMisc::decodePassword(const std::string &pass)
{
    std::string encoded = base64_decode(pass);
    std::string ret;
    ret.reserve(encoded.length());

    std::string key = PRIVKEY;
    unsigned int keyPos = 0;
    for (unsigned int i = 0; i < encoded.length(); ++i)
    {
        ret += key[keyPos] ^ encoded[i];
        keyPos++;
        if (keyPos > key.length())
        {
            keyPos = 0;
        }
    }

    return ret;
}

} // namespace utils
} /* namespace ntof */
