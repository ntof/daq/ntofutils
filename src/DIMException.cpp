/*
 * DIMException.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#include "DIMException.h"

#include <sstream>

namespace ntof {
namespace dim {
/**
 * Built exception using a string
 */
DIMException::DIMException(const std::string &Msg, int Line, int error) :
    msg(Msg), line(Line), errorCode(error)
{}

/**
 * Build exception using a char* and an error code
 */
DIMException::DIMException(const char *Msg, int Line, int error) :
    msg(Msg), line(Line), errorCode(error)
{}

/**
 * Gets error message
 */
const char *DIMException::what() const noexcept
{
    if (m_what.empty())
    {
        std::ostringstream oss;
        oss << "DIM exception at line " << line << " : " << msg;
        m_what = oss.str();
    }
    return m_what.c_str();
}

} // namespace dim
} // namespace ntof
