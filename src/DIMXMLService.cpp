/*
 * DIMXMLService.cpp
 *
 *  Created on: Nov 6, 2014
 *      Author: mdonze
 */

#include <iostream>
#include <sstream>

#include <DIMXMLService.h>

namespace ntof {
namespace dim {

/**
 * Build a new DIMXMLService object
 * @param svcName Name of the DIM service to be created
 */

DIMXMLService::DIMXMLService(const std::string &svcName) :
    m_svc(new DimService(svcName.c_str(), const_cast<char *>("")))
{}

/**
 * Build a new DIMXMLService object with predefined value
 * @param svcName Name of the DIM service to be created
 */
DIMXMLService::DIMXMLService(std::string svcName, pugi::xml_document &doc) :
    m_svc(new DimService(svcName.c_str(), const_cast<char *>("")))
{
    setData(doc);
}

DIMXMLService::~DIMXMLService()
{
    m_svc.reset();
}

/**
 * Sets new XML document to be published
 * @param doc
 */
void DIMXMLService::setData(pugi::xml_document &doc)
{
    std::ostringstream oss;
    doc.save(oss);
    m_svc->updateService(const_cast<char *>(oss.str().c_str()));
}

} /* namespace dim */
} /* namespace ntof */
