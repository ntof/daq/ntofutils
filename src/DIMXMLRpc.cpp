/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-25T09:36:54+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "DIMXMLRpc.h"

#include <sstream>

#include "DIMException.h"
#include "easylogging++.h"

#include <dis.hxx>

namespace ntof {
namespace dim {

class DIMXMLRpcPrivate : public DimRpc
{
public:
    DIMXMLRpcPrivate(DIMXMLRpc &parent, const std::string &name);

    void rpcHandler() override;

    void rpcReceived(pugi::xml_document &doc);
    void errorReceived(const std::string &msg);

    DIMXMLRpc &m_parent;
};

DIMXMLRpcPrivate::DIMXMLRpcPrivate(DIMXMLRpc &parent, const std::string &name) :
    DimRpc(name.c_str(), "C", "C"), m_parent(parent)
{}

void DIMXMLRpcPrivate::rpcHandler()
{
    const std::string data(getString(), getSize());
    pugi::xml_document doc;
    pugi::xml_parse_result parseResult = doc.load(data.c_str());

    setData(nullptr, 0); // prevent previous reply from leaking
    if (!parseResult)
    {
        // Can't do much about it
        LOG(ERROR) << "Corrupted XML rpc received on " << getName() << ": "
                   << parseResult.description();
        return;
    }

    try
    {
        DIMCmd cmd(doc);
        try
        {
            m_parent.rpcReceived(cmd);
        }
        catch (const DIMException &ex)
        {
            m_parent.setError(cmd.getKey(), -1, ex.getMessage());
        }
    }
    catch (const DIMException &ex)
    {
        LOG(ERROR) << "Invalid XML rpc received on " << getName() << ": "
                   << ex.getMessage();
    }
}

DIMXMLRpc::DIMXMLRpc(const std::string &name) :
    m_name(name), m_private(new DIMXMLRpcPrivate(*this, m_name))
{}

DIMXMLRpc::~DIMXMLRpc()
{
    m_private.reset();
}

void DIMXMLRpc::setReply(const DIMAck &reply)
{
    const pugi::xml_document &doc = reply.getXMLDocument();
    std::ostringstream oss;
    doc.save(oss);
    const std::string buffer = oss.str();

    m_private->setData(buffer.c_str(), buffer.size());
}

void DIMXMLRpc::setError(int32_t key,
                         int32_t errorCode,
                         const std::string &message)
{
    setReply(DIMAck(key, DIMAck::Rejected, errorCode, message));
}

} // namespace dim
} // namespace ntof
