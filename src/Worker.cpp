/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-26T10:01:02+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "Worker.hpp"

#include <cerrno>

#include "config.h"
#include "easylogging++.h"

#ifdef HAVE_PTHREAD_NAME
#include <pthread.h>
#endif

using namespace ntof::utils;

Worker::Worker(const std::string &name,
               std::size_t numThreads,
               std::size_t maxSize) :
    m_queue(name, maxSize)
{
    m_threads.reserve(numThreads);
    start();
}

Worker::~Worker()
{
    stop();
}

const Worker::SharedTask &Worker::post(const Worker::SharedTask &task)
{
    m_queue.post(task);
    return task;
}

Worker::SharedTask Worker::post(const std::function<void()> &funTask)
{
    Worker::SharedTask task(new FunTask(funTask));
    m_queue.post(task);
    return task;
}

void Worker::start()
{
    if (!m_threads.empty())
        return;

    for (std::size_t i = 0; i < m_threads.capacity(); ++i)
    {
        m_threads.push_back(std::unique_ptr<std::thread>(
            new std::thread(&Worker::thread_func, this)));
    }
}

void Worker::stop()
{
    m_queue.clear(true);

    for (std::size_t i = 0; i < m_threads.size(); ++i)
    {
        m_threads[i]->join();
    }
    m_threads.clear();
}

const std::string &Worker::name() const
{
    return m_queue.getQueueName();
}

void Worker::thread_func()
{
#ifdef HAVE_PTHREAD_NAME
    const std::string threadName = name().substr(0, 16);
    pthread_setname_np(pthread_self(), threadName.c_str());
#endif

    while (true)
    {
        try
        {
            Worker::SharedTask task = m_queue.pop();

            if (task->setState(Task::RUNNING))
            {
                task->run();
            }
            task->setState(Task::FINISHED);
        }
        catch (ntof::NTOFException &ex)
        {
            if (ex.getErrorCode() == EINTR)
            {
                /* time to die */
                return;
            }
            LOG(ERROR) << ex.getMessage();
        }
    }
}

Worker::Task::Task() : m_state(Worker::Task::READY), m_error(Worker::Task::OK)
{}

Worker::Task::~Task()
{
    if (m_state != Task::FINISHED)
    {
        abort();
    }
}

Worker::Task::State Worker::Task::state() const
{
    const std::lock_guard<std::mutex> lock(m_lock);
    return m_state;
}

Worker::Task::Error Worker::Task::error() const
{
    const std::lock_guard<std::mutex> lock(m_lock);
    return m_error;
}

const std::string &Worker::Task::errorString() const
{
    const std::lock_guard<std::mutex> lock(m_lock);
    return m_errorString;
}

bool Worker::Task::wait(unsigned long msecs) const
{
    return wait(std::chrono::milliseconds(msecs));
}

bool Worker::Task::wait(const std::chrono::milliseconds &msecs) const
{
    std::unique_lock<std::mutex> lock(m_lock);

    if (m_state == Task::FINISHED)
        return true;

    if (msecs.count())
    {
        m_cond.wait_for(lock, msecs);
    }
    else
    {
        m_cond.wait(lock);
    }
    return (m_state == Task::FINISHED);
}

bool Worker::Task::abort()
{
    std::unique_lock<std::mutex> lock(m_lock);
    if (m_state != Task::RUNNING)
    {
        m_error = Task::ABORTED;
        m_state = Task::FINISHED;
        lock.unlock();
        m_cond.notify_all();
        return true;
    }
    return false;
}

bool Worker::Task::setState(State state)
{
    std::unique_lock<std::mutex> lock(m_lock);
    switch (state)
    {
    case Task::READY: return false;
    case Task::RUNNING:
        if (m_state != Task::READY)
            return false;
        m_state = Task::RUNNING;
        break;
    case Task::FINISHED:
        m_state = Task::FINISHED;
        lock.unlock();
        m_cond.notify_all();
        break;
    }

    return true;
}

void Worker::Task::setError(const std::string &error)
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_state = Task::FINISHED;
    m_errorString = error;
    m_error = Task::FAILED;
    lock.unlock();
    m_cond.notify_all();
}

Worker::FunTask::FunTask(const std::function<void()> &fun) : m_fun(fun) {}

void Worker::FunTask::run()
{
    try
    {
        m_fun();
    }
    catch (const std::string &error)
    {
        setError(error);
    }
}
