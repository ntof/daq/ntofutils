/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-06T10:01:02+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferari.1@cern.ch>
**
*/

#include "Thread.hpp"

#include <easylogging++.h>
#include <pthread.h>

#include "config.h" // for HAVE_PTHREAD_NAME

namespace ntof {
namespace utils {

Thread::Thread(const std::string &name) :
    m_name(name),
    m_started(false),
    m_waked(false),
    m_policy(SCHED_OTHER),
    m_priority(-1),
    m_interval(std::chrono::milliseconds::zero())
{}

Thread::Thread(Thread &&other) noexcept :
    m_name(other.m_name),
    m_started(false),
    m_waked(false),
    m_policy(other.m_policy),
    m_priority(other.m_priority),
    m_interval(other.m_interval)
{}

Thread::~Thread()
{
    stop();
}

void Thread::start()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_started.load())
    {
        LOG(WARNING) << "Can't start thread [" << m_name << "]. "
                     << "Thread is already started!";
        return;
    }

    m_started.store(true);
    m_waked = false;
    m_thread.reset(new std::thread(&Thread::_thread_func, this));

    if (m_policy != SCHED_OTHER || m_priority != -1)
    {
        _set_thread_priority(m_policy, m_priority);
    }
}

void Thread::stop()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_started.store(false);
    m_waked = true;

    std::unique_ptr<std::thread> th;
    th.swap(m_thread);
    lock.unlock();

    m_cond.notify_all();

    if (th)
        th->join();
}

void Thread::run()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_started.load())
        return;

    m_started.store(true);
    m_waked = false;
    lock.unlock();
    _thread_func();
}

void Thread::wake()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_waked = true;
    }
    m_cond.notify_all();
}

void Thread::setPriority(int policy, int32_t priority)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (_set_thread_priority(policy, priority))
    {
        m_policy = policy;
        m_priority = priority;
    }
}

void Thread::setInterval(const std::chrono::milliseconds &ms)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_interval = ms;
}

void Thread::_thread_func()
{
#ifdef HAVE_PTHREAD_NAME
    if (!m_name.empty())
    {
        const std::string threadName = m_name.substr(0, 16);
        pthread_setname_np(pthread_self(), threadName.c_str());
    }
#endif

    thread_enter();
    while (m_started.load())
    {
        thread_func();

        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_interval.count() == 0)
            m_cond.wait(lock, [this]() { return m_waked; });
        else
            m_cond.wait_for(lock, m_interval, [this]() { return m_waked; });
        m_waked = false;
    }
    thread_exit();
}

bool Thread::_set_thread_priority(int policy, int32_t priority)
{
    if (m_started.load())
    {
        sched_param sch;
        int currentPolicy;
        pthread_getschedparam(m_thread->native_handle(), &currentPolicy, &sch);
        sch.sched_priority = priority;
        if (pthread_setschedparam(m_thread->native_handle(), policy, &sch))
        {
            LOG(ERROR) << "Failed to set policy and proprity to thread ["
                       << m_name << "]: "
                       << "Error: " << std::strerror(errno);
            return false;
        }
    }
    return true;
}

} // namespace utils
} // namespace ntof
