/**
 * @Author: Sylvain Fargier <sfargier>
 * @Date:   2019-11-14T17:47:50+01:00
 * @Email:  sylvain.fargier@cern.ch
 * @Last modified by:   sfargier
 * @License: ** Copyright (C) 2018 CERN
 */

#include "DaqTypes.h"

#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>

#include <pugixml.hpp>
#include <time.h>
#include <unistd.h>

#include "NTOFException.h"

constexpr HeaderType RunControlHeader::TITLE;
constexpr uint32_t RunControlHeader::REVISION;
constexpr HeaderType EventHeader::TITLE;
constexpr uint32_t EventHeader::REVISION;
constexpr size_t EventHeader::SIZE;
constexpr HeaderType AcquisitionHeader::TITLE;
constexpr uint32_t AcquisitionHeader::REVISION;
constexpr size_t AcquisitionHeader::SIZE;
constexpr HeaderType SkipHeader::TITLE;
constexpr uint32_t SkipHeader::REVISION;
constexpr size_t SkipHeader::SIZE;
constexpr HeaderType ModuleHeader::TITLE;
constexpr uint32_t ModuleHeader::REVISION;
constexpr size_t ChannelConfig::SIZE;
constexpr HeaderType AdditionalDataHeader::TITLE;
constexpr uint32_t AdditionalDataHeader::REVISION;
constexpr HeaderType IndexHeader::TITLE;
constexpr uint32_t IndexHeader::REVISION;
constexpr size_t IndexValue::SIZE;

static inline std::string UInt32ToStr(uint32_t num)
{
    uint32_t ret[2] = {num, 0};
    return std::string(reinterpret_cast<const char *>(ret));
}

static inline uint32_t StrToUInt32(const std::string &str)
{
    if (str.length() < 4)
    {
        std::string other(str);
        other.resize(4);
        return StrToUInt32(other);
    }
    return *reinterpret_cast<const uint32_t *>(str.c_str());
}

static inline void updateDateTime(std::time_t timestamp,
                                  uint32_t &date,
                                  uint32_t &time)
{
    struct tm *info = std::localtime(&timestamp);
    time = info->tm_hour * 10000 + info->tm_min * 100 + info->tm_sec;
    date = (uint32_t) ((info->tm_year) * 10000 + (info->tm_mon + 1) * 100 +
                       info->tm_mday);
}

HeaderLookup::HeaderLookup() : type(0) {}

std::istream &operator>>(std::istream &is, HeaderLookup &lookup)
{
    is.read(reinterpret_cast<char *>(&lookup.type), sizeof(HeaderType));
    if (is)
        is.seekg(-sizeof(HeaderType), std::ios_base::cur);
    return is;
}

static const ExperimentArea s_unknownArea{std::string(), 0,
                                          std::numeric_limits<uint32_t>::max()};
const ExperimentArea ExperimentArea::EAR1{"EAR1", 100000, 199999};
const ExperimentArea ExperimentArea::EAR2{"EAR2", 200000, 299999};
const ExperimentArea ExperimentArea::EAR3{"EAR3", 300000, 399999};
const ExperimentArea ExperimentArea::LAB{"LAB", 900000, 999999};

const ExperimentArea &ExperimentArea::findArea(uint32_t runNumber)
{
    if (runNumber < EAR1.min)
        return s_unknownArea;
    else if (runNumber <= EAR1.max)
        return EAR1;
    else if (runNumber <= EAR2.max)
        return EAR2;
    else if (runNumber <= EAR3.max)
        return EAR3;
    else if (runNumber >= LAB.min && runNumber <= LAB.max)
        return LAB;
    else
        return s_unknownArea;
}

const ExperimentArea &ExperimentArea::findArea(const std::string &name)
{
    if (name == ExperimentArea::EAR1.name)
        return ExperimentArea::EAR1;
    else if (name == ExperimentArea::EAR2.name)
        return ExperimentArea::EAR2;
    else if (name == ExperimentArea::EAR3.name)
        return ExperimentArea::EAR3;
    else if (name == ExperimentArea::LAB.name)
        return ExperimentArea::LAB;
    return s_unknownArea;
}

RunControlHeader::RunControlHeader() :
    runNumber(0),
    segmentNumber(0),
    streamNumber(0),
    experiment(0),
    date(0),
    time(0),
    totalNumberOfStreams(0),
    totalNumberOfChassis(0),
    totalNumberOfModules(0)
{}

uint32_t RunControlHeader::numberOfChannels() const
{
    return std::accumulate(numberOfChannelsPerModule.cbegin(),
                           numberOfChannelsPerModule.cend(),
                           static_cast<uint32_t>(0));
}

std::ostream &operator<<(std::ostream &os, const RunControlHeader &header)
{
    uint32_t i;
    os.write(reinterpret_cast<const char *>(&RunControlHeader::TITLE),
             sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&RunControlHeader::REVISION),
             sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = header.numberOfModules() + 11;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&header.runNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.segmentNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.streamNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.experiment),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.date), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.time), sizeof(uint32_t));
    i = header.numberOfModules();
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));
    i = header.numberOfChannels();
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.totalNumberOfStreams),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.totalNumberOfChassis),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.totalNumberOfModules),
             sizeof(uint32_t));

    for (i = 0; i < header.numberOfChannelsPerModule.size(); ++i)
    {
        uint32_t num = header.numberOfChannelsPerModule[i];
        os.write(reinterpret_cast<const char *>(&num), sizeof(uint32_t));
    }
    return os;
}

std::istream &operator>>(std::istream &is, RunControlHeader &header)
{
    uint32_t i = 0;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != RunControlHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "RCTR not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != RunControlHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "RCTR wrong revision" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i < 11)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "RCTR wrong number of words" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&header.runNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.segmentNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.streamNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.experiment), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.date), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.time), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    header.numberOfChannelsPerModule.resize(i);

    uint32_t numChannels = 0;
    is.read(reinterpret_cast<char *>(&numChannels), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&header.totalNumberOfStreams),
            sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.totalNumberOfChassis),
            sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.totalNumberOfModules),
            sizeof(uint32_t));

    for (i = 0; i < header.numberOfChannelsPerModule.size(); ++i)
    {
        is.read(reinterpret_cast<char *>(&header.numberOfChannelsPerModule[i]),
                sizeof(uint32_t));
    }
    return is;
}

void RunControlHeader::fromXML(const pugi::xml_node &root)
{
    uint32_t nbChannels = std::numeric_limits<uint32_t>::max();
    uint32_t index = 0;

    numberOfChannelsPerModule.resize(0);
    if (!root)
        throw ntof::NTOFException("Invalid root node", __FILE__, __LINE__);

    for (pugi::xml_node node = root.first_child(); node;
         node = node.next_sibling())
    {
        std::string nodeName = node.name();
        if (nodeName == "bankName")
        {
            std::string value = node.child_value();
            if (value != "RCTR")
                throw ntof::NTOFException("Invalid bankName: " + value,
                                          __FILE__, __LINE__);
        }
        else if (nodeName == "revisionNumber")
        {
            if (node.text().as_uint() != ModuleHeader::REVISION)
                throw ntof::NTOFException(
                    std::string("Invalid revision: ") + node.child_value(),
                    __FILE__, __LINE__);
        }
        else if (nodeName == "reserved")
        {
            /* ignore that one */
        }
        else if (nodeName == "nbWord")
        {
            /* ignore that one */
        }
        else if (nodeName == "runNumber")
            runNumber = node.text().as_uint();
        else if (nodeName == "runExtensionNumber")
            segmentNumber = node.text().as_uint();
        else if (nodeName == "stream")
            streamNumber = node.text().as_uint();
        else if (nodeName == "experimentIdentifier")
            setExperiment(node.child_value());
        else if (nodeName == "date")
            date = node.text().as_uint();
        else if (nodeName == "time")
            time = node.text().as_uint();
        else if (nodeName == "nbModules")
            numberOfChannelsPerModule.resize(node.text().as_uint());
        else if (nodeName == "nbChannels")
            nbChannels = node.text().as_uint();
        else if (nodeName == "nbStreams")
            totalNumberOfStreams = node.text().as_uint();
        else if (nodeName == "nbChassis")
            totalNumberOfChassis = node.text().as_uint();
        else if (nodeName == "nbTotalModules")
            totalNumberOfModules = node.text().as_uint();
        else if (nodeName == "nbChannelsUsed")
        {
            if (index >= numberOfChannelsPerModule.size())
                throw ntof::NTOFException(
                    "Invalid nbModules: " +
                        std::to_string(numberOfChannelsPerModule.size()),
                    __FILE__, __LINE__);
            numberOfChannelsPerModule[index++] = node.text().as_uint();
        }
    }

    if (numberOfChannels() != nbChannels)
        throw ntof::NTOFException(
            "Invalid nbChannels: " + std::to_string(nbChannels), __FILE__,
            __LINE__);
}

std::string RunControlHeader::getExperiment() const
{
    return UInt32ToStr(experiment);
}

void RunControlHeader::updateExperiment()
{
    setExperiment(ExperimentArea::findArea(runNumber).name);
}

void RunControlHeader::setExperiment(const std::string &type)
{
    experiment = StrToUInt32(type);
}

void RunControlHeader::updateDateTime(std::time_t timestamp)
{
    ::updateDateTime(timestamp, date, time);
}

EventHeader::EventHeader() :
    sizeOfEvent(0),
    eventNumber(0),
    runNumber(0),
    time(0),
    date(0),
    compTS(0),
    bctTS(0),
    intensity(-1),
    beamType(-1)
{}

void EventHeader::setCompTS()
{
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    compTS = (ts.tv_sec * 1E9) + ts.tv_nsec;
}

void EventHeader::updateDateTime(time_t timestamp)
{
    ::updateDateTime(timestamp, date, time);
}

void EventHeader::toXML(pugi::xml_node &eveh) const
{
    eveh.set_name("EVEH");

    pugi::xml_node node = eveh.append_child("bankName");
    node.append_attribute("value").set_value(EventHeader::TITLE);

    node = eveh.append_child("revisionNumber");
    node.append_attribute("value").set_value(EventHeader::REVISION);

    node = eveh.append_child("reserved");
    node.append_attribute("value").set_value(0);

    node = eveh.append_child("nbWord");
    node.append_attribute("value").set_value(12);

    node = eveh.append_child("sizeOfEvent");
    node.append_attribute("value").set_value(sizeOfEvent);

    node = eveh.append_child("eventNumber");
    node.append_attribute("value").set_value(eventNumber);

    node = eveh.append_child("runNumber");
    node.append_attribute("value").set_value(runNumber);

    node = eveh.append_child("time");
    node.append_attribute("value").set_value(time);

    node = eveh.append_child("date");
    node.append_attribute("value").set_value(date);

    node = eveh.append_child("empty");
    node.append_attribute("value").set_value(0);

    node = eveh.append_child("compTS");
    node.append_attribute("value").set_value(compTS);

    node = eveh.append_child("bctTS");
    node.append_attribute("value").set_value(bctTS);

    node = eveh.append_child("intensity");
    node.append_attribute("value").set_value(intensity);

    node = eveh.append_child("beamType");
    node.append_attribute("value").set_value(beamType);
}

static const uint32_t EVEH_HDR[4] = {EventHeader::TITLE, EventHeader::REVISION,
                                     0, 12};

std::ostream &operator<<(std::ostream &os, const EventHeader &header)
{
    uint32_t i;

    os.write(reinterpret_cast<const char *>(&EVEH_HDR), sizeof(EVEH_HDR));
    os.write(reinterpret_cast<const char *>(&header.sizeOfEvent),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.eventNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.runNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.time), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.date), sizeof(uint32_t));
    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.compTS), sizeof(double));
    os.write(reinterpret_cast<const char *>(&header.bctTS), sizeof(double));
    os.write(reinterpret_cast<const char *>(&header.intensity), sizeof(float));
    os.write(reinterpret_cast<const char *>(&header.beamType), sizeof(uint32_t));
    return os;
}

std::istream &operator>>(std::istream &is, EventHeader &header)
{
    uint32_t i = 0;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != EventHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "EVEH not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != EventHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "EVEH wrong revision" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != 12)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "EVEH wrong size" << std::endl;
        return is;
    }
    is.read(reinterpret_cast<char *>(&header.sizeOfEvent), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.eventNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.runNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.time), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.date), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.compTS), sizeof(double));
    is.read(reinterpret_cast<char *>(&header.bctTS), sizeof(double));
    is.read(reinterpret_cast<char *>(&header.intensity), sizeof(float));
    is.read(reinterpret_cast<char *>(&header.beamType), sizeof(uint32_t));
    return is;
}

/* ACQC Data*/
std::istream &operator>>(std::istream &is, Data &pulse)
{
    uint64_t i;
    is.read(reinterpret_cast<char *>(&pulse.detectorTimeStamp),
            sizeof(uint64_t));
    is.read(reinterpret_cast<char *>(&i), sizeof(uint64_t));

    pulse.detectorData.clear();

    try
    {
        pulse.detectorData.resize(static_cast<size_t>(i));
    }
    catch (const std::bad_alloc &e)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "Memory Allocation Error for ACQC Data" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(pulse.detectorData.data()),
            sizeof(uint16_t) * pulse.detectorData.size());
    return is;
}

AcquisitionHeader::AcquisitionHeader() :
    dataSize(0), detectorType(0), detectorId(0), str_mod_crate(0)
{}

void AcquisitionHeader::setLocation(uint32_t location)
{
    str_mod_crate = location;
}

void AcquisitionHeader::setLocation(uint8_t stream,
                                    uint8_t chassis,
                                    uint8_t module,
                                    uint8_t channel)
{
    str_mod_crate = ChannelConfig::makeLocation(stream, chassis, module,
                                                channel);
}

std::string AcquisitionHeader::getDetectorType() const
{
    return UInt32ToStr(detectorType);
}

void AcquisitionHeader::setDetectorType(const std::string &type)
{
    detectorType = StrToUInt32(type);
}

size_t AcquisitionHeader::padding() const
{
    const size_t padding = WORD_SIZE - (dataSize % WORD_SIZE);
    return (padding == WORD_SIZE) ? 0 : padding;
}

std::ostream &operator<<(std::ostream &os, const AcquisitionHeader &header)
{
    uint32_t i;
    i = AcquisitionHeader::TITLE;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = AcquisitionHeader::REVISION;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = (12 + header.dataSize + header.padding()) >> 2;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&header.detectorType),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.detectorId),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.str_mod_crate),
             sizeof(uint32_t));
    return os;
}

std::istream &operator>>(std::istream &is, AcquisitionHeader &header)
{
    uint32_t i;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != AcquisitionHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "ACQC not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != AcquisitionHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "ACQC wrong revision" << std::endl;
        return is;
    }

    // Reserved
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&header.dataSize), sizeof(uint32_t));
    if (header.dataSize < 3)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "ACQC wrong number of words" << std::endl;
        return is;
    }
    // dataSize can include padding (if any)
    header.dataSize = (header.dataSize << 2) -
        (sizeof(header.detectorType) + sizeof(header.detectorId) +
         sizeof(header.str_mod_crate));

    is.read(reinterpret_cast<char *>(&header.detectorType), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.detectorId), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&header.str_mod_crate), sizeof(uint32_t));
    return is;
}

SkipHeader::SkipHeader() : start(-1), end(-1) {}
SkipHeader::SkipHeader(int64_t start, int64_t end) : start(start), end(end) {}

std::ostream &operator<<(std::ostream &os, const SkipHeader &header)
{
    uint32_t i;
    i = SkipHeader::TITLE;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = SkipHeader::REVISION;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = 4;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&header.start), sizeof(uint64_t));
    os.write(reinterpret_cast<const char *>(&header.end), sizeof(uint64_t));
    return os;
}

std::istream &operator>>(std::istream &is, SkipHeader &header)
{
    uint32_t i = 0;

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != SkipHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "SKIP not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != SkipHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "SKIP wrong revision" << std::endl;
        return is;
    }

    // Reserved word
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != 4)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "SKIP wrong number of words" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&header.start), sizeof(uint64_t));
    is.read(reinterpret_cast<char *>(&header.end), sizeof(uint64_t));
    return is;
}

ModuleHeader::ModuleHeader(uint32_t nbChannels) : channelsConfig(nbChannels) {}

std::ostream &operator<<(std::ostream &os, const ModuleHeader &header)
{
    uint32_t i;
    i = ModuleHeader::TITLE;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = ModuleHeader::REVISION;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = header.channelsConfig.size() * (ChannelConfig::SIZE >> 2) + 1;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = header.channelsConfig.size();
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    for (i = 0; i < header.channelsConfig.size(); ++i)
        os << header.channelsConfig[i];

    return os;
}

std::istream &operator>>(std::istream &is, ModuleHeader &header)
{
    uint32_t i;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != ModuleHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "MODH not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != ModuleHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "MODH wrong revision" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    header.channelsConfig.resize(i);

    for (i = 0; i < header.channelsConfig.size(); ++i)
        is >> header.channelsConfig[i];

    return is;
}

void ModuleHeader::fromXML(const pugi::xml_node &root)
{
    uint32_t index = 0;

    channelsConfig.resize(0);
    if (!root)
        throw ntof::NTOFException("Invalid root node", __FILE__, __LINE__);

    for (pugi::xml_node node = root.first_child(); node;
         node = node.next_sibling())
    {
        std::string nodeName = node.name();
        if (nodeName == "bankName")
        {
            std::string value = node.child_value();
            if (value != "MODH")
                throw ntof::NTOFException("Invalid bankName: " + value,
                                          __FILE__, __LINE__);
        }
        else if (nodeName == "revisionNumber")
        {
            if (node.text().as_uint() != ModuleHeader::REVISION)
                throw ntof::NTOFException(
                    std::string("Invalid revision: ") + node.child_value(),
                    __FILE__, __LINE__);
        }
        else if (nodeName == "reserved")
        {
            /* ignore that one */
        }
        else if (nodeName == "nbWord")
        {
            /* not used */
        }
        else if (nodeName == "nbChannel")
        {
            uint32_t nbChannel = node.text().as_uint();
            channelsConfig.resize(nbChannel);
        }
        else if (nodeName == "config")
        {
            if (index >= channelsConfig.size())
                throw ntof::NTOFException(
                    "Invalid nbChannel: " +
                        std::to_string(channelsConfig.size()),
                    __FILE__, __LINE__);
            channelsConfig[index++].fromXML(node);
        }
    }
}

size_t ModuleHeader::size() const
{
    return 20 + (channelsConfig.size() * ChannelConfig::SIZE);
}

void ChannelConfig::fromXML(const pugi::xml_node &root)
{
    for (pugi::xml_node node = root.first_child(); node;
         node = node.next_sibling())
    {
        std::string nodeName = node.name();
        if (nodeName == "detectorType")
            setDetectorType(node.child_value());
        else if (nodeName == "detectorId")
            detectorId = node.text().as_uint();
        else if (nodeName == "moduleType")
            setModuleType(node.child_value());
        else if (nodeName == "str_mod_crate")
            str_mod_crate = node.text().as_uint();
        else if (nodeName == "sampleRate")
            sampleRate = node.text().as_float();
        else if (nodeName == "sampleSize")
            sampleSize = node.text().as_uint();
        else if (nodeName == "fullScale")
            fullScale = node.text().as_float();
        else if (nodeName == "delayTime")
            delayTime = node.text().as_int();
        else if (nodeName == "threshold")
            threshold = node.text().as_int();
        else if (nodeName == "thresholdSign")
            thresholdSign = node.text().as_int();
        else if (nodeName == "offset")
            offset = node.text().as_float();
        else if (nodeName == "preSample")
            preSample = node.text().as_uint();
        else if (nodeName == "postSample")
            postSample = node.text().as_uint();
        else if (nodeName == "clockState")
            setClockState(node.child_value());
        else if (nodeName == "zeroSuppressionStart")
            zeroSuppressionStart = node.text().as_uint();
        else if (nodeName == "masterDetectorType")
            setMasterDetectorType(node.child_value());
        else if (nodeName == "masterDetectorId")
            masterDetectorId = node.text().as_uint();
    }
}

void ChannelConfig::setLocation(uint32_t location)
{
    str_mod_crate = location;
}

void ChannelConfig::setLocation(uint8_t stream,
                                uint8_t chassis,
                                uint8_t module,
                                uint8_t channel)
{
    str_mod_crate = makeLocation(stream, chassis, module, channel);
}

uint32_t ChannelConfig::makeLocation(uint8_t stream,
                                     uint8_t chassis,
                                     uint8_t module,
                                     uint8_t channel)
{
    return channel | (module << 8) | (chassis << 16) | (stream << 24);
}

void ChannelConfig::setStream(uint8_t value)
{
    str_mod_crate = (str_mod_crate & 0x00FFFFFF) | (value << 24);
}

void ChannelConfig::setChassis(uint8_t value)
{
    str_mod_crate = (str_mod_crate & 0xFF00FFFF) | (value << 16);
}

void ChannelConfig::setModule(uint8_t value)
{
    str_mod_crate = (str_mod_crate & 0xFFFF00FF) | (value << 8);
}

void ChannelConfig::setChannel(uint8_t value)
{
    str_mod_crate = (str_mod_crate & 0xFFFFFF00) | (value);
}

std::string ChannelConfig::getModuleType() const
{
    return UInt32ToStr(moduleType);
}

void ChannelConfig::setModuleType(const std::string &type)
{
    moduleType = StrToUInt32(type);
}

std::string ChannelConfig::getDetectorType() const
{
    return UInt32ToStr(detectorType);
}

void ChannelConfig::setDetectorType(const std::string &type)
{
    detectorType = StrToUInt32(type);
}

std::string ChannelConfig::getClockState() const
{
    return UInt32ToStr(clockState);
}

void ChannelConfig::setClockState(const std::string &state)
{
    clockState = StrToUInt32(state);
}

std::string ChannelConfig::getMasterDetectorType() const
{
    return UInt32ToStr(masterDetectorType);
}

void ChannelConfig::setMasterDetectorType(const std::string &type)
{
    masterDetectorType = StrToUInt32(type);
}

std::ostream &operator<<(std::ostream &os, const ChannelConfig &conf)
{
    const char fill = os.fill();
    os.write(reinterpret_cast<const char *>(&conf), sizeof(ChannelConfig));
    os << std::setfill('\0')
       << std::setw(ChannelConfig::SIZE - sizeof(ChannelConfig))
       << std::string() << std::setfill(fill);
    return os;
}

std::istream &operator>>(std::istream &is, ChannelConfig &conf)
{
    is.read(reinterpret_cast<char *>(&conf), sizeof(ChannelConfig));
    is.seekg(20, is.cur);
    return is;
}

AdditionalDataValue::AdditionalDataValue(AdditionalDataValue::Type t,
                                         size_t reserve) :
    type(t)
{
    this->reserve(reserve);
}

AdditionalDataValue::AdditionalDataValue(const std::string &name,
                                         AdditionalDataValue::Type t,
                                         size_t reserve) :
    name(name), type(t)
{
    this->reserve(reserve);
}

size_t AdditionalDataValue::count() const
{
    return data.size() / typeSize();
}

void AdditionalDataValue::fromString(const std::string &value)
{
    const uint8_t *cstr = reinterpret_cast<const uint8_t *>(value.c_str());
    data.reserve(value.size());
    data.assign(cstr, cstr + value.size());
}

void AdditionalDataValue::reserve(size_t size)
{
    data.reserve(typeSize() * size);
}

size_t AdditionalDataValue::typeSize() const
{
    switch (type)
    {
    case TypeChar: return sizeof(char);
    case TypeInt32: return sizeof(int32_t);
    case TypeInt64: return sizeof(int64_t);
    case TypeFloat: return sizeof(float);
    case TypeDouble: return sizeof(double);
    default: return 0;
    }
}

size_t AdditionalDataValue::padding() const
{
    const size_t padding = WORD_SIZE - (data.size() % WORD_SIZE);
    return (padding == WORD_SIZE) ? 0 : padding;
}

std::ostream &operator<<(std::ostream &os, const AdditionalDataValue &value)
{
    uint32_t i;
    const char fill = os.fill();

    if (value.name.size() > 63)
    {
        const std::string name(value.name, 0, 63);
        os << name << '\0';
    }
    else
    {
        os << value.name << std::setfill('\0')
           << std::setw(64 - value.name.size()) << std::string();
    }
    i = value.type;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = value.count();
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&value.data[0]), value.data.size());

    const size_t padding = value.padding();
    if (padding != WORD_SIZE)
        os << std::setfill('\0') << std::setw(padding) << std::string();

    os << std::setfill(fill);
    return os;
}

size_t AdditionalDataHeader::size() const
{
    size_t size = 16;
    for (size_t i = 0; i < values.size(); ++i)
    {
        size += values[i].size();
    }
    return size;
}

std::ostream &operator<<(std::ostream &os, const AdditionalDataHeader &header)
{
    uint32_t i;
    os.write(reinterpret_cast<const char *>(&AdditionalDataHeader::TITLE),
             sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&AdditionalDataHeader::REVISION),
             sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = (header.size() - 16) / WORD_SIZE;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    for (size_t idx = 0; idx < header.values.size(); ++idx)
        os << header.values[idx];
    return os;
}

std::istream &operator>>(std::istream &is, AdditionalDataHeader &header)
{
    uint32_t i = 0;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != AdditionalDataHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "ADDH not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != AdditionalDataHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "ADDH wrong revision" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    std::size_t words = i;
    header.values.clear();
    while (words > 0 && is)
    {
        AdditionalDataValue value;
        char name[65];

        is.read(reinterpret_cast<char *>(&name), 64);
        name[64] = '\0';
        value.name = name;

        is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
        value.type = static_cast<AdditionalDataValue::Type>(i);

        uint32_t count;
        is.read(reinterpret_cast<char *>(&count), sizeof(uint32_t));
        if (count != 0)
        {
            value.reserve(count);
            value.data.resize(value.data.capacity());
            is.read(reinterpret_cast<char *>(value.data.data()),
                    value.data.size());
            if (value.padding())
            {
                is.seekg(value.padding(), std::ios_base::cur);
            }
        }
        words -= value.size() / WORD_SIZE;
        header.values.push_back(std::move(value));
    }
    return is;
}

IndexValue::IndexValue(uint32_t stream,
                       uint32_t segment,
                       uint32_t validatedNumber,
                       uint32_t eventNumber,
                       uint64_t offset) :
    stream(stream),
    segment(segment),
    validatedNumber(validatedNumber),
    eventNumber(eventNumber),
    offset(offset)
{}

size_t IndexHeader::size() const
{
    return 16 + indexes.size() * IndexValue::SIZE;
}

std::ostream &operator<<(std::ostream &os, const IndexValue &header)
{
    os.write(reinterpret_cast<const char *>(&header.stream), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.segment), sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.validatedNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.eventNumber),
             sizeof(uint32_t));
    os.write(reinterpret_cast<const char *>(&header.offset), sizeof(uint64_t));
    return os;
}

std::ostream &operator<<(std::ostream &os, const IndexHeader &header)
{
    uint32_t i;
    os.write(reinterpret_cast<const char *>(&IndexHeader::TITLE),
             sizeof(uint32_t));

    os.write(reinterpret_cast<const char *>(&IndexHeader::REVISION),
             sizeof(uint32_t));

    i = 0;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    i = (header.size() - 16) / WORD_SIZE;
    os.write(reinterpret_cast<const char *>(&i), sizeof(uint32_t));

    for (size_t idx = 0; idx < header.indexes.size(); ++idx)
        os << header.indexes[idx];
    return os;
}

std::istream &operator>>(std::istream &is, IndexValue &value)
{
    is.read(reinterpret_cast<char *>(&value.stream), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&value.segment), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&value.validatedNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&value.eventNumber), sizeof(uint32_t));
    is.read(reinterpret_cast<char *>(&value.offset), sizeof(uint64_t));
    return is;
}

std::istream &operator>>(std::istream &is, IndexHeader &header)
{
    uint32_t i = 0;
    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != IndexHeader::TITLE)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "INDX not found in stream" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    if (i != IndexHeader::REVISION)
    {
        is.setstate(std::ios::failbit);
        std::cerr << "INDX wrong revision" << std::endl;
        return is;
    }

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));

    is.read(reinterpret_cast<char *>(&i), sizeof(uint32_t));
    std::size_t words = i;
    header.indexes.clear();
    while (words > 0 && is)
    {
        IndexValue value;
        is >> value;

        words -= IndexValue::SIZE / WORD_SIZE;
        header.indexes.push_back(std::move(value));
    }
    return is;
}
