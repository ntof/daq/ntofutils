/*
 * DIMData.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#include "DIMData.h"

#include <algorithm>
#include <cstring>
#include <iostream>
#include <sstream>

#include <pugixml.hpp>

#include "DIMException.h"

namespace ntof {
namespace dim {
/**
 * Build an enumeration
 * @param value Value of enum
 * @param name Associated label
 */
DIMEnum::DIMEnum(int32_t value, const std::string &name) : value_(value)
{
    values_[value] = name;
}

/**
 * Copy constructor
 * @param other Enumeration to be copied from
 */
DIMEnum::DIMEnum(const DIMEnum &other) :
    value_(other.value_), values_(other.values_)
{}

/**
 * Empty enum constructor
 */
DIMEnum::DIMEnum() : value_(0) {}

/**
 * Build an anonymous enumeration for sending a command
 * @param value Value of enum
 */
DIMEnum::DIMEnum(int32_t value) : value_(value) {}

/**
 * Destructor
 */
DIMEnum::~DIMEnum() {}

/**
 * Adds a new item to the enumeration
 * @param value Value of the new item
 * @param label Label of the new item
 */
void DIMEnum::addItem(int32_t value, const std::string &label)
{
    std::map<int32_t, std::string>::iterator it = values_.find(value);
    if (it != values_.end())
    {
        throw DIMException("Item already exists in enumeration!", __LINE__);
    }
    values_[value] = label;
}

/**
 * Gets the enumeration name
 * @return Enumeration name
 */
const std::string &DIMEnum::getName() const
{
    return values_.at(value_);
}

/**
 * Gets the enumeration value
 * @return enum value
 */
int32_t DIMEnum::getValue() const
{
    return value_;
}

const DIMEnum::ValuesMap &DIMEnum::getValuesMap() const
{
    return values_;
}

/**
 * Gets iterator to enum items
 * @return
 */
std::map<int32_t, std::string>::const_iterator DIMEnum::begin() const
{
    return values_.begin();
}

/**
 * Gets end iterator to enum values
 * @return
 */
std::map<int32_t, std::string>::const_iterator DIMEnum::end() const
{
    return values_.end();
}

/**
 * Sets new enum value
 * @param value
 */
void DIMEnum::setValue(int32_t value)
{
    std::map<int32_t, std::string>::iterator it = values_.find(value);
    if (it != values_.end())
    {
        value_ = value;
    }
    else
    {
        throw DIMException("Item doesn't exists in enumeration!", __LINE__);
    }
}

void DIMEnum::setValuesMap(const DIMEnum::ValuesMap &map)
{
    values_ = map;
}

/**
 * Comparison operator
 * @param value int value to be tested
 * @return True if matches
 */
bool DIMEnum::operator==(int32_t value) const
{
    return value_ == value;
}

/**
 * Comparison operator
 * @param name Enum name to be tested
 * @return True if matches
 */
bool DIMEnum::operator==(const std::string &name) const
{
    return values_.at(value_) == name;
}

/**
 * Comparison operator
 * @param name Enumeration to be tested
 * @return True if matches
 */
bool DIMEnum::operator==(const DIMEnum &val) const
{
    return (value_ == val.value_) && (values_.at(value_) == val.getName());
}

/**
 * Assignment operator
 * @param other Enum to copy
 */
DIMEnum &DIMEnum::operator=(const DIMEnum &val)
{
    value_ = val.value_;
    values_[value_] = val.getName();
    return *this;
}
// DIMEnum end

// DIMData start
/**
 * Empty constructor
 */
DIMData::DIMData() :
    m_index(0), m_name(""), m_unit(""), m_hidden(false), m_type(TypeInvalid)
{
    m_value.ulongValue = 0;
}

/**
 * Constructs the data using DIM raw data
 * @param data DIM XML node representing this
 */
DIMData::DIMData(const pugi::xml_node &data) : m_hidden(false)
{
    // Build a command data (without name and unit)
    m_name = data.attribute("name").value();
    m_unit = data.attribute("unit").value();
    if (data.attribute("index").empty())
    {
        throw DIMException("Index attribute missing", __LINE__);
    }
    m_index = data.attribute("index").as_int();

    const pugi::xml_attribute typeAttr = data.attribute("type");
    if (!typeAttr)
    {
        m_type = TypeNested;
    }
    else
    {
        m_type = static_cast<DataType>(typeAttr.as_int(TypeNested));
        if (m_type == TypeNested)
            throw DIMException("Invalid type ", __LINE__);
    }

    if (data.attribute("value").empty() && (m_type != TypeNested))
    {
        throw DIMException("Value attribute missing", __LINE__);
    }

    switch (m_type)
    {
    case TypeInt: m_value.intValue = data.attribute("value").as_int(); break;
    case TypeDouble:
        m_value.doubleValue = data.attribute("value").as_double();
        break;
    case TypeLong:
        m_value.longValue = data.attribute("value").as_llong();
        break;
    case TypeByte:
        m_value.byteValue = static_cast<int8_t>(
            data.attribute("value").as_int());
        break;
    case TypeShort:
        m_value.shortValue = static_cast<int16_t>(
            data.attribute("value").as_int());
        break;
    case TypeFloat:
        m_value.floatValue = data.attribute("value").as_float();
        break;
    case TypeBool: m_value.intValue = data.attribute("value").as_int(); break;
    case TypeString:
        m_value.ptrValue = new std::string(data.attribute("value").value());
        break;
    case TypeEnum: { // For keeping enumNode in this local scope
        pugi::xml_node enumNode = data.child("enum");
        if (enumNode.empty())
        {
            // Build a data for command
            m_value.ptrValue = new DIMEnum(data.attribute("value").as_int(), "");
        }
        else
        {
            // Build a data for acquisition
            DIMEnum *enumValue = new DIMEnum();
            for (pugi::xml_node enumItem = enumNode.first_child(); enumItem;
                 enumItem = enumItem.next_sibling())
            {
                enumValue->addItem(enumItem.attribute("value").as_int(),
                                   enumItem.attribute("name").value());
            }
            enumValue->setValue(data.attribute("value").as_int());
            m_value.ptrValue = enumValue;
        }
        break;
    }
    case TypeULong:
        m_value.ulongValue = data.attribute("value").as_ullong();
        break;
    case TypeUInt: m_value.uintValue = data.attribute("value").as_uint(); break;
    case TypeUShort:
        m_value.ushortValue = static_cast<uint16_t>(
            data.attribute("value").as_uint());
        break;
    case TypeUByte:
        m_value.uintValue = static_cast<uint8_t>(
            data.attribute("value").as_uint());
        break;
    case TypeNested: {
        DIMData::List *childs = new DIMData::List;
        m_value.ptrValue = childs;
        for (pugi::xml_node &node : data.children("data"))
        {
            childs->push_back(DIMData(node));
        }
        m_value.ptrValue = childs;
        break;
    }
    default: throw DIMException("Unknown datatype!", __LINE__);
    }
}

DIMData::DIMData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 const std::string &value) :
    DIMData(index, name, unit)
{
    m_type = TypeString;
    m_value.ptrValue = new std::string(value);
}

DIMData::DIMData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 const DIMEnum &value) :
    DIMData(index, name, unit)
{
    m_type = TypeEnum;
    m_value.ptrValue = new DIMEnum(value);
}

DIMData::DIMData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 const DIMData::List &value) :
    DIMData(index, name, unit)
{
    m_type = TypeNested;
    m_value.ptrValue = new DIMData::List(value);
}

DIMData::DIMData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 DIMData::List &&value) :
    DIMData(index, name, unit)
{
    m_type = TypeNested;
    m_value.ptrValue = new DIMData::List(std::move(value));
}

DIMData::DIMData(Index index, const std::string &name, const std::string &unit) :
    m_index(index), m_name(name), m_unit(unit), m_hidden(false)
{}

DIMData::DIMData(const DIMData &other) :
    m_index(other.m_index),
    m_name(other.m_name),
    m_unit(other.m_unit),
    m_hidden(other.m_hidden),
    m_type(TypeInvalid)
{
    setValue(other);
}

DIMData::DIMData(DIMData &&other) :
    m_index(other.m_index),
    m_name(std::move(other.m_name)),
    m_unit(std::move(other.m_unit)),
    m_hidden(other.m_hidden),
    m_type(TypeInvalid)
{
    setValue(std::move(other));
}

DIMData &DIMData::operator=(const DIMData &other)
{
    m_index = other.m_index;
    m_name = other.m_name;
    m_unit = other.m_unit;
    m_hidden = other.m_hidden;

    setValue(other);
    return *this;
}

DIMData &DIMData::operator=(DIMData &&other)
{
    m_index = other.m_index;
    m_name = std::move(other.m_name);
    m_unit = std::move(other.m_unit);
    m_hidden = other.m_hidden;

    setValue(std::move(other));
    return *this;
}

DIMData::~DIMData()
{
    resetValue();
}

int32_t DIMData::getIntValue() const
{
    if (m_type != TypeInt)
    {
        throw DIMException("Data is not int type!", __LINE__);
    }
    return m_value.intValue;
}

double DIMData::getDoubleValue() const
{
    if (m_type != TypeDouble)
    {
        throw DIMException("Data is not double type!", __LINE__);
    }
    return m_value.doubleValue;
}

int64_t DIMData::getLongValue() const
{
    if (m_type != TypeLong)
    {
        throw DIMException("Data is not long type!", __LINE__);
    }
    return m_value.longValue;
}

int8_t DIMData::getByteValue() const
{
    if (m_type != TypeByte)
    {
        throw DIMException("Data is not byte type!", __LINE__);
    }
    return m_value.byteValue;
}

int16_t DIMData::getShortValue() const
{
    if (m_type != TypeShort)
    {
        throw DIMException("Data is not short type!", __LINE__);
    }
    return m_value.shortValue;
}

float DIMData::getFloatValue() const
{
    if (m_type != TypeFloat)
    {
        throw DIMException("Data is not float type!", __LINE__);
    }
    return m_value.floatValue;
}

bool DIMData::getBoolValue() const
{
    if (m_type != TypeBool)
    {
        throw DIMException("Data is not boolean type!", __LINE__);
    }
    return m_value.intValue == 1;
}

uint32_t DIMData::getUIntValue() const
{
    if (m_type != TypeUInt)
    {
        throw DIMException("Data is not uint32_t type!", __LINE__);
    }
    return m_value.uintValue;
}

uint64_t DIMData::getULongValue() const
{
    if (m_type != TypeULong)
    {
        throw DIMException("Data is not uint64_t type!", __LINE__);
    }
    return m_value.ulongValue;
}

uint8_t DIMData::getUByteValue() const
{
    if (m_type != TypeUByte)
    {
        throw DIMException("Data is not uint8_t type!", __LINE__);
    }
    return m_value.ubyteValue;
}

uint16_t DIMData::getUShortValue() const
{
    if (m_type != TypeUShort)
    {
        throw DIMException("Data is not uint16_t type!", __LINE__);
    }
    return m_value.ushortValue;
}

const std::string &DIMData::getStringValue() const
{
    if (m_type != TypeString)
    {
        throw DIMException("Data is not string type!", __LINE__);
    }
    return *static_cast<const std::string *>(m_value.ptrValue);
}

DIMEnum &DIMData::getEnumValue() const
{
    if (m_type != TypeEnum)
    {
        throw DIMException("Data is not enum type!", __LINE__);
    }
    return *static_cast<DIMEnum *>(m_value.ptrValue);
}

const DIMData::List &DIMData::getNestedValue() const
{
    if (m_type != TypeNested)
    {
        throw DIMException("Data is not nested type!", __LINE__);
    }
    return *static_cast<const DIMData::List *>(m_value.ptrValue);
}

DIMData::List &DIMData::getNestedValue()
{
    if (m_type != TypeNested)
    {
        throw DIMException("Data is not nested type!", __LINE__);
    }
    return *static_cast<DIMData::List *>(m_value.ptrValue);
}

#define GET_VALUE_SPEC(T, F)       \
    template<>                     \
    T DIMData::getValue<T>() const \
    {                              \
        return F();                \
    }

GET_VALUE_SPEC(int8_t, getByteValue)
GET_VALUE_SPEC(int16_t, getShortValue)
GET_VALUE_SPEC(int32_t, getIntValue)
GET_VALUE_SPEC(int64_t, getLongValue)
GET_VALUE_SPEC(uint8_t, getUByteValue)
GET_VALUE_SPEC(uint16_t, getUShortValue)
GET_VALUE_SPEC(uint32_t, getUIntValue)
GET_VALUE_SPEC(uint64_t, getULongValue)
GET_VALUE_SPEC(bool, getBoolValue)
GET_VALUE_SPEC(float, getFloatValue)
GET_VALUE_SPEC(double, getDoubleValue)
GET_VALUE_SPEC(std::string, getStringValue)
GET_VALUE_SPEC(const std::string &, getStringValue)
GET_VALUE_SPEC(DIMEnum, getEnumValue)
GET_VALUE_SPEC(DIMEnum &, getEnumValue)
GET_VALUE_SPEC(DIMData::List, getNestedValue)
GET_VALUE_SPEC(const DIMData::List &, getNestedValue)

#undef GET_VALUE_SPEC

#define IS_VALUE_SPEC(T, F)     \
    template<>                  \
    bool DIMData::is<T>() const \
    {                           \
        return F();             \
    }

IS_VALUE_SPEC(int8_t, isByte)
IS_VALUE_SPEC(int16_t, isShort)
IS_VALUE_SPEC(int32_t, isInt)
IS_VALUE_SPEC(int64_t, isLong)
IS_VALUE_SPEC(uint8_t, isUByte)
IS_VALUE_SPEC(uint16_t, isUShort)
IS_VALUE_SPEC(uint32_t, isUInt)
IS_VALUE_SPEC(uint64_t, isULong)
IS_VALUE_SPEC(bool, isBool)
IS_VALUE_SPEC(float, isFloat)
IS_VALUE_SPEC(double, isDouble)
IS_VALUE_SPEC(const std::string &, isString)
IS_VALUE_SPEC(std::string, isString)
IS_VALUE_SPEC(DIMEnum &, isEnum)
IS_VALUE_SPEC(DIMEnum, isEnum)
IS_VALUE_SPEC(const DIMData::List &, isNested)
IS_VALUE_SPEC(DIMData::List, isNested)

#undef IS_VALUE_SPEC

/**
 * Gets string representation of the data
 * @return
 */
std::string DIMData::getValueAsString() const
{
    std::ostringstream oss;
    insertIntoStream(oss);
    return oss.str();
}

/**
 * Insert value into string stream
 * @param os
 */
void DIMData::insertIntoStream(std::ostream &oss) const
{
    switch (m_type)
    {
    case TypeInt: oss << m_value.intValue; break;
    case TypeDouble: oss << m_value.doubleValue; break;
    case TypeString:
        oss << (*static_cast<const std::string *>(m_value.ptrValue));
        break;
    case TypeLong: oss << m_value.longValue; break;
    case TypeByte: oss << m_value.byteValue; break;
    case TypeShort: oss << m_value.shortValue; break;
    case TypeFloat: oss << m_value.floatValue; break;
    case TypeBool: oss << ((m_value.intValue == 1) ? "true" : "false"); break;
    case TypeEnum:
        oss << static_cast<const DIMEnum *>(m_value.ptrValue)->getName();
        break;
    case TypeUInt: oss << m_value.uintValue; break;
    case TypeULong: oss << m_value.ulongValue; break;
    case TypeUShort: oss << m_value.ushortValue; break;
    case TypeUByte: oss << m_value.ubyteValue; break;
    case TypeNested: {
        oss << "[";
        bool first = true;
        for (const DIMData &data : getNestedValue())
        {
            if (!first)
                oss << ", ";
            data.insertIntoStream(oss);
            first = false;
        }
        oss << "]";
    }
    case TypeInvalid: break;
    }
}

void DIMData::setValue(int32_t value)
{
    if (m_type != TypeInt)
    {
        throw DIMException("Data is not int type!", __LINE__);
    }
    m_value.intValue = value;
}

void DIMData::setValue(double value)
{
    if (m_type != TypeDouble)
    {
        throw DIMException("Data is not double type!", __LINE__);
    }
    m_value.doubleValue = value;
}

void DIMData::setValue(const std::string &value)
{
    if (m_type != TypeString)
    {
        throw DIMException("Data is not string type!", __LINE__);
    }
    *static_cast<std::string *>(m_value.ptrValue) = value;
}

void DIMData::setValue(int64_t value)
{
    if (m_type != TypeLong)
    {
        throw DIMException("Data is not long type!", __LINE__);
    }
    m_value.longValue = value;
}

void DIMData::setValue(int8_t value)
{
    if (m_type != TypeByte)
    {
        throw DIMException("Data is not byte type!", __LINE__);
    }
    m_value.byteValue = value;
}

void DIMData::setValue(int16_t value)
{
    if (m_type != TypeShort)
    {
        throw DIMException("Data is not short type!", __LINE__);
    }
    m_value.shortValue = value;
}

void DIMData::setValue(float value)
{
    if (m_type != TypeFloat)
    {
        throw DIMException("Data is not float type!", __LINE__);
    }
    m_value.floatValue = value;
}

void DIMData::setValue(bool value)
{
    if (m_type != TypeBool)
    {
        throw DIMException("Data is not boolean type!", __LINE__);
    }
    m_value.intValue = value ? 1 : 0;
}

void DIMData::setValue(uint8_t value)
{
    if (m_type != TypeUByte)
    {
        throw DIMException("Data is not uint8_t type!", __LINE__);
    }
    m_value.ubyteValue = value;
}

void DIMData::setValue(uint16_t value)
{
    if (m_type != TypeUShort)
    {
        throw DIMException("Data is not uint16_t type!", __LINE__);
    }
    m_value.ushortValue = value;
}

void DIMData::setValue(uint32_t value)
{
    if (m_type != TypeUInt)
    {
        throw DIMException("Data is not uint32_t type!", __LINE__);
    }
    m_value.uintValue = value;
}

void DIMData::setValue(uint64_t value)
{
    if (m_type != TypeULong)
    {
        throw DIMException("Data is not uint64_t type!", __LINE__);
    }
    m_value.ulongValue = value;
}

void DIMData::setValue(const DIMEnum &value)
{
    if (m_type != TypeEnum)
    {
        throw DIMException("Data is not ENUM type!", __LINE__);
    }
    *static_cast<DIMEnum *>(m_value.ptrValue) = value;
}

void DIMData::setValue(const DIMData::List &value)
{
    if (m_type != TypeNested)
        throw DIMException("Data is not nested type!", __LINE__);
    *static_cast<DIMData::List *>(m_value.ptrValue) = value;
}

void DIMData::setValue(DIMData::List &&value)
{
    if (m_type != TypeNested)
        throw DIMException("Data is not nested type!", __LINE__);
    *static_cast<DIMData::List *>(m_value.ptrValue) = value;
}

void DIMData::setValue(const DIMData &other)
{
    if (&other != this)
    {
        resetValue();
        m_type = other.m_type;

        if (m_type == TypeString)
            m_value.ptrValue = new std::string(other.getStringValue());
        else if (m_type == TypeEnum)
            m_value.ptrValue = new DIMEnum(other.getEnumValue());
        else if (m_type == TypeNested)
            m_value.ptrValue = new DIMData::List(other.getNestedValue());
        else
            m_value = other.m_value;
    }
}

void DIMData::setValue(DIMData &&other)
{
    if (&other != this)
    {
        resetValue();
        m_type = other.m_type;
        other.m_type = TypeInvalid;

        m_value = other.m_value;
        other.m_value.ptrValue = nullptr;
    }
}

void DIMData::updateValue(const DIMData &other, bool override)
{
    if (!isSameDataType(other))
    {
        if (!override)
        {
            throw DIMException(
                "Can't update value, type differs: " + std::to_string(m_type) +
                    " != " + std::to_string(other.getDataType()),
                __LINE__);
        }
        setValue(other);
    }
    else if (m_type == TypeNested)
    {
        const DIMData::List &updates = other.getNestedValue();
        DIMData::List &values = getNestedValue();

        for (const DIMData &update : updates)
        {
            DIMData::List::iterator it = std::find_if(
                values.begin(), values.end(), [&update](const DIMData &v) {
                    return v.getIndex() == update.getIndex();
                });
            if (it == values.end())
            {
                throw DIMException("No nested data for index " +
                                       std::to_string(update.getIndex()),
                                   __LINE__);
            }
            else if (!override && !it->isSameDataType(update))
            {
                throw DIMException("Can't override nested data for index " +
                                       std::to_string(update.getIndex()),
                                   __LINE__);
            }
            else
                it->updateValue(update);
        }
    }
    else if (m_type == TypeEnum)
        getEnumValue().setValue(other.getEnumValue().getValue());
    else
        setValue(other);
}

void DIMData::updateValue(DIMData &&other, bool override)
{
    if (!isSameDataType(other))
    {
        if (!override)
        {
            throw DIMException(
                "Can't update value, type differs: " + std::to_string(m_type) +
                    " != " + std::to_string(other.getDataType()),
                __LINE__);
        }
        setValue(std::move(other));
    }
    else if (m_type == TypeNested)
    {
        const DIMData::List &updates = other.getNestedValue();
        DIMData::List &values = getNestedValue();

        for (const DIMData &update : updates)
        {
            DIMData::List::iterator it = std::find_if(
                values.begin(), values.end(), [&update](const DIMData &v) {
                    return v.getIndex() == update.getIndex();
                });
            if (it == values.end())
            {
                throw DIMException("No nested data for index " +
                                       std::to_string(update.getIndex()),
                                   __LINE__);
            }
            else if (!override && !it->isSameDataType(update))
            {
                throw DIMException("Can't override nested data for index " +
                                       std::to_string(update.getIndex()),
                                   __LINE__);
            }
            else
                it->updateValue(std::move(update), override);
        }
    }
    else if (m_type == TypeEnum)
        getEnumValue().setValue(other.getEnumValue().getValue());
    else
        setValue(std::move(other));
}

void DIMData::resetValue()
{
    switch (m_type)
    {
    case TypeEnum:
        delete static_cast<DIMEnum *>(m_value.ptrValue);
        m_value.ptrValue = nullptr;
        break;
    case TypeString:
        delete static_cast<std::string *>(m_value.ptrValue);
        m_value.ptrValue = nullptr;
        break;
    case TypeNested:
        delete static_cast<DIMData::List *>(m_value.ptrValue);
        m_value.ptrValue = nullptr;
        break;
    default: m_value.ulongValue = 0; break;
    }
}

/**
 * Copies data information from other data
 * This method will only copy the name and unit of the data
 * It doesn't change the value
 * @param other
 */
void DIMData::copyParameterInfo(const DIMData &other)
{
    // TODO: We may check data type and index here
    m_name = other.m_name;
    m_unit = other.m_unit;
    if (other.m_type == TypeEnum)
    {
        getEnumValue().setValuesMap(other.getEnumValue().getValuesMap());
    }
}

/**
 * Insert this into acquisition XML document
 * @param doc XML document to be filled
 */
void DIMData::insertInto(pugi::xml_node &doc, bool isCmd) const
{
    if (isHidden())
        return; // Do not display hidden data

    pugi::xml_node node = doc.append_child("data");

    node.append_attribute("index").set_value(m_index);

    if (!isCmd && !m_name.empty())
        node.append_attribute("name").set_value(m_name.c_str());

    if (!isCmd && !m_unit.empty())
        node.append_attribute("unit").set_value(m_unit.c_str());

    if (m_type != TypeNested)
        node.append_attribute("type").set_value(m_type);

    switch (m_type)
    {
    case TypeInt:
        node.append_attribute("value").set_value(m_value.intValue);
        break;
    case TypeDouble:
        node.append_attribute("value").set_value(m_value.doubleValue);
        break;
    case TypeLong:
        node.append_attribute("value").set_value(
            static_cast<long long int>(m_value.longValue));
        break;
    case TypeString: {
        const std::string &value = *static_cast<const std::string *>(
            m_value.ptrValue);
        node.append_attribute("value").set_value(value.c_str());
        break;
    }
    case TypeByte:
        node.append_attribute("value").set_value(
            static_cast<int32_t>(m_value.byteValue));
        break;
    case TypeShort:
        node.append_attribute("value").set_value(
            static_cast<int32_t>(m_value.shortValue));
        break;
    case TypeFloat:
        node.append_attribute("value").set_value(
            static_cast<double>(m_value.floatValue));
        break;
    case TypeBool:
        node.append_attribute("value").set_value(m_value.intValue);
        break;
    case TypeUInt:
        node.append_attribute("value").set_value(m_value.uintValue);
        break;
    case TypeULong:
        node.append_attribute("value").set_value(
            static_cast<unsigned long long>(m_value.ulongValue));
        break;
    case TypeUShort:
        node.append_attribute("value").set_value(
            static_cast<uint32_t>(m_value.ushortValue));
        break;
    case TypeUByte:
        node.append_attribute("value").set_value(
            static_cast<uint32_t>(m_value.ubyteValue));
        break;
    case TypeEnum: {
        const DIMEnum &value = *static_cast<DIMEnum *>(m_value.ptrValue);
        node.append_attribute("value").set_value(value.getValue());
        if (isCmd)
            break;

        node.append_attribute("valueName").set_value(value.getName().c_str());
        pugi::xml_node enumValues = node.append_child("enum");
        for (std::map<int32_t, std::string>::const_iterator it = value.begin();
             it != value.end(); ++it)
        {
            pugi::xml_node val = enumValues.append_child("value");
            val.append_attribute("value").set_value(it->first);
            val.append_attribute("name").set_value(it->second.c_str());
        }
        break;
    }
    case TypeNested: {
        const DIMData::List &childs = *static_cast<const DIMData::List *>(
            m_value.ptrValue);
        for (const DIMData &child : childs)
        {
            child.insertInto(node, isCmd);
        }
        break;
    }
    default: throw DIMException("Invalid data type!", __LINE__);
    }
}

void DIMData::removeNestedData(Index index)
{
    // we use erase-remove idiom:
    // https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom
    DIMData::List &nestedDataList = getNestedValue();
    nestedDataList.erase(
        std::remove_if(nestedDataList.begin(), nestedDataList.end(),
                       [&](DIMData &x) { return x.getIndex() == index; }),
        nestedDataList.end());
}

DIMData::Index DIMData::getNextNestedIndex() const
{
    const DIMData::List &nestedDataList = getNestedValue();
    if (nestedDataList.empty())
    {
        return 0;
    }
    else
    {
        DIMData::List::const_iterator it = nestedDataList.end();
        --it;
        return it->getIndex() + 1;
    }
}

} // namespace dim
} // namespace ntof
