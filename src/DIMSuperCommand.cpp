/**
 * \file DIMSuperCommand.cpp
 * \brief Dim command with an acknowledge system
 * \author A. GIRAUD
 * \version 1.0
 * \date Sept 18, 2014
 *
 * This class is a dim server used to deal with a Dim super client
 *
 */

#include "DIMSuperCommand.h"

#include <iostream>

#include "DIMException.h"

using std::cout;
using std::endl;

namespace ntof {
namespace dim {

/**
 * \fn DIMSuperCommand(std::string name)
 * \brief Constructor of the class
 *
 * \param std::string name of the DIM command
 * \return nothing.
 */
DIMSuperCommand::DIMSuperCommand(std::string name) :
    m_ack(new DIMAck(0, DIMAck::Invalid, 0, "Not refreshed")),
    m_ackService(new DIMXMLService(name + "/Ack")),
    m_cmdService(new DIMXMLCommand(name + "/Cmd", this))
{}

/**
 * \fn ~DIMSuperCommand()
 * \brief Destructor of the class
 *
 * \return nothing.
 */
DIMSuperCommand::~DIMSuperCommand()
{
    m_cmdService.reset();
    m_ackService.reset();
    m_ack.reset();
}

/**
 * \fn void DIMSuperCommand::setStatus(int32_t key, int32_t status, int32_t
 * errorCode, const std::string errorMessage) \brief Set response to this command
 *
 * \param int32_t key it's the key contains in the command received
 * \param int32_t status it's the status of the command (OK, REJECTED)
 * \param int32_t errorCode the value of the errorCode
 * \param std::string errorMessage a message to describe the status
 * \return nothing.
 */
void DIMSuperCommand::setStatus(int32_t key,
                                DIMAck::Status status,
                                int32_t errorCode,
                                const std::string &errorMessage)
{
    m_ack->setKey(key);
    m_ack->setStatus(status);
    m_ack->setErrorCode(errorCode);
    m_ack->setMessage(errorMessage);
    m_ackService->setData(m_ack->getXMLDocument());
}

/**
 * \fn void DIMSuperCommand::setError(int32_t key, int32_t errorCode, const
 * std::string errorMessage) \brief Set response to this command associated with
 * error
 *
 * \param int32_t key it's the key contains in the command received
 * \param int32_t errorCode the value of the errorCode
 * \param std::string errorMessage a message to describe the status
 * \return nothing.
 */
void DIMSuperCommand::setError(int32_t key,
                               int32_t errorCode,
                               const std::string &errorMessage)
{
    setStatus(key, DIMAck::Rejected, errorCode, errorMessage);
}

/**
 * \fn void DIMSuperCommand::setError(int32_t key, int32_t errorCode, const
 * std::string errorMessage) \brief Set response to this command associated with
 * ok acknowledge
 *
 * \param int32_t key it's the key contains in the command received
 * \param std::string message a message to describe the status
 * \return nothing.
 */
void DIMSuperCommand::setOk(int32_t key, const std::string &message)
{
    setStatus(key, DIMAck::OK, 0, message);
}

/**
 * Called by DIMXMLCommand in case the command is corrupted
 * @param errMsg
 * @param cmd
 */
void DIMSuperCommand::errorReceived(std::string errMsg,
                                    const DIMXMLCommand * /*cmd*/)
{
    m_ack->setErrorCode(-1);
    m_ack->setMessage(errMsg);
    m_ack->setStatus(DIMAck::Rejected);
    m_ackService->setData(m_ack->getXMLDocument());
}

/**
 * Called by DIMXMLCommand when a new command is received
 * @param doc
 * @param cmd
 */
void DIMSuperCommand::dataReceived(pugi::xml_document &doc,
                                   const DIMXMLCommand * /*cmd*/)
{
    try
    {
        DIMCmd theCmd(doc);
        try
        {
            commandReceived(theCmd);
        }
        catch (const DIMException &e)
        {
            setError(theCmd.getKey(), -1, e.getMessage());
        }
    }
    catch (const DIMException &ex)
    {
        setError(0, -1, ex.getMessage());
    }
}

} // namespace dim
} // namespace ntof
