/*
 * DIMXMLInfo.cpp
 *
 *  Created on: Nov 7, 2014
 *      Author: mdonze
 */

#include "DIMXMLInfo.h"

namespace ntof {
namespace dim {

const std::string DIMXMLInfo::NoLinkError = "No link";
const std::string &DIMXMLInfoHandler::NoLinkError = DIMXMLInfo::NoLinkError;

DIMXMLInfo::DIMXMLInfo() : DimInfo(), m_handler(nullptr) {}

DIMXMLInfo::DIMXMLInfo(const std::string &serviceName,
                       DIMXMLInfoHandler *handler) :
    DimInfo(), m_handler(handler)
{
    subscribe(serviceName, 0);
}

DIMXMLInfo::DIMXMLInfo(const std::string &serviceName,
                       int time,
                       DIMXMLInfoHandler *handler) :
    DimInfo(), m_handler(handler)
{
    subscribe(serviceName, time);
}

void DIMXMLInfo::infoHandler()
{
    if (getSize() <= 0)
    {
        if (m_handler)
            m_handler->noLink(this);

        if (!errorSignal.empty())
            errorSignal(NoLinkError, *this);
    }
    else
    {
        // Got some data
        pugi::xml_document doc;
        pugi::xml_parse_result parseResult = doc.load_buffer_inplace(getData(),
                                                                     getSize());
        if (parseResult ||
            parseResult.status == pugi::status_no_document_element)
        {
            // XML parsing without errors
            if (m_handler)
                m_handler->dataReceived(doc, this);

            if (!dataSignal.empty())
                dataSignal(doc, *this);
        }
        else
        {
            // XML parsing with errors
            if (m_handler)
                m_handler->errorReceived(parseResult.description(), this);

            if (!errorSignal.empty())
                errorSignal(parseResult.description(), *this);
        }
    }
}

DIMXMLInfo::~DIMXMLInfo()
{
    /* unsubscribe before deleting properties */
    unsubscribe();
}

void DIMXMLInfo::unsubscribe()
{
    if (itsId)
    {
        dic_release_service(itsId);
        itsId = 0;
    }
}

void DIMXMLInfo::subscribe(const std::string &name, int time)
{
    unsubscribe();
    if (itsName)
    {
        /* workarround dim memory leak */
        delete itsName;
        itsName = 0;
    }
    m_serviceName = name;
    DimInfo::subscribe(const_cast<char *>(m_serviceName.c_str()), time,
                       ((void *) NULL), 0, 0);
}

static void delayed_deleter(void *data)
{
    DIMXMLInfo *info = static_cast<DIMXMLInfo *>(data);
    DISABLE_AST;
    delete info;
    ENABLE_AST;
}

void DIMXMLInfo::deleteLater()
{
    dtq_start_timer(0, delayed_deleter, this);
}

} /* namespace dim */
} /* namespace ntof */
