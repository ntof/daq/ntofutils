/*
 * DIMXMLCommand.cpp
 *
 *  Created on: Nov 7, 2014
 *      Author: mdonze
 */

#include <iostream>
#include <sstream>

#include <DIMXMLCommand.h>

namespace ntof {
namespace dim {

/**
 * Build a new DIM XML command server
 * @param cmdName Name of the command
 * @param hdl Handler to be called when a new command is received
 */
DIMXMLCommand::DIMXMLCommand(std::string cmdName, DIMXMLCommandHandler *hdl) :
    m_dimCmd(new DimCommand(cmdName.c_str(), "C", this)), handler_(hdl)
{}

/**
 * Default destructor
 */
DIMXMLCommand::~DIMXMLCommand()
{
    m_dimCmd.reset();
}

/**
 * Comparison operator
 * @param other
 * @return
 */
bool DIMXMLCommand::operator==(DIMXMLCommand const &other) const
{
    return &m_dimCmd == &other.m_dimCmd;
}

/**
 * Called by DIM when a new command is received
 */
void DIMXMLCommand::commandHandler()
{
    std::string data(m_dimCmd->getString(), m_dimCmd->getSize());
    pugi::xml_document doc;
    pugi::xml_parse_result parseResult = doc.load(data.c_str());

    if (parseResult)
    {
        // std::cout << "No parsing errors" << std::endl;
        // XML parsing without errors
        handler_->dataReceived(doc, this);
    }
    else
    {
        // XML parsing with errors
        handler_->errorReceived(parseResult.description(), this);
    }
}

/**
 * Send a XML command in non-blocking mode
 * @param cmdName Name of the command
 * @param doc XML document to be sent
 */
void DIMXMLCommand::sendCommandNB(std::string cmdName, pugi::xml_document &doc)
{
    std::ostringstream oss;
    doc.save(oss);
    DimClient::sendCommandNB(cmdName.c_str(),
                             const_cast<char *>(oss.str().c_str()));
}

/**
 * Send a XML command in blocking mode
 * @param cmdName Name of the command
 * @param doc XML document to be sent
 */
void DIMXMLCommand::sendCommand(std::string cmdName, pugi::xml_document &doc)
{
    std::ostringstream oss;
    doc.save(oss);
    DimClient::sendCommand(cmdName.c_str(),
                           const_cast<char *>(oss.str().c_str()));
}
} /* namespace dim */
} /* namespace ntof */
