/*
 * DIMParamListClient.cpp
 *
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#include "DIMParamListClient.h"

#include <pugixml.hpp>

using namespace ntof::dim;

DIMParamListClient::DIMParamListClient(const std::string &srvName) :
    m_info(new DIMXMLInfo(srvName + "/Aqn", this)),
    m_handler(0),
    m_svcName(srvName),
    m_cmdClient(new DIMSuperClient(srvName))
{}

DIMParamListClient::~DIMParamListClient()
{
    /* it will call unsubscribe before other members gets deleted */
    m_info.reset();
    m_cmdClient.reset();
}

const std::string &DIMParamListClient::getServiceName() const
{
    return m_svcName;
}

DIMAck DIMParamListClient::sendParameter(const DIMData &newParam)
{
    pugi::xml_document doc;
    pugi::xml_node params = doc.append_child("parameters");
    newParam.insertIntoCmd(params);
    return m_cmdClient->sendCommand(doc);
}

DIMAck DIMParamListClient::sendParameters(const std::vector<DIMData> &newParam)
{
    if (!newParam.empty())
    {
        pugi::xml_document doc;
        pugi::xml_node params = doc.append_child("parameters");
        for (std::vector<DIMData>::const_iterator it = newParam.begin();
             it != newParam.end(); ++it)
        {
            it->insertIntoCmd(params);
        }
        return m_cmdClient->sendCommand(doc);
    }
    return DIMAck();
}

std::vector<DIMData> DIMParamListClient::getParameters()
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_parameters;
}

void DIMParamListClient::setHandler(DIMParamListClientHandler *hdl)
{
    m_handler = hdl;
}

void DIMParamListClient::setTimeOut(const std::chrono::milliseconds &ms)
{
    m_cmdClient->setTimeOut(ms);
}

const std::chrono::milliseconds &DIMParamListClient::getTimeOut() const
{
    return m_cmdClient->getTimeOut();
}

void DIMParamListClient::errorReceived(std::string /*errMsg*/,
                                       const DIMXMLInfo * /*info*/)
{
    // Simply ignore bad updates
}

void DIMParamListClient::dataReceived(pugi::xml_document &doc,
                                      const DIMXMLInfo * /*info*/)
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_parameters.clear();
    // Test if there is DIM link
    pugi::xml_node params = doc.child("parameters");
    for (pugi::xml_node param = params.first_child(); param;
         param = param.next_sibling())
    {
        m_parameters.push_back(DIMData(param));
    }
    if (m_handler)
    {
        m_handler->parameterChanged(m_parameters, *this);
    }
    lock.unlock();
    dataSignal(*this);
}

void DIMParamListClient::noLink(const DIMXMLInfo * /*info*/)
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_parameters.clear();

    if (m_handler)
    {
        m_handler->parameterChanged(m_parameters, *this);
    }
    lock.unlock();
    errorSignal(NoLinkError, *this);
}

static void delayed_deleter(void *data)
{
    DIMParamListClient *info = static_cast<DIMParamListClient *>(data);
    DISABLE_AST;
    delete info;
    ENABLE_AST;
}

void DIMParamListClient::deleteLater()
{
    dtq_start_timer(0, delayed_deleter, this);
}
