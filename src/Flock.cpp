/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-01T14:31:16+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "Flock.hpp"

#include <vector>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <unistd.h>

#include "NTOFException.h"
#include "NTOFLogging.hpp"

using namespace ntof::utils;

const std::string Flock::DefaultLock = "/run/lock/puppetlock";

#define UNLOCKED 0
#define LOCKED 1
#define LOCKING 2

Flock::Flock(Flock::Mode mode, const std::string &file) :
    m_mode(mode), m_fileName(file), m_locked(false)
{
    m_fd = ::open(m_fileName.c_str(), O_RDONLY | O_CREAT, 0644);
    if (m_fd < 0)
    {
        throwErrno();
    }
}

Flock::~Flock()
{
    unlock();
    close(m_fd);
}

void Flock::throwErrno()
{
    std::vector<char> buf(256);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    ::strerror_r(errno, buf.data(), 256);
    const char *error = buf.data();
#else
    const char *error = ::strerror_r(errno, buf.data(), 256);
#endif

    throw NTOFException(error, __FILE__, __LINE__, errno);
}

void Flock::lock()
{
    int expected = UNLOCKED;
    if (m_locked.compare_exchange_strong(expected, LOCKED))
    {
        if (flock(m_fd, (m_mode == Exclusive) ? LOCK_EX : LOCK_SH) != 0)
        {
            m_locked.store(UNLOCKED);
            throwErrno();
        }
    }
}

bool Flock::try_lock()
{
    int expected = UNLOCKED;
    if (m_locked.compare_exchange_strong(expected, LOCKING))
    {
        if (flock(m_fd,
                  ((m_mode == Exclusive) ? LOCK_EX : LOCK_SH) | LOCK_NB) != 0)
        {
            if (errno == EWOULDBLOCK)
            {
                m_locked.store(UNLOCKED);
                return false;
            }
            else
            {
                m_locked.store(UNLOCKED);
                throwErrno();
            }
        }
        m_locked.store(LOCKED);
    }
    return m_locked.load() == LOCKED;
}

void Flock::unlock()
{
    if (m_locked.exchange(UNLOCKED) == LOCKED)
    {
        flock(m_fd, LOCK_UN);
    }
}
