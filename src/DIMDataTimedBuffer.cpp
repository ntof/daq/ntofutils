/*
 * Timestamp.h
 *
 *  Created on: Jun 15, 2020
 *      Author: matteof
 */

#include "DIMDataTimedBuffer.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>

#include "NTOFLogging.hpp"

#define DEFAULT_MAX_EVENTS 50
#define DEFAULT_MARGIN 100000

namespace ntof {
namespace utils {

DIMDataTimedBuffer::DIMDataTimedBuffer() :
    m_margin(DEFAULT_MARGIN), m_maxEvents(DEFAULT_MAX_EVENTS)
{}

void ntof::utils::DIMDataTimedBuffer::addEvent(
    const ntof::dim::DIMData::List &event)
{
    Timestamp ts = retrieveTimestamp(event);
    if (ts <= 0)
    {
        LOG(ERROR) << "[DIMDataTimedBuffer]: invalid event received!";
        return;
    }

    {
        std::lock_guard<std::mutex> lock(m_mutex);
        // Do cleanup
        for (uint32_t i = m_maxEvents; i < m_events.size(); ++i)
        {
            m_events.erase(m_events.begin());
        }
        m_events[ts] = event;
        VLOG(1) << "[DIMDataTimedBuffer]: event received timeStamp:" << ts;
    }
    m_cond.notify_all();
}

Timestamp DIMDataTimedBuffer::retrieveTimestamp(const dim::DIMData::List &event)
{
    try
    {
        const dim::DIMData &data = event.at(1);
        return Timestamp(data.getLongValue());
    }
    catch (...)
    {
        return Timestamp(-1);
    }
}

void DIMDataTimedBuffer::setTimeMargin(int64_t margin)
{
    m_margin = margin;
}

int64_t DIMDataTimedBuffer::getTimeMargin() const
{
    return m_margin;
}

/**
 * Sets the maximum event numbers to keep
 * @param size
 */
void DIMDataTimedBuffer::setMaxEventsToKeep(int size)
{
    m_maxEvents = size;
}

void DIMDataTimedBuffer::getCurrentEvent(dim::DIMData::List &event) const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    event = m_currentEvent;
}

/**
 * Waits for a new event
 */
void DIMDataTimedBuffer::waitForNewEvent(dim::DIMData::List &event)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_cond.wait(lock);
    if (m_events.empty())
        event = dim::DIMData::List();
    else
        event = m_events.rbegin()->second; /* last event */
}

bool DIMDataTimedBuffer::getEventByTime(dim::DIMData::List &event,
                                        const ntof::utils::Timestamp &time,
                                        int32_t timeout)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    // Check if the oldest timestamp in our collection is not recent than the
    // one we want to test
    if (!m_events.empty() && (m_events.begin()->first > (time + m_margin)))
    {
        event = dim::DIMData::List();
        LOG(ERROR)
            << "[DIMDataTimedBuffer]: no possible match for " << time.getValue()
            << " oldest event: " << m_events.begin()->second << " ("
            << (m_events.begin()->first - time.getValue()) << "us older)";
        return false;
    }

    EventsMap::iterator it;
    m_cond.wait_for(lock, std::chrono::milliseconds(timeout),
                    [this, &time, &it]() {
                        it = std::find_if(m_events.begin(), m_events.end(),
                                          checkTS(time, m_margin));
                        return (it != m_events.end());
                    });

    if (it != m_events.end())
    {
        m_currentEvent = event = it->second;
        // Discard all past events
        m_events.erase(m_events.begin(), ++it);
        VLOG(1) << "[DIMDataTimedBuffer]: event for " << time.getValue()
                << " event:" << m_currentEvent;
        return true;
    }
    LOG(ERROR) << "[EventReader]: no event found for " << time.getValue()
               << " buffered:" << m_events.size();
    return false;
}

DIMDataTimedBuffer::checkTS::checkTS(const Timestamp &ts, int64_t margin) :
    chkVal(ts), m_margin(margin)
{}

bool DIMDataTimedBuffer::checkTS::operator()(
    const std::pair<Timestamp, dim::DIMData::List> &v) const
{
    int64_t diff = chkVal.getValue() - v.first.getValue();
    if (llabs(diff) > m_margin)
    {
        // Not within margin
        return false;
    }
    // Equal
    return true;
}

} /* namespace utils */
} /* namespace ntof */
