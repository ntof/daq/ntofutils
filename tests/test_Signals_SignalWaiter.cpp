/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-22T11:23:29+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Signals.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;

class TestSignals_SignalWaiter : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSignals_SignalWaiter);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(check);
    CPPUNIT_TEST(timeout);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        ntof::utils::signal<void(int)> sig;

        SignalWaiter waiter(sig);
        std::future<void> f = std::async(std::launch::async,
                                         [&sig]() { sig(12); });

        EQ(true, waiter.wait());
        EQ(false, waiter.wait(2, 100));
        sig(2);

        EQ(true, waiter.wait(2, 0));
        EQ(std::size_t(2), waiter.count());
    }

    void check()
    {
        int value = 0;
        ntof::utils::signal<void(int)> sig;

        SignalWaiter waiter;

        waiter.listen(sig);
        std::future<void> f = std::async(std::launch::async, [&value, &sig]() {
            ++value;
            sig(12);
        });

        EQ(true, waiter.wait([&value]() { return value == 1; }));
        EQ(false, waiter.wait([&value]() { return value > 1; }, 250));
        std::future<void> f2 = std::async(std::launch::async, [&value, &sig]() {
            ++value;
            sig(12);
        });
        EQ(true, waiter.wait([&value]() { return value > 1; }, 250));
        EQ(std::size_t(2), waiter.count());
    }

    void timeout()
    {
        /* timer was restarted on every signal */
        ntof::utils::signal<void()> sig;
        std::atomic<bool> ret(false);

        SignalWaiter waiter(sig);
        std::future<void> f = std::async(std::launch::async, [&sig, &ret]() {
            for (std::size_t i = 0; i < 5; ++i)
            {
                /* loop is shorter than timeout to wake-up the SignalWaiter */
                sig();
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
            }
            /* total loop is longer than SignalWaiter, thus it must timeout */
            ret.store(true);
            sig();
        });

        EQ(false,
           waiter.wait([&ret]() -> bool { return ret; },
                       std::chrono::milliseconds(200)));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSignals_SignalWaiter);
