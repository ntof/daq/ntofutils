/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-24T09:54:32+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMDataSet.h"
#include "DIMDataSetClient.h"
#include "NTOFLogging.hpp"
#include "Signals.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMDataSet : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMDataSet);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(getters);
    CPPUNIT_TEST(nested);
    CPPUNIT_TEST(lockRef);
    CPPUNIT_TEST(hidden);
    CPPUNIT_TEST(addData_lvalue);
    CPPUNIT_TEST(subscribe);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        DIMDataSetClient cli;
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);
        DIMDataSet dataset("test");
        cli.subscribe("test");

        EQ(true, waiter.wait(1));
        uint32_t idx = 0;
        dataset.addData(idx++, "int8_t", "", int8_t(42), AddMode::CREATE, false);
        dataset.addData(idx++, "int16_t", "", int16_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "int32_t", "", int32_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "int64_t", "", int64_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "uint8_t", "", uint8_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "uint16_t", "", uint16_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "uint32_t", "", uint32_t(42), AddMode::CREATE,
                        false);
        dataset.addData(idx++, "uint64_t", "", uint64_t(42), AddMode::CREATE,
                        false);

        dataset.addData(idx++, "string", "", std::string("value"),
                        AddMode::CREATE, false);
        dataset.addData(idx++, "bool", "", false, AddMode::CREATE, false);

        DIMEnum e;
        e.addItem(0, "test");
        e.addItem(1, "another");
        dataset.addData(idx++, "enum", "", e, AddMode::CREATE, false);

        dataset.addData("autoindex", "", uint32_t(4242), AddMode::CREATE, false);

        EQ(int(idx + 1), dataset.getDataCount());
        EQ(uint32_t(4242), dataset.getDataAt(idx).getUIntValue());
        EQ(uint32_t(4242), dataset.getValue<uint32_t>(idx));
        EQ(uint32_t(4242), dataset.getUIntValue(idx));

        dataset.addData("autoindex", "", uint32_t(4243), AddMode::OVERRIDE,
                        false);
        EQ(int(idx + 1), dataset.getDataCount()); // nothing added
        EQ(uint32_t(4243), dataset.getUIntValue(idx));

        dataset.addData("autoindex", "", uint32_t(4244), AddMode::UPDATE, false);
        EQ(int(idx + 1), dataset.getDataCount()); // nothing added
        EQ(uint32_t(4244), dataset.getUIntValue(idx));

        dataset.updateData();

        EQ(true, waiter.wait(2));
        std::vector<DIMData> data = cli.getLatestData();
        EQ(std::size_t(idx + 1), data.size());

        EQ(std::string("autoindex"), data[idx].getName());
        EQ(uint32_t(idx), data[idx].getIndex());
        EQ(uint32_t(4244), data[idx].getUIntValue());

        dataset.removeData(0);
        EQ(true, waiter.wait(3));
        CPPUNIT_ASSERT_THROW(dataset.getDataAt(0), DIMException);
        dataset.removeData("int16_t");
        EQ(true, waiter.wait(4));
        CPPUNIT_ASSERT_THROW(dataset.getDataAt(1), DIMException);
        CPPUNIT_ASSERT_THROW(dataset.getDataAt(888), DIMException);
        CPPUNIT_ASSERT_THROW(dataset.removeData(888);, DIMException);
        dataset.clearData();
        EQ(true, waiter.wait(5));
        EQ(int(0), dataset.getDataCount());
    }

    void getters()
    {
        DIMDataSet dataset("test");
        uint32_t idx = 0;
        dataset.addData(idx, "test", "", std::string("test"), AddMode::CREATE,
                        false);

        idx++;
        dataset.addData(idx, "int8_t", "", int8_t(42), AddMode::CREATE, false);
        EQ(int8_t(42), dataset.getDataAt(idx).getByteValue());
        EQ(int8_t(42), dataset.getValue<int8_t>(idx));
        EQ(int8_t(42), dataset.getByteValue(idx));
        EQ(int8_t(42), dataset.getByteValue("int8_t"));
        EQ(true, dataset.is<int8_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getByteValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "int16_t", "", int16_t(42), AddMode::CREATE, false);
        EQ(int16_t(42), dataset.getDataAt(idx).getShortValue());
        EQ(int16_t(42), dataset.getValue<int16_t>(idx));
        EQ(int16_t(42), dataset.getShortValue(idx));
        EQ(int16_t(42), dataset.getShortValue("int16_t"));
        EQ(true, dataset.is<int16_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getShortValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "int32_t", "", int32_t(42), AddMode::CREATE, false);
        EQ(int32_t(42), dataset.getDataAt(idx).getIntValue());
        EQ(int32_t(42), dataset.getValue<int32_t>(idx));
        EQ(int32_t(42), dataset.getIntValue(idx));
        EQ(int32_t(42), dataset.getIntValue("int32_t"));
        EQ(true, dataset.is<int32_t>(idx));
        EQ(false, dataset.is<int8_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getIntValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "int64_t", "", int64_t(42), AddMode::CREATE, false);
        EQ(int64_t(42), dataset.getDataAt(idx).getLongValue());
        EQ(int64_t(42), dataset.getValue<int64_t>(idx));
        EQ(int64_t(42), dataset.getLongValue(idx));
        EQ(int64_t(42), dataset.getLongValue("int64_t"));
        EQ(true, dataset.is<int64_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getLongValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "uint8_t", "", uint8_t(42), AddMode::CREATE, false);
        EQ(uint8_t(42), dataset.getDataAt(idx).getUByteValue());
        EQ(uint8_t(42), dataset.getValue<uint8_t>(idx));
        EQ(uint8_t(42), dataset.getUByteValue(idx));
        EQ(uint8_t(42), dataset.getUByteValue("uint8_t"));
        EQ(true, dataset.is<uint8_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getUByteValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "uint16_t", "", uint16_t(42), AddMode::CREATE,
                        false);
        EQ(uint16_t(42), dataset.getDataAt(idx).getUShortValue());
        EQ(uint16_t(42), dataset.getValue<uint16_t>(idx));
        EQ(uint16_t(42), dataset.getUShortValue(idx));
        EQ(uint16_t(42), dataset.getUShortValue("uint16_t"));
        EQ(true, dataset.is<uint16_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getUShortValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "uint32_t", "", uint32_t(42), AddMode::CREATE,
                        false);
        EQ(uint32_t(42), dataset.getDataAt(idx).getUIntValue());
        EQ(uint32_t(42), dataset.getValue<uint32_t>(idx));
        EQ(uint32_t(42), dataset.getUIntValue(idx));
        EQ(uint32_t(42), dataset.getUIntValue("uint32_t"));
        EQ(true, dataset.is<uint32_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getUIntValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "uint64_t", "", uint64_t(42), AddMode::CREATE,
                        false);
        EQ(uint64_t(42), dataset.getDataAt(idx).getULongValue());
        EQ(uint64_t(42), dataset.getValue<uint64_t>(idx));
        EQ(uint64_t(42), dataset.getULongValue(idx));
        EQ(uint64_t(42), dataset.getULongValue("uint64_t"));
        EQ(true, dataset.is<uint64_t>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getULongValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "double", "", double(42.42), AddMode::CREATE,
                        false);
        EQ(double(42.42), dataset.getDataAt(idx).getDoubleValue());
        EQ(double(42.42), dataset.getValue<double>(idx));
        EQ(double(42.42), dataset.getDoubleValue(idx));
        EQ(double(42.42), dataset.getDoubleValue("double"));
        EQ(true, dataset.is<double>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getDoubleValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "float", "", float(42.42), AddMode::CREATE, false);
        EQ(float(42.42), dataset.getDataAt(idx).getFloatValue());
        EQ(float(42.42), dataset.getValue<float>(idx));
        EQ(float(42.42), dataset.getFloatValue(idx));
        EQ(float(42.42), dataset.getFloatValue("float"));
        EQ(true, dataset.is<float>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getFloatValue("test"), DIMException);

        idx++;
        dataset.addData(idx, "string", "", std::string("42"), AddMode::CREATE,
                        false);
        EQ(std::string("42"), dataset.getDataAt(idx).getStringValue());
        EQ(std::string("42"), dataset.getValue<const std::string &>(idx));
        EQ(std::string("42"), dataset.getStringValue(idx));
        EQ(std::string("42"), dataset.getStringValue("string"));
        EQ(true, dataset.is<const std::string &>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getStringValue("float"), DIMException);

        idx++;
        dataset.addData(idx, "bool", "", bool(true), AddMode::CREATE, false);
        EQ(bool(true), dataset.getDataAt(idx).getBoolValue());
        EQ(bool(true), dataset.getValue<bool>(idx));
        EQ(bool(true), dataset.getBoolValue(idx));
        EQ(bool(true), dataset.getBoolValue("bool"));
        EQ(true, dataset.is<bool>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getBoolValue("test"), DIMException);

        idx++;
        DIMEnum e;
        e.addItem(0, "test");
        e.addItem(1, "another");
        e.setValue(1);
        dataset.addData(idx, "enum", "", e, AddMode::CREATE, false);
        ASSERT(e == dataset.getDataAt(idx).getEnumValue());
        ASSERT(e == dataset.getValue<DIMEnum &>(idx));
        ASSERT(e == dataset.getEnumValue(idx));
        ASSERT(e == dataset.getEnumValue("enum"));
        EQ(true, dataset.is<DIMEnum &>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getEnumValue("test"), DIMException);

        idx++;
        DIMData::List childs;
        childs.push_back(DIMData(0, uint16_t(12)));
        childs.push_back(DIMData(1, childs)); // nested in nested, must work
        dataset.addData(idx, "nested", "", std::move(childs), AddMode::CREATE,
                        false);
        EQ(uint16_t(12),
           dataset.getDataAt(idx).getNestedValue().at(0).getValue<uint16_t>());
        EQ(true, dataset.is<const DIMData::List &>(idx));
        EQ(false, dataset.is<int32_t>(idx));
        CPPUNIT_ASSERT_THROW(dataset.getNestedValue("test"), DIMException);
    }

    void nested()
    {
        DIMDataSetClient cli("test");
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);
        DIMDataSet dataset("test");
        EQ(true, waiter.wait(1));

        DIMData::List childs;
        childs.push_back(DIMData(0, uint16_t(12)));
        childs.push_back(DIMData(1, childs)); // nested in nested, must work

        dataset.addData(0, "nested", "", std::move(childs), AddMode::CREATE,
                        false);
        EQ(uint16_t(12),
           dataset.getNestedValue("nested").at(0).getValue<uint16_t>());

        // modify the dataset
        childs = dataset.getNestedValue("nested");
        childs[0].setValue(uint16_t(16));
        dataset.setValue("nested", std::move(childs), false);

        {
            DIMData data = dataset.getDataAt(0);
            DIMDataSet::Index index = data.addNestedData(
                std::string(), std::string(), uint16_t(24));
            data.addNestedData(std::string(), std::string(), uint16_t(36));
            data.removeNestedData(index);
            dataset.setValue(0, std::move(data), false);
        }

        EQ(int(1), dataset.getDataCount());
        dataset.updateData();

        EQ(true, waiter.wait(2));
        std::vector<DIMData> data = cli.getLatestData();
        EQ(std::size_t(1), data.size());
        EQ(uint16_t(16), data[0].getNestedValue()[0].getUShortValue());
        EQ(uint16_t(36), data[0].getNestedValue()[2].getUShortValue());
    }

    void lockRef()
    {
        DIMDataSet dataset("test");
        dataset.addData(0, "nested", "", DIMData::List(), AddMode::CREATE,
                        false);
        dataset.lockDataAt(0)->addNestedData<int32_t>(0, "test", "", 42);
        dataset.updateData();

        SignalWaiter waiter;
        DIMDataSetClient cli("test");
        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(1));
        EQ(int32_t(42),
           cli.getLatestData().at(0).getNestedValue()[0].getValue<int32_t>());

        dataset.lockDataAt(0)->getNestedValue().at(0).setValue(int32_t(12));
        dataset.updateData();
        EQ(true, waiter.wait(2));
        EQ(int32_t(12),
           cli.getLatestData().at(0).getNestedValue()[0].getValue<int32_t>());
    }

    void hidden()
    {
        SignalWaiter waiter;
        DIMDataSetClient cli("test");
        waiter.listen(cli.dataSignal);

        DIMDataSet dataset("test");
        dataset.addData(0, "int8_t", "", int8_t(42), AddMode::CREATE, true);

        EQ(true, waiter.wait(1));
        EQ(std::size_t(1), cli.getLatestData().size());

        dataset.setHidden(0, true);
        EQ(true, dataset.isHidden(0));

        EQ(true, waiter.wait(2));
        EQ(std::size_t(0), cli.getLatestData().size());
    }

    void addData_lvalue()
    {
        /*
        ensure that lvalue is being used when calling addData
        template operator overloading has a weird behavior regarding that.
        */
        DIMDataSet dataset("test");

        DIMData::List list;
        list.emplace_back(0, "test", "test", 42);
        dataset.addData(0, "test", "test", list, AddMode::CREATE);
        // If move operator is called this will not be true
        EQ(std::size_t(1), list.size());
    }

    void subscribe()
    {
        SignalWaiter waiter;
        DIMDataSetClient cli;
        waiter.listen(cli.dataSignal);

        DIMDataSet dataset("test");
        dataset.addData(0, "int8_t", "", int8_t(42), AddMode::CREATE, true);
        cli.subscribe("test");

        EQ(true, waiter.wait(1));

        cli.subscribe("test"); // should unsub/resub
        EQ(true, waiter.wait(2));
        EQ(int8_t(42), cli.getLatestData().at(0).getValue<int8_t>());

        cli.unsubscribe();
        dataset.setValue<int8_t>(0, 12);
        EQ(false, waiter.wait(3, std::chrono::seconds(1)));
        cli.subscribe("test");

        EQ(true, waiter.wait(3));
        EQ(int8_t(12), cli.getLatestData().at(0).getValue<int8_t>());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMDataSet);
