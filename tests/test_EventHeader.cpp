
#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "EventReader.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class TestEventHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestEventHeader);
    CPPUNIT_TEST(fromEventReader);
    CPPUNIT_TEST(setCompTS);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST(updateDateTime);
    CPPUNIT_TEST_SUITE_END();

public:
    void fromEventReader()
    {
        EventHeader header;

        EventReader::Data data;
        data.cycleNb = 1234;
        data.timeStamp = 5678;
        data.cycleStamp = 1573765646 * 1E9;
        data.evtType = "PARASITIC";
        data.dest = "HERE";
        data.dest2 = "THERE";
        data.evtNumber = 131415;

        header << data;

        CPPUNIT_ASSERT_EQUAL(header.bctTS, (double) 1573765646 * 1E9);
        CPPUNIT_ASSERT_EQUAL(header.eventNumber, (uint32_t) 131415);
        CPPUNIT_ASSERT_EQUAL(header.beamType, (uint32_t) EventHeader::PARASITIC);
        CPPUNIT_ASSERT_EQUAL(header.time, (uint32_t) 220726);
        CPPUNIT_ASSERT_EQUAL(header.date, (uint32_t) 1191114);
    }

    void setCompTS()
    {
        EventHeader header;

        CPPUNIT_ASSERT(header.compTS == 0);
        header.setCompTS();
        CPPUNIT_ASSERT(header.compTS != 0);
    }

    void dump()
    {
        EventHeader eveh;
        std::ostringstream oss;

        eveh.sizeOfEvent = 0x12345678;
        eveh.eventNumber = 0xDEADBEEF;
        eveh.runNumber = 0xFEEDFEED;
        eveh.time = 1010;
        eveh.date = 1718;
        eveh.beamType = EventHeader::PARASITIC;

        oss << eveh;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>(EventHeader::SIZE), data.length());

        EQ(string("EVEH"), data.substr(0, 4));
        EQ(string("\x01\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x0C\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words
        EQ(string("\x78\x56\x34\x12"), data.substr(4 << 2, 4)); // sizeOfEvent
        EQ(string("\xEF\xBE\xAD\xDE"), data.substr(5 << 2, 4)); // eventNumber
        EQ(string("\xED\xFE\xED\xFE"), data.substr(6 << 2, 4)); // eventNumber

        {
            std::istringstream iss(data);
            EventHeader headerOut;
            iss >> headerOut;

            oss.str("");
            oss.clear();
            oss << headerOut;

            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());
            EQ(true, iss.good());

            iss >> headerOut;
            EQ(false, iss.good());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(EventHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }

    void updateDateTime()
    {
        EventHeader eveh;

        EQ(static_cast<uint32_t>(0), eveh.date);
        EQ(static_cast<uint32_t>(0), eveh.time);

        std::time_t t = std::time(nullptr);
        eveh.updateDateTime(t);
        EQ(static_cast<uint32_t>(t % 60), eveh.time % 100);
        // Note: this will fail on some very specific timezones
        EQ(static_cast<uint32_t>(t / 60 % 60), eveh.time / 100 % 100);
        ASSERT(eveh.date != 0);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEventHeader);
