
#include <DIMData.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMDataTimedBuffer.hpp"
#include "Timestamp.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMDataTimedBuffer : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMDataTimedBuffer);
    CPPUNIT_TEST(simple_init);
    CPPUNIT_TEST(add_event);
    CPPUNIT_TEST(check_events);
    CPPUNIT_TEST(marginLimit);
    CPPUNIT_TEST(spuriousEvent);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    DIMData::List makeEvent(const int64_t timestamp)
    {
        DIMData::List event;
        event.push_back(DIMData(0, "test", "", int32_t(42)));
        event.push_back(DIMData(1, "timestamp", "", int64_t(timestamp)));
        return event;
    }

    void simple_init()
    {
        DIMDataTimedBuffer buffer;
        CPPUNIT_ASSERT_EQUAL(int64_t(100000), buffer.getTimeMargin());

        DIMData::List event;
        buffer.getCurrentEvent(event);
        CPPUNIT_ASSERT_EQUAL(std::size_t(0), event.size());
        buffer.getEventByTime(event, Timestamp(), 200);
        CPPUNIT_ASSERT_EQUAL(std::size_t(0), event.size());
    }

    void add_event()
    {
        DIMDataTimedBuffer buffer;
        Timestamp now = Timestamp();
        DIMData::List event = makeEvent(now);
        // Delete the second element (timestamp)
        event.erase(event.begin() + 1);
        buffer.addEvent(event);

        // case retrieveTimestamp return -1
        DIMData::List getEvent;
        buffer.getEventByTime(getEvent, now);
        CPPUNIT_ASSERT_EQUAL(std::size_t(0), getEvent.size());
        buffer.getCurrentEvent(getEvent);
        CPPUNIT_ASSERT_EQUAL(std::size_t(0), getEvent.size());

        // case ok
        buffer.addEvent(makeEvent(now));
        buffer.getEventByTime(getEvent, now);
        CPPUNIT_ASSERT_EQUAL(std::size_t(2), getEvent.size());
        buffer.getCurrentEvent(getEvent);
        CPPUNIT_ASSERT_EQUAL(std::size_t(2), getEvent.size());
    }

    void check_events()
    {
        DIMDataTimedBuffer buffer;
        Timestamp now = Timestamp();
        buffer.addEvent(makeEvent(now));

        DIMData::List event;
        // get nothing out of margin
        buffer.getEventByTime(event, now + buffer.getTimeMargin() + int64_t(1));
        CPPUNIT_ASSERT_EQUAL(std::size_t(0), event.size());

        // get event in margin
        buffer.getEventByTime(event, now);
        CPPUNIT_ASSERT_EQUAL(std::size_t(2), event.size());
        CPPUNIT_ASSERT_EQUAL(int32_t(42), event.at(0).getIntValue());
        CPPUNIT_ASSERT_EQUAL(now.getValue(), event.at(1).getLongValue());

        // check current is returning same thing
        DIMData::List eventCurrent;
        buffer.getCurrentEvent(eventCurrent);
        CPPUNIT_ASSERT_EQUAL(std::size_t(2), event.size());

        // Add new event
        buffer.addEvent(makeEvent(now + int64_t(100)));
        buffer.getEventByTime(event, now + int64_t(200));
        CPPUNIT_ASSERT_EQUAL(std::size_t(2), event.size());
        CPPUNIT_ASSERT_EQUAL(now.getValue() + int64_t(100),
                             event.at(1).getLongValue());
    }

    /**
     * @details event withing margins exist but is older than requested time
     */
    void marginLimit()
    {
        DIMDataTimedBuffer buffer;
        Timestamp now;
        buffer.addEvent(makeEvent(now));

        DIMData::List event;
        ASSERT(buffer.getEventByTime(event, now - (buffer.getTimeMargin() - 1)));
        EQ(false, event.empty());
    }

    /**
     * @details non-matching event arrives whilst waiting
     */
    void spuriousEvent()
    {
        DIMDataTimedBuffer buffer;
        Timestamp now;

        DIMData::List event;
        std::future<bool> future = std::async(
            std::launch::async, [&event, &buffer, &now]() {
                return buffer.getEventByTime(event, now);
            });

        // wait for thread to start and settle
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        buffer.addEvent(makeEvent(now - (buffer.getTimeMargin() + 1)));

        // wait for thread to process event
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        buffer.addEvent(makeEvent(now));

        // default timeout 200ms
        EQ(std::future_status::ready,
           future.wait_for(std::chrono::milliseconds(300)));
        EQ(false, event.empty());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMDataTimedBuffer);
