/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-24T09:54:32+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMDataSet.h"
#include "easylogging++.h"
#include "test_helpers.hpp"

using namespace ntof::dim;

class TestDIMData : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMData);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(nested);
    CPPUNIT_TEST(nestedUpdate);
    CPPUNIT_TEST(insertHidden);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        DIMData data;
        EQ(DIMData::TypeInvalid, data.getDataType());

        data = DIMData(0, "double", std::string(), double(12.12));
        EQ(DIMData::TypeDouble, data.getDataType());
        DBL_EQ(double(12.12), data.getDoubleValue(), 0.01);
        data.setValue(double(42.42));
        DBL_EQ(double(42.42), data.getValue<double>(), 0.01);

        data = DIMData(0, float(12.12));
        EQ(DIMData::TypeFloat, data.getDataType());
        DBL_EQ(double(12.12), data.getFloatValue(), 0.01);
        data.setValue(float(42.42));
        DBL_EQ(double(42.42), data.getValue<float>(), 0.01);

        data = DIMData(0, std::string("sample string"));
        EQ(DIMData::TypeString, data.getDataType());
        EQ(std::string("sample string"), data.getStringValue());
        data.setValue(std::string("another string"));
        EQ(std::string("another string"), data.getValue<const std::string &>());

        data = DIMData(0, "int", std::string(), int32_t(-42));
        EQ(DIMData::TypeInt, data.getDataType());
        EQ(int32_t(-42), data.getIntValue());
        data.setValue(int32_t(24));
        EQ(int32_t(24), data.getValue<int32_t>());

        data = DIMData(0, "long", std::string(), int64_t(-42));
        EQ(DIMData::TypeLong, data.getDataType());
        EQ(int64_t(-42), data.getLongValue());
        data.setValue(int64_t(24));
        EQ(int64_t(24), data.getValue<int64_t>());

        data = DIMData(0, "byte", std::string(), int8_t(-42));
        EQ(DIMData::TypeByte, data.getDataType());
        EQ(int8_t(-42), data.getByteValue());
        data.setValue(int8_t(24));
        EQ(int8_t(24), data.getValue<int8_t>());

        data = DIMData(0, "short", std::string(), int16_t(42));
        EQ(DIMData::TypeShort, data.getDataType());
        EQ(int16_t(42), data.getShortValue());
        data.setValue(int16_t(24));
        EQ(int16_t(24), data.getValue<int16_t>());

        data = DIMData(0, "bool", std::string(), true);
        EQ(DIMData::TypeBool, data.getDataType());
        EQ(true, data.getBoolValue());
        data.setValue(false);
        EQ(false, data.getValue<bool>());

        /* using simpler constructors */
        data = DIMData(0, uint32_t(42));
        EQ(DIMData::TypeUInt, data.getDataType());
        EQ(uint32_t(42), data.getUIntValue());
        data.setValue(uint32_t(24));
        EQ(uint32_t(24), data.getValue<uint32_t>());

        data = DIMData(0, uint64_t(42));
        EQ(DIMData::TypeULong, data.getDataType());
        EQ(uint64_t(42), data.getULongValue());
        data.setValue(uint64_t(24));
        EQ(uint64_t(24), data.getValue<uint64_t>());

        data = DIMData(0, uint8_t(42));
        EQ(DIMData::TypeUByte, data.getDataType());
        EQ(uint8_t(42), data.getUByteValue());
        data.setValue(uint8_t(24));
        EQ(uint8_t(24), data.getValue<uint8_t>());

        data = DIMData(0, uint16_t(42));
        EQ(DIMData::TypeUShort, data.getDataType());
        EQ(uint16_t(42), data.getUShortValue());
        data.setValue(uint16_t(24));
        EQ(uint16_t(24), data.getValue<uint16_t>());
    }

    void enums()
    {
        DIMEnum e;

        e.addItem(0, "test");
        e.addItem(1, "another");

        e.setValue(1);
        EQ(std::string("another"), e.getName());
        EQ(int32_t(1), e.getValue());

        DIMData data(0, e);
        EQ(DIMData::TypeEnum, data.getDataType());
        ASSERT(e == data.getEnumValue());
        ASSERT(e == data.getValue<DIMEnum &>());
    }

    void nested()
    {
        DIMData::List childs;

        childs.push_back(DIMData(0, uint16_t(12)));
        childs.push_back(DIMData(1, childs)); // nested in nested, must work

        DIMData *ptr = childs.data();
        DIMData data(0, std::move(childs));

        EQ(DIMData::TypeNested, data.getDataType());
        EQ(ptr, data.getNestedValue().data()); // ensure zero copy

        const DIMData::List &values = data.getNestedValue();
        EQ(std::size_t(2), values.size());

        EQ(DIMData::TypeUShort, values[0].getDataType());
        EQ(DIMData::TypeNested, values[1].getDataType());
        // data was copied before insert
        EQ(std::size_t(1), values[1].getNestedValue().size());
        EQ(values[0].getUShortValue(),
           values[1].getNestedValue()[0].getUShortValue());

        // Test addNestedData
        uint32_t index = data.addNestedData(std::string(), std::string(),
                                            uint16_t(24));
        EQ(std::size_t(3), values.size());
        EQ(values[2].getUShortValue(), uint16_t(24));
        data.removeNestedData(index - 1);
        EQ(std::size_t(2), values.size());
    }

    void nestedUpdate()
    {
        DIMData::List childs{DIMData(0, uint32_t(12)),
                             DIMData(1, std::string("text"))};

        childs.push_back(DIMData(2, childs));
        DIMData data(0, childs);

        CPPUNIT_ASSERT_THROW(data.updateValue(DIMData(0, uint8_t(42)), false),
                             DIMException);

        {
            DIMData::List update{DIMData(1, std::string("pwet"))};
            update.push_back(DIMData(2, update));

            /* this should update all stringwith a new value */
            data.updateValue(DIMData(0, update), false);

            EQ(std::size_t(3), data.getNestedValue().size());
            EQ(std::string("pwet"), data.getNestedValue()[1].getStringValue());
            EQ(std::string("pwet"),
               data.getNestedValue()[2].getNestedValue()[1].getStringValue());
        }

        data.updateValue(DIMData(0, uint8_t(42)), true);
        EQ(DIMData::TypeUByte, data.getDataType());
    }

    void insertHidden()
    {
        /* hidden data mustn't be inserted in documents */
        DIMData::List childs{DIMData(0, uint32_t(12)),
                             DIMData(1, std::string("text"))};
        childs[1].setHidden(true);
        childs.push_back(DIMData(2, childs));
        DIMData data(0, childs);

        {
            pugi::xml_document doc;
            pugi::xml_node root = doc.append_child("root");
            data.insertInto(root, true);
            EQ(true, bool(root.first_child()));
            DIMData ret(root.first_child());
            EQ(DIMData::TypeNested, ret.getDataType());
            EQ(std::size_t(2), ret.getNestedValue().size());
            EQ(uint32_t(0), ret.getNestedValue()[0].getIndex());
            EQ(std::size_t(1), ret.getNestedValue()[1].getNestedValue().size());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMData);
