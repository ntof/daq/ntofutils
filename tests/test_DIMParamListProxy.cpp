/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-09T23:02:24+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMParamList.h"
#include "DIMParamListClient.h"
#include "NTOFLogging.hpp"
#include "Signals.hpp"
#include "proxy/DIMParamListProxy.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMParamListProxy : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMParamListProxy);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(protect);
    CPPUNIT_TEST(disconnect);
    CPPUNIT_TEST(singleShot);
    // CPPUNIT_TEST(sendParameters);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        DIMParamListClient cli("testMirror");
        DIMParamList params("test");
        SignalWaiter waiter;
        int32_t sigcount = 1;

        waiter.listen(cli.dataSignal);
        DIMParamListProxy proxy("testMirror", "test");

        EQ(true, waiter.wait(sigcount++));

        params.addParameter<uint32_t>(1, "my param", "", 42, AddMode::CREATE);
        proxy.setSyncing();

        EQ(true, waiter.wait(sigcount++));
        std::vector<DIMData> data = cli.getParameters();
        EQ(std::size_t(2), data.size());
        EQ(uint32_t(42), data[1].getValue<uint32_t>());

        /* not protected */
        DIMAck ack = cli.setParameter<uint32_t>(1, 44);
        EQ(DIMAck::OK, ack.getStatus());

        EQ(true, waiter.wait(sigcount++)); /* should receive an update back */
        data = cli.getParameters();
        EQ(std::size_t(2), data.size());
        EQ(uint32_t(44), data[1].getValue<uint32_t>());

        params.setValue<uint32_t>(1, 45);
        EQ(true, waiter.wait(sigcount++)); /* should receive an update back */
        data = cli.getParameters();
        EQ(std::size_t(2), data.size());
        EQ(uint32_t(45), data[1].getValue<uint32_t>());
    }

    void protect()
    {
        DIMParamListProxy proxy("testMirror", "test");
        DIMParamListClient cli("testMirror");
        DIMParamList params("test");
        SignalWaiter waiter;
        int32_t sigcount = 1;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(sigcount++));

        params.addParameter<uint32_t>(1, "my param", "", 42, AddMode::CREATE);
        proxy.setSyncing();

        EQ(true, waiter.wait(sigcount++));
        std::vector<DIMData> data = cli.getParameters();
        EQ(std::size_t(2), data.size());

        proxy.setNoUpdatesOnSync(true);
        try
        {
            /* protected */
            cli.setParameter<uint32_t>(1, 44);
            throw std::runtime_error("should fail");
        }
        catch (DIMException &ex)
        {
            EQ(std::string("Service unavailable: syncing in progress"),
               std::string(ex.getMessage()));
        }
    }

    void disconnect()
    {
        DIMParamList params("test");
        DIMParamListProxy proxy("testMirror", "test");
        DIMParamListClient cli("testMirror");
        SignalWaiter waiter;
        int32_t sigcount = 1;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(sigcount++));

        params.addParameter<uint32_t>(1, "nested", "", 42, AddMode::CREATE);
        proxy.syncSignal.connect(
            [](DIMParamListProxy &proxy) { proxy.disconnect(); });
        proxy.setSyncing();

        EQ(true, waiter.wait(sigcount++));
        std::vector<DIMData> data = cli.getParameters();
        EQ(std::size_t(2), data.size());

        EQ(uint32_t(42), data[1].getValue<uint32_t>());
        EQ(false, proxy.isSyncing());

        /* not syncing */
        params.setValue<uint32_t>(1, 44);
        EQ(false, waiter.wait(sigcount));

        proxy.setSyncing();
        EQ(true, waiter.wait(sigcount));
        EQ(false, proxy.isSyncing());
    }

    void singleShot()
    {
        DIMParamListProxy proxy("testMirror", "test");
        DIMParamListClient cli("testMirror");
        DIMParamList params("test");
        SignalWaiter waiter;
        int32_t sigcount = 1;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(sigcount++));

        params.addParameter<uint32_t>(1, "nested", "", 42, AddMode::CREATE);
        proxy.syncSignal.connect(
            [](DIMParamListProxy &proxy) { proxy.setSyncing(false); });
        proxy.setSyncing();

        EQ(true, waiter.wait(sigcount++));
        std::vector<DIMData> data = cli.getParameters();
        EQ(std::size_t(2), data.size());

        EQ(uint32_t(42), data[1].getValue<uint32_t>());
        EQ(false, proxy.isSyncing());

        /* not syncing */
        params.setValue<uint32_t>(1, 44);
        EQ(false, waiter.wait(sigcount));

        proxy.setSyncing();
        EQ(true, waiter.wait(sigcount));
        EQ(false, proxy.isSyncing());
    }

    void sendParameters()
    {
        DIMParamListClient cli("test");
        DIMParamList params("test");
        SignalWaiter waiter;
        int32_t sigcount = 1;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(sigcount++));

        params.addParameter<uint32_t>(1, "param", "", 42, AddMode::CREATE,
                                      false);
        params.addParameter<uint32_t>(2, "readOnlyParam", "", 42,
                                      AddMode::CREATE);
        EQ(true, waiter.wait(sigcount++));

        DIMParamListProxy proxy("testMirror", "test");
        {
            SignalWaiter syncWaiter;
            proxy.syncSignal.connect(
                [](DIMParamListProxy &proxy) { proxy.setSyncing(false); });
            syncWaiter.listen(proxy.syncSignal);
            proxy.setSyncing();
            EQ(true, syncWaiter.wait(1));
        }

        proxy.setValue<uint32_t>(1, 44);
        proxy.setValue<uint32_t>(2, 44);

        proxy.setReadOnly(2);
        DIMAck ack = proxy.sendParameters();
        EQ(DIMAck::OK, ack.getStatus());

        EQ(true, waiter.wait(sigcount++));
        std::vector<DIMData> data = cli.getParameters();
        EQ(std::size_t(3), data.size());

        EQ(uint32_t(44), data[1].getValue<uint32_t>());
        EQ(uint32_t(42), data[2].getValue<uint32_t>());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMParamListProxy);
