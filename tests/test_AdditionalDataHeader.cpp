
#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class AdditionalDataHeaderTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(AdditionalDataHeaderTest);
    CPPUNIT_TEST(simpleValue);
    CPPUNIT_TEST(paddedValue);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST_SUITE_END();

public:
    void simpleValue()
    {
        AdditionalDataValue value(AdditionalDataValue::TypeInt64);

        EQ(AdditionalDataValue::TypeInt64, value.type);
        value.reserve(2);
        EQ(size_t(0), value.count());

        value.append<uint64_t>(0x1234);
        EQ(size_t(1), value.count());

        value.append<uint64_t>(0x5678);
        EQ(size_t(2), value.count());

        EQ(uint64_t(0x5678), value.at<uint64_t>(1));
        EQ(size_t(88), value.size());

        value.name = "shortname";
        EQ(size_t(0), value.padding());

        std::ostringstream oss;
        oss << value;
        EQ(true, oss.good());

        string data(oss.str());
        EQ(size_t(88), data.length());

        EQ(string("shortname") + string(64 - 9, '\0'),
           data.substr(0, 64));                                // name
        EQ(string("\x02\x00\x00\x00", 4), data.substr(64, 4)); // type
        EQ(string("\x02\x00\x00\x00", 4), data.substr(68, 4)); // repeat
        EQ(string("\x34\x12\x00\x00\x00\x00\x00\x00", 8), data.substr(72, 8));
    }

    void paddedValue()
    {
        AdditionalDataValue value(AdditionalDataValue::TypeChar);

        value.fromString("te"); // half of "test" :)
        EQ(size_t(72 + 4), value.size());
        EQ(size_t(2), value.count());
        EQ(size_t(2), value.padding());

        std::ostringstream oss;
        oss << value;
        EQ(true, oss.good());

        string data(oss.str());
        EQ(size_t(72 + 4), data.length());

        EQ(string(64, '\0'), data.substr(0, 64));              // name
        EQ(string("\x00\x00\x00\x00", 4), data.substr(64, 4)); // type
        EQ(string("\x02\x00\x00\x00", 4), data.substr(68, 4)); // repeat
        EQ(string("te\x00\x00", 4), data.substr(72, 4));
    }

    void dump()
    {
        AdditionalDataHeader header;

        AdditionalDataValue value(AdditionalDataValue::TypeChar);
        value.name = "string" + string(100, 'x'); // very long name on purpose
        value.fromString("this is a test");
        header.values.push_back(value);

        value = AdditionalDataValue(AdditionalDataValue::TypeDouble);
        value.name = "double";
        value.append<double>(12345.12345);
        header.values.push_back(value);

        EQ(size_t(16 + 88 + 80), header.size());
        std::ostringstream oss;
        oss << header;
        EQ(true, oss.good());

        string data(oss.str());
        EQ(size_t(16 + 88 + 80), data.length());

        EQ(string("ADDH"), data.substr(0, 4));
        EQ(string("\x00\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x2A\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words
        EQ(string("string", 6), data.substr(4 << 2, 6));
        EQ(string("double", 6), data.substr(26 << 2, 6));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(AdditionalDataHeaderTest);
