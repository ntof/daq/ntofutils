
#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

static_assert(sizeof(ChannelConfig) == 22 * 4 - 20,
              "Wrong size for ChannelConfig");

class TestModuleHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestModuleHeader);
    CPPUNIT_TEST(channelConfig);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST_SUITE_END();

public:
    void channelConfig()
    {
        ChannelConfig config;

        config.setLocation(0x11, 0x12, 0x13, 0x14);
        EQ(static_cast<uint32_t>(0x11121314), config.str_mod_crate);

        config.setStream(0xAA);
        EQ(static_cast<uint8_t>(0xAA), config.getStream());
        EQ(static_cast<uint32_t>(0xAA121314), config.str_mod_crate);

        config.setChassis(0xBB);
        EQ(static_cast<uint8_t>(0xBB), config.getChassis());
        EQ(static_cast<uint32_t>(0xAABB1314), config.str_mod_crate);

        config.setModule(0xCC);
        EQ(static_cast<uint8_t>(0xCC), config.getModule());
        EQ(static_cast<uint32_t>(0xAABBCC14), config.str_mod_crate);

        config.setChannel(0xDD);
        EQ(static_cast<uint8_t>(0xDD), config.getChannel());
        EQ(static_cast<uint32_t>(0xAABBCCDD), config.str_mod_crate);

        config.setModuleType("ACQC");
        EQ(string("ACQC"), config.getModuleType());
        EQ(static_cast<uint32_t>(0x43514341), config.moduleType);

        config.setDetectorType("BAF2");
        EQ(string("BAF2"), config.getDetectorType());
        EQ(static_cast<uint32_t>(0x32464142), config.detectorType);

        config.setClockState("INTC");
        EQ(string("INTC"), config.getClockState());
        EQ(static_cast<uint32_t>(0x43544E49), config.clockState);

        config.setMasterDetectorType("BAF4");
        EQ(string("BAF4"), config.getMasterDetectorType());
        EQ(static_cast<uint32_t>(0x34464142), config.masterDetectorType);
    }

    void dump()
    {
        ModuleHeader header(2);
        std::ostringstream oss;

        EQ(static_cast<std::size_t>(2), header.channelsConfig.size());
        ChannelConfig &config = header.channelsConfig[0];

        config.detectorId = 0xDEADBEEF;
        config.setLocation(0x11, 0x12, 0x13, 0x14);
        config.setStream(0xAA);
        config.setChassis(0xBB);
        config.setModule(0xCC);
        config.setChannel(0xDD);
        config.setModuleType("ACQC");
        config.setDetectorType("BAF2");
        config.setClockState("INTC");
        config.setMasterDetectorType("BAF4");
        EQ(std::size_t(20 + 88 * 2), header.size());

        oss << header;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>((22 * 2 + 5) << 2), data.length());

        EQ(string("MODH"), data.substr(0, 4));
        EQ(string("\x04\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x2D\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words
        EQ(string("\x02\x00\x00\x00", 4),
           data.substr(4 << 2, 4)); // num channels

        EQ(string("BAF2"), data.substr(5 << 2, 4));
        EQ(string("\xEF\xBE\xAD\xDE"), data.substr(6 << 2, 4)); // detectorId
        EQ(string("ACQC"), data.substr(7 << 2, 4));
        EQ(string("\xDD\xCC\xBB\xAA", 4), data.substr(8 << 2, 4));

        EQ(string("INTC", 4), data.substr(18 << 2, 4));
        EQ(string("BAF4", 4), data.substr(20 << 2, 4));

        {
            std::istringstream iss(data);
            ModuleHeader headerOut;
            iss >> headerOut;

            oss.str("");
            oss.clear();
            oss << headerOut;

            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());
            EQ(true, iss.good());

            iss >> headerOut;
            EQ(false, iss.good());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(ModuleHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestModuleHeader);
