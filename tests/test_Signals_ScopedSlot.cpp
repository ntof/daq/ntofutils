/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-22T08:48:23+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <condition_variable>
#include <future>
#include <mutex>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Signals.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;

class TestSignals_ScopedSlot : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSignals_ScopedSlot);
    CPPUNIT_TEST(construct);
    CPPUNIT_TEST(connect);
    CPPUNIT_TEST(track);
    CPPUNIT_TEST_SUITE_END();

public:
    void construct()
    {
        ntof::utils::signal<void()> sig;
        std::future<void> ret;
        std::mutex m;
        std::unique_lock<std::mutex> lock(m);
        std::condition_variable cond;
        uint32_t data = 0x0;

        {
            scoped_slot scopedSlot(sig, [&m, &cond, &data]() {
                {
                    std::unique_lock<std::mutex> lock(m);
                    cond.notify_all();
                }
                // really important to slow down this one, to ensure that
                // the scoped_connection really waits for handlers to be
                // resolved
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                data = 0xDEADBEEF;
            });

            ret = std::async(std::launch::async, [&sig]() { sig(); });

            ASSERT(cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
                   std::cv_status::no_timeout);
        }
        EQ(std::future_status::ready, ret.wait_for(std::chrono::seconds(0)));
        EQ(uint32_t(0xDEADBEEF), data);
    }

    void connect()
    {
        ntof::utils::signal<void()> sig;
        std::future<void> ret;
        std::mutex m;
        std::unique_lock<std::mutex> lock(m);
        std::condition_variable cond;
        uint32_t data = 0x0;

        {
            scoped_slot scopedSlot;
            scopedSlot.connect(sig, [&m, &cond, &data]() {
                {
                    std::unique_lock<std::mutex> lock(m);
                    cond.notify_all();
                }
                // really important to slow down this one, to ensure that
                // the scoped_connection really waits for handlers to be
                // resolved
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                data = 0xDEADBEEF;
            });

            ret = std::async(std::launch::async, [&sig]() { sig(); });

            ASSERT(cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
                   std::cv_status::no_timeout);
        }
        EQ(std::future_status::ready, ret.wait_for(std::chrono::seconds(0)));
        EQ(uint32_t(0xDEADBEEF), data);
    }

    void track()
    {
        ntof::utils::signal<void()> sig;
        std::future<void> ret;
        std::mutex m;
        std::unique_lock<std::mutex> lock(m);
        std::condition_variable cond;
        uint32_t data = 0x0;

        {
            scoped_slot scopedSlot;
            // one can also use: decltype(sig)::signature_type
            sig.connect(scopedSlot.track<void()>([&m, &cond, &data]() {
                {
                    std::unique_lock<std::mutex> lock(m);
                    cond.notify_all();
                }
                // really important to slow down this one, to ensure that
                // the scoped_connection really waits for handlers to be
                // resolved
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                data = 0xDEADBEEF;
            }));

            ret = std::async(std::launch::async, [&sig]() { sig(); });

            ASSERT(cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
                   std::cv_status::no_timeout);
        }
        EQ(std::future_status::ready, ret.wait_for(std::chrono::seconds(0)));
        EQ(uint32_t(0xDEADBEEF), data);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSignals_ScopedSlot);
