/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-24T13:42:53+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class TestSkipHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSkipHeader);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST_SUITE_END();

public:
    void dump()
    {
        SkipHeader skip(0x1234, 0x5678);
        std::ostringstream oss;

        oss << skip;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>(SkipHeader::SIZE), data.length());

        EQ(string("SKIP"), data.substr(0, 4));
        EQ(string("\x02\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x04\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words
        EQ(string("\x34\x12\x00\x00\x00\x00\x00\x00", 8),
           data.substr(4 << 2, 8)); // sizeOfEvent
        EQ(string("\x78\x56\x00\x00\x00\x00\x00\x00", 8),
           data.substr(6 << 2, 8)); // sizeOfEvent

        {
            std::istringstream iss(data);
            SkipHeader header;

            EQ((int64_t) -1, header.start);
            EQ((int64_t) -1, header.end);

            iss >> header;
            EQ(true, iss.good());

            oss.clear();
            oss.str("");
            oss << header;
            EQ(true, oss.good());
            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());

            iss >> header;
            EQ(true, iss.eof());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(SkipHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSkipHeader);
