/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-25T13:15:42+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cerrno>
#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMAck.h"
#include "DIMCmd.h"
#include "DIMException.h"
#include "DIMSuperClient.h"
#include "DIMXMLRpc.h"
#include "easylogging++.h"
#include "test_helpers.hpp"

using namespace ntof::dim;

class RpcService : public DIMXMLRpc
{
public:
    RpcService(const std::string &name) : DIMXMLRpc(name) {}

    enum TestCases
    {
        SimpleTest = 1,
        TimeoutTest = 2,
        ErrorTest = 3
    };

    void rpcReceived(DIMCmd &cmd) override
    {
        pugi::xml_node node = cmd.getData();
        DIMAck ack = makeReply(cmd.getKey());

        switch (node.attribute("value").as_int(-1))
        {
        case SimpleTest:
            ack.getRoot().append_child("result").append_attribute("value") = 42;
            setReply(ack);
            break;
        case TimeoutTest: break; /* should be sufficient */
        case ErrorTest: setError(cmd.getKey(), -12, "this is an error"); break;
        default: setError(cmd.getKey(), -1, "missing value"); break;
        }
    }
};

class TestDIMXMLRpc : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMXMLRpc);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(timeout);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        DIMSuperClient cli("test", true);
        RpcService srv("test");

        pugi::xml_document doc;
        doc.append_child("command").append_attribute(
            "value") = RpcService::SimpleTest;
        DIMAck ack = cli.sendCommand(doc);
        EQ(DIMAck::OK, ack.getStatus());

        pugi::xml_node result = ack.getRoot().child("result");
        EQ(int(42), result.attribute("value").as_int(-1));
    }

    void timeout()
    {
        DIMSuperClient cli("test", true);
        RpcService srv("test");

        cli.setTimeOut(std::chrono::milliseconds(200));
        pugi::xml_document doc;
        doc.append_child("command").append_attribute(
            "value") = RpcService::TimeoutTest;
        try
        {
            cli.sendCommand(doc);
            throw std::string("should have failed");
        }
        catch (DIMException &ex)
        {
            EQ(ETIMEDOUT, ex.getErrorCode());
        }
    }

    void error()
    {
        DIMSuperClient cli("test", true);
        RpcService srv("test");

        pugi::xml_document doc;
        doc.append_child("command").append_attribute(
            "value") = RpcService::ErrorTest;
        try
        {
            cli.sendCommand(doc);
            throw std::string("should have failed");
        }
        catch (DIMException &ex)
        {
            EQ(-12, ex.getErrorCode());
            EQ(std::string("this is an error"), std::string(ex.getMessage()));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMXMLRpc);
