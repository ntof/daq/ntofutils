/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-16T19:40:04+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMParamList.h"
#include "DIMParamListClient.h"
#include "NTOFException.h"
#include "NTOFLogging.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMParamListThreaded : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMParamListThreaded);
    CPPUNIT_TEST(threaded);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void threaded()
    {
        const size_t max = 100;
        DIMParamList dataset("test");

        dataset.addParameter(1, "test", "", std::string(""), AddMode::CREATE,
                             true);

        std::thread th1([max]() {
            DIMParamListClient cli("test");
            for (size_t i = 0; i < max; ++i)
                cli.sendParameter(DIMData(1, std::to_string(i)));
        });

        std::thread th2([&dataset, max]() {
            for (size_t i = 0; i < max; ++i)
            {
                // This is needed to access already free'd data
                LOG(ERROR) << dataset.getStringValue(1);
                dataset.setValue<std::string>(1, std::to_string(i) + "e");
            }
        });

        th1.join();
        th2.join();
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMParamListThreaded);
