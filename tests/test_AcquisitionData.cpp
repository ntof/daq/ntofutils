
/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-05-28T10:00:00+01:00
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "config.h"

namespace bfs = boost::filesystem;
using std::string;

class TestAcquisitionData : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAcquisitionData);
    CPPUNIT_TEST(read_fixed_data);
    CPPUNIT_TEST(read_random_data);
    CPPUNIT_TEST(read_RawFile); // Requires a .raw file in 'tests/data'
    CPPUNIT_TEST_SUITE_END();

public:
    void read_fixed_data()
    {
        Data pulse;
        std::istringstream iss;
        std::vector<uint16_t> origin_samples{169, 245, 873, 231, 94, 642};
        string timestamp("\x40\xE2\x01\x00\x00\x00\x00\x00", 8); // 123456
        string length("\x06\x00\x00\x00\x00\x00\x00\x00", 8);    // 6
        string data("\xA9\x00\xF5\x00\x69\x03\xE7\x00\x5E\x00\x82\x02",
                    12); // 169, 245, 873, 231, 94, 642

        iss.str(timestamp + length + data);
        iss >> pulse;
        CPPUNIT_ASSERT_EQUAL(true, iss.good());

        CPPUNIT_ASSERT_EQUAL((uint64_t) 123456, pulse.detectorTimeStamp);
        CPPUNIT_ASSERT_EQUAL((size_t) 6, pulse.detectorData.size());
        CPPUNIT_ASSERT_EQUAL(true, origin_samples == pulse.detectorData);

        /* test lookahead */
        HeaderLookup lookup;
        iss >> lookup;
        CPPUNIT_ASSERT_EQUAL(false, iss.good());
    }

    void read_random_data()
    {
        Data pulse;
        std::istringstream iss;
        std::ostringstream oss;
        std::vector<uint16_t> origin_samples;
        uint64_t timestamp = 1234567890;
        srand(time(NULL));
        uint64_t length = rand() % 5000;
        uint16_t sample;
        oss.write(reinterpret_cast<char *>(&timestamp), sizeof(uint64_t));
        oss.write(reinterpret_cast<char *>(&length), sizeof(uint64_t));

        for (uint64_t i = 0; i < length; ++i)
        {
            sample = static_cast<uint16_t>(rand() % 65536);
            origin_samples.push_back(sample);
            oss.write(reinterpret_cast<char *>(&sample), sizeof(uint16_t));
        }
        iss.str(oss.str());

        iss >> pulse; // Data from input stream
        CPPUNIT_ASSERT_EQUAL(true, iss.good());

        CPPUNIT_ASSERT_EQUAL((uint64_t) 1234567890, pulse.detectorTimeStamp);
        CPPUNIT_ASSERT_EQUAL((uint64_t) length, pulse.detectorData.size());

        CPPUNIT_ASSERT_EQUAL(true, origin_samples == pulse.detectorData);

        /* test lookahead */
        HeaderLookup lookup;
        iss >> lookup;
        CPPUNIT_ASSERT_EQUAL(false, iss.good());
    }

    void read_RawFile()
    {
        std::ifstream ifs(
            (bfs::path(SRCDIR) / "tests" / "data" / "example.raw").string());
        CPPUNIT_ASSERT_EQUAL(true, ifs.is_open() && ifs.good());

        SkipHeader sh;
        ifs >> sh; // Read SKIP Header
        CPPUNIT_ASSERT_EQUAL(true, ifs.good());

        ifs.seekg(sh.start, std::ios_base::beg); // Skip zero byte padding
        HeaderLookup hl;
        AcquisitionHeader ah;
        do
        {
            ifs >> hl;
            CPPUNIT_ASSERT_EQUAL(true, ifs.good());
            // Check if the next header is of type ACQC
            CPPUNIT_ASSERT_EQUAL(AcquisitionHeader::TITLE, hl.type);

            ifs >> ah; // Read ACQC Header
            CPPUNIT_ASSERT_EQUAL(true, ifs.good());

            Data pulse;
            size_t byte_counter = 0;
            int pulse_counter = 0;
            while (byte_counter < (ah.dataSize - 2)) // Consider padding
            {
                ifs >> pulse; // Read Samples
                CPPUNIT_ASSERT_EQUAL(true, ifs.good());
                byte_counter += 16 + (pulse.detectorData.size() << 1);
                pulse_counter += 1;
            }

            // Take into account eventual padding
            if (byte_counter < ah.dataSize)
            {
                byte_counter += 2;
                ifs.seekg(2, std::ios_base::cur);
            }
            CPPUNIT_ASSERT_EQUAL(byte_counter, ah.dataSize);

        } while (ifs.good() && ifs.tellg() < std::ios::pos_type(sh.end));
        CPPUNIT_ASSERT_EQUAL(ifs.tellg(), std::ios::pos_type(sh.end));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestAcquisitionData);