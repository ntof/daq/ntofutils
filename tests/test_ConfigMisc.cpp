/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-19T10:48:13+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "ConfigMisc.h"
#include "config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::utils;
using std::string;

class TestConfigMisc : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfigMisc);
    CPPUNIT_TEST(dimDns);
    CPPUNIT_TEST(fileLock);
    CPPUNIT_TEST_SUITE_END();

public:
    bfs::path configPath(const std::string &name)
    {
        return (bfs::path(SRCDIR) / "tests" / "data" / name);
    }

    void dimDns()
    {
        {
            ConfigMisc misc(configPath("config-dimdns.xml").string());
            EQ(std::string("localhost"), misc.getDimDns());
            EQ(int(12345), misc.getDimDnsPort());
        }

        {
            ConfigMisc misc(configPath("config-dimdns2.xml").string());
            EQ(std::string("127.0.0.1"), misc.getDimDns());
            EQ(int(2505), misc.getDimDnsPort());
        }
    }

    void fileLock()
    {
        ConfigMisc misc(configPath("config.xml").string());
        EQ(std::string("/tmp/lock"), misc.lockFile());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfigMisc);
