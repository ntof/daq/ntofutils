/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-01T14:58:01+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <mutex>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Flock.hpp"
#include "NTOFException.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::utils;

class TestFlock : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestFlock);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(stl);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = bfs::temp_directory_path() / "notfutils";
        bfs::create_directories(m_path);
    }

    void tearDown() override { bfs::remove_all(m_path); }

    void simple()
    {
        Flock l(Flock::Shared, (m_path / "lock").string());
        EQ((m_path / "lock").string(), l.fileName());
        EQ(Flock::Shared, l.mode());
        EQ(false, l.isLocked());

        l.lock();
        EQ(true, l.isLocked());

        // already locked should not do anything
        EQ(true, l.try_lock());
        l.lock();

        {
            Flock other(Flock::Shared, (m_path / "lock").string());
            EQ(true, other.try_lock());
        }

        {
            Flock other(Flock::Exclusive, (m_path / "lock").string());
            EQ(false, other.try_lock());
            EQ(false, other.isLocked());
            l.unlock();

            EQ(true, other.try_lock());
            EQ(true, other.isLocked());

            EQ(false, l.try_lock());
        }

        EQ(true, l.try_lock());
    }

    void stl()
    {
        Flock l(Flock::Shared, (m_path / "lock").string());

        {
            std::unique_lock<Flock> locker(l);
            EQ(true, l.isLocked());
            locker.unlock();

            EQ(true, locker.try_lock());
        }
        EQ(false, l.isLocked());

        {
            std::lock_guard<Flock> locker(l);
            EQ(true, l.isLocked());
        }
        EQ(false, l.isLocked());
    }

    void error()
    {
        CPPUNIT_ASSERT_THROW(
            Flock(Flock::Shared, (m_path / "doesnotexist" / "lock").string()),
            ntof::NTOFException);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestFlock);
