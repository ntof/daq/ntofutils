
#include <condition_variable>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Singleton.hpp"

// include this only in cpp file
#include "Singleton.hxx"

using namespace ntof::utils;

class MyTestSingleton : public Singleton<MyTestSingleton>
{
public:
    MyTestSingleton() : value(42) {}

    int value;
};

class MyDeadlockSingleton : public Singleton<MyDeadlockSingleton>
{
public:
    MyDeadlockSingleton()
    {
        std::unique_lock<std::mutex> lock(mutex);
        th = std::thread(&MyDeadlockSingleton::thread_func, this);
        cond.wait(lock); // wait for thread to start
    }

    ~MyDeadlockSingleton()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            cond.notify_all();
        }

        // thread_func will try to create/destroy another singleton, racing for
        // SingletonMutex
        th.join();
    }

    void thread_func()
    {
        std::unique_lock<std::mutex> lock(mutex);
        cond.notify_all(); // unblock constructor
        cond.wait(lock);   // wait destructor

        MyTestSingleton::destroy();
        /* creates/destroy another singleton */
        MyTestSingleton::instance();
    }

    std::mutex mutex;
    std::condition_variable cond;
    std::thread th;
};

class TestSingleton : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSingleton);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(deadlock);
    CPPUNIT_TEST_SUITE_END();

public:
    void tearDown() { MyTestSingleton::destroy(); }

    void simple()
    {
        CPPUNIT_ASSERT_EQUAL(42, MyTestSingleton::instance().value);

        MyTestSingleton::instance().value = 44;
        CPPUNIT_ASSERT_EQUAL(44, MyTestSingleton::instance().value);
        CPPUNIT_ASSERT_EQUAL(&MyTestSingleton::instance(),
                             &MyTestSingleton::instance());

        MyTestSingleton::destroy();
        CPPUNIT_ASSERT_EQUAL(42, MyTestSingleton::instance().value);
    }

    /**
     * @details destructor is waiting on a ressource that requires singleton
     * mutex
     */
    void deadlock()
    {
        MyDeadlockSingleton::instance();
        MyDeadlockSingleton::destroy();
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSingleton);
