/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-24T09:54:32+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMState.h"
#include "DIMStateClient.h"
#include "Signals.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMState : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMState);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        std::unique_ptr<DIMState> state(new DIMState("test"));
        DIMStateClient cli("test");

        state->addStateValue(1, "TEST1");
        state->addStateValue(2, "TEST2");
        state->setValue(1);
        SignalWaiter waiter;
        SignalWaiter errorWaiter;

        waiter.listen(cli.dataSignal);
        errorWaiter.listen(cli.errorSignal);

        EQ(true, waiter.wait(1));
        EQ(int32_t(1), cli.getActualState());

        state->setValue(2);
        EQ(true, waiter.wait(2));
        EQ(int32_t(2), cli.getActualState());

        // depending on dns may have received a noLink at beginning
        errorWaiter.reset();
        state.reset();
        EQ(true, errorWaiter.wait(1));
    }

    void error()
    {
        std::unique_ptr<DIMState> state(new DIMState("test"));
        DIMStateClient cli("test");

        state->addStateValue(1, "TEST1");
        state->setValue(1);
        SignalWaiter waiter;
        SignalWaiter errorWaiter;

        waiter.listen(cli.dataSignal);
        errorWaiter.listen(cli.errorSignal);

        state->addError(-1, "test error");
        state->addWarning(-2, "warn1");
        state->addWarning(-3, "warn2");
        EQ(std::size_t(2), state->warningsCount());
        EQ(std::size_t(1), state->errorsCount());
        EQ(std::size_t(1), state->getErrors().size());
        EQ(std::size_t(2), state->getWarnings().size());
        EQ(DIMState::ErrorCode(-1), state->getErrors()[-1].getCode());

        waiter.wait(
            [&cli]() { return cli.getActualState() == DIMState::ErrorState; });
        EQ(std::size_t(1), cli.getActiveErrors().size());
        EQ(DIMState::ErrorCode(-1), cli.getActiveErrors()[0].getCode());

        EQ(true, state->removeError(-1));
        EQ(false, state->removeError(-1));
        EQ(true, state->removeWarning(-2));
        EQ(false, state->removeWarning(-2));
        waiter.wait([&cli]() { return cli.getActualState() == 1; });

        // depending on dns may have received a noLink at beginning
        errorWaiter.reset();
        state.reset();
        EQ(true, errorWaiter.wait(1));
        EQ(DIMState::NotReadyState, cli.getActualState());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMState);
