/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T14:19:40+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMState.h"
#include "DIMStateClient.h"
#include "DIMUtils.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;

class TestDIMUtils : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMUtils);
    CPPUNIT_TEST(scopedConnection);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void scopedConnection()
    {
        DIMState state("test");
        DIMStateClient cli("test");

        state.addStateValue(1, "test1", false);
        state.addStateValue(2, "test2", false);
        uint32_t data = 0x0;
        {
            std::mutex m;
            std::unique_lock<std::mutex> lock(m);
            std::condition_variable cond;

            scoped_connection conn = cli.dataSignal.connect(
                [&data, &m, &cond](DIMStateClient &) {
                    {
                        std::unique_lock<std::mutex> lock(m);
                        cond.notify_all();
                    }
                    // really important to slow down this one, to ensure that
                    // the scoped_connection really waits for handlers to be
                    // resolved
                    std::this_thread::sleep_for(std::chrono::milliseconds(250));
                    data = 0xDEADBEEF;
                });
            state.setValue(2);
            ASSERT(cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
                   std::cv_status::no_timeout);
        }
        EQ(uint32_t(0xDEADBEEF), data);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMUtils);
