/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-26T11:50:07+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMData.h"
#include "NTOFLogging.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;

class TestNTOFLogging : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNTOFLogging);
    CPPUNIT_TEST(data);
    CPPUNIT_TEST(indent);
    CPPUNIT_TEST(debug);
    CPPUNIT_TEST(flagsguard);
    CPPUNIT_TEST_SUITE_END();

public:
    void data()
    {
        DIMData::List list;
        list.emplace_back();
        list.emplace_back(0, "double", "test", double(12.12));
        list.emplace_back(0, float(12.12));
        list.emplace_back(0, std::string("sample string"));
        list.emplace_back(0, "int", std::string(), int32_t(-42));
        list.emplace_back(0, "long", std::string(), int64_t(-42));
        list.emplace_back(0, "byte", std::string(), int8_t(-42));
        list.emplace_back(0, "short", std::string(), int16_t(42));
        list.emplace_back(0, "bool", std::string(), true);
        list.emplace_back(0, uint32_t(42));
        list.emplace_back(0, uint64_t(42));
        list.emplace_back(0, uint8_t(42));
        list.emplace_back(0, uint16_t(42));
        LOG(INFO) << list;
        LOG(INFO) << ntof::log::debug() << list;
        LOG(INFO) << ntof::log::debug()
                  << DIMData(0, "nested", std::string(), std::move(list));
    }

    void indent()
    {
        std::ostringstream oss;

        ntof::log::IndentGuard indent(2);
        oss << ++indent << 4;
        {
            ntof::log::IndentGuard other(oss, 3); /* init on construct */
            /* since other has already updated oss it shouldn't matter if we use
             * indent or other */
            oss << indent << 1;
        }
        oss << --indent << 2;
        EQ(std::string("    4       1  2"), oss.str());
    }

    void debug()
    {
        /* bug fix */
        std::ostringstream oss;
        oss << ntof::log::debug() << "test";
        EQ(false, ntof::log::DebugFlag::isDebug(oss));
    }

    void flagsguard()
    {
        std::ostringstream oss;
        {
            ntof::log::FlagsGuard flags(oss);
            oss << std::hex << 1234;
        }
        // should be stored in decimal
        oss << 1234;
        EQ(std::string("4d21234"), oss.str());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNTOFLogging);
