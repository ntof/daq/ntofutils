/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-16T17:28:47+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class TestIndexHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestIndexHeader);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST_SUITE_END();

public:
    void dump()
    {
        IndexHeader header;
        std::ostringstream oss;

        header.indexes.emplace_back(1, 0, 1, 1, 1234);
        header.indexes.emplace_back(1, 0, 2, 2, 12345);
        EQ(std::size_t(16 + 24 * 2), header.size());

        oss << header;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>(16 + 24 * 2), data.length());

        EQ(string("INDX"), data.substr(0, 4));
        EQ(string("\x01\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x0C\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words

        EQ(string("\x01\x00\x00\x00", 4), data.substr(4 << 2, 4));
        EQ(string("\x00\x00\x00\x00", 4), data.substr(5 << 2, 4));
        EQ(string("\x01\x00\x00\x00", 4), data.substr(6 << 2, 4));
        EQ(string("\x01\x00\x00\x00", 4), data.substr(7 << 2, 4));
        EQ(string("\xD2\x04\x00\x00", 4), data.substr(8 << 2, 4));

        {
            std::istringstream iss(data);
            IndexHeader headerOut;
            iss >> headerOut;

            oss.str("");
            oss.clear();
            oss << headerOut;

            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());
            EQ(true, iss.good());

            iss >> headerOut;
            EQ(false, iss.good());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(IndexHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestIndexHeader);
