/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:29:47+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "test_helpers.hpp"

#include <chrono>
#include <sstream>
#include <thread>

#include <signal.h>
#include <spawn.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

CPPUNIT_NS_BEGIN

    CPPUNIT_TRAITS_OVERRIDE(ntof::dim::EventReader::Data)
    {
        std::string out;
        std::ostringstream oss(out);

        oss << "EventReader::Data("
            << "timeStamp=" << t.timeStamp << ",cycleStamp=" << t.cycleStamp
            << ",cycleNb=" << t.cycleNb << ",evtType=" << t.evtType
            << ",dest=" << t.dest << ",dest2=" << t.dest2
            << ",evtNumber=" << t.evtNumber << ")";
        return out;
    }

    CPPUNIT_TRAITS_OVERRIDE(std::future_status)
    {
        std::string out;
        std::ostringstream oss(out);

        oss << "std::future_status(";
        switch (t)
        {
        case std::future_status::ready: oss << "ready)"; break;
        case std::future_status::timeout: oss << "timeout)"; break;
        case std::future_status::deferred: oss << "deferred)"; break;
        };
        return out;
    }

CPPUNIT_NS_END

pid_t aspawn(const char *const argv[])
{
    pid_t pid;
    posix_spawn_file_actions_t actions;

    if (posix_spawn_file_actions_init(&actions) != 0)
        return -1;
    posix_spawn_file_actions_addclose(&actions, STDIN_FILENO);

    if (posix_spawn(&pid, argv[0], &actions, NULL, (char *const *) (argv),
                    environ) != 0)
    {
        pid = -1;
    }
    posix_spawn_file_actions_destroy(&actions);
    return pid;
}

void terminate(pid_t pid)
{
    int stat_loc;
    if (pid < 0)
        return;

    kill(pid, SIGTERM);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    if (waitpid(pid, &stat_loc, WNOHANG) == -1)
    {
        kill(pid, SIGKILL);
        waitpid(pid, &stat_loc, 0);
    }
}
