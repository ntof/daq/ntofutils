/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-24T13:42:53+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class TestAcquisitionHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAcquisitionHeader);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST_SUITE_END();

public:
    void dump()
    {
        AcquisitionHeader header;
        std::ostringstream oss;

        header.dataSize = 0x142424242; /* large on purpose */
        header.setDetectorType("BAF2");
        header.detectorId = 0xDEADBEEF;
        header.setLocation(0x11, 0x12, 0x13, 0x14);

        oss << header;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>(AcquisitionHeader::SIZE), data.length());

        EQ(string("ACQC"), data.substr(0, 4));
        EQ(string("\x02\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x94\x90\x90\x50", 4), data.substr(3 << 2, 4)); // words
        EQ(string("BAF2"), data.substr(4 << 2, 4)); // detectorType
        EQ(string("\xEF\xBE\xAD\xDE", 4), data.substr(5 << 2, 4)); // detectorId
        EQ(string("\x14\x13\x12\x11", 4),
           data.substr(6 << 2, 4)); // Channel/Module/Chassis/Stream

        {
            std::istringstream iss(data);
            AcquisitionHeader ACQCheader;
            iss >> ACQCheader;
            EQ(true, iss.good());

            oss.clear();
            oss.str("");
            oss << ACQCheader;
            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());

            iss >> ACQCheader;
            EQ(true, iss.eof());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(AcquisitionHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestAcquisitionHeader);
