/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-24T09:54:32+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <atomic>
#include <memory>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMParamList.h"
#include "DIMParamListClient.h"
#include "NTOFException.h"
#include "Signals.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMParamList : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMParamList);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(enums);
    CPPUNIT_TEST(nested);
    CPPUNIT_TEST(lockRef);
    CPPUNIT_TEST(hidden);
    CPPUNIT_TEST(loop);
    CPPUNIT_TEST(addParameter_lvalue);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        DIMParamListClient cli("test");
        DIMParamList dataset("test");

        uint32_t idx = 1; // parameter zero is reserved
        dataset.addParameter(idx++, "int8_t", "", int8_t(42), AddMode::CREATE,
                             false);

        DIMEnum e;
        e.addItem(0, "test");
        e.addItem(1, "another");
        dataset.addParameter(idx++, "enum", "", e, AddMode::CREATE, true);

        cli.setParameter(1, int8_t(24));
        EQ(int8_t(24), dataset.getParameterAt(1).getByteValue());

        CPPUNIT_ASSERT_THROW(cli.setParameter(1, int32_t(42)), DIMException);
    }

    void enums()
    {
        DIMParamListClient cli("test");
        DIMParamList dataset("test");

        DIMEnum e;
        e.addItem(0, "test");
        e.addItem(1, "another");
        e.setValue(0);
        dataset.addParameter(1, "enum", "", e, AddMode::CREATE, true);
        EQ(int32_t(0), dataset.getParameterAt(1).getEnumValue().getValue());
        EQ(std::string("test"),
           dataset.getParameterAt(1).getEnumValue().getName());

        DIMAck ack = cli.setParameter(1, DIMEnum(1));
        EQ(DIMAck::OK, ack.getStatus());

        DIMData data = dataset.getParameterAt(1);
        EQ(int32_t(1), data.getEnumValue().getValue());
        EQ(std::string("another"), data.getEnumValue().getName());
    }

    void nested()
    {
        DIMParamListClient cli("test");
        DIMParamList dataset("test");
        SignalWaiter waiter;

        {
            DIMData::List childs;
            childs.push_back(DIMData(0, uint16_t(12)));
            childs.push_back(DIMData(1, childs)); // nested in nested, must work
            childs.push_back(DIMData(2, uint16_t(12)));

            dataset.addParameter(1, "nested", "", std::move(childs),
                                 AddMode::CREATE, true);
        }

        DIMData::List update;
        update.push_back(DIMData(2, uint16_t(42)));
        DIMAck ack = cli.setParameter(1, update);
        EQ(DIMAck::OK, ack.getStatus());
        DIMData data = dataset.getParameterAt(1);
        EQ(std::size_t(3), data.getNestedValue().size());
        EQ(uint16_t(42), data.getNestedValue()[2].getUShortValue());
    }

    void lockRef()
    {
        DIMParamList dataset("test");
        dataset.addParameter(1, "nested", "", DIMData::List(), AddMode::CREATE,
                             false);
        dataset.lockParameterAt("nested")->addNestedData<int32_t>(0, "test", "",
                                                                  42);
        dataset.updateList();

        SignalWaiter waiter;
        DIMParamListClient cli("test");
        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(1));
        EQ(int32_t(42),
           cli.getParameters().at(1).getNestedValue()[0].getValue<int32_t>());

        dataset.lockParameterAt(1)->getNestedValue().at(0).setValue(int32_t(12));
        dataset.updateList();
        EQ(true, waiter.wait(2));
        EQ(int32_t(12),
           cli.getParameters().at(1).getNestedValue()[0].getValue<int32_t>());
    }

    void hidden()
    {
        DIMParamListClient cli("test");
        DIMParamList dataset("test");
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);

        dataset.addParameter(1, "test", "", 42);
        EQ(true,
           waiter.wait([&cli]() { return cli.getParameters().size() == 2; }));

        dataset.setHidden(1);
        EQ(true,
           waiter.wait([&cli]() { return cli.getParameters().size() == 1; }));
    }

    void loop()
    {
        std::atomic<bool> running(true);
        std::thread t([&running]() {
            DIMParamList list("test");

            int i = 42;
            list.addParameter(1, "test", "", i);
            while (running)
            {
                list.setValue(1, i++);
                std::this_thread::sleep_for(std::chrono::microseconds(10));
            }
        });

        const int num_loop = 5000;
        for (int i = 0; i < num_loop; ++i)
        {
            // create/destroy this object to subcscribe/unsubscribe services
            DIMParamListClient cli("test");

            std::this_thread::sleep_for(std::chrono::microseconds(1000));
        }

        running = false;
        t.join();
    }

    void addParameter_lvalue()
    {
        /*
        ensure that lvalue is being used when calling addData
        template operator overloading has a weird behavior regarding that.
        */
        DIMParamList params("test");

        DIMData::List list;
        list.emplace_back(0, "test", "test", 42);
        params.addParameter(1, "test", "test", list, AddMode::CREATE);
        // If move operator is called this will not be true
        EQ(std::size_t(1), list.size());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMParamList);
