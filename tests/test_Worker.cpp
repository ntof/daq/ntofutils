/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-26T11:26:25+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <mutex>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Worker.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;

class TestTask : public Worker::Task
{
public:
    TestTask(int &result, std::mutex &lock) :
        value(result), m_lock(lock), m_delay(0)
    {}

    void setDelay(const std::chrono::milliseconds &delay) { m_delay = delay; }

protected:
    int &value;
    std::mutex &m_lock;
    std::chrono::milliseconds m_delay;

    void run() override
    {
        if (m_delay.count())
            std::this_thread::sleep_for(m_delay);

        const std::lock_guard<std::mutex> lock(m_lock);
        value += 1;
    }
};

class TestWorker : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestWorker);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(constTask);
    CPPUNIT_TEST(abort);
    CPPUNIT_TEST(abortPending);
    CPPUNIT_TEST(funTask);
    CPPUNIT_TEST(funTaskError);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        Worker worker("TestWorker", 5, 2000);
        std::mutex lock;
        int value = 0;
        std::vector<Worker::SharedTask> tasks;

        for (std::size_t i = 0; i < 500; ++i)
        {
            tasks.push_back(Worker::SharedTask(new TestTask(value, lock)));
            worker.post(tasks.back());
        }

        for (Worker::SharedTask &task : tasks)
        {
            EQ(true, task->wait(1000));
        }
        EQ(int(tasks.size()), value);
    }

    void constTask()
    {
        Worker worker("TestWorker", 5, 2000);
        std::mutex lock;
        int value = 0;
        std::vector<Worker::SharedTask> tasks;

        /* the shared pointer is a temporary const */
        Worker::SharedTask task = worker.post(
            Worker::SharedTask(new TestTask(value, lock)));
        EQ(true, task->wait(1000));
    }

    void abort()
    {
        Worker worker("TestWorker", 5, 2000);
        std::mutex lock;
        int value = 0;

        {
            TestTask task(value, lock);
            EQ(true, task.abort());
            EQ(Worker::Task::ABORTED, task.error());
            EQ(Worker::Task::FINISHED, task.state());
        }

        Worker::SharedTask task(new TestTask(value, lock));
        static_cast<TestTask &>(*task).setDelay(std::chrono::milliseconds(10));
        worker.post(task);

        while (task->state() == Worker::Task::READY)
            std::this_thread::sleep_for(std::chrono::milliseconds(1));

        EQ(Worker::Task::RUNNING, task->state());
        EQ(false, task->abort());
    }

    void abortPending()
    {
        Worker worker("TestWorker", 5, 2000);
        std::mutex lock;
        int value = 0;
        std::vector<Worker::SharedTask> tasks;

        for (std::size_t i = 0; i < 500; ++i)
        {
            TestTask *task = new TestTask(value, lock);
            task->setDelay(std::chrono::milliseconds(1));
            tasks.push_back(Worker::SharedTask(task));
            worker.post(tasks.back());
        }

        for (Worker::SharedTask &task : tasks)
        {
            task->abort();
        }

        for (Worker::SharedTask &task : tasks)
        {
            EQ(true, task->wait(1000));
        }
        ASSERT(std::size_t(value) < tasks.size());
    }

    void funTask()
    {
        Worker worker("TestWorker", 5, 2000);
        std::mutex lock;
        int value = 0;
        std::vector<Worker::SharedTask> tasks;
        std::function<void()> fun = [&lock, &value] {
            const std::lock_guard<std::mutex> locker(lock);
            value += 1;
        };

        for (std::size_t i = 0; i < 500; ++i)
        {
            tasks.push_back(worker.post(fun));
        }

        for (Worker::SharedTask &task : tasks)
        {
            EQ(true, task->wait(1000));
        }
        EQ(int(tasks.size()), value);
    }

    void funTaskError()
    {
        Worker worker("TestWorker", 1, 200);
        Worker::SharedTask task = worker.post(
            [] { throw std::string("this is an error"); });

        EQ(true, task->wait(1000));
        EQ(Worker::Task::FAILED, task->error());
        EQ(std::string("this is an error"), task->errorString());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestWorker);
