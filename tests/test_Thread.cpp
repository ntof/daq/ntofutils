/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-06T16:01:02+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferari.1@cern.ch>
**
*/

#include <mutex>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Thread.hpp"
#include "easylogging++.h"
#include "test_helpers.hpp"

using namespace ntof::utils;

class ImplThread : public ntof::utils::Thread
{
public:
    ImplThread() : counter_enter_exit(0), counter(0) {};
    ~ImplThread() override = default;

    void thread_enter() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        LOG(INFO) << "[ImplThread] incrementing counter_enter_exit. "
                  << " From " << counter_enter_exit << " to "
                  << (counter_enter_exit + 1);
        counter_enter_exit++;
    }

    void thread_func() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        LOG(INFO) << "[ImplThread] thread_func incrementing counter. "
                  << " From " << counter << " to " << (counter + 1);
        counter++;
        ext_cond.notify_all();
    }

    void thread_exit() override { thread_enter(); }

    uint32_t counter_enter_exit;
    uint32_t counter;
    std::condition_variable ext_cond;
    std::mutex mutex;
};

class TestThread : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestThread);
    CPPUNIT_TEST(wakeMode);
    CPPUNIT_TEST(pollingMode);
    CPPUNIT_TEST(runMode);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override {}

    void tearDown() override {}

    void wakeMode()
    {
        ImplThread th;
        std::unique_lock<std::mutex> lock(th.mutex);
        EQ((uint32_t) 0, th.counter_enter_exit);
        EQ((uint32_t) 0, th.counter);
        th.start();
        ASSERT(th.ext_cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
               std::cv_status::no_timeout);
        EQ((uint32_t) 1, th.counter);
        th.wake();
        ASSERT(th.ext_cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
               std::cv_status::no_timeout);
        EQ((uint32_t) 2, th.counter);
    }

    void pollingMode()
    {
        ImplThread th;
        std::unique_lock<std::mutex> lock(th.mutex);
        th.setInterval(std::chrono::milliseconds(10));
        EQ((uint32_t) 0, th.counter_enter_exit);
        EQ((uint32_t) 0, th.counter);
        th.start();

        for (uint32_t saveCounter = th.counter; saveCounter < 5;
             saveCounter = th.counter)
        {
            ASSERT(th.ext_cond.wait_for(lock, std::chrono::milliseconds(2000)) ==
                   std::cv_status::no_timeout);
            EQ((uint32_t) saveCounter + 1, th.counter);
        }
        th.setPriority(SCHED_FIFO, 10);
    }

    void runMode()
    {
        ImplThread th;
        EQ((uint32_t) 0, th.counter_enter_exit);
        EQ((uint32_t) 0, th.counter);
        std::thread stopper([&th]() {
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            LOG(INFO) << "Stopping the thread...";
            th.stop();
        });
        th.run();
        stopper.join();
        EQ((uint32_t) 2, th.counter_enter_exit);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestThread);
