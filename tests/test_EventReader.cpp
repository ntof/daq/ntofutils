/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-07T14:12:18+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <future>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <pugixml.hpp>

#include "EventReader.h"
#include "Timestamp.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestEventHandler : public EventHandler
{
public:
    virtual void eventReceived(const EventReader::Data &event);
    virtual void noLink();

    std::string m_name;
};

void TestEventHandler::eventReceived(const EventReader::Data &event)
{
    m_name = event.dest;
}

void TestEventHandler::noLink() {}

class TestEventReader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestEventReader);
    CPPUNIT_TEST(eventDataXML);
    CPPUNIT_TEST(consumeEvents);
    CPPUNIT_TEST(registerHandler);
    CPPUNIT_TEST(marginLimit);
    CPPUNIT_TEST(spuriousEvent);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void eventDataXML()
    {
        EventReader::Data data;
        pugi::xml_document doc;
        pugi::xml_node node = doc.append_child("event");

        CPPUNIT_ASSERT(!data.isValid());
        CPPUNIT_ASSERT(!data.toXML(node));
        data.timeStamp = 1234;
        data.cycleStamp = 1234;
        data.cycleNb = 0;
        data.evtNumber = 42;

        CPPUNIT_ASSERT(data.toXML(node));

        EventReader::Data data2;
        CPPUNIT_ASSERT(data2.fromXML(node));
        CPPUNIT_ASSERT_EQUAL(data, data2);
    }

    EventReader::Data makeEvent(const std::string &name)
    {
        static int64_t num = 0;

        EventReader::Data data;
        data.evtType = "PRIMARY";
        data.dest = name;
        data.dest2 = "None";
        data.timeStamp = Timestamp() * 1000;
        data.cycleStamp = data.timeStamp;
        data.cycleNb = 0;
        data.evtNumber = ++num;
        data.lsaCycle = "cycleName";
        data.user = "userName";
        return data;
    }

    /**
     * check that events are consumed by reader.getEventByTime
     */
    void consumeEvents()
    {
        EventReader reader;
        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            makeEvent("test").toXML(node);

            reader.dataReceived(doc, 0);
        }

        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            makeEvent("test2").toXML(node);

            reader.dataReceived(doc, 0);
        }

        EventReader::Data data;
        CPPUNIT_ASSERT(reader.getEventByTime(data, Timestamp(), 1));
        CPPUNIT_ASSERT_EQUAL(std::string("test"), data.dest);

        /* ensure that second event is test2 */
        CPPUNIT_ASSERT(reader.getEventByTime(data, Timestamp(), 1));
        CPPUNIT_ASSERT_EQUAL(std::string("test2"), data.dest);
    }

    void registerHandler()
    {
        EventReader reader;
        TestEventHandler hdlr;
        reader.registerHandler(&hdlr);

        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            makeEvent("test").toXML(node);

            reader.dataReceived(doc, 0);
        }
        CPPUNIT_ASSERT_EQUAL(std::string("test"), hdlr.m_name);

        reader.removeHandler(&hdlr);
        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            makeEvent("test2").toXML(node);

            reader.dataReceived(doc, 0);
        }
        /* no changes */
        CPPUNIT_ASSERT_EQUAL(std::string("test"), hdlr.m_name);
    }

    /**
     * @details event withing margins exist but is older than requested time
     */
    void marginLimit()
    {
        EventReader reader;
        EventReader::Data event = makeEvent("test");
        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            event.toXML(node);

            reader.dataReceived(doc, 0);
        }

        EventReader::Data data;
        CPPUNIT_ASSERT(reader.getEventByTime(
            data,
            Timestamp(event.timeStamp / 1000) - (reader.getTimeMargin() - 1)));
        CPPUNIT_ASSERT_EQUAL(std::string("test"), data.dest);
    }

    /**
     * @details non-matching event arrives whilst waiting
     */
    void spuriousEvent()
    {
        EventReader reader;
        EventReader::Data data;
        EventReader::Data event = makeEvent("test");
        std::future<bool> future = std::async(
            std::launch::async, [&data, &reader, &event]() {
                return reader.getEventByTime(data,
                                             Timestamp(event.timeStamp / 1000));
            });

        // wait for thread to start and settle
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        EventReader::Data another = event;
        another.timeStamp -= (reader.getTimeMargin() + 1) * 1000;
        another.cycleStamp = another.timeStamp;
        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            another.toXML(node);
            reader.dataReceived(doc, 0);
        }

        // wait for thread to process event
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("timing").append_child(
                "event");
            event.toXML(node);
            reader.dataReceived(doc, 0);
        }
        // default timeout 200ms

        EQ(std::future_status::ready,
           future.wait_for(std::chrono::milliseconds(300)));
        CPPUNIT_ASSERT_EQUAL(std::string("test"), data.dest);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEventReader);
