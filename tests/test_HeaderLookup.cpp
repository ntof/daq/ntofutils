/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-22T13:40:16+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;

class TestHeaderLookup : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestHeaderLookup);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        /* this class is already tested in different places, let's make simple
         * tests */
        {
            std::stringstream iss;
            HeaderLookup lookup;
            iss >> lookup;
            EQ(false, bool(iss));
            EQ(true, iss.eof());
        }

        {
            std::stringstream iss;
            HeaderType type = 0x42424242;
            HeaderLookup lookup;

            iss.write(reinterpret_cast<const char *>(&type), sizeof(HeaderType));
            iss.seekg(0);

            iss >> lookup;
            EQ(true, bool(iss));
            EQ(std::stringstream::pos_type(0), iss.tellg());
            EQ(HeaderType(0x42424242), lookup.type);
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestHeaderLookup);
