
#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

static_assert(sizeof(ChannelConfig) == 22 * 4 - 20,
              "Wrong size for ChannelConfig");

class TestRunControlHeader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRunControlHeader);
    CPPUNIT_TEST(config);
    CPPUNIT_TEST(dump);
    CPPUNIT_TEST(updateDateTime);
    CPPUNIT_TEST(updateExperiment);
    CPPUNIT_TEST_SUITE_END();

public:
    void config()
    {
        RunControlHeader header;

        header.setExperiment("EAR1");
        EQ(static_cast<uint32_t>(0x31524145), header.experiment);
        EQ(string("EAR1"), header.getExperiment());

        header.numberOfChannelsPerModule.resize(4);
        EQ(static_cast<uint32_t>(4), header.numberOfModules());
        EQ(static_cast<uint32_t>(0), header.numberOfChannels());

        header.numberOfChannelsPerModule[2] = 4;
        header.numberOfChannelsPerModule[3] = 1;
        EQ(static_cast<uint32_t>(4), header.numberOfModules());
        EQ(static_cast<uint32_t>(5), header.numberOfChannels());
    }

    void dump()
    {
        RunControlHeader header;
        std::ostringstream oss;

        header.setExperiment("EAR1");
        header.runNumber = 0xFEEDFEED;
        header.segmentNumber = 0xDEADBEEF;
        header.totalNumberOfModules = 42;
        header.numberOfChannelsPerModule.resize(4);
        header.numberOfChannelsPerModule[2] = 4;
        header.numberOfChannelsPerModule[3] = 1;

        oss << header;
        EQ(true, oss.good());
        string data(oss.str());
        EQ(static_cast<std::size_t>((19) << 2), data.length());

        EQ(string("RCTR"), data.substr(0, 4));
        EQ(string("\x03\x00\x00\x00", 4), data.substr(1 << 2, 4)); // rev
        EQ(string("\x00\x00\x00\x00", 4), data.substr(2 << 2, 4)); // reserved
        EQ(string("\x0F\x00\x00\x00", 4), data.substr(3 << 2, 4)); // words
        EQ(string("\xED\xFE\xED\xFE", 4), data.substr(4 << 2, 4));
        EQ(string("\xEF\xBE\xAD\xDE"), data.substr(5 << 2, 4));

        EQ(string("EAR1"), data.substr(7 << 2, 4));

        EQ(string("\x04\x00\x00\x00", 4),
           data.substr(10 << 2, 4)); // modules in stream
        EQ(string("\x05\x00\x00\x00", 4),
           data.substr(11 << 2, 4)); // channels in stream

        EQ(string("\x04\x00\x00\x00", 4),
           data.substr(17 << 2, 4)); // chans on module[2]

        {
            std::istringstream iss(data);
            RunControlHeader headerOut;
            iss >> headerOut;

            oss.str("");
            oss.clear();
            oss << headerOut;

            EQ(data.size(), oss.str().size());
            EQ(data, oss.str());
            EQ(true, iss.good());

            iss >> headerOut;
            EQ(false, iss.good());
        }

        {
            /* test lookahead */
            HeaderLookup lookup;
            std::istringstream iss(data);
            iss >> lookup;

            EQ(true, iss.good());
            EQ(RunControlHeader::TITLE, lookup.type);
            EQ(std::istringstream::pos_type(0), iss.tellg());
        }
    }

    void updateDateTime()
    {
        RunControlHeader rctr;

        EQ(static_cast<uint32_t>(0), rctr.date);
        EQ(static_cast<uint32_t>(0), rctr.time);

        rctr.updateDateTime();
        ASSERT(rctr.date != 0);
        ASSERT(rctr.time != 0);

        std::time_t t = std::time(nullptr);
        rctr.updateDateTime(t);
        EQ(static_cast<uint32_t>(t % 60), rctr.time % 100);
        // Note: this will fail on some very specific timezones
        EQ(static_cast<uint32_t>(t / 60 % 60), rctr.time / 100 % 100);
    }

    void updateExperiment()
    {
        RunControlHeader rctr;

        rctr.updateExperiment();
        ASSERT(rctr.getExperiment().empty());

        rctr.runNumber = 100001;
        rctr.updateExperiment();
        EQ(std::string("EAR1"), rctr.getExperiment());

        rctr.runNumber = 200001;
        rctr.updateExperiment();
        EQ(std::string("EAR2"), rctr.getExperiment());

        rctr.runNumber = 900001;
        rctr.updateExperiment();
        EQ(std::string("LAB"), rctr.getExperiment());

        rctr.runNumber = 700001;
        rctr.updateExperiment();
        EQ(std::string(), rctr.getExperiment());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRunControlHeader);
