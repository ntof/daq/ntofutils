/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-15T18:39:24+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMXMLInfo.h"
#include "DIMXMLService.h"
#include "test_helpers.hpp"

using namespace ntof::dim;

class TestDIMXMLInfo : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMXMLInfo);
    CPPUNIT_TEST(dataSignal);
    CPPUNIT_TEST(errorSignal);
    CPPUNIT_TEST(emptyService);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void dataSignal()
    {
        std::condition_variable cond;
        std::mutex mutex;
        DIMXMLService service("/SRV");

        {
            pugi::xml_document doc;
            pugi::xml_node root = doc.append_child("test");
            root.append_attribute("value").set_value(42);
            service.setData(doc);
        }

        DIMXMLInfo info;
        pugi::xml_document result;
        std::unique_lock<std::mutex> lock(mutex);

        info.dataSignal.connect(
            [&result, &mutex, &cond](pugi::xml_document &doc, DIMXMLInfo &) {
                result.reset(doc);
                std::unique_lock<std::mutex> lock(mutex);
                cond.notify_all();
            });
        info.subscribe("/SRV");

        ASSERT(std::cv_status::no_timeout ==
               cond.wait_for(lock, std::chrono::seconds(2)));
        EQ(int(42), result.child("test").attribute("value").as_int(0));
    }

    void errorSignal()
    {
        std::condition_variable cond;
        std::mutex mutex;
        DIMXMLInfo info;
        std::string error;
        std::unique_lock<std::mutex> lock(mutex);

        info.errorSignal.connect(
            [&error, &mutex, &cond](const std::string &message, DIMXMLInfo &) {
                error = message;
                std::unique_lock<std::mutex> lock(mutex);
                cond.notify_all();
            });
        info.subscribe("/SRV", 1);

        ASSERT(std::cv_status::no_timeout ==
               cond.wait_for(lock, std::chrono::seconds(3)));
        EQ(std::string("No link"), error);
        EQ(DIMXMLInfo::NoLinkError, error);
    }

    void emptyService()
    {
        /* should not trigger an error but send an empty document */
        std::condition_variable cond;
        std::mutex mutex;
        int errors = 0;
        DIMXMLService service("/SRV");

        DIMXMLInfo info;
        std::unique_lock<std::mutex> lock(mutex);

        info.dataSignal.connect(
            [&mutex, &cond](pugi::xml_document &doc, DIMXMLInfo &) {
                EQ(false, bool(doc.document_element()));
                std::unique_lock<std::mutex> lock(mutex);
                cond.notify_all();
            });
        info.errorSignal.connect(
            [&errors](const std::string &, DIMXMLInfo &) { ++errors; });

        info.subscribe("/SRV", 1);

        ASSERT(std::cv_status::no_timeout ==
               cond.wait_for(lock, std::chrono::seconds(3)));
        EQ(0, errors);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMXMLInfo);
