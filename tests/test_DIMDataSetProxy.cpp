/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-09T23:02:24+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DIMDataSet.h"
#include "DIMDataSetClient.h"
#include "Signals.hpp"
#include "proxy/DIMDataSetProxy.hpp"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::utils;

class TestDIMDataSetProxy : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDIMDataSetProxy);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(disconnect);
    CPPUNIT_TEST(singleShot);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() { m_dim.reset(new DimTestHelper()); }

    void tearDown() { m_dim.reset(); }

    void simple()
    {
        DIMDataSetProxy proxy("testMirror", "test");
        DIMDataSetClient cli("testMirror");
        DIMDataSet dataset("test");
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(1));

        dataset.addData<uint32_t>(0, "nested", "", 42, AddMode::CREATE);
        proxy.setSyncing();

        EQ(true, waiter.wait(2));
        std::vector<DIMData> data = cli.getLatestData();
        EQ(std::size_t(1), data.size());
        EQ(uint32_t(42), data[0].getValue<uint32_t>());

        dataset.setValue<uint32_t>(0, 44);
        EQ(true, waiter.wait(3));
        data = cli.getLatestData();
        EQ(std::size_t(1), data.size());
        EQ(uint32_t(44), data[0].getValue<uint32_t>());
    }

    void disconnect()
    {
        DIMDataSet dataset("test");
        DIMDataSetProxy proxy("testMirror", "test");
        DIMDataSetClient cli("testMirror");
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(1));

        dataset.addData<uint32_t>(0, "nested", "", 42, AddMode::CREATE);
        proxy.syncSignal.connect(
            [](DIMDataSetProxy &proxy) { proxy.disconnect(); });
        proxy.setSyncing();

        EQ(true, waiter.wait(2));
        std::vector<DIMData> data = cli.getLatestData();
        EQ(std::size_t(1), data.size());

        EQ(uint32_t(42), data[0].getValue<uint32_t>());
        EQ(false, proxy.isSyncing());

        /* not syncing */
        dataset.setValue<uint32_t>(0, 44);
        EQ(false, waiter.wait(3));

        proxy.setSyncing();
        EQ(true, waiter.wait(3));
        EQ(false, proxy.isSyncing());
    }

    void singleShot()
    {
        DIMDataSetProxy proxy("testMirror", "test");
        DIMDataSetClient cli("testMirror");
        DIMDataSet dataset("test");
        SignalWaiter waiter;

        waiter.listen(cli.dataSignal);
        EQ(true, waiter.wait(1));

        dataset.addData<uint32_t>(0, "nested", "", 42, AddMode::CREATE);
        proxy.syncSignal.connect(
            [](DIMDataSetProxy &proxy) { proxy.setSyncing(false); });
        proxy.setSyncing();

        EQ(true, waiter.wait(2));
        std::vector<DIMData> data = cli.getLatestData();
        EQ(std::size_t(1), data.size());

        EQ(uint32_t(42), data[0].getValue<uint32_t>());
        EQ(false, proxy.isSyncing());

        /* not syncing */
        dataset.setValue<uint32_t>(0, 44);
        EQ(false, waiter.wait(3));

        proxy.setSyncing();
        EQ(true, waiter.wait(3));
        EQ(false, proxy.isSyncing());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDIMDataSetProxy);
