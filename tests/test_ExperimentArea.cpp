
#include <cstring>
#include <sstream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "DaqTypes.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using std::string;

class TestExperimentArea : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestExperimentArea);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        EQ(std::string("EAR1"), ExperimentArea::findArea("EAR1").name);
        EQ(std::string("EAR1"),
           ExperimentArea::findArea(ExperimentArea::EAR1.min).name);
        EQ(std::string("EAR1"),
           ExperimentArea::findArea(ExperimentArea::EAR1.max).name);

        EQ(std::string("EAR2"), ExperimentArea::findArea("EAR2").name);
        EQ(std::string("EAR2"),
           ExperimentArea::findArea(ExperimentArea::EAR2.min).name);
        EQ(std::string("EAR2"),
           ExperimentArea::findArea(ExperimentArea::EAR2.max).name);

        EQ(std::string("LAB"), ExperimentArea::findArea("LAB").name);
        EQ(std::string("LAB"),
           ExperimentArea::findArea(ExperimentArea::LAB.min).name);
        EQ(std::string("LAB"),
           ExperimentArea::findArea(ExperimentArea::LAB.max).name);

        EQ(std::string(""), ExperimentArea::findArea("PWET").name);
        EQ(std::string(""), ExperimentArea::findArea(0).name);
        EQ(std::string(""),
           ExperimentArea::findArea(std::numeric_limits<uint32_t>::max()).name);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestExperimentArea);
