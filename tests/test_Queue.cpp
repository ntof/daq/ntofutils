
#include <chrono>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "NTOFException.h"
#include "Queue.h"
#include "Thread.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;

class TestQueueElement
{
public:
    explicit TestQueueElement(int value) : value(value) { s_count += 1; }
    TestQueueElement(const TestQueueElement &other) : value(other.value)
    {
        s_count += 1;
    }

    TestQueueElement &operator=(const TestQueueElement &) = default;

    ~TestQueueElement() { s_count -= 1; }

    int value;
    static std::size_t s_count;
};
std::size_t TestQueueElement::s_count = 0;

class TestQueueZCPElement
{
public:
    explicit TestQueueZCPElement(int value) : value(value) {}
    TestQueueZCPElement(TestQueueZCPElement &&) = default;
    TestQueueZCPElement &operator=(TestQueueZCPElement &&) = default;

    ~TestQueueZCPElement() {}

    int value;

private:
    TestQueueZCPElement(const TestQueueZCPElement &other) = default;
    TestQueueZCPElement &operator=(const TestQueueZCPElement &) = default;
};

class TestQueueThread : public ntof::utils::Thread
{
public:
    TestQueueThread(Queue<TestQueueElement> &queue) :
        m_queue(queue), m_delay(0), m_count(0)
    {}

    void thread_func() override
    {
        try
        {
            while (true)
            {
                TestQueueElement elt = m_queue.pop();
                ++m_count;

                if (m_delay.count())
                    std::this_thread::sleep_for(m_delay);
            }
        }
        catch (ntof::NTOFException &ex)
        {
            /* time to die */
        }
    }

    inline std::size_t count() const { return m_count; }

    inline void setDelay(const std::chrono::milliseconds &duration)
    {
        m_delay = duration;
    }

protected:
    Queue<TestQueueElement> &m_queue;
    std::chrono::milliseconds m_delay;
    std::size_t m_count;
};

class TestQueue : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestQueue);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(allocated);
    CPPUNIT_TEST(threaded);
    CPPUNIT_TEST(moveConstruct);
    CPPUNIT_TEST(zeroCopy);
    CPPUNIT_TEST(flush);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        Queue<TestQueueElement> queue("testQueue", 2);

        queue.post(TestQueueElement(12));
        queue.post(TestQueueElement(42));

        CPPUNIT_ASSERT_THROW_MESSAGE("should throw",
                                     queue.post(TestQueueElement(84)),
                                     ntof::NTOFException);

        EQ(12, queue.pop().value);
        EQ(42, queue.pop().value);

        {
            /* use copy method */
            TestQueueElement elt(22);
            queue.post(elt);
            EQ(22, queue.pop().value);
        }

        EQ(std::size_t(0), TestQueueElement::s_count);
    }

    void allocated()
    {
        Queue<TestQueueElement *> queue("testQueue", 2);

        queue.post(new TestQueueElement(12));
        queue.post(new TestQueueElement(42));

        CPPUNIT_ASSERT_THROW_MESSAGE("should throw",
                                     queue.post(new TestQueueElement(84)),
                                     ntof::NTOFException);

        std::unique_ptr<TestQueueElement> ret(queue.pop());

        EQ(true, bool(ret));
        EQ(12, ret->value);

        ret.reset(queue.pop());
        EQ(true, bool(ret));
        EQ(42, ret->value);

        ret.reset();
        EQ(std::size_t(0), TestQueueElement::s_count);
    }

    void threaded()
    {
        Queue<TestQueueElement> queue("testQueue", 2048);
        TestQueueThread thread[2] = {TestQueueThread(queue),
                                     TestQueueThread(queue)};
        thread[0].start();
        thread[1].start();

        for (int i = 0; i < 2048; ++i)
        {
            queue.post(TestQueueElement(i));
        }

        while (queue.count() != 0)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        queue.clear(true);
        EQ(std::size_t(2048), thread[0].count() + thread[1].count());
    }

    void moveConstruct()
    {
        /* ensure that this object can be rvalue constructed */
        Queue<int> queue = Queue<int>("test", 10);
    }

    void zeroCopy()
    {
        /* this won't even compile if zcp was not supported */
        Queue<TestQueueZCPElement> queue("testQueue", 10);
        queue.post(TestQueueZCPElement(42));

        TestQueueZCPElement elt = queue.pop();
    }

    void flush()
    {
        /* ensure that even if the queue is not waiting it'll exit on explicit
         * flush */
        Queue<TestQueueElement> queue("testQueue", 2048);
        TestQueueThread thread(queue);
        thread.setDelay(std::chrono::milliseconds(100));
        thread.start();

        for (int i = 0; i < 10; ++i)
        {
            queue.post(TestQueueElement(i));
        }

        while (queue.count() == 10)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        /* we're sure that the thread is sleeping on its sleep_for statement */

        queue.clear(true);
        /* test will deadlock if thread was not properly released */
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestQueue);
