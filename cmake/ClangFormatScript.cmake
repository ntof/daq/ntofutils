# Arguments: SRCDIRS (list of directories to analyse)
separate_arguments(SRCDIRS)
string(TOLOWER "${MODE}" MODE)

set(S "${CMAKE_CURRENT_LIST_DIR}/..")
set(B "${CMAKE_BINARY_DIR}")

find_program(CLANG_FORMAT_EXE NAMES cquery-clang-format clang-format
    HINTS /opt/cquery/bin
    PATHS ${CLANG_FORMAT_EXE} ENV PATH $ENV{CLANGPP_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)

foreach(F IN LISTS SRCDIRS)
    list(APPEND GLOB_PATTERN "${S}/${F}/*.cpp" "${S}/${F}/*.hpp")
    list(APPEND GLOB_PATTERN "${S}/${F}/*.cc" "${S}/${F}/*.hh" "${S}/${F}/*.h")
endforeach()
file(GLOB_RECURSE SRCS RELATIVE "${S}" ${GLOB_PATTERN})

function(gen SRCS)
    foreach(F IN LISTS SRCS)
        message(STATUS "Running Clang Format on \"${F}\"")
        execute_process(
            COMMAND "${CLANG_FORMAT_EXE}" -i "${F}"
            WORKING_DIRECTORY "${S}"
            # ERROR_FILE "${B}/${F}.clang-format-err"
            ERROR_STRIP_TRAILING_WHITESPACE)
    endforeach()
endfunction()

gen("${SRCS}")
