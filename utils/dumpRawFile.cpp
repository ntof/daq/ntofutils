/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-16T10:20:00+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <fstream>
#include <iostream>
#include <string>

#include "ConfigMisc.h"
#include "DaqTypes.h"
#include "NTOFLogging.hpp"
#include "easylogging++.h"

using namespace ntof::utils;
using namespace ntof::log;

void usage(const char *app)
{
    std::cerr << "Usage: " << app << " <RawFile>" << std::endl;
}

int main(int argc, char **argv)
{
    if (argc <= 1)
    {
        usage(argv[0]);
        return 1;
    }
    const std::string path(argv[1]);
    if (path == "-h" || path == "--help") {
        usage(argv[0]);
        return 0;
    }

    {
        el::Configurations conf;
        conf.setToDefault();
        conf.set(el::Level::Global, el::ConfigurationType::Format,
                 "%level: %msg");
        conf.set(el::Level::Global, el::ConfigurationType::ToFile, "false");
        conf.set(el::Level::Global, el::ConfigurationType::ToStandardOutput,
                 "true");
        conf.set(el::Level::Global, el::ConfigurationType::Enabled, "true");
        el::Loggers::reconfigureAllLoggers(conf);

        conf.set(el::Level::Global, el::ConfigurationType::Enabled, "true");
        conf.set(el::Level::Global, el::ConfigurationType::Format, "%msg");
        el::Logger *l = el::Loggers::getLogger("out", true);
        l->configure(conf);
    }

    std::ifstream ifs(path);
    if (!ifs)
    {
        LOG(ERROR) << "failed to open \"" << path << "\"";
        return 1;
    }
    while (ifs && !ifs.eof())
    {
        HeaderLookup hl;
        ifs >> hl;

        if (ifs.eof())
        {
            // must read past end to trigger eof (and there may be some padding)
            break;
        }

        switch (hl.type)
        {
        case SkipHeader::TITLE:
        {
            SkipHeader hdr;
            ifs >> hdr;
            CLOG(INFO, "out") << "[SKIP]"
                              << " start:" << hdr.start << " end:" << hdr.end;
            ifs.seekg(hdr.start, std::ios_base::beg);
            break;
        }
        case RunControlHeader::TITLE:
        {
            RunControlHeader rctr;
            ifs >> rctr;
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out") << "[RCTR]"
                              << " runNumber:" << rctr.runNumber
                              << " segmentNumber:" << rctr.segmentNumber
                              << " streamNumber:" << rctr.streamNumber;
            CLOG(INFO, "out") << indent << "experiment:" << rctr.experiment
                              << " date:" << rctr.date << " time:" << rctr.time;
            CLOG(INFO, "out")
                << indent << "totalStreams:" << rctr.totalNumberOfStreams
                << " totalChassis:" << rctr.totalNumberOfChassis
                << " totalModules:" << rctr.totalNumberOfModules;
            CLOG(INFO, "out")
                << indent
                << "channelsPerModule:" << rctr.numberOfChannelsPerModule;
            break;
        }
        case ModuleHeader::TITLE:
        {
            ModuleHeader modh;
            ifs >> modh;
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out")
                << "[MODH]"
                << " numberOfChannels:" << modh.channelsConfig.size();
            for (const ChannelConfig &conf : modh.channelsConfig)
            {
                CLOG(INFO, "out")
                    << indent << "[CHAN:m" << int(conf.getChassis()) << ":"
                    << int(conf.getModule()) << ":" << int(conf.getChannel())
                    << "]";
                CLOG(INFO, "out")
                    << ++indent << "detectorType:" << conf.getDetectorType()
                    << " detectorId:" << conf.detectorId
                    << " moduleType:" << conf.getModuleType();
                CLOG(INFO, "out")
                    << indent << "stream:" << int(conf.getStream())
                    << " chassis:" << int(conf.getChassis())
                    << " module:" << int(conf.getModule())
                    << " channel:" << int(conf.getChannel())
                    << " str_mod_crate:" << conf.str_mod_crate;
                CLOG(INFO, "out") << indent << "sampleRate:" << conf.sampleRate
                                  << " sampleSize:" << conf.sampleSize
                                  << " fullScale:" << conf.fullScale;
                CLOG(INFO, "out") << indent << "delayTime:" << conf.delayTime
                                  << " offset:" << conf.offset
                                  << " clockState:" << conf.getClockState();
                CLOG(INFO, "out") << indent << "threshold:" << conf.threshold
                                  << " thresholdSign:" << conf.thresholdSign
                                  << " zspStart:" << conf.zeroSuppressionStart;
                CLOG(INFO, "out") << indent << "preSample:" << conf.preSample
                                  << " postSample:" << conf.postSample;
                CLOG(INFO, "out")
                    << indent << "masterType:" << conf.getMasterDetectorType()
                    << " masterId:" << conf.masterDetectorId;
                --indent;
            }
            break;
        }
        case EventHeader::TITLE:
        {
            EventHeader eveh;
            ifs >> eveh;
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out") << "[EVEH]"
                              << " eventNumber:" << eveh.eventNumber
                              << " runNumber:" << eveh.runNumber;
            CLOG(INFO, "out") << indent << "sizeOfEvent:" << eveh.sizeOfEvent
                              << " date:" << eveh.date << " time:" << eveh.time;
            CLOG(INFO, "out") << indent << "computerStamp:" << eveh.compTS
                              << " bctStamp:" << eveh.bctTS;
            {
                std::string type;
                switch (eveh.beamType)
                {
                case EventHeader::PARASITIC: type = "PARASITIC"; break;
                case EventHeader::PRIMARY: type = "PRIMARY"; break;
                case EventHeader::CALIBRATION: type = "CALIBRATION"; break;
                case EventHeader::DOUBLE: type = "DOUBLE"; break;
                default: type = "UNKNOWN"; break;
                }
                CLOG(INFO, "out") << indent << "eventType:" << type
                                  << " intensity:" << eveh.intensity;
            }
            break;
        }
        case AdditionalDataHeader::TITLE:
        {
            AdditionalDataHeader addh;
            ifs >> addh;
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out") << "[ADDH] size:" << addh.size()
                              << " count:" << addh.values.size();
            for (const AdditionalDataValue &value : addh.values)
            {
                std::ostringstream valueStr;
                if (value.type == AdditionalDataValue::TypeChar)
                {
                    valueStr << value.toString();
                }
                else
                {
                    if (value.count() > 1)
                    {
                        valueStr << "[";
                    }
                    for (std::size_t i = 0; i < value.count(); ++i)
                    {
                        if (i != 0)
                        {
                            valueStr << ", ";
                        }
                        switch (value.type)
                        {
                        case AdditionalDataValue::TypeInt32:
                            valueStr << value.at<int32_t>(i);
                            break;
                        case AdditionalDataValue::TypeInt64:
                            valueStr << value.at<int64_t>(i);
                            break;
                        case AdditionalDataValue::TypeFloat:
                            valueStr << value.at<float>(i);
                            break;
                        case AdditionalDataValue::TypeDouble:
                            valueStr << value.at<double>(i);
                            break;
                        default: valueStr << "unknown"; break;
                        }
                    }

                    if (value.count() > 1)
                    {
                        valueStr << "]";
                    }
                }
                CLOG(INFO, "out") << indent << "name:" << value.name
                                  << " value:" << valueStr.str();
            }
            break;
        }
        case IndexHeader::TITLE:
        {
            IndexHeader idx;
            ifs >> idx;
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out") << "[INDX] count:" << idx.indexes.size();
            for (const IndexValue &value : idx.indexes)
            {
                CLOG(INFO, "out")
                    << indent << "validatedNumber:" << value.validatedNumber
                    << " eventNumber:" << value.eventNumber
                    << " segment:" << value.segment
                    << " stream:" << value.stream << " offset:" << value.offset;
            }
            break;
        }
        case AcquisitionHeader::TITLE:
        {
            AcquisitionHeader acqc;
            ifs >> acqc;
            ifs.seekg(acqc.dataSize, std::ios_base::cur);
            ntof::log::IndentGuard indent(4);
            CLOG(INFO, "out") << "[ACQC] dataSize:" << acqc.dataSize
                              << " detectorType:" << acqc.getDetectorType()
                              << " detectorId:" << acqc.detectorId;
            CLOG(INFO, "out") << indent << "str_mod_crate:0x" << std::hex
                              << acqc.str_mod_crate << std::dec;
            break;
        }
        default:
        {
            el::base::Writer &writer(CLOG(ERROR, "out"));
            FlagsGuard guard(writer);
            writer << "Unknown header:0x" << std::hex << hl.type;
            ifs.setstate(std::ios::failbit);
            break;
        }
        }
    }
    ifs.close();
    return (ifs.fail() && !ifs.eof()) ? 1 : 0;
}
