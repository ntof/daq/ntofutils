/*
 * DIMProxyClient.h
 *  Client for proxy services
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#ifndef DIMPROXYCLIENT_H_
#define DIMPROXYCLIENT_H_

#include <mutex>
#include <string>
#include <vector>

#include "DIMData.h"
#include "DIMException.h"
#include "DIMSuperClient.h"
#include "DIMXMLInfo.h"

namespace ntof {
namespace dim {
class DIMProxyClient;
/**
 * Handler class to get notified when proxy data is published
 */
class DIMProxyClientHandler
{
public:
    virtual ~DIMProxyClientHandler() {};
    /**
     * Called when the DIM service is refreshed by the server
     * @param dataSet New list of data
     * @param client Client responsible of this callback (can be shared)
     */
    virtual void dataChanged(std::vector<DIMData> &dataSet,
                             const DIMProxyClient &client) = 0;

    /**
     * Called when an error occured on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    virtual void errorReceived(const std::string &errMsg,
                               const DIMProxyClient &client) = 0;
};

class DIMProxyClient : private DIMXMLInfoHandler
{
public:
    /**
     * Constructs a new client to connect to a DIMProxy service
     * @param srvName DIM proxy service name to connect to
     */
    explicit DIMProxyClient(const std::string &srvName);

    /**
     * Default destructor
     */
    virtual ~DIMProxyClient();

    /**
     * Gets the DIM service name used by this client
     * @return
     */
    const std::string &getServiceName() const;

    /**
     * Comparison operator, based on DIM service name
     * @param lhs Other object to compare
     * @return  True if DIM service name are identical
     */
    inline bool operator==(const DIMProxyClient &lhs) const
    {
        return lhs.svcName == svcName;
    }

    /**
     * Send a data to proxy
     * @param service Name of the service to be set
     * @param newParam vector containing data to be sent to server
     */
    static void sendData(std::string &service, std::vector<DIMData> &newParam);

    /**
     * Gets the list of actual datas
     * @return A vector (copy) containing datas
     */
    std::vector<DIMData> getLatestData();

    /**
     * Sets the handler used to be notified when the server updates the DIM
     * service
     * @param hdl Handler to be used
     */
    void setHandler(DIMProxyClientHandler *hdl);

    using DIMXMLInfoHandler::NoLinkError;

protected:
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc, const DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const DIMXMLInfo *info) override;
    std::unique_ptr<DIMXMLInfo> m_info; //!< Acquisition service
    DIMProxyClientHandler *handler;     //!< Callback class
    std::string svcName;                //!< Service name
    std::vector<DIMData> datas;         //!< Vector containing data
    std::mutex mutex;                   //!< Mutex to protect concurrent access
    DIMProxyClient(const DIMProxyClient &); //!< Don't allow copy of this
    DIMProxyClient &operator=(const DIMProxyClient &); //!< Don't allow copy of
                                                       //!< this
};
} // namespace dim
} // namespace ntof

#endif /* DIMPARAMLISTCLIENT_H_ */
