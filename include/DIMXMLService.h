/*
 * DIMXMLService.h
 *
 *  Created on: Nov 6, 2014
 *      Author: mdonze
 */

#ifndef DIMXMLSERVICE_H_
#define DIMXMLSERVICE_H_

#include <memory>
#include <string>

#include <pugixml.hpp>

#include <dis.hxx>

namespace ntof {
namespace dim {

/**
 * This class will publish a XML acquisition trough DIM (like DimService but
 * with XML content)
 */
class DIMXMLService
{
public:
    /**
     * Build a new DIMXMLService object
     * @param svcName Name of the DIM service to be created
     */
    explicit DIMXMLService(const std::string &svcName);

    /**
     * Build a new DIMXMLService object with predefined value
     * @param svcName Name of the DIM service to be created
     * @param doc XML document to get data for initial DIM publication
     */
    DIMXMLService(std::string svcName, pugi::xml_document &doc);

    /**
     * Destructor
     */
    virtual ~DIMXMLService();

    /**
     * Sets new XML document to be published
     * @param doc
     */
    void setData(pugi::xml_document &doc);

protected:
    std::unique_ptr<DimService> m_svc; //!<< DIM service object
};

} /* namespace dim */
} /* namespace ntof */

#endif /* DIMXMLACQUISITION_H_ */
