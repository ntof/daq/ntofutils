/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-01T14:21:21+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef FLOCK_HPP__
#define FLOCK_HPP__

#include <atomic>
#include <string>

namespace ntof {
namespace utils {

/**
 * @brief flock(2) c++ wrapper
 */
class Flock
{
public:
    enum Mode
    {
        Shared,
        Exclusive
    };

    /**
     * @throws NTOFException on open failure
     */
    Flock(Mode mode = Mode::Shared, const std::string &fileName = DefaultLock);
    ~Flock();
    Flock(const Flock &) = delete;
    Flock &operator=(const Flock &) = delete;

    /**
     * @brief try to lock in non-blocking mode
     * @return true if lock is held
     * @throws NTOFException on error
     */
    bool try_lock();

    /**
     * @brief lock the file-lock
     * @throws NTOFException on error
     */
    void lock();

    /**
     * @brief unlock the file-lock
     */
    void unlock();

    inline Mode mode() const { return m_mode; }

    inline bool isLocked() const { return m_locked.load() == 1; }

    inline const std::string &fileName() const { return m_fileName; }

    static const std::string DefaultLock;

protected:
    void throwErrno();

    const Mode m_mode;
    const std::string m_fileName;
    std::atomic<int> m_locked;
    int m_fd;
};

} // namespace utils
} // namespace ntof

#endif
