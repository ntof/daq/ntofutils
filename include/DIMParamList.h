/*
 * DIMParamList.h
 *
 *  Generic class for multi-parameter list
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#ifndef DIMPARAMLIST_H_
#define DIMPARAMLIST_H_

#include <map>
#include <string>
#include <vector>

#include <pugixml.hpp>
#include <stdint.h>

#include "DIMData.h"
#include "DIMDataSet.h"
#include "DIMSuperCommand.h"
#include "DIMXMLService.h"
#include "macros.hpp"

namespace ntof {
namespace dim {
class DIMParamList;
/**
 * Handler class to get notified when a parameter is changed
 */
class DIMParamListHandler
{
public:
    virtual ~DIMParamListHandler() {};

    /**
     * @brief check/validate parameters
     * @param[in,out]  settingsChanged new settings
     * @param[in]  list the parent
     * @param[out] errCode error code (when return is != 0)
     * @param[out] errMsg error message (when return is != 0)
     * @return 0 on success
     *
     * @details settingsChanged can be modified by this function
     */
    virtual int parameterChanged(std::vector<DIMData> &settingsChanged,
                                 const DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) = 0;
};

/**
 * Class for containing a list of parameters
 */
class DIMParamList : public DIMSuperCommand
{
public:
    typedef DIMData::Index Index;
    static const Index NbParamsIndex;

    /**
     * Constructs a list of parameters
     * @param serviceName Name of the DIM service to be created
     */
    explicit DIMParamList(const std::string &serviceName);
    DIMParamList(const DIMParamList &) = delete;
    DIMParamList &operator=(const DIMParamList &) = delete;

    virtual ~DIMParamList();

    /**
     * Registers a new parameter to this list
     * @param index Index of the new parameter
     * @param name Name of the new parameter
     * @param unit Unit if the new parameter
     * @param initValue Initial value of the parameter
     * @param mode Add mode
     * @param updateNow If true, the DIM list value will be refreshed
     */
    template<typename T>
    void addParameter(Index index,
                      const std::string &name,
                      const std::string &unit,
                      const T &value,
                      const AddMode &mode = AddMode::CREATE,
                      bool updateNow = true)
    {
        m_dataset->addData(index, name, unit, value, mode, false);
        if (updateNow)
            updateAcquisition();
    }
    template<typename T, RVALUE_ONLY(T)>
    void addParameter(Index index,
                      const std::string &name,
                      const std::string &unit,
                      T &&value,
                      const AddMode &mode = AddMode::CREATE,
                      bool updateNow = true)
    {
        m_dataset->addData(index, name, unit, std::move(value), mode, false);
        if (updateNow)
            updateAcquisition();
    }

    /**
     * Registers a new parameter to this list
     * @param index Index of the new parameter
     * @param name Name of the new parameter
     * @param unit Unit if the new parameter
     * @param initValue Initial value of the parameter
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addParameter()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addNewParameter(Index index,
                         const std::string &name,
                         const std::string &unit,
                         const T &value,
                         bool updateNow = true)
    {
        m_dataset->addNewData(index, name, unit, value, false);
        if (updateNow)
            updateAcquisition();
    }
    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addNewParameter(Index index,
                         const std::string &name,
                         const std::string &unit,
                         T &&value,
                         bool updateNow = true)
    {
        m_dataset->addNewData(index, name, unit, std::move(value), false);
        if (updateNow)
            updateAcquisition();
    }

    /**
     * Registers a new parameter to this list
     * @param name Name of the new parameter
     * @param unit Unit if the new parameter
     * @param initValue Initial value of the parameter
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addParameter()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    Index addNewParameter(const std::string &name,
                          const std::string &unit,
                          const T &value,
                          bool updateNow = true)
    {
        Index ret = m_dataset->addNewData(name, unit, value, false);
        if (updateNow)
            updateAcquisition();
        return ret;
    }

    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    Index addNewParameter(const std::string &name,
                          const std::string &unit,
                          T &&value,
                          bool updateNow = true)
    {
        Index ret = m_dataset->addNewData(name, unit, std::move(value), false);
        if (updateNow)
            updateAcquisition();
        return ret;
    }

    /**
     * Removes a parameter from the list
     * @param index Index of parameter to be removed
     */
    inline void removeParameter(Index index, bool updateNow = true)
    {
        m_dataset->removeData(index, false);
        if (updateNow)
            updateAcquisition();
    }

    /**
     * Removes all parameters from the list
     * @details the "Number of parameters" automatically restored
     */
    void clearParameters(bool updateNow = true);

    /**
     * Gets a parameter double value (parameter must be a double type!)
     * @param index Index of the parameter to be read
     * @return The parameter value
     */
    inline double getDoubleValue(Index index) const
    {
        return m_dataset->getDoubleValue(index);
    }

    /**
     * Gets a parameter int32_t value (parameter must be a int32_t type!)
     * @param index Index of the parameter to be read
     * @return The parameter value
     */
    inline int32_t getIntValue(Index index) const
    {
        return m_dataset->getIntValue(index);
    }

    /**
     * Gets a parameter string value (parameter must be a string type!)
     * @param index Index of the parameter to be read
     * @return The parameter value
     */
    inline std::string getStringValue(Index index) const
    {
        return m_dataset->getStringValue(index);
    }

    /**
     * Gets a parameter int64_t value (parameter must be a int64_t type!)
     * @param index Index of the parameter to be read
     * @return The parameter value
     */
    inline int64_t getLongValue(Index index) const
    {
        return m_dataset->getLongValue(index);
    }

    /**
     * Gets a parameter enum value (parameter must be a enum type!)
     * @param index Index of the parameter to be read
     * @return The parameter value
     */
    DIMEnum getEnumValue(Index index) const
    {
        return m_dataset->getEnumValue(index);
    }

    /**
     * @brief return nested values
     * @param  name name of the data to retrieve
     * @return the nested data values
     */
    inline DIMData::List getNestedValue(Index index) const
    {
        return m_dataset->getNestedValue(index);
    }
    inline DIMData::List getNestedValue(Index index)
    {
        return m_dataset->getNestedValue(index);
    }

    /**
     * Sets a value for specified parameter (must be in double type)
     * @param index Index of the parameter to be set
     * @param value Value of the parameter to be set
     * @param updateNow If true, DIM service is automatically refreshed
     */
    template<typename T>
    void setValue(Index index, const T &value, bool updateNow = true)
    {
        m_dataset->setValue(index, value, false);
        if (updateNow)
            updateAcquisition();
    }
    template<typename T, RVALUE_ONLY(T)>
    void setValue(Index index, T &&value, bool updateNow = true)
    {
        m_dataset->setValue(index, std::move(value), false);
        if (updateNow)
            updateAcquisition();
    }

    /**
     * Refreshes associated DIM service
     */
    void updateList();

    /**
     * Gets the name of the parameter specified by index
     * @param index
     * @return
     */
    std::string getParameterName(Index index) const
    {
        return m_dataset->getDataName(index);
    }

    /**
     * Sets the handler used to be notified in parameter changes
     * @param hdl Pointer to class implementing parameterChanged method
     */
    void setHandler(DIMParamListHandler *hdl);

    /**
     * Gets the name of the DIM service associated with this list
     * @return
     */
    const std::string &getServiceName() const;

    /**
     * Comparison operator, based on service name
     * @param lhs Other object to be compared to
     * @return True if objects have the same DIM service name
     */
    inline bool operator==(const DIMParamList &lhs) const
    {
        return lhs.getServiceName() == svcName;
    }

    /**
     * Gets a copy of the DIMData object at specified index
     * @param index Index of the parameter to be read
     * @return A copy of the parameter object
     */
    DIMData getParameterAt(Index index) const;

    /**
     * Gets number of parameters
     * @return The number of parameters into the list
     */
    inline int getParameterCount() const { return m_dataset->getDataCount(); }

    /**
     * @brief test if the ParamList has given index
     * @return true if index exists
     */
    inline bool hasParameterAt(Index index) const
    {
        return m_dataset->hasDataAt(index);
    }

    /**
     * @brief hide/show a parameter
     */
    inline void setHidden(Index index, bool value = true, bool updateNow = true)
    {
        m_dataset->setHidden(index, value, false);
        if (updateNow)
            updateAcquisition();
    }

    inline bool isHidden(Index index) const
    {
        return m_dataset->isHidden(index);
    }

    /**
     * @brief retrieve and lock data for modifications
     * @details only one LockRef can be used at a time, those objects are meant
     * to be temporary and just held the time of a modification
     */
    inline utils::LockRef<DIMData> lockParameterAt(Index index)
    {
        return m_dataset->lockDataAt(index);
    }

    inline utils::LockRef<DIMData> lockParameterAt(const std::string &name)
    {
        return m_dataset->lockDataAt(name);
    }

protected:
    /**
     * Called by DIMSuperCommand, must not be directly called!
     * @param cmd Value of the DIM command received
     */
    void commandReceived(DIMCmd &cmd) override;

    void updateAcquisition();

    inline DIMDataSet::DataMap &getDataMap() { return m_dataset->dataList; }

    DIMParamListHandler *handler; //!< Pointer to handler object
    int emptySvc;                 //!< Dummy value for DIM
    std::string svcName;          //!< Name of the service
    std::unique_ptr<DIMDataSet> m_dataset;

    static const std::string s_rootName;
};
} // namespace dim
} // namespace ntof

#endif /* DIMPARAMLIST_H_ */
