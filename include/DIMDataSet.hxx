/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-24T16:37:11+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMDATASET_HXX__
#define DIMDATASET_HXX__

#include "DIMDataSet.h"

namespace ntof {
namespace dim {

template<typename T>
void DIMDataSet::addData(Index index,
                         const std::string &name,
                         const std::string &unit,
                         const T &initValue,
                         AddMode mode,
                         bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    DataMap::iterator it = dataList.find(index);
    if (it == dataList.end()) // always add
    {
        dataList[index] = DIMData(index, name, unit, initValue);
    }
    else if (mode == AddMode::UPDATE)
    {
        it->second.setValue(initValue);
    }
    else if (mode == AddMode::OVERRIDE)
    {
        it->second = DIMData(index, name, unit, initValue);
    }
    else
    {
        std::ostringstream oss;
        oss << __FILE__ << " : Data with index " << index << " already exists!";
        throw DIMException(oss.str(), __LINE__);
    }
    if (updateNow)
        _updateData(lock);
}

template<typename T, class>
void DIMDataSet::addData(Index index,
                         const std::string &name,
                         const std::string &unit,
                         T &&initValue,
                         AddMode mode,
                         bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    DataMap::iterator it = dataList.find(index);
    if (it == dataList.end()) // always add
    {
        dataList[index] = DIMData(index, name, unit, std::move(initValue));
    }
    else if (mode == AddMode::UPDATE)
    {
        it->second.setValue(std::move(initValue));
    }
    else if (mode == AddMode::OVERRIDE)
    {
        it->second = DIMData(index, name, unit, std::move(initValue));
    }
    else
    {
        std::ostringstream oss;
        oss << __FILE__ << " : Data with index " << index << " already exists!";
        throw DIMException(oss.str(), __LINE__);
    }
    if (updateNow)
        _updateData(lock);
}

template<typename T>
DIMDataSet::Index DIMDataSet::addData(const std::string &name,
                                      const std::string &unit,
                                      const T &initValue,
                                      AddMode mode,
                                      bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    try
    {
        DIMData &d = _findData(name);
        if (mode == AddMode::UPDATE)
        {
            d.setValue(initValue);
        }
        else if (mode == AddMode::OVERRIDE)
        {
            d = DIMData(d.getIndex(), name, unit, initValue);
        }
        else
        {
            std::ostringstream oss;
            oss << __FILE__ << " : Data with name " << name
                << " already exists!";
            throw DIMException(oss.str(), __LINE__);
        }
        if (updateNow)
            _updateData(lock);
        return d.getIndex();
    }
    catch (DIMException &ex)
    {
        const Index index = _getNextIndex();
        dataList[index] = DIMData(index, name, unit, initValue);
        if (updateNow)
            _updateData(lock);
        return index;
    }
}

template<typename T, class>
DIMDataSet::Index DIMDataSet::addData(const std::string &name,
                                      const std::string &unit,
                                      T &&initValue,
                                      AddMode mode,
                                      bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    try
    {
        DIMData &d = _findData(name);
        if (mode == AddMode::UPDATE)
        {
            d.setValue(std::move(initValue));
        }
        else if (mode == AddMode::OVERRIDE)
        {
            d = DIMData(d.getIndex(), name, unit, std::move(initValue));
        }
        else
        {
            std::ostringstream oss;
            oss << __FILE__ << " : Data with name " << name
                << " already exists!";
            throw DIMException(oss.str(), __LINE__);
        }
        if (updateNow)
            _updateData(lock);
        return d.getIndex();
    }
    catch (DIMException &ex)
    {
        const Index index = _getNextIndex();
        dataList[index] = DIMData(index, name, unit, std::move(initValue));
        if (updateNow)
            _updateData(lock);
        return index;
    }
}

template<typename T>
void DIMDataSet::addOrUpdateData(Index index,
                                 const std::string &name,
                                 const std::string &unit,
                                 const T &initValue,
                                 bool updateNow)
{
    addData(index, name, unit, initValue, AddMode::UPDATE, updateNow);
}

template<typename T>
DIMData::Index DIMDataSet::addOrUpdateData(const std::string &name,
                                           const std::string &unit,
                                           const T &initValue,
                                           bool updateNow)
{
    return addData(name, unit, initValue, AddMode::UPDATE, updateNow);
}

template<typename T, class>
void DIMDataSet::addOrUpdateData(Index index,
                                 const std::string &name,
                                 const std::string &unit,
                                 T &&initValue,
                                 bool updateNow)
{
    addData(index, name, unit, std::move(initValue), AddMode::UPDATE, updateNow);
}

template<typename T, class>
DIMData::Index DIMDataSet::addOrUpdateData(const std::string &name,
                                           const std::string &unit,
                                           T &&initValue,
                                           bool updateNow)
{
    return addData(name, unit, std::move(initValue), AddMode::UPDATE, updateNow);
}

template<typename T>
void DIMDataSet::addNewData(Index index,
                            const std::string &name,
                            const std::string &unit,
                            const T &initValue,
                            bool updateNow)
{
    addData(index, name, unit, initValue, AddMode::CREATE, updateNow);
}

template<typename T, class>
void DIMDataSet::addNewData(Index index,
                            const std::string &name,
                            const std::string &unit,
                            T &&initValue,
                            bool updateNow)
{
    addData(index, name, unit, std::move(initValue), AddMode::CREATE, updateNow);
}

template<typename T>
void DIMDataSet::setValue(Index index, const T &value, bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    DIMData &p = _findData(index);
    p.setValue(value);

    if (updateNow)
        _updateData(lock);
}

template<typename T, class>
void DIMDataSet::setValue(Index index, T &&value, bool updateNow)
{
    std::unique_lock<std::mutex> lock(m_lock);
    DIMData &p = _findData(index);
    p.setValue(std::move(value));

    if (updateNow)
        _updateData(lock);
}

template<typename T>
T DIMDataSet::getValue(Index index) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return _findData(index).getValue<T>();
}

template<typename T>
T DIMDataSet::getValue(const std::string &name) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return _findData(name).getValue<T>();
}

/* not implemented on purpose (not thread safe) */
template<>
const std::string &DIMDataSet::getValue<const std::string &>(
    const std::string &name) const;

template<typename T>
bool DIMDataSet::is(Index index) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return _findData(index).is<T>();
}

template<typename T>
bool DIMDataSet::is(const std::string &name) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return _findData(name).is<T>();
}

} // namespace dim
} // namespace ntof
#endif
