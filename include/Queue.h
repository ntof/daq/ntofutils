/*
 * Queue.h
 *  Template queue for posting data between threads
 *  Created on: Oct 20, 2014
 *      Author: mdonze
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <cerrno>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <mutex>
#include <sstream>

#include <queue>

#include "NTOFException.h"

namespace ntof {
namespace utils {

template<typename T>
class QueueDeleter
{
public:
    QueueDeleter() = default;
    void operator()(const T &) const {}
};

/*
 * holly crap ! partial class template specialization ! wizz !
 */
template<typename T>
class QueueDeleter<T *>
{
public:
    QueueDeleter() = default;
    void operator()(T *const &t) const { delete t; }
};

/**
 * Queue for posting messages
 *
 */
template<typename T>
class Queue
{
public:
    /**
     * @brief Queue constructor
     * @param[in] queueName name of the queue (used in exception messages)
     * @param[in] maxSize   maximum size of the queue
     */
    Queue(const std::string &queueName, std::size_t maxSize);

    /* disable copy*/
    Queue(const Queue<T> &) = delete;
    Queue<T> &operator=(const Queue<T> &) = delete;
    /* allow rvalue construct */
    Queue(Queue<T> &&);

    virtual ~Queue();

    /**
     * @brief post an element in the queue
     * @throws NTOFException if queue is full
     */
    int post(const T &service);
    int post(T &&service);

    /**
     * @brief pop an element from the queue
     * @details waits until an event is queued
     */
    T pop();

    /**
     * @brief pop an element from the queue, throwing on timeout
     * @param[in]  timeOut timeout value in miliseconds
     *
     * @throws NTOFException on timeout
     */
    T pop(unsigned long timeOut);
    T pop(const std::chrono::milliseconds &msecs);

    /**
     * @brief clear the queue
     * @param[in] flush awake pending threads
     * @details awoken threads will throw an NTOFException
     */
    void clear(bool flush = false);

    /**
     * @brief return number of items in the queue
     * @return number of items in the queue
     */
    std::size_t count() const;

    /**
     * @brief retrieve queue name
     */
    inline const std::string &getQueueName() const { return m_queueName; }

protected:
    std::size_t m_maxSize;
    std::string m_queueName;
    std::queue<T> m_queue;
    mutable std::mutex m_mutex;
    std::condition_variable m_condVar;
};

template<typename T>
Queue<T>::Queue(const std::string &queueName, std::size_t maxSize) :
    m_maxSize(maxSize), m_queueName(queueName)
{}

template<typename T>
Queue<T>::Queue(Queue &&other) :
    m_maxSize(other.m_maxSize), m_queueName(std::move(other.m_queueName))
{}

template<typename T>
Queue<T>::~Queue()
{
    clear();
}

template<typename T>
int Queue<T>::post(const T &element)
{
    return post(T(element)); /* use move implementation */
}

template<typename T>
int Queue<T>::post(T &&element)
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    if (m_queue.size() >= m_maxSize)
    {
        QueueDeleter<T>()(element);
        std::ostringstream oss;
        oss << "Queue::post : Queue " << m_queueName
            << ((m_maxSize == 0) ? " is flushed!" : " is full!");
        throw NTOFException(oss.str(), __FILE__, __LINE__, EOVERFLOW);
    }
    m_queue.push(std::move(element));
    m_condVar.notify_one();
    return m_queue.size();
}

template<typename T>
T Queue<T>::pop()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    // Wait for the queue to have something
    if (m_queue.empty())
    {
        if (m_maxSize != 0)
            m_condVar.wait(lock);
        if (m_queue.empty() || (m_maxSize == 0))
        {
            std::ostringstream oss;
            oss << "Queue::pop : Queue " << m_queueName << " explicit wakeup";
            throw NTOFException(oss.str(), __FILE__, __LINE__, EINTR);
        }
    }
    T ret = std::move(m_queue.front());
    m_queue.pop();
    return ret;
}

template<typename T>
T Queue<T>::pop(unsigned long timeOut)
{
    return pop(std::chrono::milliseconds(timeOut));
}

template<typename T>
T Queue<T>::pop(const std::chrono::milliseconds &msecs)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    // Wait for the queue to have something
    if (m_queue.empty())
    {
        if ((m_maxSize != 0) &&
            (m_condVar.wait_for(lock, msecs) == std::cv_status::timeout))
        {
            std::ostringstream oss;
            oss << "Queue::pop : Queue " << m_queueName
                << " timeout while waiting for element!";
            throw NTOFException(oss.str(), __FILE__, __LINE__, ETIMEDOUT);
        }
        else if (m_queue.empty() || (m_maxSize == 0))
        {
            std::ostringstream oss;
            oss << "Queue::pop : Queue " << m_queueName << " explicit wakeup";
            throw NTOFException(oss.str(), __FILE__, __LINE__, EINTR);
        }
    }
    T ret = std::move(m_queue.front());
    m_queue.pop();
    return ret;
}

template<typename T>
std::size_t Queue<T>::count() const
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    return m_queue.size();
}

template<typename T>
void Queue<T>::clear(bool flush)
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    while (!m_queue.empty())
    {
        T ret = std::move(m_queue.front());
        m_queue.pop();
        QueueDeleter<T>()(ret);
    }
    if (flush)
    {
        m_maxSize = 0;
        m_condVar.notify_all();
    }
}

} // namespace utils
} // namespace ntof

#endif /* QUEUE_H_ */
