/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-26T09:35:23+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef WORKER_HPP__
#define WORKER_HPP__

#include <chrono>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include "Queue.h"

namespace ntof {
namespace utils {

/**
 * @brief Threaded worker class
 */
class Worker
{
public:
    class Task; // declared below
    typedef std::shared_ptr<Task> SharedTask;

    /**
     * @brief create a new Worker
     * @param[in] numThreads number of threads to create
     * @param[in] maxSize    maximum queue size
     */
    explicit Worker(const std::string &name,
                    std::size_t numThreads = 1,
                    std::size_t maxSize = 2048);

    /**
     * @details will stop ongoing threads (and wait to join)
     * @details pending tasks will be aborted
     */
    ~Worker();

    Worker(const Worker &) = delete;
    Worker &operator=(const Worker &) = delete;

    /**
     * @brief post a task on the worker
     * @param[in]  task the task to execute
     * @return the input task (for convenience)
     */
    const Worker::SharedTask &post(const Worker::SharedTask &task);

    /**
     * @brief post a lambda based task
     * @param[in]  funTask the lambda based function
     * @return the generated SharedTask
     */
    Worker::SharedTask post(const std::function<void()> &funTask);

    /**
     * @brief start the worker thread
     * @details the worker thread is already started on construct
     */
    void start();

    /**
     * @brief stop the worker thread
     * @details this method is synchronuous
     */
    void stop();

    /**
     * @brief get Worker name
     */
    const std::string &name() const;

    /**
     * @brief Worker task
     */
    class Task
    {
    public:
        Task();
        virtual ~Task();

        enum State
        {
            READY,
            RUNNING,
            FINISHED
        };

        enum Error
        {
            OK,
            FAILED,
            ABORTED
        };

        /**
         * @brief retrieve current task state
         */
        State state() const;

        /**
         * @brief retrieve current task error status
         */
        Error error() const;

        /**
         * @brief return current error
         */
        const std::string &errorString() const;

        /**
         * @brief wait for the task to finish
         * @param[0]  msecs timeout in milliseconds (0 for infinite)
         * @return false on timeout
         */
        bool wait(unsigned long msecs = 0) const;

        /**
         * @brief wait for the task to finish
         * @param[0]  msecs timeout in milliseconds (0 for infinite)
         * @return false on timeout
         */
        bool wait(const std::chrono::milliseconds &msecs) const;

        /**
         * @brief abort task
         * @details ongoing tasks may not be aborted
         */
        virtual bool abort();

    protected:
        virtual void run() = 0;

        bool setState(State state);

        /**
         * @brief set error status and FINISHED state
         * @param[in] error error message
         * @details function that may be used be child classes
         */
        void setError(const std::string &error);

        friend class Worker; // calls run() and plays with state

    private:
        State m_state;
        Error m_error;
        mutable std::mutex m_lock;
        mutable std::condition_variable m_cond;
        std::string m_errorString;
    };

    /**
     * @brief convenience class to spawn lambda tasks
     */
    class FunTask : public Task
    {
    public:
        explicit FunTask(const std::function<void()> &fun);

        /**
         * @details do throw a string exception to raise an error
         */
        void run() override;

    protected:
        std::function<void()> m_fun;
    };

protected:
    void thread_func();

    ntof::utils::Queue<Worker::SharedTask> m_queue;
    std::vector<std::unique_ptr<std::thread>> m_threads;
};

} // namespace utils
} // namespace ntof

#endif
