/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T18:21:30+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMPARAMLISTPROXY_HPP__
#define DIMPARAMLISTPROXY_HPP__

#include <cstdint>
#include <set>

#include "DIMParamList.h"
#include "DIMParamListClient.h"
#include "Signals.hpp"

namespace ntof {
namespace dim {

/**
 * @brief this service synchronises a remote service
 * @param proxy [description]
 */
class DIMParamListProxy : public DIMParamList
{
public:
    typedef ntof::utils::signal<void(DIMParamListProxy &proxy)> SyncSignal;

    enum Flags
    {
        Syncing = 0x01,
        NoUpdatesOnSync = 0x02, //<! block updates from DIM whilst syncing
    };

    /**
     * @Constructor
     * @param  serviceName DIM service name to publish data
     * @param  remoteService remote service to monitor
     */
    explicit DIMParamListProxy(const std::string &serviceName,
                               const std::string &remoteService);
    DIMParamListProxy(const DIMParamListProxy &) = delete;
    DIMParamListProxy &operator=(const DIMParamListProxy &) = delete;

    virtual ~DIMParamListProxy();

    /**
     * @brief turn on/off syncing mode
     * @param syncing syncing mode
     * @details syncing mode is off by default
     */
    void setSyncing(bool syncing = true);

    /**
     * @brief check if the service is syncing
     * @return true if the service is waiting for an update
     */
    inline bool isSyncing() const { return m_flags & Syncing; }

    /**
     * @brief forbid DIM updates on the DIMParamList whilst syncing
     * @param value whereas to enable or disable this feature
     */
    void setNoUpdatesOnSync(bool value = true);
    inline bool isNoUpdatesOnSync() const { return m_flags & NoUpdatesOnSync; }

    /**
     * @brief stop syncing and disconnect underlying DIMParamListClient
     */
    void disconnect();

    /**
     * @brief add/remove parameters that should not be sent back to remote
     * service
     * @param[in]  index    Paramter index
     * @paramp[in] readOnly Whereas to flag it as readOnly or not
     */
    void setReadOnly(DIMData::Index index, bool readOnly = true);
    inline bool isReadOnly(DIMData::Index index) const
    {
        return m_readOnly.count(index) != 0;
    }

    /**
     * @brief sync back the service on its remote
     * @return DIMAck on success
     * @throw DIMException (depends on the error)
     */
    DIMAck sendParameters();

    /**
     * @brief update service with client data
     * @emits syncSignal
     */
    void operator()(DIMParamListClient &client);

    SyncSignal syncSignal;

protected:
    void commandReceived(DIMCmd &cmd) override;

    std::set<DIMData::Index> m_readOnly;
    std::unique_ptr<DIMParamListClient> m_client;
    std::string m_remoteService;
    uint8_t m_flags;
};

} // namespace dim
} // namespace ntof

#endif
