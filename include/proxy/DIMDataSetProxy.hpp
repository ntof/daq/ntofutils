/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-09T14:30:33+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMDATASETPROXY_HPP__
#define DIMDATASETPROXY_HPP__

#include "DIMDataSet.h"
#include "DIMDataSetClient.h"
#include "Signals.hpp"

namespace ntof {
namespace dim {

class DIMDataSetProxy : public DIMDataSet
{
public:
    typedef ntof::utils::signal<void(DIMDataSetProxy &proxy)> SyncSignal;
    /**
     * @Constructor
     * @param  serviceName DIM service name to publish data
     * @param  remoteService remote service to monitor
     */
    explicit DIMDataSetProxy(const std::string &serviceName,
                             const std::string &remoteService);
    DIMDataSetProxy(const DIMDataSetProxy &) = delete;
    DIMDataSetProxy &operator=(const DIMDataSetProxy &) = delete;

    virtual ~DIMDataSetProxy();

    /**
     * @brief turn on/off syncing mode
     * @param syncing syncing mode
     * @details syncing mode is off by default
     */
    void setSyncing(bool syncing = true);

    /**
     * @brief check if the service is syncing
     * @return true if the service is waiting for an update
     */
    inline bool isSyncing() const { return m_syncing; }

    /**
     * @brief stop syncing and disconnect underlying DIMDataSetClient
     */
    void disconnect();

    /**
     * @brief update service with client data
     * @emits syncSignal
     */
    void operator()(DIMDataSetClient &client);

    SyncSignal syncSignal;

protected:
    std::unique_ptr<DIMDataSetClient> m_client;
    std::string m_remoteService;
    bool m_syncing;
};

} // namespace dim
} // namespace ntof

#endif
