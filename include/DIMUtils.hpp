/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T13:57:00+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMUTILS_HPP__
#define DIMUTILS_HPP__

#include "Signals.hpp"

namespace ntof {
namespace dim {

/**
 * @brief convenience class to take/release DIM lock
 */
class DIMLockGuard
{
public:
    DIMLockGuard();
    ~DIMLockGuard();
};
typedef DIMLockGuard lock_guard;

/**
 * @brief helper class to handle scopped signal connections that may be
 * triggered from DIM
 * @details this ensures that no DIM handlers are
 */
class DIMScopedConnection
{
public:
    // cppcheck-suppress noExplicitConstructor
    DIMScopedConnection(const utils::connection &connection);
    DIMScopedConnection() = default;
    ~DIMScopedConnection();
    DIMScopedConnection(DIMScopedConnection &&other);
    DIMScopedConnection &operator=(DIMScopedConnection &&);
    DIMScopedConnection &operator=(const utils::connection &conn);

    DIMScopedConnection(const DIMScopedConnection &) = delete;
    DIMScopedConnection &operator=(const DIMScopedConnection &) = delete;

    void disconnect();

    inline utils::connection &connection() { return m_conn; }

protected:
    utils::connection m_conn;
};
typedef DIMScopedConnection scoped_connection;

} // namespace dim
} // namespace ntof

#endif
