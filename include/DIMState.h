/*
 * DIMState.h
 *
 *  Created on: Nov 18, 2014
 *      Author: mdonze
 */

#ifndef DIMSTATE_H_
#define DIMSTATE_H_

#include <condition_variable>
#include <cstdint>
#include <map>
#include <mutex>
#include <string>

#include "DIMXMLService.h"

namespace ntof {
namespace dim {

/**
 * Represents an error or warning
 */
class DIMErrorWarning
{
public:
    DIMErrorWarning();

    /**
     * Constructor
     * @param code Error/warning code
     * @param msg Information message
     */
    DIMErrorWarning(int32_t code, const std::string &msg);
    virtual ~DIMErrorWarning();

    /**
     * Gets the error/warning message associated
     * @return Message
     */
    const std::string &getMessage() const;

    /**
     * Gets the execution code
     * @return
     */
    int32_t getCode() const;

    /**
     * Sets message
     * @param msg
     */
    void setMessage(const std::string &msg);

    /**
     * Comparison operator
     * @param value to be tested
     * @return True if matches
     */
    bool operator==(int32_t code) const;

    /**
     * Comparison operator
     * @param value to be tested
     * @return True if matches
     */
    bool operator==(const DIMErrorWarning &val) const;

    /**
     * Assignment operator
     * @param val Value to be copied
     */
    DIMErrorWarning &operator=(const DIMErrorWarning &val);

protected:
    int32_t m_code;
    std::string m_msg;
};

/**
 * This class is an helper to publish the state of something
 */
class DIMState
{
public:
    typedef int32_t State;
    typedef int32_t ErrorCode;
    typedef std::map<ErrorCode, DIMErrorWarning> ErrorMap;
    typedef ErrorMap WarningMap;

    /**
     * Build a new DIM state service
     * @param svcName DIM service name
     */
    explicit DIMState(const std::string &svcName);
    DIMState(const DIMState &) = delete;
    DIMState &operator=(const DIMState &) = delete;

    /**
     * Destructor
     */
    virtual ~DIMState();

    /**
     * Declare a new possible state
     * @param stateValue Value of the state enum
     * @param name Name of the enumeration
     * @param refreshNow If true, DIM will be updated with new state
     */
    void addStateValue(State stateValue,
                       const std::string &name,
                       bool refreshNow = false);

    /**
     * Gets the actual state value
     * @return
     */
    State getValue() const;

    /**
     * Sets value of this state
     * @param value
     */
    void setValue(State value);

    /**
     * Gets name of the state
     * @return
     */
    std::string getValueAsString() const;

    /**
     * Sets error message and activate error state
     * @param errorCode Error code
     * @param msg Error message
     */
    inline void setError(ErrorCode errorCode = -1, const std::string &msg = "")
    {
        addError(errorCode, msg);
    }

    /**
     * Remove error
     */
    inline void clearError() { clearErrors(); }

    /**
     * Gets actual error message
     * @return
     */
    std::string getErrorMessage() const;

    /**
     * Gets actual error code
     * @return
     */
    ErrorCode getErrorCode() const;

    /**
     * Adds a new error to this
     * @param errorCode
     * @param msg
     */
    void addError(ErrorCode errorCode, const std::string &msg = "");

    /**
     * Removes the specified error from this
     */
    bool removeError(ErrorCode errorCode);

    /**
     * @brief return errors list
     */
    ErrorMap getErrors() const;

    /**
     * @brief return number of errors
     */
    std::size_t errorsCount() const;

    /**
     * Removes all errors
     */
    void clearErrors();

    /**
     * Adds a new warning to this
     * @param WarningCode
     * @param msg
     */
    void addWarning(ErrorCode warningCode, const std::string &msg = "");

    /**
     * Removes the specified warning from this
     */
    bool removeWarning(ErrorCode warningCode);

    /**
     * @brief return warnings list
     */
    WarningMap getWarnings() const;

    /**
     * @brief return number of warnings
     */
    std::size_t warningsCount() const;

    /**
     * Removes all warnings
     */
    void clearWarnings();

    /**
     * Waits for new state during specified timeout
     * Default timeout time will make the method to wait without timeout
     * This code raises a DIMStateException in case the client sends an error or
     * timedout
     * @param timeOutMs Timeout in ms (negative means no timeout)
     * @return State in int32_t format
     */
    int32_t waitForNewState(long timeOutMs = -1);

    /**
     * Waits for new state during specified timeout
     * Default timeout time will make the method to wait without timeout
     * This code raises a DIMStateException in case the client sends an error or
     * timedout
     * @param timeOutMs Timeout in ms (negative means no timeout)
     * @return State in string format
     */
    std::string waitForNewStateAsString(long timeOutMs = -1);

    /**
     * Sets if the service will be refreshed automatically
     * @param enabled, true if refresh will be done
     */
    void setAutoRefresh(bool enabled = true);

    /**
     * Ask to refresh the service content
     */
    void refresh();

    static const DIMState::State ErrorState;
    static const DIMState::State NotReadyState;

protected:
    /**
     * Refreshes DIM service
     */
    void refreshDIM(bool force = false);

    std::unique_ptr<DIMXMLService> m_svc;  //!< For publishing through DIM
    std::map<State, std::string> m_values; //!< For storing value/name pairs
    int32_t m_actualValue;                 //!< Actual value of this state
    bool m_autoRefresh;                    //!< Auto refresh flag

    mutable std::mutex m_lock;
    std::condition_variable m_cond; //!< Condition variable for synchronization

    ErrorMap m_errors;   //!< Active errors
    ErrorMap m_warnings; //!< Active warnings
};

} /* namespace dim */
} /* namespace ntof */

#endif /* DIMSTATE_H_ */
