/*
 * DIMCmd.h
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#ifndef DIMCMD_H_
#define DIMCMD_H_

#include <string>

#include <pugixml.hpp>
#include <stdint.h>

namespace ntof {
namespace dim {
/**
 * Represents a command received from DIM
 */
class DIMCmd
{
public:
    /**
     * Empty constructor, initializes fields to default values
     */
    DIMCmd();

    /**
     * Constructs this using raw buffer coming from DIM
     * @param doc Raw data buffer from DIM
     */
    explicit DIMCmd(pugi::xml_document &doc);

    /**
     * Construct this using predefined values
     * @param key Command key
     * @param doc XML document to get command data from
     */
    DIMCmd(int32_t key, pugi::xml_document &doc);

    /**
     * Copy constructor to copy this
     * @param other Other instance to be copied from
     */
    DIMCmd(const DIMCmd &other);

    /**
     * Assignment operator
     * @param other Other instance to be copied from
     * @return Reference to this instance
     */
    DIMCmd &operator=(DIMCmd other);

    /**
     * Destructor
     */
    virtual ~DIMCmd();

    /**
     * Gets the command key
     * @return The command key
     */
    int32_t getKey();

    /**
     * Gets command data
     */
    pugi::xml_node getData();

    /**
     * Sets the command key
     * @param key Command key to be set
     */
    void setKey(int32_t key);

    /**
     * Sets the command data to this
     * @param cmdData XML data containing command
     */
    void setData(pugi::xml_document &cmdData);

    /**
     * Gets the complete XML document representing this
     * @return xml_document containing command data and command header
     */
    pugi::xml_document &getRootData();

private:
    pugi::xml_document doc_;
};
} // namespace dim
} // namespace ntof

#endif /* DIMCmd_H_ */
