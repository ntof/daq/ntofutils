/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-26T11:36:47+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef NTOFLOGGING_HPP__
#define NTOFLOGGING_HPP__

#include <ios>
#include <ostream>

#include "DIMData.h"
#include "easylogging++.h" // not needed, but will facilitate dev

namespace ntof {
namespace log {

/*
 * @brief initialize logging facility
 */
void init(int argc = 0, char *argv[] = nullptr);

class DebugFlag
{
public:
    DebugFlag();
    ~DebugFlag();

    static bool isDebug(const std::ostream &os);

    void setDebug(std::ostream &os, bool value) const;

protected:
    mutable std::ostream *m_os;
    static const int s_key;
};

DebugFlag debug();

/**
 * @brief indentation helper, manages current level in stream
 */
class IndentGuard
{
public:
    explicit IndentGuard(std::ostream &os, int level = 4);
    explicit IndentGuard(int level = 4);
    ~IndentGuard();

    IndentGuard &operator++();
    IndentGuard &operator--();

    std::ostream &operator()(std::ostream &os) const;

protected:
    mutable std::ostream *m_os;
    int m_level;
    int m_value;

    static const int s_key;
};

/**
 * @brief save stream flags and restore it on deletion
 */
class FlagsGuard
{
public:
    explicit FlagsGuard(std::ostream &oss);
    explicit FlagsGuard(el::base::Writer &writer);
    ~FlagsGuard();

    void save(std::ostream &oss) const;

protected:
    mutable std::ostream *m_oss;
    mutable std::ios_base::fmtflags m_flags;
};

std::ostream &operator<<(std::ostream &oss, const FlagsGuard &saver);
std::ostream &operator<<(std::ostream &os, const DebugFlag &indent);
std::ostream &operator<<(std::ostream &os, const IndentGuard &indent);

} // namespace log
} // namespace ntof

namespace el {
namespace base {

template<>
MessageBuilder &MessageBuilder::operator<< <ntof::dim::DIMData>(
    const ntof::dim::DIMData &data);

/**
 * @details provides better indentation than base implementation
 */
template<typename T>
MessageBuilder &MessageBuilder::writeIterator(T begin_,
                                              T end_,
                                              std::size_t size_)
{
    type::ostream_t &os = m_logger->stream();
    if (begin_ == end_)
        os << "[]";
    else if (size_ == 1)
        *this << "[" << (*begin_) << "]";
    else if (Loggers::hasFlag(LoggingFlag::NewLineForContainer) ||
             ntof::log::DebugFlag::isDebug(os))
    {
        ntof::log::IndentGuard indent(os);
        os << "[\n";
        for (std::size_t i = 0;
             begin_ != end_ && i < base::consts::kMaxLogPerContainer;
             ++i, ++begin_)
        {
            *this << indent << (*begin_) << "\n";
        }
        if (begin_ != end_)
        {
            os << ELPP_LITERAL("...");
        }
        *this << --indent << "]";
    }
    else
    {
        os << ELPP_LITERAL("[");
        for (std::size_t i = 0;
             begin_ != end_ && i < base::consts::kMaxLogPerContainer;
             ++i, ++begin_)
        {
            *this << (*begin_);
            os << ((i < size_ - 1) ? m_containerLogSeperator : ELPP_LITERAL(""));
        }
        if (begin_ != end_)
        {
            os << ELPP_LITERAL("...");
        }
        os << ELPP_LITERAL("]");
        if (ELPP->hasFlag(LoggingFlag::AutoSpacing))
        {
            os << " ";
        }
    }
    return *this;
}

} // namespace base
} // namespace el

#endif
