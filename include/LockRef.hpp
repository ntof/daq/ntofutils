/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-17T09:37:02+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMDATAREF_HPP__
#define DIMDATAREF_HPP__

#include <mutex>

#include "DIMData.h"

namespace ntof {
namespace utils {

/**
 * @brief hold an object keeping a mutex locked
 * @details this is used in DIMParamList to access DIMData in a thread-safe way
 */
template<typename T>
class LockRef
{
public:
    LockRef(T &data, std::unique_lock<std::mutex> &lock) : m_data(data)
    {
        m_lock.swap(lock);
    }
    LockRef(LockRef &&other) : m_data(other.m_data)
    {
        m_lock.swap(other.m_lock);
    }
    LockRef(const LockRef &other) = delete;
    LockRef &operator=(const LockRef &other) = delete;

    inline T &operator*() { return m_data; }
    inline const T &operator*() const { return m_data; }
    inline T *operator->() { return &m_data; };
    inline const T *operator->() const { return &m_data; }

protected:
    std::unique_lock<std::mutex> m_lock;
    T &m_data;
};

} // namespace utils
} // namespace ntof

#endif
