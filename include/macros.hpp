//
// Created by matteof on 7/17/20.
//

#ifndef NTOFUTILS_MACROS_HPP
#define NTOFUTILS_MACROS_HPP

// TODO when switching to c++14, use [[deprecated]] instead
#ifndef DEPRECATED
#ifdef __GNUC__
#define DEPRECATED(MSG) __attribute__((deprecated(#MSG)))
#elif defined(_MSC_VER)
#define DEPRECATED(MSG) __declspec(deprecated(#MSG))
#else
#define DEPRECATED(MSG)
#endif
#endif

#define RVALUE_ONLY(T) \
    class = typename std::enable_if<!std::is_lvalue_reference<T>::value>::value

#endif // NTOFUTILS_MACROS_HPP
