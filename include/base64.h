/*
 * base64.h
 *
 *  Created on: Mar 18, 2016
 *      Author: mdonze
 */

#ifndef INCLUDE_BASE64_H_
#define INCLUDE_BASE64_H_

#include <string>

std::string base64_encode(unsigned char const *, unsigned int len);
std::string base64_decode(std::string const &s);

#endif /* INCLUDE_BASE64_H_ */
