#ifndef DAQTYPE_H_
#define DAQTYPE_H_

#include <ctime>
#include <ostream>
#include <string>
#include <vector>

#include <stdint.h>

#define DATA_SCALE 1000;
#define NB_MAX_CHANNEL 256
#define NB_MAX_ADDH_PARAM 256
#define VALUE_MAX_SIZE 256
#define WORD_SIZE 4

namespace pugi {
class xml_node;
}

/**
 * \struct ParamAqu
 * \brief One of the parameters of the acquisition cards
 *
 * Allow to group all the data about one of the parameters of the acquisition
 * cards. The value is given as a char[] and will be convert later.
 */
namespace ChannelConfiguration {
enum paramIDs
{
    enabled = 1,
    detectorType,
    detectorId,
    moduleType,
    channel,
    module,
    chassis,
    stream,
    sampleRate,
    sampleSize,
    fullScale,
    delayTime,
    threshold,
    thresholdSign,
    zeroSuppressionStart,
    offset,
    preSample,
    postSample,
    clockState,
    inputImpedance
};
}
/* ************************************************* */
/*                                                   */
/*                 Writer Structures                 */
/*                                                   */
/* ************************************************* */

namespace writer {
enum maps
{
    fileHeader = 1,
    dataHeader,
    data
};
}

typedef uint32_t HeaderType;

/**
 * @brief header file lookup
 * @details brief convenience class to be used with ifstream to retrieve next
 * header type
 */
struct HeaderLookup
{
    HeaderLookup();
    HeaderType type;
};
std::istream &operator>>(std::istream &is, HeaderLookup &lookup);

class ExperimentArea
{
public:
    const std::string name;
    const uint32_t min;
    const uint32_t max;

    /**
     * @brief retrieve the area name
     * @param  area the area
     * @return null ExperimentArea object (empty name) on failure
     */
    static const ExperimentArea &findArea(uint32_t runNumber);

    /**
     * @brief find area according to its name
     * @param  name area name (EAR1, EAR2 ...)
     * @return null ExperimentArea object (empty name) on failure
     */
    static const ExperimentArea &findArea(const std::string &name);

    static const ExperimentArea EAR1;
    static const ExperimentArea EAR2;
    static const ExperimentArea EAR3;
    static const ExperimentArea LAB;
};

/**
 * \struct RCTR_HEADER
 * \brief Struct for the RCTR (Run Control Header).
 *
 * In this structure, we store all the variables necessary to describe the run.
 *
 */
struct RunControlHeader
{
    RunControlHeader();

    RunControlHeader(const RunControlHeader &) = default;
    RunControlHeader &operator=(const RunControlHeader &) = default;

    /**
     * @brief load from xml
     * @param root xml root node
     *
     * @throws NTOFException on error
     */
    void fromXML(const pugi::xml_node &root);

    inline uint32_t numberOfModules() const
    {
        return numberOfChannelsPerModule.size();
    }

    uint32_t numberOfChannels() const;

    std::string getExperiment() const;
    void setExperiment(const std::string &type);

    /**
     * @brief update experiment value according to runNumber
     */
    void updateExperiment();

    /**
     * @brief set current date/time using current localtime
     * @param[in] timestamp timestamp to use
     */
    void updateDateTime(std::time_t timestamp);
    inline void updateDateTime() { updateDateTime(std::time(nullptr)); }

    uint32_t runNumber; // Is created automatically by the data acquisition
                        // software and is increased every time when the user
                        // stops the run
    uint32_t segmentNumber; // If the size of a data stream reaches the 2 GB
                            // limit, the file is closed and a new file is
                            // created. The run extension number is increased
                            // for each file.
    uint32_t streamNumber; // It is used to indent the type of stream, the index
                           // stream has the stream number 0 and the data
                           // streams have the stream numbers 1.
    uint32_t experiment; // Experiment identifier: EAR1 (Experimental Area 1) or
                         // EAR2 (Experimental area 2)
    uint32_t date; // Start of run date: Encoded as year * 10000 + month * 100 +
                   // day
    uint32_t time; // Start of run time: Encoded as hours * 10000 + minutes *
                   // 100 + seconds
    uint32_t totalNumberOfStreams; // The number of the data streams plus the
                                   // index stream.
    uint32_t totalNumberOfChassis; // The same as the number of streams.
    uint32_t totalNumberOfModules; // The total number of physical digitizer
                                   // cards used in the system
    std::vector<uint32_t> numberOfChannelsPerModule; // Number of used channels
                                                     // per module

    static constexpr HeaderType TITLE = 0x52544352;
    static constexpr uint32_t REVISION = 3;
};
typedef RunControlHeader RCTR_HEADER;

std::ostream &operator<<(std::ostream &os, const RunControlHeader &conf);
std::istream &operator>>(std::istream &is, RunControlHeader &conf);

/**
 * \struct MODH_HEADER
 * \brief Struct for the MODH (Module Header).
 *
 * In this structure, we store all the variables necessary to describe a module.
 *
 *
 * Channel definition for the digitizer modules. This is the setting for one
 * channel and can change for each individual channel the setup is stored in
 * Conf/READOUT.conf (see DaqDefs.h file) file and at the beginning of the index
 * file. The data file (file from one stream)  will only have the local settings
 * relevant for it's readout part.
 *
 * This struct is accessed in the _BOS_MODH class (at header.h file) using a
 * vector for all the channels involved in the current event. The vector is
 * defined as
 *
 * typedef vector< MODH_HEADER > Config_V;
 *
 * Considerations.
 *
 * 1. All the functions are inline defined and they are used just to access all
 * the data defined in the class. Their names can be used to know easy what they
 * do..
 *
 * 2. Full scale encodes the input range and has values from 0 .. 6
 * corresponding to 50mV, 100mV, 200mV, 500mV, 1V, 2V and 5V. Together with the
 * offset in mV, the conversion windows is calculated as
 *
 * -fullscale/2-offset < UInput < fullscale/2-offset
 *
 * where fullscale is the value in mV.
 *
 * 3. For the Threshold calculation. The value in Volts is calculated as
 *
 * U = -fullscale/2 - offset + threshold/256 * fullscale
 *
 */

/*
 typedef struct {
 char title[4];
 uint32_t revNumber;
 uint32_t reserved;
 uint32_t length;
 uint32_t nbChannels;
 CHAN_CONFIG channelsConfig[10];
 } MODH_HEADER;
 */

/**
 * Zero suppression mode for channel
 */
enum ZeroSuppressionMode
{
    INDEPENDANT = 0, //!< INDEPENDANT
    MASTER_SLAVE     //!< MASTER_SLAVE
};

struct ChannelConfig
{
    /**
     * @brief load from xml
     * @param root xml root node
     *
     * @throws NTOFException on error
     */
    void fromXML(const pugi::xml_node &root);

    static uint32_t makeLocation(uint8_t stream,
                                 uint8_t chassis,
                                 uint8_t module,
                                 uint8_t channel);
    static uint8_t locationGetStream(uint32_t str_mod_crate)
    {
        return (str_mod_crate >> 24) & 0xFF;
    }
    static uint8_t locationGetChassis(uint32_t str_mod_crate)
    {
        return (str_mod_crate >> 16) & 0xFF;
    }
    static uint8_t locationGetModule(uint32_t str_mod_crate)
    {
        return (str_mod_crate >> 8) & 0xFF;
    }
    static uint8_t locationGetChannel(uint32_t str_mod_crate)
    {
        return (str_mod_crate) & 0xFF;
    }

    void setLocation(uint32_t location);
    void setLocation(uint8_t stream,
                     uint8_t chassis,
                     uint8_t module,
                     uint8_t channel);
    void setStream(uint8_t stream);
    inline uint8_t getStream() const
    {
        return locationGetStream(str_mod_crate);
    }
    void setChassis(uint8_t stream);
    inline uint8_t getChassis() const
    {
        return locationGetChassis(str_mod_crate);
    }
    void setModule(uint8_t stream);
    inline uint8_t getModule() const
    {
        return locationGetModule(str_mod_crate);
    }
    void setChannel(uint8_t stream);
    inline uint8_t getChannel() const
    {
        return locationGetChannel(str_mod_crate);
    }

    std::string getModuleType() const;
    void setModuleType(const std::string &type);

    std::string getDetectorType() const;
    void setDetectorType(const std::string &type);

    std::string getClockState() const;
    void setClockState(const std::string &state);

    std::string getMasterDetectorType() const;
    void setMasterDetectorType(const std::string &type);

    uint32_t detectorType; // Detector type (4*char, for example 'SILI' or 'C6D6')
    uint32_t detectorId;   // Physical detector channel

    uint32_t moduleType; // "ACQC", "SPDV"

    uint32_t str_mod_crate; // chan/module/chassis/stream

    float sampleRate; // Setting for the digitizer. The sample rate is the
                      // sampling frequency (in MHz) of the digitizer and can be
                      // 250, 400, 500, 1000 for Acqiris modules. MS/s
    uint32_t sampleSize; // Setting for the digitizer. The sample size is the
                         // amount of data (in kB) acquired before zero
                         // suppression. MB
    float fullScale; // Setting for the digitizer. Full scale encodes the input
                     // range and has values from 0 .. 6 corresponding to 50mV,
                     // 100mV, 200mV, 500mV, 1V, 2V and 5V
    int32_t delayTime; // Setting for the digitizer. Delay time is the time
                       // shift between trigger and start of the acquisition:
                       // The acquiris digitizer are able to start data taking
                       // at a given time after or before (!) the trigger. The
                       // number is given in samples.
    int32_t threshold; // Setting for the digitizer. Threshold is used for the
                       // zero suppression and is given in ADC digits, ranging
                       // from 0 to 255.
    int32_t thresholdSign; // Setting for the digitizer. Threshold Sign specifies
                           // whether the part of the pulse below or above the
                           // threshold is wanted. If set to 0, the part below
                           // is wanted, as needed for negative pulses.
    float offset;        // Setting for the digitizer. Offset voltage in mV.
    uint32_t preSample;  // Setting for the digitizer. It is the number of
                         // samples before a threshold crossing which are saved
                         // to disk. Allowed values are 0,16,32,48, ... 2032.
    uint32_t postSample; // Setting for the digitizer. is the number of samples
                         // which are saved to disk after the pulse is below the
                         // threshold again. Allowed values are 0,16,32,48, ...
                         // 4080.
    uint32_t clockState; // INTC /EXTC internal/external clock (Acqiris only)
    uint32_t zeroSuppressionStart; // Used to delay the zeroSuppression
    uint32_t masterDetectorType;   // Master channel detector type
    uint32_t masterDetectorId;     // Master channel detector ID

    static constexpr size_t SIZE = 88; // this header is bigger than
                                       // sizeof(ChannelConfig)
};
typedef ChannelConfig CHAN_CONFIG;

std::ostream &operator<<(std::ostream &os, const ChannelConfig &conf);
std::istream &operator>>(std::istream &is, ChannelConfig &header);

struct ModuleHeader
{
    ModuleHeader(uint32_t nbChannels = 0);

    /**
     * @brief load from xml
     * @param root xml root node
     *
     * @throws NTOFException on error
     */
    void fromXML(const pugi::xml_node &root);

    /**
     * @brief get size of header in bytes
     * @return size of header
     */
    size_t size() const;

    std::vector<ChannelConfig> channelsConfig;

    static constexpr HeaderType TITLE = 0x48444F4D;
    static constexpr uint32_t REVISION = 4;
};
typedef ModuleHeader MODH_HEADER;

std::ostream &operator<<(std::ostream &os, const ModuleHeader &header);
std::istream &operator>>(std::istream &is, ModuleHeader &header);

/**
 * \struct EventHeader
 * \brief Struct for the EVEH (Event Header).
 *
 * In this structure, we store all the variables necessary to describe an event.
 * The EventHeader is followed by the data records (ACQC_HEADER).
 *
 */
struct EventHeader
{
    EventHeader();

    enum BeamType
    {
        CALIBRATION = 1,
        PRIMARY = 2,
        PARASITIC = 3,
        DOUBLE = 4
    };

    /**
     * @brief set local timestamp
     */
    void setCompTS();

    /**
     * @brief set current date/time using current localtime
     * @param[in] timestamp timestamp to use
     */
    void updateDateTime(time_t timestamp);

    void toXML(pugi::xml_node &eveh) const;

    uint32_t sizeOfEvent; // The size of event in words specifies the size of
                          // the EVEH bank including the EVEH header. It is the
                          // 'distance' in words between this EVEH bank and the
                          // next EVEH bank in the file.
    uint32_t eventNumber; // The event number corresponds to the number of
                          // proton pulses since start of the run.
    uint32_t runNumber;   // The run number is the same as in the RCTR bank.
    uint32_t time; // The event time is hhmmss. Encoded as hours * 10000 +
                   // minutes * 100 + seconds.
    uint32_t date; // The event date yyymmdd. Encoded as year * 10000 + month *
                   // 100 + day.
    double compTS; // Computer event time-stamp
    double bctTS;  // BCT acquisition event time-stamp
    float intensity;   // Beam intensity
    uint32_t beamType; // Beam type

    static constexpr HeaderType TITLE = 0x48455645;
    static constexpr uint32_t REVISION = 1;
    static constexpr size_t SIZE = 64;
};
typedef EventHeader EVENT_HEADER;

std::ostream &operator<<(std::ostream &os, const EventHeader &header);
std::istream &operator>>(std::istream &os, EventHeader &header);

typedef struct
{
    uint64_t detectorTimeStamp;
    std::vector<uint16_t> detectorData;
} Data;
std::istream &operator>>(std::istream &is, Data &pulse);

/**
 * \struct ACQC_HEADER
 * \brief Struct for the ACQC (Acquisition Header).
 *
 * In this structure, we store all the variables necessary to describe a data
 * acquisition.
 *
 */
typedef struct
{
    uint32_t title;
    uint32_t revNumber;
    uint32_t reserved;
    uint32_t length;
    uint32_t detectorType; // Detector Type.
    uint32_t detectorId;   // Detector Id.
    // uint32_t detectorChannel; //Detector channel.
    uint32_t str_crate_mod_ch;
} ACQC_HEADER;

/**
 * \struct ACQC_HEADER
 * \brief Struct for the ACQC (Acquisition Header).
 *
 * In this structure, we store all the variables necessary to describe a data
 * acquisition.
 *
 */
typedef struct
{
    uint32_t title;
    uint32_t revNumber;
    uint32_t reserved;
    uint32_t length;
    uint32_t detectorType; // Detector Type.
    uint32_t detectorId;   // Detector Id.
    // uint32_t detectorChannel; //Detector channel.
    uint32_t str_crate_mod_ch;
    Data *data;
} ACQC_HEADER_READER;

/**
 * @brief AcquisitionHeader object
 * @details ACQC_HEADER is still being used by DaqManager, no typedefs on that
 * one
 */
class AcquisitionHeader
{
public:
    AcquisitionHeader();

    void setLocation(uint32_t location);
    void setLocation(uint8_t stream,
                     uint8_t chassis,
                     uint8_t module,
                     uint8_t channel);

    std::string getDetectorType() const;
    void setDetectorType(const std::string &type);

    /**
     * Padding size if dataSize is not word aligned
     */
    size_t padding() const;

    size_t dataSize;       // data size in bytes
    uint32_t detectorType; // Detector type (4*char, for example 'SILI' or 'C6D6')
    uint32_t detectorId;   // Physical detector channel

    uint32_t str_mod_crate; // chan/module/chassis/stream

    static constexpr HeaderType TITLE = 0x43514341;
    static constexpr uint32_t REVISION = 2;
    static constexpr size_t SIZE = 28;
};
std::ostream &operator<<(std::ostream &os, const AcquisitionHeader &header);
std::istream &operator>>(std::istream &is, AcquisitionHeader &header);

typedef struct
{
    uint32_t title;
    uint32_t revNumber;
    uint32_t reserved;
    uint32_t length;
    int64_t start; // Number of bytes to skip starting from end of this header
    int64_t end; // Number of bytes in file to ignore the complete rest of bytes
                 // in the file starting from given offset
} SKIP_HEADER;

/**
 * @brief skip header used to add some padding in a file
 */
class SkipHeader
{
public:
    SkipHeader();
    SkipHeader(int64_t start, int64_t end);

    int64_t start; /* start offset from begining of file */
    int64_t end;   /* end offset from begining of file */

    static constexpr HeaderType TITLE = 0x50494B53;
    static constexpr uint32_t REVISION = 2;
    static constexpr size_t SIZE = 32;
};
std::ostream &operator<<(std::ostream &os, const SkipHeader &header);
std::istream &operator>>(std::istream &is, SkipHeader &header);

class AdditionalDataValue
{
public:
    enum Type
    {
        TypeChar = 0,
        TypeInt32 = 1,
        TypeInt64 = 2,
        TypeFloat = 3,
        TypeDouble = 4
    };

    explicit AdditionalDataValue(Type t = TypeChar, size_t reserve = 1);
    explicit AdditionalDataValue(const std::string &name,
                                 Type t = TypeChar,
                                 size_t reserve = 1);

    AdditionalDataValue(const AdditionalDataValue &) = default;
    AdditionalDataValue &operator=(const AdditionalDataValue &) = default;

    size_t count() const;

    template<typename T>
    T at(size_t i) const
    {
        return *reinterpret_cast<const T *>(&data[i * sizeof(T)]);
    }

    inline std::string toString() const
    {
        return std::string(reinterpret_cast<const char *>(&data[0]),
                           data.size());
    }

    template<typename T>
    void append(const T &t)
    {
        const uint8_t *raw = reinterpret_cast<const uint8_t *>(&t);
        data.insert(data.end(), raw, raw + sizeof(T));
    }

    void fromString(const std::string &value);

    /**
     * @brief reserve data memory
     * @param[i] size number of items to store
     */
    void reserve(size_t size);

    /**
     * @brief header size in bytes
     * @details includes padding
     */
    inline size_t size() const { return 72 + data.size() + padding(); }

    /**
     * @brief required padding size in bytes
     */
    size_t padding() const;

    std::string name;
    Type type;
    std::vector<uint8_t> data;

protected:
    size_t typeSize() const;
};

std::ostream &operator<<(std::ostream &os, const AdditionalDataValue &value);

class AdditionalDataHeader
{
public:
    AdditionalDataHeader() = default;
    AdditionalDataHeader(const AdditionalDataHeader &other) = default;
    AdditionalDataHeader &operator=(const AdditionalDataHeader &other) = default;

    size_t size() const;

    std::vector<AdditionalDataValue> values;

    static constexpr HeaderType TITLE = 0x48444441;
    static constexpr uint32_t REVISION = 0;
};
typedef AdditionalDataHeader ADDH_HEADER;

std::ostream &operator<<(std::ostream &os, const AdditionalDataHeader &header);
std::istream &operator>>(std::istream &is, AdditionalDataHeader &header);

class IndexValue
{
public:
    IndexValue() = default;
    IndexValue(uint32_t stream,
               uint32_t segment,
               uint32_t validatedNumber,
               uint32_t eventNumber,
               uint64_t offset);
    IndexValue(const IndexValue &other) = default;
    IndexValue &operator=(const IndexValue &other) = default;

    uint32_t stream;
    uint32_t segment;
    uint32_t validatedNumber;
    uint32_t eventNumber;
    uint64_t offset;

    static constexpr size_t SIZE = 24;
};

std::ostream &operator<<(std::ostream &os, const IndexValue &header);
std::istream &operator>>(std::istream &is, IndexValue &value);

class IndexHeader
{
public:
    IndexHeader() = default;
    IndexHeader(const IndexHeader &other) = default;
    IndexHeader &operator=(const IndexHeader &other) = default;

    size_t size() const;

    std::vector<IndexValue> indexes;

    static constexpr HeaderType TITLE = 0x58444e49;
    static constexpr uint32_t REVISION = 1;
};

std::ostream &operator<<(std::ostream &os, const IndexHeader &header);
std::istream &operator>>(std::istream &is, IndexHeader &header);

//
///* *********************************** */
///*                                     */
///*         for the index file          */
///*                                     */
///* *********************************** */
//
///**
// * \struct INFO_HEADER
// * \brief Struct for the INFO (Information Header).
// *
// * In this structure, we store all the variables given by the user to describe
// this run.
// *
// */
// typedef struct {
//        uint32_t title;
//        uint32_t revNumber;
//        uint32_t reserved;
//        uint32_t length;
//        uint32_t data[512];
//} INFO_HEADER;
//
///**
// * \struct INFO_HEADER
// * \brief Struct for the INDX (Index Header).
// *
// * In this structure, we store all the data we need for the index file.
// *
// */
// typedef struct {
//        uint32_t title;
//        uint32_t revNumber;
//        uint32_t reserved;
//        uint32_t length;
//        int32_t data[MB_MAX_CHANNEL];
//} INDX_HEADER;
//
///**
// * \struct SLOW_HEADER
// * \brief Struct for the SLOW (Slow Header).
// *
// * Struct for the slow parameters. They are:
// *
// * - Cooling Circuit (from Technical Network)
// *
// * - Conductivity -> CT1 & CT2
// * - Flow -> FT1 & FT2
// * - Oxygen -> Oxygen O2T1 % O2T2
// * - pH -> pH1 & pH2
// * - Pressure -> PT1, PT2, PT3, PT4 & PT5
// * - Temperature -> TT2, TT3, TT4, TT5, TT6, TT6, TT7 & TT8
// *
// * - Moderator Circuit (from Technical Network)
// *
// * - Temperature -> TT9 & TT10
// * - Pressure -> PT6 & PT7
// * - pH -> pH3 & pH4
// * - Conductivity -> CT3, CT4 & CT5
// *
// * - Radiation Monitor (from Technical Network)
// *
// * - Zone 1 -> PAXTOF01, PAXTOF02, PAXTOF03, PAXTOF04 & PAXTOF05
// * - Zone 2 -> PMIBL01 & PMIBL02
// * - Zone 3 -> PMITOF01, PMITOF02, PMITOF03, PMITOF04 & PMITOF05
// *
// * - Magnet Information:
// *
// * - Intensity
// * - Status
// *
// * - Experimental area temperature:
// *
// * -Temperature.
// *
// * - Vacuum Parameters:
// *
// * - RACK 1.
// * - 3 x TPG 300 PFEIFFER VACUUM
// * - SECTOR S1.PARTIE 1
// * - A1: TOF.VGR 1
// * - A2: TOF.VGR 1A
// * - SECTOR S1.PARTIE 2
// * - A1: TOF.VGR 2
// * - A2: TOF.VGR 2A
// * - SECTOR S1.PARTIE 3
// * - A1: TOF.VGR 3
// * - A2: TOF.VGR 3A
// * - RACK 2.
// * - 2 x TPG 300 BALZER
// * - SECTOR EXP.
// * - A1: TOF.VGR EXP
// * - A2: TOF.VGR EXP A
// * - B1: TOF.VGR EXP B
// * - SECTOR S2
// * - A1: TOF.VGR 4
// * - A2: TOF.VGR 4A
// * - RACK 3.
// * - Status of VVS1 VALVE CONTROL UNIT AT680-4256-050 (F = 400)
// * - Status of VVS2 VALVE CONTROL UNIT AT680-4256-060 (F = 200)
// * - RACK 4.
// * - POMPE PRIMAIRE 1. CDE PP1
// * - POMPE PRIMAIRE 2. CDE PP2
// * - POMPE PRIMAIRE 3. CDE PP3
// * - RACK 5.
// * - POMPE PRIMAIRE EXPERIENCE
// * - POMPE PRIMAIRE 4. CDE PP4
// *
// */
//
// typedef struct {
//        uint32_t title;
//        uint32_t revNumber;
//        uint32_t reserved;
//        uint32_t length;
//        float CT1;
//        float CT2;
//        float CT3;
//        float CT4;
//        float CT5;
//        float FT1;
//        float FT2;
//        float O2T1;
//        float O2T2;
//        float PT1;
//        float PT2;
//        float PT3;
//        float PT4;
//        float PT5;
//        float PT6;
//        float PT7;
//        float pH1;
//        float pH2;
//        float pH3;
//        float pH4;
//        float TT2;
//        float TT3;
//        float TT4;
//        float TT5;
//        float TT6;
//        float TT7;
//        float TT8;
//        float TT9;
//        float TT10;
//        float PAXTOF01;
//        float PAXTOF02;
//        float PAXTOF03;
//        float PAXTOF04;
//        float PAXTOF05;
//        float PMIBL01;
//        float PMIBL02;
//        float PMITOF01;
//        float PMITOF02;
//        float PMITOF03;
//        float PMITOF04;
//        float PMITOF05;
//        float MagnetIntensity;
//        int32_t MagnetStatus;
//        // Vacuum definition for EAR1
//        float VAC_TOF_VGR_1_PR;
//        float VAC_TOF_VGR_1_Stat;
//        float VAC_TOF_VGR_1A_PR;
//        float VAC_TOF_VGR_1A_Stat;
//        float VAC_TOF_VGR_2_PR;
//        float VAC_TOF_VGR_2_Stat;
//        float VAC_TOF_VGR_2A_PR;
//        float VAC_TOF_VGR_2A_Stat;
//        float VAC_TOF_VGR_3_PR;
//        float VAC_TOF_VGR_3_Stat;
//        float VAC_TOF_VGR_3A_PR;
//        float VAC_TOF_VGR_3A_Stat;
//        float VAC_TOF_VGR_4_PR;
//        float VAC_TOF_VGR_4_Stat;
//        float VAC_TOF_VGR_4A_PR;
//        float VAC_TOF_VGR_4A_Stat;
//        float VAC_TOF_EXP_PR;
//        float VAC_TOF_EXP_Stat;
//        float VAC_TOF_EXP_A_PR;
//        float VAC_TOF_EXP_A_Stat;
//        float VAC_TOF_EXP_B_PR;
//        float VAC_TOF_EXP_B_Stat;
//        float VAC_TOFVVS01_Stat;
//        float VAC_TOFVVS02_Stat;
//        float VAC_TOFVVS01;
//        float VAC_TOFVVS02;
//        float VAC_TOF2VVS01;
//        // Vacuum definition for EAR2
//        float VAC_TOF2_VGR_1_PR;
//        float VAC_TOF2_VGR_2_PR;
//        float VAC_TOF2_VGR_3_PR;
//        float VAC_TOF2_VGR_4_PR;
//        //Exp area temperature:
//        float ExpTemp_EAR1_IN;
//        float ExpTemp_EAR1_OUT;
//        float ExpTemp_EAR2_IN;
//        float ExpTemp_EAR2_OUT;
//} SLOW_HEADER;
//
///**
// * \struct BEAM_HEADER
// * \brief Struct for the BEAM (Beam Header).
// *
// * In this structure, we store all the variables necessary to describe the
// beam.
// *
// */
// typedef struct {
//        uint32_t title;
//        uint32_t revNumber;
//        uint32_t reserved;
//        uint32_t length;
//        float timestamp; //the date
//        float intensity; //the intensity
//        //char type[4]; //Beam coming from EASTC, TOF, reading failed
//        (beamEASTC, beamTOF, beamInvd). int32_t type; //-1:no type ; 1:TOF ;
//        2:EAST
//} BEAM_HEADER;
//
///**
// * \struct HIVO_HEADER
// * \brief Struct for the HV channel data.
// *
// * This struct is used to store the parameters of a channel of the High
// Voltage
// *
// */
// typedef struct {
//        unsigned short hvBoard; /**< The HV module*/
//        unsigned short hvChannel; /**< The HV channel in the module.*/
////        unsigned short index; /**< Index to locate the elements in the GUI
/// table.*/
//        float VSet; /**< Vset in Volts.*/
//        float VMon; /**< Vmon in Volts.*/
//        float VMax; /**< Vmax in Volts.*/
//        float ISet; /**< Iset in microAmperes.*/
//        float IMon; /**< Imon in Volts.*/
//        int32_t Status; /**< 0 = off (V<Vset-deltaV), 1 = on |Vset-V|<deltaV 2
//        = V>Vset+deltaV*/ bool Pw; /**< On/Off */ float RDWn; /**< Fall speed
//        for voltage.*/ float RUp; /**< Rise speed for voltage.*/ float Trip;
//        /**< Maximum time in overcurrent situation.*/
////        char hvCrate[12]; /**< May be 0, if only one crate is used.*/
//        char hvChannelName[12]; /**< Defined channel Name (by the user).*/
//} HIVO_HEADER;
//
//
//
///*
///*****************************************************************************************************************************************************************************************************************************
///*/
//
//
////                DEPRECATED BUT STILL USED
//
//
///*
///*****************************************************************************************************************************************************************************************************************************
///*/
//
///**
// * \struct CHAN_HEADER
// * \brief Struct for the Channel configuration (Event Header).
// *
// * In this structure, we store all the variables necessary to describe an
// event.
// * The EVENT_HEADER is followed by the data records (ACQC_HEADER).
// *
// */
// typedef struct {
//        char detectorType[5]; // Detector type (4*char, for example 'SILI' or
//        'C6D6') uint32_t detectorId; // Physical detector channel char
//        moduleType[5]; unsigned char channel; unsigned char module; unsigned
//        char chassis; unsigned char stream; uint32_t sampleRate; // Setting
//        for the digitizer. The sample rate is the sampling frequency (in MHz)
//        of the digitizer and can be 250, 400, 500, 1000 for Acqiris modules.
//        MS/s uint32_t sampleSize; // Setting for the digitizer. The sample
//        size is the amount of data (in kB) acquired before zero suppression.
//        MB uint32_t fullScale; // Setting for the digitizer. 50mV, 100mV,
//        200mV, 500mV, 1V, 2V and 5V int32_t delayTime; // Setting for the
//        digitizer. Delay time is the time shift between trigger and start of
//        the acquisition: The acquiris digitizer are able to start data taking
//        at a given time after or before (!) the trigger. The number is given
//        in samples. uint32_t threshold; // Setting for the digitizer.
//        Threshold is used for the zero suppression and is given in ADC digits,
//        ranging from 0 to 255. int32_t thresholdSign; // Setting for the
//        digitizer. Threshold Sign specifies whether the part of the pulse
//        below or above the threshold is wanted. If set to 0, the part below is
//        wanted, as needed for negative pulses. float offset; // Setting for
//        the digitizer. Offset voltage in mV. uint32_t preSample; // Setting
//        for the digitizer. It is the number of samples before a threshold
//        crossing which are saved to disk. Allowed values are 0,16,32,48, ...
//        2032. uint32_t postSample; // Setting for the digitizer. is the number
//        of samples which are saved to disk after the pulse is below the
//        threshold again. Allowed values are 0,16,32,48, ... 4080. char
//        clockState[5]; // INTC /EXTC internal/external clock (Acqiris only)
//        float inputImpedance;
//        bool enabled;
//} CHAN_CONFIG_REV0;
//
// typedef struct {
//        uint32_t detectorType; // Detector type (4*char, for example 'SILI' or
//        'C6D6') uint32_t detectorId; // Physical detector channel char
//        moduleType[5]; // 1 = acqiris (USED). 2 = Other type of ADCs.. uint32_t
//        str_mod_crate; uint32_t sampleRate; // Setting for the digitizer. The
//        sample rate is the sampling frequency (in MHz) of the digitizer and
//        can be 250, 400, 500, 1000 for Acqiris modules. MS/s uint32_t
//        sampleSize; // Setting for the digitizer. The sample size is the
//        amount of data (in kB) acquired before zero suppression. MB uint32_t
//        fullScale; // Setting for the digitizer. Full scale encodes the input
//        range and has values from 0 .. 6 corresponding to 50mV, 100mV, 200mV,
//        500mV, 1V, 2V and 5V int32_t delayTime; // Setting for the digitizer.
//        Delay time is the time shift between trigger and start of the
//        acquisition: The acquiris digitizer are able to start data taking at a
//        given time after or before (!) the trigger. The number is given in
//        samples. uint32_t threshold; // Setting for the digitizer. Threshold
//        is used for the zero suppression and is given in ADC digits, ranging
//        from 0 to 255. uint32_t thresholdSign; // Setting for the digitizer.
//        Threshold Sign specifies whether the part of the pulse below or above
//        the threshold is wanted. If set to 0, the part below is wanted, as
//        needed for negative pulses. float offset; // Setting for the
//        digitizer. Offset voltage in mV. uint32_t preSample; // Setting for
//        the digitizer. It is the number of samples before a threshold crossing
//        which are saved to disk. Allowed values are 0,16,32,48, ... 2032.
//        uint32_t postSample; // Setting for the digitizer. is the number of
//        samples which are saved to disk after the pulse is below the threshold
//        again. Allowed values are 0,16,32,48, ... 4080. uint32_t clockState;
//        // INTC /EXTC internal/external clock (Acqiris only)
//} CHAN_CONFIG_WRITER_REV0;
//
// typedef struct {
//        uint32_t title;
//        uint32_t revNumber;
//        uint32_t reserved;
//        uint32_t length;
//        uint32_t nbChannels;
//        CHAN_CONFIG_WRITER_REV0 channelsConfig[MB_MAX_CHANNEL];
//} MODH_HEADER_REV0;

#endif
