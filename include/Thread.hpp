/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-06T10:01:02+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferari.1@cern.ch>
**
*/
#ifndef NTOFUTILS_THREAD_HPP
#define NTOFUTILS_THREAD_HPP

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

namespace ntof {
namespace utils {

class Thread
{
public:
    Thread() : Thread(std::string()) {}; // For backward compatibility
    /**
     * @brief Thread constructor
     * @param[in] name name of the thread (used with pthread to set the name)
     */
    explicit Thread(const std::string &name);

    virtual ~Thread();

    /* disable copy */
    Thread(const Thread &) = delete;
    Thread &operator=(const Thread &) = delete;
    /* allow rvalue construct */
    Thread(Thread &&) noexcept;

    /**
     * @brief start the thread
     */
    void start();

    /**
     * @brief stop the thread
     */
    void stop();

    /**
     * @brief run the thread function in the current context
     */
    void run();

    /**
     * @brief wake the thread
     */
    void wake();

    /**
     * @brief set interval (auto wake)
     */
    void setInterval(const std::chrono::milliseconds &ms);

    /**
     * @brief set policy and priority
     * @details see "man pthread_setschedparam"
     */
    void setPriority(int policy, int32_t priority);

    /**
     * @brief first function executed after the thread is started
     */
    virtual void thread_enter() {};

    /**
     * @brief function executed on every awake of the thread
     */
    virtual void thread_func() = 0;

    /**
     * @brief function executed after stop is called
     */
    virtual void thread_exit() {};

protected:
    void _thread_func();
    bool _set_thread_priority(int policy, int32_t priority);

    std::string m_name;
    std::atomic<bool> m_started;
    bool m_waked;
    int m_policy;
    int32_t m_priority;

    mutable std::mutex m_mutex;
    mutable std::condition_variable m_cond;
    std::unique_ptr<std::thread> m_thread;
    std::chrono::milliseconds m_interval;
};

} // namespace utils
} // namespace ntof

#endif // NTOFUTILS_THREAD_HPP
