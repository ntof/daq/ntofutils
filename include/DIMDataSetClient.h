/*
 * DIMDataSetClient.h
 *  Client for dataset services
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#ifndef DIMDATATSETCLIENT_H_
#define DIMDATATSETCLIENT_H_

#include <mutex>
#include <string>
#include <vector>

#include "DIMData.h"
#include "DIMException.h"
#include "DIMXMLInfo.h"
#include "Signals.hpp"

namespace ntof {
namespace dim {

class DIMDataSetClient;

/**
 * Handler class to get notified when dataset data is published
 * @deprecated replaced by signals
 */
class DIMDataSetClientHandler
{
public:
    DIMDataSetClientHandler();
    virtual ~DIMDataSetClientHandler() {};
    /**
     * Called when the DIM service is refreshed by the server
     * @param dataSet New list of data
     * @param client Client responsible of this callback (can be shared)
     */
    virtual void dataChanged(std::vector<DIMData> &dataSet,
                             const DIMDataSetClient &client) = 0;

    /**
     * Called when an error occured on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    virtual void errorReceived(const std::string &errMsg,
                               const DIMDataSetClient &client) = 0;
};

class DIMDataSetClient : private DIMXMLInfoHandler
{
public:
    typedef ntof::utils::signal<void(DIMDataSetClient &client)> DataSignal;
    typedef ntof::utils::signal<void(const std::string &message,
                                     DIMDataSetClient &client)>
        ErrorSignal;

    /**
     * Constructs a new client to connect to a DIMDataSet service
     * @param srvName DIM dataset service name to connect to
     * @param hdl Handler to notify at service change
     */
    explicit DIMDataSetClient(const std::string &srvName,
                              DIMDataSetClientHandler *hdl = nullptr);
    /**
     * @details api to use with dataSignal and errorSignal,
     * do subscribe once signals are connected.
     */
    explicit DIMDataSetClient(DIMDataSetClientHandler *hdl = nullptr);

    DIMDataSetClient(const DIMDataSetClient &) = delete;
    DIMDataSetClient &operator=(const DIMDataSetClient &) = delete;

    /**
     * Default destructor
     */
    virtual ~DIMDataSetClient();

    /**
     * Gets the DIM service name used by this client
     * @return
     */
    const std::string &getServiceName() const { return m_svcName; }

    /**
     * Comparison operator, based on DIM service name
     * @param lhs Other object to compare
     * @return  True if DIM service name are identical
     */
    inline bool operator==(const DIMDataSetClient &lhs) const
    {
        return lhs.m_svcName == m_svcName;
    }

    /**
     * Gets the list of actual datas
     * @return A vector (copy) containing datas
     */
    std::vector<DIMData> getLatestData() const;

    /**
     * Sets the handler used to be notified when the server updates the DIM
     * service
     * @param hdl Handler to be used
     */
    void setHandler(DIMDataSetClientHandler *hdl);

    /**
     * @brief (re-)subscribe the given service
     * @param[in] serviceName the service to subscribe
     */
    void subscribe(const std::string &serviceName);

    /**
     * @brief unsubscribe any connected service
     * @details does clear the serviceName
     */
    void unsubscribe();

    /**
     * @delay object destruction
     * @details used to disconnect from signal handlers
     * @details DIM lock will be held during destruction
     */
    virtual void deleteLater();

    DataSignal dataSignal;
    ErrorSignal errorSignal;

    using DIMXMLInfoHandler::NoLinkError;

protected:
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc, const DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const DIMXMLInfo *info) override;

    std::unique_ptr<DIMXMLInfo> m_info; //!< Acquisition service
    DIMDataSetClientHandler *m_handler; //!< Callback class
    std::string m_svcName;              //!< Service name
    std::vector<DIMData> m_datas;       //!< Vector containing data
    mutable std::mutex m_lock;          //!< Mutex to protect concurrent access
};

} // namespace dim
} // namespace ntof

#endif /* DIMPARAMLISTCLIENT_H_ */
