/*
 * Timestamp.h
 *
 *  Created on: Apr 20, 2016
 *      Author: mdonze
 */

#ifndef SRC_TIMESTAMP_H_
#define SRC_TIMESTAMP_H_

#include <stdint.h>

namespace ntof {
namespace utils {

class Timestamp
{
public:
    /**
     * Default constructor
     * Initialize the time with actual computer clock
     */
    Timestamp();

    /**
     * Constructor
     * @param time Timestamp in usec since epoch
     */
    explicit Timestamp(int64_t time);

    virtual ~Timestamp();

    bool operator<(const Timestamp &other) const;
    bool operator>(const Timestamp &other) const;
    bool operator==(const Timestamp &other) const;

    Timestamp operator-(const Timestamp &other) const;
    Timestamp operator+(const Timestamp &other) const;

    Timestamp operator-(int64_t uSec) const;
    Timestamp operator+(int64_t uSec) const;

    operator int64_t() const;

    /**
     * Gets the timestamp in usec since epoch UTC
     * @return
     */
    int64_t getValue() const;

private:
    int64_t timeUsec_;
};

} /* namespace utils */
} /* namespace ntof */

#endif /* SRC_TIMESTAMP_H_ */
