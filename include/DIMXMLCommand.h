/*
 * DIMXMLCommand.h
 *
 *  Created on: Nov 7, 2014
 *      Author: mdonze
 */

#ifndef DIMXMLCOMMAND_H_
#define DIMXMLCOMMAND_H_

#include <memory>
#include <string>

#include <pugixml.hpp>

#include <dic.hxx>
#include <dis.hxx>

namespace ntof {
namespace dim {

class DIMXMLCommand;

/**
 * Callback handler for DIMXMLCommands received
 */
class DIMXMLCommandHandler
{
public:
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param cmd DIMXMLCommand object who made this callback
     */
    virtual void errorReceived(std::string errMsg, const DIMXMLCommand *cmd) = 0;

    /**
     * Callback when a new DIMXMLCommand is received
     * @param doc XML document containing new data
     * @param cmd DIMXMLCommand object who made this callback
     */
    virtual void dataReceived(pugi::xml_document &doc,
                              const DIMXMLCommand *cmd) = 0;
    virtual ~DIMXMLCommandHandler() {};
};

/**
 * Class for receiving an XML DIM command
 */
class DIMXMLCommand : public DimCommandHandler
{
public:
    /**
     * Build a new DIM XML command server
     * @param cmdName Name of the command
     * @param hdl Handler to be called when a new command is received
     */
    DIMXMLCommand(std::string cmdName, DIMXMLCommandHandler *hdl);

    /**
     * Destructor
     */
    virtual ~DIMXMLCommand();

    /**
     * Comparison operator
     * @param other
     * @return
     */
    bool operator==(DIMXMLCommand const &other) const;

    /**
     * Send a XML command in non-blocking mode
     * @param cmdName Name of the command
     * @param doc XML document to be sent
     */
    static void sendCommandNB(std::string cmdName, pugi::xml_document &doc);

    /**
     * Send a XML command in blocking mode
     * @param cmdName Name of the command
     * @param doc XML document to be sent
     */
    static void sendCommand(std::string cmdName, pugi::xml_document &doc);

private:
    void commandHandler(); //!<< DIM command handler implementation
    DIMXMLCommand(const DIMXMLCommand &); //!<< Don't allow copy of this
    DIMXMLCommand &operator=(const DIMXMLCommand &); //!<< Don't allow copy of
                                                     //!< this
    std::unique_ptr<DimCommand> m_dimCmd;            //!<< DIM Command object
    DIMXMLCommandHandler *handler_;                  //!<< Callback handler
};

} /* namespace dim */
} /* namespace ntof */

#endif /* DIMXMLCOMMAND_H_ */
