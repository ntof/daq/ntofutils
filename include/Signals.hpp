/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-21T09:57:38+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef SIGNALS_HPP__
#define SIGNALS_HPP__

#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <mutex>

#include <boost/shared_ptr.hpp>

// see: https://bugzilla.redhat.com/show_bug.cgi?id=999320
#include <boost/config.hpp>
#if defined(__clang__) && __cplusplus >= 201103L
#undef BOOST_NO_CXX11_HDR_TUPLE
#endif
#include <boost/signals2.hpp>

namespace ntof {
namespace utils {

/**
 * @brief convenience slot tracking class
 * @details use this class to safely disconnect signals in multi-threaded
 * environment
 * @details signal is disconnected when this object is destroyed, ensuring that
 * no callbacks are in progress
 * @details do not destroy this object in its signal handler
 */
class ScopedSlot
{
public:
    ScopedSlot() = default;

    template<typename Signal, typename Callback>
    ScopedSlot(Signal &signal, Callback cb)
    {
        connect(signal, cb);
    }

    ~ScopedSlot();

    template<typename Signature>
    boost::signals2::slot<Signature> track(std::function<Signature> f)
    {
        if (!m_track)
            m_track.reset(new std::lock_guard<std::mutex>(m_mutex));
        return boost::signals2::slot<Signature>(f).track(m_track);
    }

    template<typename Signal, typename Callback>
    boost::signals2::connection connect(Signal &signal, Callback cb)
    {
        if (!m_track)
            m_track.reset(new std::lock_guard<std::mutex>(m_mutex));
        return signal.connect(((typename Signal::slot_type)(cb)).track(m_track));
    }

    void disconnect();

protected:
    boost::shared_ptr<std::lock_guard<std::mutex>> m_track;
    std::mutex m_mutex;
};
typedef ScopedSlot scoped_slot;

/**
 * @brief wait on a signal
 */
class SignalWaiter
{
public:
    SignalWaiter();

    template<typename S>
    explicit SignalWaiter(S &sig) : m_count(0)
    {
        listen(sig);
    }

    SignalWaiter(const SignalWaiter &other) = delete;
    SignalWaiter &operator=(const SignalWaiter &other) = delete;
    ~SignalWaiter();

    void reset();
    bool wait(std::size_t count, const std::chrono::milliseconds &ms);
    bool wait(std::function<bool()> check, const std::chrono::milliseconds &ms);

    inline bool wait(std::size_t count = 1, unsigned int ms = 3000)
    {
        return wait(count, std::chrono::milliseconds(ms));
    }
    inline bool wait(std::function<bool()> check, unsigned int ms = 3000)
    {
        return wait(check, std::chrono::milliseconds(ms));
    }

    template<typename S>
    void listen(S &sig)
    {
        m_slot.connect(sig, std::bind(&SignalWaiter::operator(), this));
    }

    inline std::size_t count() const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return m_count;
    }

protected:
    void operator()();

    ntof::utils::scoped_slot m_slot;
    std::size_t m_count;
    mutable std::mutex m_lock;
    std::condition_variable m_cond;
};
typedef SignalWaiter signal_waiter;

/*
 * Type aliases for boost::signals2
 */
template<class... Types>
using signal = boost::signals2::signal<Types...>;
using scoped_connection = boost::signals2::scoped_connection;
using connection = boost::signals2::connection;

} // namespace utils
} // namespace ntof

#endif
