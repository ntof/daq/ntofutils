/*
 * ConfigMisc.h
 *
 *  Created on: Jan 29, 2015
 *      Author: agiraud
 */

#ifndef CONFIG_MISC_H_
#define CONFIG_MISC_H_

#include <map>
#include <string>

#include <sys/types.h>

namespace ntof {
namespace utils {
class ConfigMisc
{
public:
    typedef std::map<int32_t, std::string> DaqMap;

    ConfigMisc();
    explicit ConfigMisc(const std::string &configFile);
    virtual ~ConfigMisc();

    int32_t getDaqID(std::string daqName);
    const std::string &getDimDns() const;
    inline int getDimDnsPort() const { return dimDnsPort_; }
    const std::string &getExperimentalArea() const;
    const DaqMap &getDaq() const;
    const std::string &getOraclePass() const;
    const std::string &getOracleServer() const;
    const std::string &getOracleUser() const;
    inline const std::string &lockFile() const { return m_lockFile; }
    int32_t getEventsPerFile() const;

    /**
     * Encode a password
     * @param pass
     * @return
     */
    static std::string encodePassword(const std::string &pass);

    /**
     * Decodes the given encoded password
     * @param pass
     * @return
     */
    static std::string decodePassword(const std::string &pass);

protected:
    void loadConfig(const std::string &configFile);

    DaqMap daq_;
    std::string dimDns_;
    int dimDnsPort_;
    std::string experimentalArea_;

    // nTOF Database credentials
    std::string oracleUser_;
    std::string oraclePass_;
    std::string oracleServer_;

    int32_t eventsPerFile_;

    std::string m_lockFile;
};

} // namespace utils
} /* namespace ntof */

#endif /* CONFIG_MISC_H_ */
