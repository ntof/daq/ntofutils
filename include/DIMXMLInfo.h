/*
 * DIMXMLInfo.h
 *
 *  Created on: Nov 7, 2014
 *      Author: mdonze
 */

#ifndef DIMXMLINFO_H_
#define DIMXMLINFO_H_

#include <string>

#include <pugixml.hpp>

#include "Signals.hpp"

#include <dic.hxx>

namespace ntof {
namespace dim {
class DIMXMLInfo;

class DIMXMLInfoHandler
{
public:
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    virtual void errorReceived(std::string errMsg, const DIMXMLInfo *info) = 0;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    virtual void dataReceived(pugi::xml_document &doc,
                              const DIMXMLInfo *info) = 0;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    virtual void noLink(const DIMXMLInfo *info) = 0;
    virtual ~DIMXMLInfoHandler() {};

    /**
     * @details most derivate class inherit from this one, let's expose error
     * code
     */
    static const std::string &NoLinkError;
};

/**
 * This class is used to read XML content from a DIMXMLService server
 */
class DIMXMLInfo : public DimInfo
{
public:
    typedef ntof::utils::signal<void(pugi::xml_document &doc, DIMXMLInfo &info)>
        DataSignal;
    typedef ntof::utils::signal<void(const std::string &message,
                                     DIMXMLInfo &client)>
        ErrorSignal;

    /**
     * @brief create an empty DIMXMLInfo
     * @details this constructor is designed to be used with signals
     * to manually call subscribe once everything is settled.
     */
    explicit DIMXMLInfo();

    /**
     * Construct this
     * @param serviceName Name of the service to connect to
     * @param handler Handler to be call at service update
     */
    DIMXMLInfo(const std::string &serviceName, DIMXMLInfoHandler *handler);

    /**
     * Construct this
     * @param serviceName Name of the service to connect to
     * @param time Time to force service refresh
     * @param handler Handler to be call at service update
     */
    DIMXMLInfo(const std::string &serviceName,
               int time,
               DIMXMLInfoHandler *handler);
    virtual ~DIMXMLInfo();

    void subscribe(const std::string &serviceName, int time = 0);
    void unsubscribe();

    /**
     * @delay object destruction
     * @details used to disconnect from signal handlers
     * @details DIM lock will be held during destruction
     */
    virtual void deleteLater();

    DataSignal dataSignal;
    ErrorSignal errorSignal;

    /**
     * @brief message sent in errorSignal for noLink
     */
    static const std::string NoLinkError;

protected:
    DIMXMLInfoHandler *m_handler;
    std::string m_serviceName;

    using DimInfo::subscribe; // dangerous one, do not expose
    void infoHandler();
};

} /* namespace dim */
} /* namespace ntof */

#endif /* DIMXMLINFO_H_ */
