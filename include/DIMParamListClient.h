/*
 * DIMParamListClient.h
 *  Client for DIMData services
 *  Created on: Oct 17, 2014
 *      Author: mdonze
 */

#ifndef DIMPARAMLISTCLIENT_H_
#define DIMPARAMLISTCLIENT_H_

#include <mutex>
#include <string>
#include <vector>

#include "DIMData.h"
#include "DIMException.h"
#include "DIMSuperClient.h"
#include "DIMXMLInfo.h"
#include "Signals.hpp"

namespace ntof {
namespace dim {

class DIMParamListClient;

/**
 * Handler class to get notified when list of parameters is published
 */
class DIMParamListClientHandler
{
public:
    virtual ~DIMParamListClientHandler() {};
    /**
     * Called when the DIM service is refreshed by the server
     * @param settingsChanged New list of parameters
     * @param list List responsible of this callback
     */
    virtual void parameterChanged(const std::vector<DIMData> &settingsChanged,
                                  const DIMParamListClient &list) = 0;
};

class DIMParamListClient : private DIMXMLInfoHandler
{
public:
    typedef ntof::utils::signal<void(DIMParamListClient &client)> DataSignal;
    typedef ntof::utils::signal<void(const std::string &message,
                                     DIMParamListClient &client)>
        ErrorSignal;

    /**
     * Constructs a new client to connect to a DIMParamList server
     * @param srvName DIM service name to connect to
     */
    explicit DIMParamListClient(const std::string &srvName);

    DIMParamListClient(const DIMParamListClient &) = delete;
    DIMParamListClient &operator=(const DIMParamListClient &) = delete;

    /**
     * Default destructor
     */
    virtual ~DIMParamListClient();

    /**
     * Gets the DIM service name used by this client
     * @return
     */
    const std::string &getServiceName() const;

    // TODO: Move this to cpp source
    /**
     * Comparison operator, based on DIM service name
     * @param lhs Other object to compare
     * @return  True if DIM service name are identical
     */
    inline bool operator==(const DIMParamListClient &lhs) const
    {
        return lhs.m_svcName == m_svcName;
    }

    /**
     * Send a new parameter to server
     * @param newParam DIMData to be set
     */
    DIMAck sendParameter(const DIMData &newParam);

    /**
     * Send a list of parameters to server
     * @param newParam vector containing parameters to be sent to server
     */
    DIMAck sendParameters(const std::vector<DIMData> &newParam);

    /**
     * Set a specific parameter and send it
     * @param index Index of the parameter to be set
     * @param value Value of the parameter to be set
     */
    template<typename T>
    DIMAck setParameter(int32_t index, const T &value)
    {
        return sendParameter(DIMData(index, value));
    }

    /**
     * Gets the list of actual parameters
     * @return A vector containing parameters
     */
    std::vector<DIMData> getParameters();

    /**
     * Sets the handler used to be notified when the server updates the DIM
     * service
     * @param hdl Handler to be used
     */
    void setHandler(DIMParamListClientHandler *hdl);

    /**
     * @brief configure the timeout value for sending parameters
     */
    void setTimeOut(const std::chrono::milliseconds &ms);
    const std::chrono::milliseconds &getTimeOut() const;

    /**
     * @delay object destruction
     * @details used to disconnect from signal handlers
     * @details DIM lock will be held during destruction
     */
    virtual void deleteLater();

    inline DIMSuperClient &cmdClient() { return *(m_cmdClient); }

    DataSignal dataSignal;
    ErrorSignal errorSignal;

    using DIMXMLInfoHandler::NoLinkError;

protected:
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc, const DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const DIMXMLInfo *info) override;

    std::unique_ptr<DIMXMLInfo> m_info;   //!< Acquisition service
    DIMParamListClientHandler *m_handler; //!< Callback class
    std::string m_svcName;                //!< Service name
    std::vector<DIMData> m_parameters;    //!< Vector containing parameters
    std::unique_ptr<DIMSuperClient> m_cmdClient; //!< For sending commands
    std::mutex m_lock;
};

} // namespace dim
} // namespace ntof

#endif /* DIMPARAMLISTCLIENT_H_ */
