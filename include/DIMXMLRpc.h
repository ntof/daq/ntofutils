/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-25T08:26:18+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef DIMXMLRPC_H__
#define DIMXMLRPC_H__

#include <memory>
#include <string>

#include <pugixml.hpp>

#include "DIMAck.h"
#include "DIMCmd.h"

namespace ntof {
namespace dim {

class DIMXMLRpcPrivate;

/**
 * Class for receiving an XML DIM Rpc
 */
class DIMXMLRpc
{
public:
    DIMXMLRpc(const std::string &name);
    virtual ~DIMXMLRpc();

    inline DIMAck makeReply(int32_t key) const
    {
        return DIMAck(key, DIMAck::OK, 0);
    }

    void setReply(const DIMAck &reply);
    void setError(int32_t key,
                  int32_t errorCode,
                  const std::string &errorMessage);

    virtual void rpcReceived(DIMCmd &cmd) = 0;

protected:
    friend class DIMXMLRpcPrivate;

    const std::string m_name;
    std::unique_ptr<DIMXMLRpcPrivate> m_private;
};

} // namespace dim
} // namespace ntof

#endif
