/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-13T14:52:00+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef DIMDATA_HXX_
#define DIMDATA_HXX_

#include "DIMData.h"

namespace ntof {
namespace dim {

template<typename T>
void DIMData::addNestedData(Index index,
                            const std::string &name,
                            const std::string &unit,
                            T value)
{
    // getNestedValue() will throw if it is not a Nested Type
    DIMData::List &nestedDataList = getNestedValue();
    nestedDataList.emplace_back(index, name, unit, value);
}

template<typename T>
uint32_t DIMData::addNestedData(const std::string &name,
                                const std::string &unit,
                                T value)
{
    // getNestedValue() will throw if it is not a Nested Type
    const Index index = getNextNestedIndex();
    addNestedData(index, name, unit, value);
    return index;
}

} // namespace dim
} // namespace ntof

#endif /* DIMDATA_HXX_ */
