/*
 * DIMSuperClient.h
 *
 *  Created on: Sep 18, 2014
 *      Author: agiraud
 */

#ifndef DIMSUPERCLIENT_H_
#define DIMSUPERCLIENT_H_

#include <chrono>
#include <iostream>
#include <mutex>
#include <string>

#include <time.h> // For time,

#include "DIMAck.h"
#include "DIMXMLInfo.h" // To get the acknowledge
#include "Queue.h"
#include "macros.hpp"

using std::cout;
using std::endl;

namespace ntof {
namespace dim {
class DIMSuperClient : public DIMXMLInfoHandler
{
public:
    explicit DIMSuperClient(const std::string &name, bool isRpc = false);
    virtual ~DIMSuperClient();
    DIMAck sendCommand(pugi::xml_document &doc);

    DIMSuperClient(const DIMSuperClient &) = delete;
    DIMSuperClient &operator=(const DIMSuperClient &) = delete;

    /**
     * @brief configure the timeout value
     */
    void setTimeOut(const std::chrono::milliseconds &ms);
    const std::chrono::milliseconds &getTimeOut() const;

    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc, const DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const DIMXMLInfo *info) override;

private:
    ntof::utils::Queue<DIMAck *> ack;
    std::chrono::milliseconds m_timeOut;
    std::string serviceName;
    bool listening; //!<< Are we listening for an ack?

    bool m_isRpc;
    std::unique_ptr<DIMXMLInfo> m_dimInfo;
};
} // namespace dim
} // namespace ntof

#endif /* DIMSUPERCLIENT_H_ */
