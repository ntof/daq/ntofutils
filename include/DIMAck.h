/*
 * DIMAck.h
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#ifndef DIMACK_H_
#define DIMACK_H_

#include <memory>
#include <string>

#include <pugixml.hpp>
#include <stdint.h>

namespace ntof {
namespace dim {

//! Represents a command acknowledge received over DIM
class DIMAck
{
public:
    /**
     * Enumeration containing status codes
     */
    enum Status
    {
        Invalid = 0,
        NoAck = 1, //!< No acknowledge received
        OK,        //!< Command execution is ok
        Rejected   //!< Command is rejected by system
    };
    typedef Status STATUS; // backward compatibility

    /**
     * Empty constructor
     */
    DIMAck();

    /**
     * Constructs the acknowledge using DIM received data
     * @param doc XML document received
     */
    explicit DIMAck(pugi::xml_document &doc);

    /**
     * Build an acknowledge with predefined values
     * @param key Command key received from client
     * @param status Status of the command execution \see STATUS enum
     * @param errorCode Error code of execution (0 if execution is ok)
     * @param message Execution message or error message
     */
    DIMAck(int32_t key,
           DIMAck::Status status,
           int32_t errorCode,
           const std::string &message);
    DIMAck(int32_t key, int32_t status, int32_t errorCode);

    /**
     * Copy constructor to copy this class
     * @param other
     */
    DIMAck(const DIMAck &other);
    DIMAck(DIMAck &&other);

    /**
     * Assignment operator
     * @param other Class assigned from
     * @return reference to this
     */
    DIMAck &operator=(const DIMAck &other);
    DIMAck &operator=(DIMAck &&other);

    /**
     * Destructor
     */
    virtual ~DIMAck();

    /**
     * Gets the execution message
     * @return The execution message
     */
    std::string getMessage();

    /**
     * Gets status code
     * @return The status code
     */
    DIMAck::Status getStatus();

    /**
     * Gets error code
     * @return The error code
     */
    int32_t getErrorCode();

    /**
     * Gets the command key
     * @return The command key
     */
    int32_t getKey();

    /**
     * Sets the execution message to this
     * @param message execution message to be set
     */
    void setMessage(const std::string &message);

    /**
     * Sets execution status
     * @param status code of status
     */
    void setStatus(DIMAck::Status status);

    /**
     * Sets the error code
     * @param code Error code to be set
     */
    void setErrorCode(int32_t code);

    /**
     * Sets the command key
     * @param key Command key to be set
     */
    void setKey(int32_t key);

    /**
     * Gets the XML document representing this acknowledge
     * @return
     */
    inline pugi::xml_document &getXMLDocument() { return *doc_; }
    inline const pugi::xml_document &getXMLDocument() const { return *doc_; }

    /**
     * @brief get the XML document root node (acknowledge)
     */
    inline pugi::xml_node &getRoot() { return root_; }
    inline const pugi::xml_node &getRoot() const { return root_; }

protected:
    void clone(pugi::xml_document &doc);

    std::unique_ptr<pugi::xml_document> doc_; //!< XML document representing the
                                              //!< acknowledge
    pugi::xml_node root_;
    pugi::xml_node msg_;
};
} // namespace dim
} // namespace ntof

#endif /* DIMACK_H_ */
