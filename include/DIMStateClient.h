/*
 * DIMStateClient.h
 *
 *  Created on: Jan 29, 2015
 *      Author: mdonze
 */

#ifndef DIMSTATECLIENT_H_
#define DIMSTATECLIENT_H_

#include <condition_variable>
#include <mutex>
#include <string>
#include <vector>

#include "DIMState.h"
#include "DIMXMLInfo.h"
#include "NTOFException.h"

namespace ntof {
namespace dim {

class DIMStateClient;

/**
 * Exception thrown when a client is in error
 */
class DIMStateException : public NTOFException
{
public:
    /**
     * Construct this
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     * @param error Error code
     */
    DIMStateException(const std::string &Msg,
                      const std::string &file,
                      int Line,
                      int error);

    /**
     * Destroy this
     */
    virtual ~DIMStateException() throw();
};

class DIMStateHandler
{
public:
    /**
     * Callback when an error is made by XML parser
     * @param errCode Error code
     * @param errMsg Error message in string
     * @param client DIMStateClient object who made this callback
     */
    virtual void errorReceived(DIMState::ErrorCode errCode,
                               const std::string &errMsg,
                               const DIMStateClient *client) = 0;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param stateValue int32_t value of the state
     * @param stateString string representation of the state
     * @param client DIMStateClient object who made this callback
     */
    virtual void stateReceived(DIMState::State stateValue,
                               const std::string &stateString,
                               const DIMStateClient *client) = 0;

    /**
     * Callback when a not link is present on DIMStateClient
     * @param client DIMStateClient object who made this callback
     */
    virtual void noLink(const DIMStateClient *client) = 0;
    virtual ~DIMStateHandler() {};
};

/**
 * This class is the client of DIMState
 */
class DIMStateClient : public DIMXMLInfoHandler
{
public:
    typedef ntof::utils::signal<void(DIMStateClient &client)> DataSignal;
    typedef ntof::utils::signal<void(const std::string &message,
                                     DIMStateClient &client)>
        ErrorSignal;
    typedef std::vector<DIMErrorWarning> ErrWarnVector;

    /**
     * Construct a DIMState client for using in polling mode
     * @param serviceName Name of the DIM service of the state to connect to
     */
    explicit DIMStateClient(const std::string &serviceName);

    /**
     * Construct a DIMState client for using in asynchronous mode
     * @param serviceName Name of the DIM service of the state to connect to
     * @param handler Pointer to callback to be called
     */
    DIMStateClient(const std::string &serviceName, DIMStateHandler *handler);

    virtual ~DIMStateClient();

    /**
     * Gets actual state in int32_t format
     * This code raises a DIMStateException in case the client sends an error
     * @return ID of the state
     */
    DIMState::State getActualState();

    /**
     * Gets actual state in string format
     * This code raises a DIMStateException in case the client sends an error
     * @return Name of the state (as provided by client)
     */
    std::string getActualStateAsString();

    /**
     * Waits for new state during specified timeout
     * Default timeout time will make the method to wait without timeout
     * This code raises a DIMStateException in case the client sends an error or
     * timedout
     * @param timeOutMs Timeout in ms (negative means no timeout)
     * @return State in int32_t format
     */
    DIMState::State waitForNewState(long timeOutMs = -1);

    /**
     * Waits for new state during specified timeout
     * Default timeout time will make the method to wait without timeout
     * This code raises a DIMStateException in case the client sends an error or
     * timedout
     * @param timeOutMs Timeout in ms (negative means no timeout)
     * @return State in string format
     */
    std::string waitForNewStateAsString(long timeOutMs = -1);

    /**
     * Gets list of active error
     * @return
     */
    ErrWarnVector getActiveErrors();

    /**
     * Gets list of active warnings
     * @return
     */
    ErrWarnVector getActiveWarnings();

    DataSignal dataSignal;
    ErrorSignal errorSignal;

private:
    /**
     * @see DIMXMLInfoHandler
     * @param errMsg
     * @param info
     */
    void errorReceived(std::string errMsg, const DIMXMLInfo *info) override;

    /**
     * @see DIMXMLInfoHandler
     * @param doc
     * @param info
     */
    void dataReceived(pugi::xml_document &doc, const DIMXMLInfo *info) override;

    /**
     * @see DIMXMLInfoHandler
     * @param info
     */
    void noLink(const DIMXMLInfo *info) override;

    /**
     * Gets the latest error message
     * @param errCode
     * @param errMessage
     */
    void getLatestError(int32_t &errCode, std::string &errMessage);

    // Private members
    std::unique_ptr<DIMXMLInfo> m_info; //!<< XML info for connecting to service
    DIMStateHandler *m_hld;  //!<< State handler provided by the client
    std::string m_svcName;   //!<< Service name
    DIMState::State m_state; //!<< Actual state in int
    std::string m_stateStr;  //!<< Actual state in String format
    std::mutex m_lock;
    std::condition_variable m_cond; //!<< Condition variable for synchronization

    ErrWarnVector m_errors;   //!< Active errors
    ErrWarnVector m_warnings; //!< Active warnings
};

} /* namespace dim */
} /* namespace ntof */

#endif /* DIMSTATECLIENT_H_ */
