/*
 * DIMDataSet.h
 *
 *  Created on: Oct 2, 2015
 *      Author: mdonze
 */

#ifndef INCLUDE_DIMDATASET_H_
#define INCLUDE_DIMDATASET_H_

#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>

#include <pugixml.hpp>
#include <stdint.h>

#include "DIMData.h"
#include "DIMException.h"
#include "DIMXMLService.h"
#include "LockRef.hpp"
#include "macros.hpp"

namespace ntof {
namespace dim {

enum class AddMode
{
    CREATE, // It fails if a data already exist with that specified index
    UPDATE, // It fails if data with that index doesn't exist (throw if type
    // is different)
    OVERRIDE, // It will create if not exists. Override if it exists
};

class DIMParamList;

/**
 * Represents a list of data published trought DIM
 */
class DIMDataSet
{
public:
    typedef DIMData::Index Index;
    typedef std::map<Index, DIMData> DataMap;

    /**
     * Constructor
     * @param svcName DIM service name to publish data
     */
    explicit DIMDataSet(const std::string &serviceName);

    DIMDataSet(const DIMDataSet &) = delete; //!< Don't allow copy of this
    DIMDataSet &operator=(const DIMDataSet &) = delete; //!< Don't allow copy of
                                                        //!< this
    virtual ~DIMDataSet();

    /**
     * Add, update or override a DIM Data
     * @tparam T data type
     * @param index Index of the new data
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param mode create only or override mode
     * @param updateNow If true, the DIM list value will be refreshed
     */
    template<typename T>
    void addData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 const T &initValue,
                 AddMode mode = AddMode::CREATE,
                 bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    void addData(Index index,
                 const std::string &name,
                 const std::string &unit,
                 T &&initValue,
                 AddMode mode = AddMode::CREATE,
                 bool updateNow = true);

    /**
     * Add, update or override a DIM Data
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param updateNow If true, the DIM list value will be refreshed
     */
    template<typename T>
    Index addData(const std::string &name,
                  const std::string &unit,
                  const T &initValue,
                  AddMode mode = AddMode::CREATE,
                  bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    Index addData(const std::string &name,
                  const std::string &unit,
                  T &&initValue,
                  AddMode mode = AddMode::CREATE,
                  bool updateNow = true);

    /**
     * Registers a new data to this list
     * @param index Index of the new data
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addData()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addNewData(Index index,
                    const std::string &name,
                    const std::string &unit,
                    const T &initValue,
                    bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addNewData(Index index,
                    const std::string &name,
                    const std::string &unit,
                    T &&initValue,
                    bool updateNow = true);

    /**
     * Registers a new data to this list
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addData()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    Index addNewData(const std::string &name,
                     const std::string &unit,
                     const T &initValue,
                     bool updateNow = true)
    {
        const uint32_t index = getNextIndex();
        addNewData(index, name, unit, initValue, updateNow);
        return index;
    }
    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    Index addNewData(const std::string &name,
                     const std::string &unit,
                     T &&initValue,
                     bool updateNow = true)
    {
        const uint32_t index = getNextIndex();
        addNewData(index, name, unit, std::move(initValue), updateNow);
        return index;
    }

    /**
     * Registers a new data to this list
     * @param index Index of the new data
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addData()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addOrUpdateData(Index index,
                         const std::string &name,
                         const std::string &unit,
                         const T &initValue,
                         bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    void addOrUpdateData(Index index,
                         const std::string &name,
                         const std::string &unit,
                         T &&initValue,
                         bool updateNow = true);

    /**
     * Registers or update a data to this list
     * @param name Name of the new data
     * @param unit Unit if the new data
     * @param initValue Initial value of the data
     * @param updateNow If true, the DIM list value will be refreshed
     *
     * \deprecated This method will go away in future versions. Use instead
     * addData()
     */
    template<typename T>
    DEPRECATED("This api is deprecated. Use instead addData()")
    DIMData::Index addOrUpdateData(const std::string &name,
                                   const std::string &unit,
                                   const T &initValue,
                                   bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    DEPRECATED("This api is deprecated. Use instead addData()")
    DIMData::Index addOrUpdateData(const std::string &name,
                                   const std::string &unit,
                                   T &&initValue,
                                   bool updateNow = true);

    /**
     * Removes a data from the list
     * @param index Index of data to be removed
     */
    void removeData(Index index, bool updateNow = true);

    /**
     * Removes a data from the list
     * @param name Name of data to be removed
     */
    void removeData(const std::string &name, bool updateNow = true);

    /**
     * @brief remove all data from the dataset
     */
    void clearData(bool updateNow = true);

    /**
     * Gets a data double value (data must be a double type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline double getDoubleValue(Index index) const
    {
        return getValue<double>(index);
    }

    /**
     * Gets a data float value (data must be a float type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline float getFloatValue(Index index) const
    {
        return getValue<float>(index);
    }

    /**
     * Gets a data bool value (data must be a bool type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline bool getBoolValue(Index index) const
    {
        return getValue<bool>(index);
    }

    /**
     * Gets a data int8_t value (data must be a int8_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline int8_t getByteValue(Index index) const
    {
        return getValue<int8_t>(index);
    }

    /**
     * Gets a data int16_t value (data must be a int16_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline int16_t getShortValue(Index index) const
    {
        return getValue<int16_t>(index);
    }

    /**
     * Gets a data int32_t value (data must be a int32_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline int32_t getIntValue(Index index) const
    {
        return getValue<int32_t>(index);
    }

    /**
     * Gets a data int64_t value (data must be a int64_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline int64_t getLongValue(Index index) const
    {
        return getValue<int64_t>(index);
    }

    /**
     * Gets a data uint8_t value (data must be a uint8_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline uint8_t getUByteValue(Index index) const
    {
        return getValue<uint8_t>(index);
    }

    /**
     * Gets a data uint16_t value (data must be a uint16_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline uint16_t getUShortValue(Index index) const
    {
        return getValue<uint16_t>(index);
    }

    /**
     * Gets a data uint32_t value (data must be a uint32_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline uint32_t getUIntValue(Index index) const
    {
        return getValue<uint32_t>(index);
    }

    /**
     * Gets a data uint64_t value (data must be a uint64_t type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline uint64_t getULongValue(Index index) const
    {
        return getValue<uint64_t>(index);
    }

    /**
     * Gets a data string value (data must be a string type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline std::string getStringValue(Index index) const
    {
        return getValue<std::string>(index);
    }

    /**
     * Gets a data enum value (data must be a enum type!)
     * @param index Index of the data to be read
     * @return The data value
     */
    inline DIMEnum getEnumValue(Index index) const
    {
        return getValue<DIMEnum>(index);
    }

    /**
     * @brief get nested data values
     * @param index Index of the data to be read
     * @return The data value
     */
    inline DIMData::List getNestedValue(Index index) const
    {
        return getValue<DIMData::List>(index);
    }

    /**
     * Gets a data double value (data must be a double type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline double getDoubleValue(const std::string &name) const
    {
        return getValue<double>(name);
    }

    /**
     * Gets a data float value (data must be a float type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline float getFloatValue(const std::string &name) const
    {
        return getValue<float>(name);
    }

    /**
     * Gets a data bool value (data must be a bool type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline bool getBoolValue(const std::string &name) const
    {
        return getValue<bool>(name);
    }

    /**
     * Gets a data int8_t value (data must be a int8_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline int8_t getByteValue(const std::string &name) const
    {
        return getValue<int8_t>(name);
    }

    /**
     * Gets a data int16_t value (data must be a int16_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline int16_t getShortValue(const std::string &name) const
    {
        return getValue<int16_t>(name);
    }

    /**
     * Gets a data int32_t value (data must be a int32_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline int32_t getIntValue(const std::string &name) const
    {
        return getValue<int32_t>(name);
    }

    /**
     * Gets a data int64_t value (data must be a int64_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline int64_t getLongValue(const std::string &name) const
    {
        return getValue<int64_t>(name);
    }

    /**
     * Gets a data uint8_t value (data must be a uint8_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline uint8_t getUByteValue(const std::string &name) const
    {
        return getValue<uint8_t>(name);
    }

    /**
     * Gets a data uint16_t value (data must be a uint16_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline uint16_t getUShortValue(const std::string &name) const
    {
        return getValue<uint16_t>(name);
    }

    /**
     * Gets a data uint32_t value (data must be a uint32_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline uint32_t getUIntValue(const std::string &name) const
    {
        return getValue<uint32_t>(name);
    }

    /**
     * Gets a data uint64_t value (data must be a uint64_t type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline uint64_t getULongValue(const std::string &name) const
    {
        return getValue<uint64_t>(name);
    }

    /**
     * Gets a data string value (data must be a string type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline std::string getStringValue(const std::string &name) const
    {
        return getValue<std::string>(name);
    }

    /**
     * Gets a data enum value (data must be a enum type!)
     * @param name Name of the data to retrieve
     * @return The data value
     */
    inline DIMEnum getEnumValue(const std::string &name) const
    {
        return getValue<DIMEnum>(name);
    }

    /**
     * @brief return nested values
     * @param  name name of the data to retrieve
     * @return the nested data values
     */
    inline const DIMData::List getNestedValue(const std::string &name) const
    {
        return getValue<DIMData::List>(name);
    }

    /**
     * Sets a value for specified data
     * @param index Index of the data to be set
     * @param value Value of the data to be set
     * @param updateNow If true, DIM service is automatically refreshed
     */
    template<typename T>
    void setValue(Index index, const T &value, bool updateNow = true);
    template<typename T, RVALUE_ONLY(T)>
    void setValue(Index index, T &&value, bool updateNow = true);

    /**
     * Sets a value for specified data
     * @param name Name of the data to be set
     * @param value Value of the data to be set
     * @param updateNow If true, DIM service is automatically refreshed
     */
    template<typename T>
    void setValue(const std::string &name, const T &value, bool updateNow = true)
    {
        setValue(getIndex(name), value, updateNow);
    }
    template<typename T, RVALUE_ONLY(T)>
    void setValue(const std::string &name, T &&value, bool updateNow = true)
    {
        setValue(getIndex(name), std::move(value), updateNow);
    }

    /**
     * @brief used to update data retrieved with getDataAt
     */
    void setData(const DIMData &data, bool updateNow);
    void setData(DIMData &&data, bool updateNow);

    /**
     * Get a value according to its type
     * @param index Index of the data to retrieve
     * @return the data value
     */
    template<typename T>
    T getValue(Index index) const;

    /**
     * Get a value according to its type
     * @param name Name of the data to retrieve
     * @return the data value
     */
    template<typename T>
    T getValue(const std::string &name) const;

    /**
     * Check if a value is of a type
     * @param index Index of the data to retrieve
     * @return true if data is of specified type
     */
    template<typename T>
    bool is(Index index) const;

    /**
     * Check if a value is of a type
     * @param name Name of the data to retrieve
     * @return true if data is of specified type
     */
    template<typename T>
    bool is(const std::string &name) const;

    /**
     * Refreshes associated DIM service
     */
    void updateData()
    {
        std::unique_lock<std::mutex> lock(m_lock);
        _updateData(lock);
    }

    /**
     * Gets the name of the data specified by index
     * @param index
     * @return
     */
    std::string getDataName(Index index) const;

    /**
     * Gets the name of the DIM service associated with this list
     * @return
     */
    inline const std::string &getServiceName() const { return svcName; }

    /**
     * Comparison operator, based on service name
     * @param lhs Other object to be compared to
     * @return True if objects have the same DIM service name
     */
    inline bool operator==(const DIMDataSet &lhs) const
    {
        return lhs.getServiceName() == svcName;
    }

    /**
     * Gets a copy of the DIMData object at specified index
     * @param index Index of the data to be read
     * @return A copy of the data object
     */
    inline DIMData getDataAt(Index index) const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return _findData(index);
    }

    /**
     * @brief retrieve and lock data for modifications
     * @details only one LockRef can be used at a time, those objects are meant
     * to be temporary and just held the time of a modification
     */
    inline utils::LockRef<DIMData> lockDataAt(Index index)
    {
        std::unique_lock<std::mutex> lock(m_lock);
        return utils::LockRef<DIMData>(_findData(index), lock);
    }

    inline utils::LockRef<DIMData> lockDataAt(const std::string &name)
    {
        std::unique_lock<std::mutex> lock(m_lock);
        return utils::LockRef<DIMData>(_findData(name), lock);
    }

    /**
     * Gets number of data
     * @return The number of data into the list
     */
    int getDataCount() const;

    /**
     * @brief test if the DataSet has given index
     * @return true if index exists
     */
    bool hasDataAt(Index index) const;

    /**
     * @brief set/clear hidden flag on given data
     */
    void setHidden(Index index, bool value = true, bool updateNow = true);
    inline bool isHidden(Index index) const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return _findData(index).isHidden();
    }

    /**
     * Build a DataMap from XML document
     * @param doc
     * @return
     */
    static DataMap buildDataMap(const pugi::xml_document &doc);

protected:
    /**
     * @brief protected constructor for DIMParamList
     */
    DIMDataSet(const std::string &serviceName, const std::string &rootName);

    void _updateData(std::unique_lock<std::mutex> &lock);

    /**
     * Finds a data in the list and return its reference
     * @param index Index of the data to get form
     * @return A reference to the data
     */
    const DIMData &_findData(Index index) const;
    DIMData &_findData(Index index);

    /**
     * Finds a data in the list and return its reference
     * @param name Name of the data to get form
     * @return A reference to the data
     */
    inline const DIMData &_findData(const std::string &name) const
    {
        return _findData(_getIndex(name));
    }
    inline DIMData &_findData(const std::string &name)
    {
        return _findData(_getIndex(name));
    }

    /**
     * Gets index of the specified data name
     * @param name
     * @return
     */
    Index _getIndex(const std::string &name) const;
    inline Index getIndex(const std::string &name) const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return _getIndex(name);
    }

    /**
     * Gets the next available index
     * @return
     */
    Index _getNextIndex() const;
    inline Index getNextIndex() const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return _getNextIndex();
    }

    friend class DIMParamList;
    friend class DIMParamListProxy;

    mutable std::mutex m_lock;
    DataMap dataList;                     //!< Map containing list of know datas
    std::string svcName;                  //!< Name of the service
    std::unique_ptr<DIMXMLService> m_svc; //!< DIM service to publish data
    const std::string &m_rootName;

    static const std::string s_rootName;
};

} // namespace dim
} // namespace ntof

#include "DIMDataSet.hxx"

#endif /* INCLUDE_DIMDATASET_H_ */
