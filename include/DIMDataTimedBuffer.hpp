/*
 * Timestamp.h
 *
 *  Created on: Jun 15, 2020
 *      Author: matteof
 */

#ifndef NTOFUTILS_DIMDATATIMEDBUFFER_HPP
#define NTOFUTILS_DIMDATATIMEDBUFFER_HPP

#include <condition_variable>
#include <map>
#include <mutex>

#include <DIMData.h>
#include <Timestamp.h>

namespace ntof {
namespace utils {

class DIMDataTimedBuffer
{
public:
    DIMDataTimedBuffer();
    virtual ~DIMDataTimedBuffer() = default;

    /**
     * @brief Timestamp extractor from DIMData. By default is taking
     * the timestamp at index 1. Override this function for custom extraction
     * @param[in]       event data
     * @param[return]   timestamp of the occurring event
     */
    virtual Timestamp retrieveTimestamp(const dim::DIMData::List &event);

    /**
     * @brief Waits until a new timing event was received
     * @param[in] event   the received event
     */
    void addEvent(const dim::DIMData::List &event);

    /**
     * @brief Waits until a new timing event was received
     * @param[out] event   the received event (invalid event on error)
     * @param[in]  time    timestamp of the occuring event
     * @param[in]  timeout maximum waiting time in milliseconds (default : 200)
     */
    bool getEventByTime(dim::DIMData::List &event,
                        const Timestamp &time,
                        int32_t timeout = 200) const;

    /**
     * Sets the margin in usec used to match (or not) a given event
     * @param margin
     */
    void setTimeMargin(int64_t margin);

    /**
     * Gets the margin in usec used to match (or not) a given event
     * @return Actual margin in usec
     */
    int64_t getTimeMargin() const;

    /**
     * Sets the maximum event numbers to keep
     * @param size
     */
    void setMaxEventsToKeep(int size);

    /**
     * @brief Get last event returned by getEventByTime
     * @param[out] event current event
     */
    void getCurrentEvent(dim::DIMData::List &event) const;

    /**
     * Waits until a new timing event was received
     * @param[out] event the received event
     */
    void waitForNewEvent(dim::DIMData::List &event);

    /**
     * @brief Waits until a new timing event was received
     * @param[out] event   the received event (invalid event on error)
     * @param[in]  time    timestamp of the occurring event
     * @param[in]  timeout maximum waiting time in milliseconds (default : 200)
     */
    bool getEventByTime(dim::DIMData::List &event,
                        const Timestamp &time,
                        int32_t timeout = 200);

protected:
    typedef std::map<ntof::utils::Timestamp, dim::DIMData::List> EventsMap;

    struct checkTS
    {
        checkTS(const Timestamp &ts, int64_t margin);
        bool operator()(const std::pair<Timestamp, dim::DIMData::List> &v) const;

    protected:
        const Timestamp &chkVal;
        int64_t m_margin;
    };

    mutable std::mutex m_mutex;
    std::condition_variable m_cond;

    dim::DIMData::List m_currentEvent;
    int64_t m_margin;
    int m_maxEvents;
    EventsMap m_events;
};

} /* namespace utils */
} /* namespace ntof */

#endif // NTOFUTILS_DIMDATATIMEDBUFFER_HPP
