/*
 * EventReader.h
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#ifndef EVENTREADER_H_
#define EVENTREADER_H_

#include <condition_variable>
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include <DIMXMLInfo.h>
#include <Timestamp.h>
#include <stdint.h>

#include "NTOFLogging.hpp"

struct EventHeader; /* forward declare for operator << */

namespace ntof {
namespace dim {

class EventHandler;

class EventReader : public ntof::dim::DIMXMLInfoHandler
{
public:
    static const std::string DefaultService;

    struct Data
    {
        Data();

        int cycleNb;
        int64_t timeStamp;
        int64_t cycleStamp;
        std::string evtType;
        std::string dest;
        std::string dest2;
        int64_t evtNumber;
        std::string lsaCycle;
        std::string user;

        bool operator==(const Data &other) const;
        inline bool operator!=(const Data &other) const
        {
            return !(*this == other);
        }

        inline bool isValid() const { return timeStamp > 0; }

        /**
         * @brief load event from xml
         * @param[in]  node "event" xml node
         * @return true on success, false on error
         *
         * @details Data will be set to invalid on error.
         */
        bool fromXML(const pugi::xml_node &node);

        /**
         * @brief dump info in an xml node
         * @return true on success
         *
         * @details will return false only if current Data is invalid.
         */
        bool toXML(pugi::xml_node &node) const;
    };

    EventReader(std::string eventService = DefaultService);
    virtual ~EventReader();

    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const ntof::dim::DIMXMLInfo *info);

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info);

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info);

    /**
     * Waits until a new timing event was received
     * @param[out] event the received event
     */
    void waitForNewEvent(EventReader::Data &event);

    /**
     * Registers a new event handler
     * @param handler
     */
    void registerHandler(EventHandler *handler);

    /**
     * Remove specified handler
     * @param handler
     */
    void removeHandler(EventHandler *handler);

    /**
     * @brief Waits until a new timing event was received
     * @param[out] event   the received event (invalid event on error)
     * @param[in]  time    timestamp of the occuring event
     * @param[in]  timeout maximum waiting time in milliseconds (default : 200)
     */
    bool getEventByTime(EventReader::Data &event,
                        const ntof::utils::Timestamp &time,
                        int32_t timeout = 200);

    /**
     * @brief Get last event returned by getEventByTime
     * @param[out] event current event
     */
    void getCurrentEvent(EventReader::Data &event) const;

    /**
     * Sets the maximum event numbers to keep
     * @param size
     */
    static void setMaxEventsToKeep(int size);

    /**
     * Sets the margin in usec used to match (or not) a given event
     * @param margin
     */
    void setTimeMargin(int64_t margin);

    /**
     * Gets the margin in usec used to match (or not) a given event
     * @return Actual margin in usec
     */
    int64_t getTimeMargin();

protected:
    typedef std::map<ntof::utils::Timestamp, Data> EventsMap;

    struct checkTS
    {
        checkTS(const ntof::utils::Timestamp &ts, int64_t margin);
        bool operator()(const std::pair<ntof::utils::Timestamp, Data> &v) const;

    protected:
        const ntof::utils::Timestamp &chkVal;
        int64_t m_margin;
    };

    static int maxEvents;

    std::unique_ptr<ntof::dim::DIMXMLInfo> m_info;
    Data m_currentEvent;
    int64_t m_margin;
    std::vector<EventHandler *> m_handlers;

    mutable std::mutex m_mutex;
    mutable std::mutex m_hdlrsMutex;
    std::condition_variable m_cond;

    EventsMap m_events;
};

class EventHandler
{
public:
    /**
     * Callback called when a new timing event is received
     * @param event received event
     */
    virtual void eventReceived(const EventReader::Data &event) = 0;

    /**
     * Called when link is lost with timing machine
     */
    virtual void noLink() = 0;
    virtual ~EventHandler() {};
};

} /* namespace dim */
} /* namespace ntof */

/**
 * @brief inject Timing event information in EVEH header.
 * @details updates:
 *  - eventNumber
 *  - time, date
 *  - bctTS
 *  - beamType
 */
EventHeader &operator<<(EventHeader &header,
                        const ntof::dim::EventReader::Data &data);

namespace el {
namespace base {

template<>
MessageBuilder &MessageBuilder::operator<< <ntof::dim::EventReader::Data>(
    const ntof::dim::EventReader::Data &data);

}
} // namespace el

#endif /* EVENTREADER_H_ */
