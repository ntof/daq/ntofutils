/*
 * DIMData.h
 *
 *  Created on: Sep 24, 2014
 *      Author: mdonze
 */

#ifndef DIMDATA_H_
#define DIMDATA_H_

#include <cstdint>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <pugixml.hpp>

namespace ntof {
namespace dim {
//! Represents an enumeration
class DIMEnum
{
public:
    typedef std::map<int32_t, std::string> ValuesMap;
    /**
     * Build an enumeration
     * @param value Value of enum
     * @param name Associated label
     */
    DIMEnum(int32_t value, const std::string &name);

    /**
     * Copy constructor
     * @param other Enumeration to be copied from
     */
    DIMEnum(const DIMEnum &other);

    /**
     * Empty enum constructor
     */
    DIMEnum();

    /**
     * Build an anonymous enumeration for sending a command
     * @param value Value of enum
     */
    explicit DIMEnum(int32_t value);

    /**
     * Destructor
     */
    virtual ~DIMEnum();

    /**
     * Adds a new item to the enumeration
     * @param value Value of the new item
     * @param label Label of the new item
     */
    void addItem(int32_t value, const std::string &label);

    /**
     * Gets the enumeration name
     * @return Enumeration name
     */
    const std::string &getName() const;

    /**
     * Gets the enumeration value
     * @return enum value
     */
    int32_t getValue() const;

    /**
     * Gets the enumeration value map
     * @return enum value map
     */
    const ValuesMap &getValuesMap() const;

    /**
     * Gets first iterator to enum values
     * @return
     */
    ValuesMap::const_iterator begin() const;

    /**
     * Gets end iterator to enum values
     * @return
     */
    ValuesMap::const_iterator end() const;

    /**
     * Sets new enum value
     * @param value
     */
    void setValue(int32_t value);

    /**
     * Sets new enum value map
     * @param map value map
     */
    void setValuesMap(const ValuesMap &map);

    /**
     * Comparison operator
     * @param value int value to be tested
     * @return True if matches
     */
    bool operator==(int32_t value) const;

    /**
     * Comparison operator
     * @param name Enum name to be tested
     * @return True if matches
     */
    bool operator==(const std::string &name) const;

    /**
     * Comparison operator
     * @param val Enumeration to be tested
     * @return True if matches
     */
    bool operator==(const DIMEnum &val) const;

    /**
     * Assignment operator
     * @param val Enum to copy
     */
    DIMEnum &operator=(const DIMEnum &val);

private:
    int32_t value_;
    ValuesMap values_;
};

//! Represents a DIM data received/sent over DIM
class DIMData
{
public:
    typedef uint32_t Index;

    /**
     * Represents the type of the data
     */
    enum DataType
    {
        TypeInvalid = -1, //!< Invalid object type
        TypeNested = 0,   //!< Type Array<DIMData>
        TypeInt = 1,      //!< Type int32_t
        TypeDouble = 2,   //!< Type double
        TypeString = 3,   //!< Type string
        TypeLong = 4,     //!< Type int64_t
        TypeEnum = 5,     //!< Type enumeration
        TypeByte = 6,     //!< Type byte
        TypeShort = 7,    //!< Type short
        TypeFloat = 8,    //!< Type float
        TypeBool = 9,     //!< Type boolean
        TypeUInt = 11,    //!< Type uint32_t
        TypeULong = 14,   //!< Type uint64_t
        TypeUByte = 16,   //!< Type uint8_t
        TypeUShort = 17   //!< Type uint16_t
    };
    /* backward compatibility, deprecated */
    typedef DataType dataType;

    typedef std::vector<DIMData> List;

    /**
     * Empty constructor
     */
    DIMData();

    /**
     * Constructs the data using DIM raw data
     * @param data DIM XML node representing this
     */
    explicit DIMData(const pugi::xml_node &data);

    /**
     * Constructs a int32_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            int32_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeInt;
        m_value.intValue = value;
    }

    /**
     * Constructs a double data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            double value) :
        DIMData(index, name, unit)
    {
        m_type = TypeDouble;
        m_value.doubleValue = value;
    }

    /**
     * Constructs a int64_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            int64_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeLong;
        m_value.longValue = value;
    }

    /**
     * Constructs a int8_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            int8_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeByte;
        m_value.byteValue = value;
    }

    /**
     * Constructs a int16_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            int16_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeShort;
        m_value.shortValue = value;
    }

    /**
     * Constructs a float data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            float value) :
        DIMData(index, name, unit)
    {
        m_type = TypeFloat;
        m_value.floatValue = value;
    }

    /**
     * Constructs a boolean data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            bool value) :
        DIMData(index, name, unit)
    {
        m_type = TypeBool;
        m_value.intValue = value ? 1 : 0;
    }

    /**
     * Constructs a uint8_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            uint8_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeUByte;
        m_value.ubyteValue = value;
    }
    /**
     * Constructs a uint16_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            uint16_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeUShort;
        m_value.ushortValue = value;
    }

    /**
     * Constructs a uint32_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            uint32_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeUInt;
        m_value.uintValue = value;
    }

    /**
     * Constructs a uint64_t data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            uint64_t value) :
        DIMData(index, name, unit)
    {
        m_type = TypeULong;
        m_value.ulongValue = value;
    }

    /**
     * Constructs a string data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            const std::string &value);

    /**
     * @brief constructs a nested data
     * @param[in] index Index of the data in the list
     * @param[in] name Name of the data
     * @param[in] unit Unit of the data
     * @param[in] value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            const DIMData::List &value);

    /**
     * @brief move construct nested data
     * @param[in] index Index of the data in the list
     * @param[in] name  Name of the data
     * @param[in] unit  unit of the data
     * @param[in] value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            DIMData::List &&value);

    /**
     * Constructs an enum data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    DIMData(Index index,
            const std::string &name,
            const std::string &unit,
            const DIMEnum &value);

    /**
     * Constructs nameless/unitless objects
     * @param[in] index index of the data in the list
     * @param[in] value data value
     */
    template<typename T>
    DIMData(Index index, const T &value) :
        DIMData(index, std::string(), std::string(), value)
    {}

    /**
     * @brief convenience move constructor for nested data
     */
    DIMData(Index index, List &&value) :
        DIMData(index, std::string(), std::string(), std::move(value))
    {}

    /**
     * Copy constructor to copy this class
     * @param other
     */
    DIMData(const DIMData &other);
    DIMData(DIMData &&other);

    /**
     * Assignment operator
     * @param other Class assigned from
     * @return reference to this
     */
    DIMData &operator=(const DIMData &other);
    DIMData &operator=(DIMData &&other);

    /**
     * Destructor
     */
    virtual ~DIMData();

    /**
     * Add a new DIMData to nested data
     * @param index Index of the data in the list
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     */
    template<typename T>
    void addNestedData(Index index,
                       const std::string &name,
                       const std::string &unit,
                       T value);

    /**
     * Add a new DIMData to nested data
     * @param name Name of the data
     * @param unit Unit of the data
     * @param value Default value of the data
     * @return The index calculated inside nested data
     */
    template<typename T>
    Index addNestedData(const std::string &name,
                        const std::string &unit,
                        T value);

    /**
     * Remove a DIMData from nested data
     * @param index Index of the data in the list
     */
    void removeNestedData(Index index);

    /**
     * Gets data index
     * @return The data index
     */
    inline Index getIndex() const { return m_index; }

    /**
     * Gets data name
     * @return The data name
     */
    inline const std::string &getName() const { return m_name; }

    /**
     * Gets data unit
     * @return The data unit
     */
    inline const std::string &getUnit() const { return m_unit; }

    /**
     * Gets value of the data in int32_t format
     * @return
     */
    int32_t getIntValue() const;

    /**
     * Gets value of the data in double format
     * @return
     */
    double getDoubleValue() const;

    /**
     * Gets value of the data in string format
     * @return
     */
    const std::string &getStringValue() const;

    /**
     * Gets value of the data in int64_t format
     * @return
     */
    int64_t getLongValue() const;

    /**
     * Gets value of the data in enum format
     * @return
     */
    DIMEnum &getEnumValue() const;

    /**
     * Gets value of the data in int8_t format
     * @return
     */
    int8_t getByteValue() const;

    /**
     * Gets value of the data in int16_t format
     * @return
     */
    int16_t getShortValue() const;

    /**
     * Gets value of the data in float format
     * @return
     */
    float getFloatValue() const;

    /**
     * Gets value of the data in boolean format
     * @return
     */
    bool getBoolValue() const;

    /**
     * Gets value of the data in uint32_t format
     */
    uint32_t getUIntValue() const;

    /**
     * Gets value of the data in uint64_t format
     */
    uint64_t getULongValue() const;

    /**
     * Gets value of the data in uint8_t format
     */
    uint8_t getUByteValue() const;

    /**
     * Gets value of the data in uint16_t format
     */
    uint16_t getUShortValue() const;

    /**
     * @brief get nested value
     */
    const DIMData::List &getNestedValue() const;
    DIMData::List &getNestedValue();

    /**
     * templated value accessor
     * @return value
     */
    template<typename T>
    T getValue() const;

    /**
     * Gets string representation of the data
     * @return
     */
    std::string getValueAsString() const;

    /**
     * Insert value into string stream
     * @param os
     */
    void insertIntoStream(std::ostream &oss) const;

    /**
     * Sets int32_t value of data
     * @param value Value to be set
     */
    void setValue(int32_t value);

    /**
     * Sets double value of data
     * @param value Value to be set
     */
    void setValue(double value);

    /**
     * Sets string value of data
     * @param value Value to be set
     */
    void setValue(const std::string &value);

    /**
     * Sets long value of data
     * @param value Value to be set
     */
    void setValue(int64_t value);

    /**
     * Sets byte value of data
     * @param value Value to be set
     */
    void setValue(int8_t value);

    /**
     * Sets short value of data
     * @param value Value to be set
     */
    void setValue(int16_t value);

    /**
     * Sets float value of data
     * @param value Value to be set
     */
    void setValue(float value);

    /**
     * Sets boolean value of data
     * @param value Value to be set
     */
    void setValue(bool value);

    /**
     * Sets uint*_t value
     * @param value Value to be set
     */
    void setValue(uint8_t value);
    void setValue(uint16_t value);
    void setValue(uint32_t value);
    void setValue(uint64_t value);

    /**
     * Sets ENUM value of data
     * @param value Value to be set
     */
    void setValue(const DIMEnum &value);

    /**
     * @brief set nested value
     */
    void setValue(const DIMData::List &value);
    void setValue(DIMData::List &&value);

    /**
     * Sets value from another data
     * @param other Value to take value from
     */
    void setValue(const DIMData &other);
    void setValue(DIMData &&other);

    /**
     * Update or sets the value
     * @param[in] other values to update
     * @param[in] override value type changes are accepted if set
     * @details this method is identical to setValue, unless for nested values
     * and enums
     */
    void updateValue(const DIMData &other, bool override = true);
    void updateValue(DIMData &&other, bool override = true);

    /**
     * Copies data information from other data
     * This method will only copy the name and unit of the data
     * It doesn't change the value
     * @param other
     */
    void copyParameterInfo(const DIMData &other);

    /**
     * Insert this into acquisition XML document
     * @param doc XML document to be filled
     */
    inline void insertIntoAqn(pugi::xml_node &doc) const
    {
        insertInto(doc, false);
    }

    /**
     * Insert this into command XML document
     * @param doc XML document to be filled
     */
    inline void insertIntoCmd(pugi::xml_node &doc) const
    {
        insertInto(doc, true);
    }

    /**
     * @brief insert in an xml document
     * @param[out] doc node where to inject the data
     * @param[in] isCmd whereas the generated XML is a command
     * @details some details are elided in command mode
     */
    void insertInto(pugi::xml_node &doc, bool isCmd = false) const;

    /**
     * Checks if data is flagged as invalid
     * @return True if data is Invalid type
     */
    inline bool isValid() const { return m_type != TypeInvalid; }

    /**
     * Gets if data is int32_t type
     * @return True if data is int32_t type
     */
    inline bool isInt() const { return m_type == TypeInt; }

    /**
     * Gets if data is double type
     * @return True if data is double type
     */
    inline bool isDouble() const { return m_type == TypeDouble; }

    /**
     * Gets if data is string type
     * @return True if data is string type
     */
    inline bool isString() const { return m_type == TypeString; }

    /**
     * Gets if data is long type
     * @return True if data is long type
     */
    inline bool isLong() const { return m_type == TypeLong; }

    /**
     * Gets if data is enum type
     * @return True if data is enum type
     */
    inline bool isEnum() const { return m_type == TypeEnum; }

    /**
     * Gets if data is byte type
     * @return True if data is byte type
     */
    inline bool isByte() const { return m_type == TypeByte; }

    /**
     * Gets if data is short type
     * @return True if data is short type
     */
    inline bool isShort() const { return m_type == TypeShort; }

    /**
     * Gets if data is float type
     * @return True if data is float type
     */
    inline bool isFloat() const { return m_type == TypeFloat; }

    /**
     * Gets if data is boolean type
     * @return True if data is boolean type
     */
    inline bool isBool() const { return m_type == TypeBool; }

    /**
     * Gets if data is uint32_t type
     * @return True if data is uint32_t type
     */
    inline bool isUInt() const { return m_type == TypeUInt; }

    /**
     * Gets if data is uint64_t type
     * @return True if data is uint64_t type
     */
    inline bool isULong() const { return m_type == TypeULong; }

    /**
     * Gets if data is uint8_t type
     * @return True if data is uint8_t type
     */
    inline bool isUByte() const { return m_type == TypeUByte; }

    /**
     * Gets if data is uint16_t type
     * @return True if data is uint16_t type
     */
    inline bool isUShort() const { return m_type == TypeUShort; }

    /**
     * @brief check if data is nested type
     * @return true if data is nested type
     */
    inline bool isNested() const { return m_type == TypeNested; }

    /**
     * Checks if data have same data type
     * @param other
     * @return True if data type match
     */
    inline bool isSameDataType(const DIMData &other)
    {
        return m_type == other.m_type;
    }

    /**
     * templated is value checker
     * @return true if value is T type
     */
    template<typename T>
    bool is() const;

    /**
     * Sets if the data must be hidden in the DIMParameter list
     * @param hidden True, if hidden
     */
    inline void setHidden(bool hidden) { m_hidden = hidden; }

    /**
     * Gets if this data is hidden
     * @return
     */
    inline bool isHidden() const { return m_hidden; }

    /**
     * Gets the type of the data
     * @return enumeration of supported types
     */
    inline DataType getDataType() const { return m_type; };

protected:
    /**
     * base constructor
     * @details protected on purpose, does not init data
     */
    DIMData(Index index, const std::string &name, const std::string &unit);

    /**
     * Gets the next available index in the nested vector
     * @return The next available nested index
     */
    Index getNextNestedIndex() const;

    /**
     * @brief reset the values
     * @details free allocated memory, leaves data in an unitialized state.
     */
    void resetValue();

    /**
     * Union for storing a numeric data
     */
    union Value
    {
        int32_t intValue;
        double doubleValue;
        int64_t longValue;
        int8_t byteValue;
        int16_t shortValue;
        float floatValue;
        uint32_t uintValue;
        uint16_t ushortValue;
        uint64_t ulongValue;
        uint8_t ubyteValue;
        void *ptrValue;
    };

    // Members
    Index m_index;
    std::string m_name;
    std::string m_unit;
    bool m_hidden;
    DataType m_type;
    Value m_value;
};

} // namespace dim
} // namespace ntof

#include "DIMData.hxx"

#endif /* DIMDATA_H_ */
