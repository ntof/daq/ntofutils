/*
 * DIMSuperCommand.h
 *
 *  Created on: Sep 18, 2014
 *      Author: agiraud
 */

#ifndef DIMSUPERCOMMAND_H_
#define DIMSUPERCOMMAND_H_

#include <string>

#include <DIMXMLCommand.h>
#include <DIMXMLService.h>

#include "DIMAck.h"
#include "DIMCmd.h"

namespace ntof {
namespace dim {
class DIMSuperCommand : private DIMXMLCommandHandler
{
public:
    explicit DIMSuperCommand(std::string name);
    ~DIMSuperCommand() override;
    void setStatus(int32_t key,
                   DIMAck::Status status,
                   int32_t errorCode,
                   const std::string &errorMessage);
    void setError(int32_t key,
                  int32_t errorCode,
                  const std::string &errorMessage);
    void setOk(int32_t key, const std::string &message);
    virtual void commandReceived(DIMCmd &cmd) = 0;

    DIMSuperCommand(const DIMSuperCommand &) = delete;
    DIMSuperCommand &operator=(const DIMSuperCommand &) = delete;

protected:
    void errorReceived(std::string errMsg, const DIMXMLCommand *cmd) override;
    void dataReceived(pugi::xml_document &doc,
                      const DIMXMLCommand *cmd) override;
    std::unique_ptr<DIMAck> m_ack;
    std::unique_ptr<DIMXMLService> m_ackService;
    std::unique_ptr<DIMXMLCommand> m_cmdService;
};
} // namespace dim
} // namespace ntof

#endif /* DIMSUPERCOMMAND_H_ */
