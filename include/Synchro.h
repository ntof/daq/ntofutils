/*
 *  Synchro.h
 *
 *  Used to synchronize multiple threads
 *  Created on: Sep 22, 2014
 *      Author: mdonze
 */

#ifndef NTOF_SYNCHRO_H_
#define NTOF_SYNCHRO_H_

#ifndef WIN32
#include <pthread.h>
#else
#include <windows.h>
#endif
#include <exception>
#include <string>

#include <stdint.h>

namespace ntof {
namespace utils {

// Mutex object
/**
 * @deprecated do prefer C++11 std::mutex and std::recursive_mutex
 */
class Mutex
{
public:
    Mutex();
    virtual ~Mutex();
    void lock();
    void unlock();
#ifndef WIN32
    pthread_mutex_t *getMutex();
#else
    CRITICAL_SECTION *getMutex();
#endif
private:
#ifndef WIN32
    pthread_mutex_t *mlock;
#else
    CRITICAL_SECTION mlock;
#endif
};

// Lock object
class Lock
{
public:
    explicit Lock(Mutex &mutex) : mMutex(&mutex) { mMutex->lock(); }
    ~Lock() { mMutex->unlock(); }

private:
    Mutex *mMutex;
};

// Condition variable
class CondVar
{
public:
    CondVar();
    virtual ~CondVar();
    void signal();
    void signalAll();
    void wait(Mutex &m);
    int wait(Mutex &m, long delayMs);

private:
#ifndef WIN32
    pthread_cond_t mCondition;
#else
    CONDITION_VARIABLE mCondition;
#endif
};

} // namespace utils
} // namespace ntof

#endif /* DIMSYNCHRO_H_ */
